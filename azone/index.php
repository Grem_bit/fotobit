<?
// ini_set('display_errors',1); error_reporting(E_ALL);
ini_set('display_errors',0); error_reporting(NULL);
	$RootPath = '../';
	$AuthRequire = true;
	$IsAzone = true;

	// ini_set('display_errors',1);
	// error_reporting(E_ALL);
	header("Content-type: text/html; charset=utf-8");
	
	$url_parts = explode('/', $_SERVER['REQUEST_URI']);
	$module_name = $url_parts[2];
	
	switch($module_name) {
	
		default: 
			$ClassName = 'FrontPage';
			break;
		
		case 'az-users':
			$ClassName = 'AzUsers';
			break;
		
		case 'az-file-manager':
			$ClassName = 'AzFileManager';
			include $RootPath."/core/lib/ckfinder/ckfinder.php";
			$finder = new CKFinder() ;
			break;
		
		case 'urls':
			$ClassName = 'Urls';
			break;

		case 'news':
		case 'events':
			$ClassName = 'News';
			break;		
		/*case 'articles':
			$ClassName = 'Art';
			break;		*/	
		case 'gallery':
			$ClassName = 'Gallery';
			break;			
		case 'shops':
			$ClassName = 'Shops';
			break;
		case 'emails':
			$ClassName = 'Emails';
			break;

		case 'pages':
			$ClassName = 'Pages';
			break;
			
		case 'banners':
			$ClassName = 'Banners';
			break;	
		
		/*case 'faq':
			$ClassName = 'Faq';
			break;*/

		case 'system-variables':
			$ClassName = 'SystemVariables';
			break;

		/*case 'currency':
			$ClassName = 'Currency';
			break;*/

		case 'categories':
			$ClassName = 'Categories';
			break;
			
		/*case 'brands':
			$ClassName = 'Brands';
			break;*/
			
		case 'products':
			$ClassName = 'Products';
			break;
			
		case 'options':
			$ClassName = 'Options';
			break;

		case 'orders':
			$ClassName = 'Orders';
			break;	
			
		case 'users':
			$ClassName = 'Users';
			break;	

		/*case 'import':
			$ClassName = 'Import';
			break;	*/

		case 'state':
			$ClassName = 'ProductState';
			break;

		/*case 'export':
			$ClassName = 'Export';
			break;*/

		case 'language-constants':
			$ClassName = 'LanguageConstants';
			break;	
		case 'country':
			$ClassName = 'Country';
			break;
		case 'cities':
			$ClassName = 'Cities';
			break;
		case 'experts':
			$ClassName = 'Experts';
			break;
		case 'blogs':
			$ClassName = 'Blogs';
			break;
		case 'imageuploader':
			$ClassName = 'ImageUploader';
			$AuthRequire = false;
			break;
		case 'imageuploadergallery':
			$ClassName = 'ImageUploaderGallery';
			$AuthRequire = false;
			break;
		case 'contact-messages':
			$ClassName = 'ContactMessages';
			break;
		case 'reviews':
			$ClassName = 'Reviews';
			break;	
			
	}
	
	require_once $RootPath.'core/initialize.php';
?>
