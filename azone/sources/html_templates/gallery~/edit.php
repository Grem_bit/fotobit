<div class="categories">
	<div class="top-panel"><div class="tp-right">
		<a href="<?= $this->RootUrl ?>"><img class="close" src="<?= $this->RootUrl ?>public/images/top-panel/close.gif" alt="" /></a>
		<? if($this->Perm['edit']){ ?>
			<a class="create-item" href="<?= $this->ModuleInfo['href'] ?>?Event=New">Создать</a>
		<? } ?>
	</div></div>

	<div class="form-container">
		<div class="form engandsize"><div class="bt"><div class="br"><div class="bb"><div class="bl"><div class="ctl"><div class="ctr"><div class="cbr"><div class="cbl">
			<? if($this->DisplayError){ ?>
				<p class="error">Ошибка, проверте выделенные поля</p>
			<? } ?>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<form id="form-demo" action="/azone/imageuploadergallery/?sid=<?= session_id() ?>" method="post" enctype="multipart/form-data">
							<select style="visibility: hidden;" id="photo_type" name="photo_type">
								<option value="photo">Фото</option>
								<option value="plan">План</option>
							</select>
							<fieldset id="demo-fallback" style="display: none>
								<label for="demo-photoupload" ">
									Upload :
									<input type="file" name="photoupload" id="demo-photoupload" />
								</label>
							</fieldset>
							<div id="demo-status" class="hide">
								<p class="ctrl">
									<a href="#" id="demo-browse-all" style="display: none">Browse Files</a>
									<a href="#" id="demo-browse-images">Просмотр</a>&nbsp;
									<a href="#" id="demo-clear">Очистить список</a>&nbsp;
									<a href="#" id="demo-upload">Загрузить</a>
								</p>
								<div>
									<strong class="overall-title">Загрузка списка</strong><br />
									<img src="/core/lib/fancy/bar.gif" class="progress overall-progress" />
								</div>
								<div>
									<strong class="current-title">Загрузка файла</strong><br />
									<img src="/core/lib/fancy/bar.gif" class="progress current-progress" />
								</div>
								<div style="display: none"><br />
									Выберите альбом: <br />								
									<select size="1" id="photo_caregory" name="photo_caregory">
    									<?php for ($i = 0; $i < sizeof($this->List_al); $i++)  { ?>
    									<option value="<?= $this->List_al[$i]['id']?>"><?= $this->List_al[$i]['t_title']?></option>
    									<?php }?>
   									</select>
								</div>
								<div class="current-text"></div>
								
							</div>
							<ul id="demo-list"></ul>
						 <input type="hidden" name="Event" value="<=$_GET['folder']>" />
						</form>
					</td>
				</tr>	
			</table>
			<form id="form" action="" method="post">
				<table>
					<tr>	
						<td colspan="2">
							<button type="button" class="cancel" onclick="window.location='<?= $this->ModuleInfo['href'] ?>?page=<?= $_GET['page']?>&els=<?= $_GET['els']?>'"><img src="<?= $this->RootUrl ?>public/images/buttons/cancel.gif" alt="" /></button>
						</td>
					</tr>
				</table>
			</form>

		</div></div></div></div></div></div></div></div></div>

	</div>

</div>