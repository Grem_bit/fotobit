<?
	// _debug($this->ProdInfo, 1);
	// _debug($this->PageInfo, 1);
?>
<div class="categories">
	<div class="top-panel"><div class="tp-right">
		<a href="<?= $this->RootUrl ?>"><img class="close" src="<?= $this->RootUrl ?>public/images/top-panel/close.gif" alt="" /></a>
		<? if($this->Perm['edit']){ ?>
			<a class="create-item" href="<?= $this->ModuleInfo['href'] ?>?Event=New">Создать</a>
		<? } ?>
	</div></div>
	
	
	<div class="form-container">
		<div class="form big-width"><div class="bt"><div class="br"><div class="bb"><div class="bl"><div class="ctl"><div class="ctr"><div class="cbr"><div class="cbl">
			
			<? if($this->DisplayError){ ?>
				<p class="error">Ошибка, проверте выделенные поля</p>
			<? } ?>
			<?if($this->FileError){?>
				<p class="error"><?=$this->FileError?></p>
			<?}?>
			<form action="" method="post" id="form" enctype="multipart/form-data">
			<div id="tabs1">
			<div id="tab_el_main" class="x-hide-display">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class="min-width <?= $this->Errors['category'] ? 'error' : '' ?>"><label for="category">Категория:</label></td>
						<td class="input">
							<?
								$selected = 0;
								if($_POST['category_id:int']){
									$selected = $_POST['category_id:int'];	
								}elseif($this->ProdInfo['category_id']){
									$selected = $this->ProdInfo['category_id'];
								}
								echo _generate_SELECT(array(
									'name' 			=> 'category_id:int',
									'class'			=> 'small-width',
									'group' 		=> 'group',
									'first_val' 	=> 0, 
									'first_title' 	=> 'Выберите категорию',
									'elements' 		=> $this->Categories, 
									'selected' 		=> $selected, 
									'val' 			=> array('val' => 'id', 'title' => 'display_name')
								));
							?>
						</td>
					</tr>
	
					<?/* <tr>
						<td class="min-width <?= $this->Errors['status'] ? 'error' : '' ?>"><label for="status">Статус товара:</label></td>
						<td class="input">
							<?
								$status = GetAll('SELECT * FROM product_status');
								$selected = 0;
								if($_POST['status_id:int']){
									$selected = $_POST['status_id:int'];	
								}elseif($this->ProdInfo['status_id']){
									$selected = $this->ProdInfo['status_id'];
								}
								echo _generate_SELECT(array(
									'name' 			=> 'status_id:int',
									'class'			=> 'small-width',
									'first_val' 	=> 0, 
									'first_title' 	=> '-- Статус товара --',
									'elements' 		=> $status, 
									'selected' 		=> $selected, 
									'val' 			=> array('val' => 'id', 'title' => 'title')
								));
							?>
						</td>
					</tr> */?>
					
					<?/* <tr>
						<td class="min-width <?= $this->Errors['status'] ? 'error' : '' ?>"><label for="rec">С этим товаром рекомендуем:</label></td>
						<td class="input">
							<?
								
								$selected = 0;
								if($_POST['rec:int']){
									$selected = $_POST['rec:int'];	
								}elseif($this->ProdInfo['rec']){
									$selected = $this->ProdInfo['rec'];
								}
								echo _generate_SELECT(array(
									'name' 			=> 'rec:int',
									'class'			=> 'small-width',
									'first_val' 	=> 0, 
									'first_title' 	=> '-- С этим товаром рекомендуем --',
									'elements' 		=> $this->RecProd, 
									'selected' 		=> $selected, 
									'val' 			=> array('val' => 'id', 'title' => 'title')
								));
							?>
						</td>
					</tr> */?>
					
					<tr>
						<td class="min-width <?= $this->Errors['url'] ? 'error' : '' ?>"><label for="url">Ссылка:</label></td>
						<td class="input"><input type="text" id="url" name="url:str"  class="small-width" value="<?= $_POST['url:str'] ?  $_POST['url:str'] : $this->ProdInfo['url'] ?>" /></td>
					</tr>
					<tr>
						<td class="min-width <?= $this->Errors['order_by'] ? 'error' : '' ?>"><label for="order_by">Порядок:</label></td>
						<td class="input"><input type="text" id="order_by" name="order_by:int"  class="small-width" value="<?= $_POST['order_by:int'] ?  $_POST['order_by:int'] : $this->ProdInfo['order_by'] ?>" /></td>
					</tr>
							
					<tr>
						<?	
							$checked = 0;
							if($_POST){
								
								if($_POST['publish:int'] == 1){
									$checked  = 1;
								}else{
									$checked  = 0;
								}
							}else{
								if($this->ProdInfo){
									if($this->ProdInfo['publish'] == 1){
										$checked  = 1;										
									}else{
										$checked  = 0;										
									}
								}
							}
						?>
						<td class="min-width"><label for="publish">Опубликован:</label></td>
						<td class="input publish">
							<input type="radio" style="width: auto;" name="publish:int" id="publish" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="publish">Да</label><br />
							<input type="radio" style="width: auto;" name="publish:int" id="publish_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="publish_no">Нет</label>
						</td>
					</tr>
					
					<?/* <tr>
						<?	
							$checked = 0;
							if($_POST){
								
								if($_POST['new:int'] == 1){
									$checked  = 1;
								}else{
									$checked  = 0;
								}
							}else{
								if($this->ProdInfo){
									if($this->ProdInfo['new'] == 1){
										$checked  = 1;										
									}else{
										$checked  = 0;										
									}
								}
							}
						?>
						<td class="min-width"><label for="new">На Главной:</label></td>
						<td class="input publish">
							<input type="radio" style="width: auto;" name="new:int" id="new" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="new">Да</label><br />
							<input type="radio" style="width: auto;" name="new:int" id="new_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="new_no">Нет</label>
						</td>
					</tr> */?>
					
					<?/* <tr>
						<?	
							$checked = 0;
							if($_POST){
								
								if($_POST['month:int'] == 1){
									$checked  = 1;
								}else{
									$checked  = 0;
								}
							}else{
								if($this->ProdInfo){
									if($this->ProdInfo['month'] == 1){
										$checked  = 1;										
									}else{
										$checked  = 0;										
									}
								}
							}
						?>
						<td class="min-width"><label for="month">Предложение месяца:</label></td>
						<td class="input month">
							<input type="radio" style="width: auto;" name="month:int" id="month" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="new">Да</label><br />
							<input type="radio" style="width: auto;" name="month:int" id="month_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="new_no">Нет</label>
						</td>
					</tr> */?>
					
					<?/* <tr>
						<?	
							$checked = 0;
							if($_POST){
								
								if($_POST['product_top:int'] == 1){ 
									$checked  = 1;
								}else{
									$checked  = 0;
								}
							}else{
								if($this->ProdInfo){
									if($this->ProdInfo['product_top'] == 1){ 
										$checked  = 1;										
									}else{
										$checked  = 0;										
									}
								}
							}
						?>
						<td class="min-width"><label for="product_top">Новинка:</label></td>
						<td class="input product_top">
							<input type="radio" style="width: auto;" name="product_top:int" id="product_top" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="product_top">Да</label><br />
							<input type="radio" style="width: auto;" name="product_top:int" id="product_top_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="product_top_no">Нет</label>
						</td>
					</tr> */?>
					<tr>
						<td colspan="2">
							<div id="diff_content">
								<? require_once PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'characteristics.php'; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td class="min-width"><label for="filename:str">Изображение с названием:</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['filename']){?><img src="<?= $this->ProdInfo['filename']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePath" name="filename:str" value="<?= $_POST['filename:str'] ?  $_POST['filename:str'] : $this->ProdInfo['filename'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePath' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<?/* <tr>
						<td class="min-width"><label for="gallery:str">Изображение для галлереи:</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['gallery']){?><img src="/core/lib/phpThumb/phpThumb.php?w=500&src=<?= $this->ProdInfo['gallery']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePath2" name="gallery:str" value="<?= $_POST['gallery:str'] ?  $_POST['gallery:str'] : $this->ProdInfo['gallery'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePath2' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr> */?>
					<tr>
						<td class="min-width"><label for="filename2:str">Изображение с названием (маленькое):</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['filename2']){?><img src="<?= $this->ProdInfo['filename2']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePath2" name="filename2:str" value="<?= $_POST['filename2:str'] ?  $_POST['filename2:str'] : $this->ProdInfo['filename2'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePath2' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="min-width"><label for="filename3:str">Изображение с названием (полный Продукт):</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['filename3']){?><img src="<?= $this->ProdInfo['filename3']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePath3" name="filename3:str" value="<?= $_POST['filename3:str'] ?  $_POST['filename3:str'] : $this->ProdInfo['filename3'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePath3' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2"><div class="hr">&nbsp;</div></td>
					</tr>
					<tr>
						<td class="min-width"><label for="img:str">Картинка большая:</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<?/* <tr><td colspan="2"><?if($this->ProdInfo['img']){?><img src="/core/lib/phpThumb/phpThumb.php?w=500&src=<?= $this->ProdInfo['img']?>" /><?}?></td></tr> */?>
								<tr><td colspan="2"><?if($this->ProdInfo['img']){?><img width="500" src="<?= $this->ProdInfo['img']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePathIMG" name="img:str" value="<?= $_POST['img:str'] ?  $_POST['img:str'] : $this->ProdInfo['img'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePathIMG' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="min-width"><label for="img_preview:str">Картинка маленькая:</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['img_preview']){?><img src="<?= $this->ProdInfo['img_preview']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePathIMG_PREVIEW" name="img_preview:str" value="<?= $_POST['img_preview:str'] ?  $_POST['img_preview:str'] : $this->ProdInfo['img_preview'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePathIMG_PREVIEW' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<?/* <tr>
						<td class="min-width"><label for="img_preview:str">Картинка светится:</label></td>
						<td class="input">
							<table cellpadding="0" cellspacing="0">
								<tr><td colspan="2"><?if($this->ProdInfo['img_glow']){?><img src="<?= $this->ProdInfo['img_glow']?>" /><?}?></td></tr>
								<tr>
									<td style="width: 260px;">
										<input style="width:170px; margin: 0;" type="input" id="xImagePathIMG_GLOW" name="img_glow:str" value="<?= $_POST['img_glow:str'] ?  $_POST['img_glow:str'] : $this->ProdInfo['img_glow'] ?>" />
									</td> 
									<td align="left"><img onclick="BrowseServer( 'Images:/', 'xImagePathIMG_GLOW' );"  src="<?= $this->RootUrl ?>public/images/buttons/view.gif" /></td>
								</tr>
							</table>
						</td>
					</tr> */?>
					<tr>
						<td colspan="2"><div class="hr">&nbsp;</div></td>
					</tr>
					<tr>
						<td colspan="2">
							<input style="float: right" onclick="$('redirect').value = 1;" type="image" src="<?= $this->RootUrl ?>public/images/buttons/add-photo.gif" ><br/>
							<!--<a <?= !$_GET['id'] ? ' $(\'form\').submit(); return false;"' : '' ?> class="add-description" id="add-description" href="<?= $this->ModuleInfo['href'] ?>?Event=AddFoto&id=<?=$_GET['id']?>&page=<?= $_GET['page']?>&brand=<?= $_GET['brand']?>&sort=<?= $_GET['sort']?>&direct=<?= $_GET['direct']?>&cat=<?= $_GET['cat']?>">Добавить фото</a>
							--><h4>Фото</h4>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?if ($_GET['id']){?>
								<? $this->GridPhoto->Display() ?>
							<?}?>	
						</td>
					</tr>
					<?/* <tr>
						<td colspan="2">
							<input style="float: right" onclick="$('redirect').value = 1;$('video').value = 1;" type="image" src="<?= $this->RootUrl ?>public/images/buttons/add-photo.gif" ><br/>
							<h4>Видео</h4>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?if ($_GET['id']){?>
								<? $this->GridVideo->Display() ?>
							<?}?>	
						</td>
					</tr> */?>
						</table>
					
				</div>
			</div>

			<div id="tab_el_ru" class="x-hide-display">
				<?= $this->_get_lang_html('ru'); ?>
			</div>
			<?/* <div id="tab_el_ua" class="x-hide-display">
				<?= $this->_get_lang_html('ua'); ?>
			</div> */?>
			<div id="tab_el_en" class="x-hide-display">
				<?= $this->_get_lang_html('en'); ?>
			</div>

			<table  cellpadding="0" cellspacing="0" >
					<tr>
						<td colspan="2"><div class="hr">&nbsp;</div></td>
					</tr>
					<tr>	
						<td colspan="2">
							<button type="submit" class="save"><img src="<?= $this->RootUrl ?>public/images/buttons/save.gif" alt="" /></button>
							<button type="button" class="cancel" onclick="window.location='<?= $this->RootUrl ?>products/?cat=<?= $_GET['cat']?>&page=<?= $_GET['page']?>&brand=<?= $_GET['brand']?>&sort=<?= $_GET['sort']?>&direct=<?= $_GET['direct']?>&els=<?= $_GET['els']?>'"><img src="<?= $this->RootUrl ?>public/images/buttons/cancel.gif" alt="" /></button>
							<? if($_GET['Event'] == 'New'){ ?>
								<input type="hidden" name="Event" value="Insert" />
							<? } ?>
							<? if($_GET['Event'] == 'Edit'){ ?>
								<input type="hidden" name="Event" value="Update" />
							<? } ?>
							<input type="hidden" name="redirect" value="" id="redirect" />
							<input type="hidden" name="video" value="" id="video" />
						</td>
					</tr>
			</table>
			
			</form>
		</div></div></div></div></div></div></div></div></div>
	</div>
</div>