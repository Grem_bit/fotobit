<table cellpadding="0" cellspacing="0">
	<tr>
		<td class="min-width <?= $this->Errors['title_'.$this->Lang] ? 'error' : '' ?>"><label for="title_<?= $this->Lang ?>">Заголовок:</label></td>
		<td class="input">
			<input type="text" class="ArticlesTitle" value="<?= htmlspecialchars ( $_POST['translation'][$this->Lang]['title:str'] ? $_POST['translation'][$this->Lang]['title:str'] : $this->ArticlesInfo['translation'][$this->Lang]['title'])?>" name="translation[<?= $this->Lang ?>][title:str]" />
		</td>
	</tr>
	<tr>
		<td class="min-width <?= $this->Errors['name1'] ? 'error' : '' ?>"><label for="name_<?= $this->Lang ?>">Заголовок страницы:</label></td>
		<td class="input">
			<input type="text" class="ArticlesTitle" value="<?=htmlspecialchars( $_POST['translation'][$this->Lang]['page_title:str'] ? $_POST['translation'][$this->Lang]['page_title:str'] : $this->ArticlesInfo['translation'][$this->Lang]['page_title'])?>" name="translation[<?= $this->Lang ?>][page_title:str]" />
		</td>
	</tr>

	<tr>
		<td class="min-width "><label for="keywords_<?= $this->Lang ?>">Keywords:</label></td>
		<td class="input">
			<textarea rows="5" name="translation[<?= $this->Lang ?>][keywords:str]" id="keywords" cols="10"><?= $_POST['translation'][$this->Lang]['keywords:str'] ? $_POST['translation'][$this->Lang]['keywords']: $this->ArticlesInfo['translation'][$this->Lang]['keywords']?></textarea>
		</td>
	</tr>
	<tr>
		<td class="min-width "><label for="description_<?= $this->Lang ?>t">Description:</label></td>
		<td class="input">
			<textarea rows="5" name="translation[<?= $this->Lang ?>][description:str]" id="description_<?= $this->Lang ?>" cols="10"><?= $_POST['translation'][$this->Lang]['description:str'] ? $_POST['translation'][$this->Lang]['description:str']: $this->ArticlesInfo['translation'][$this->Lang]['description']?></textarea>
		</td>
	</tr>
	<?/*
	<tr>
		<td class="min-width "><label for="front_<?= $this->Lang ?>t">В анонсе новостей:</label></td>
		<td class="input">
			<textarea rows="5" name="translation[<?= $this->Lang ?>][front:str]" id="front_<?= $this->Lang ?>" cols="10"><?= $_POST['translation'][$this->Lang]['front:str'] ? $_POST['translation'][$this->Lang]['description:str']: $this->ArticlesInfo['translation'][$this->Lang]['front']?></textarea>
		</td>
	</tr>
	*/?>
	<tr>
		<td class="min-width <?= $this->Errors['name1'] ? 'error' : '' ?>"><label for="introtext_<?= $this->Lang ?>">Вступительный текст:</label></td>
		<td class="input">
			<textarea rows="5" name="translation[<?= $this->Lang ?>][introtext:str]" id="introtext_<?= $this->Lang ?>" cols="10"><?= $_POST['translation'][$this->Lang]['introtext:str'] ? $_POST['translation'][$this->Lang]['introtext:str']: $this->ArticlesInfo['translation'][$this->Lang]['introtext']?></textarea>
		</td>
	</tr>
	<tr>
		<td valign="top" class="min-width valign-top <?= $this->Errors['fulltext_'.$this->Lang] ? 'error' : '' ?>"><label for="fulltext_<?= $this->Lang ?>">Полный текст:</label></td>
		<td class="input">
			<textarea rows="10" name="translation[<?= $this->Lang ?>][fulltext:str]" id="fulltext_<?= $this->Lang ?>" cols="10"><?=$_POST['translation'][$this->Lang]['fulltext:str'] ? $_POST['translation'][$this->Lang]['fulltext:str']: $this->ArticlesInfo['translation'][$this->Lang]['fulltext']?></textarea>							
		</td>
	</tr>
	<?/* <tr>
		<td class="min-width <?= $this->Errors['extratext1_'.$this->Lang] ? 'error' : '' ?>"><label for="extratext1_<?= $this->Lang ?>">Текстовое поле 1:</label></td>
		<td class="input">
			<input type="text" class="ArticlesTitle" value="<?= htmlspecialchars ( $_POST['translation'][$this->Lang]['extratext1:str'] ? $_POST['translation'][$this->Lang]['extratext1:str'] : $this->ArticlesInfo['translation'][$this->Lang]['extratext1'])?>" name="translation[<?= $this->Lang ?>][extratext1:str]" />
		</td>
	</tr>
	<tr>
		<td class="min-width <?= $this->Errors['extratext2_'.$this->Lang] ? 'error' : '' ?>"><label for="extratext2_<?= $this->Lang ?>">Текстовое поле 2:</label></td>
		<td class="input">
			<input type="text" class="ArticlesTitle" value="<?= htmlspecialchars ( $_POST['translation'][$this->Lang]['extratext2:str'] ? $_POST['translation'][$this->Lang]['extratext2:str'] : $this->ArticlesInfo['translation'][$this->Lang]['extratext2'])?>" name="translation[<?= $this->Lang ?>][extratext2:str]" />
		</td>
	</tr> */?>
</table>