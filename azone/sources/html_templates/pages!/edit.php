<div class="prod-description">
	<div class="top-panel"><div class="tp-right">
		<a href="<?= $this->RootUrl ?>"><img class="close" src="<?= $this->RootUrl ?>public/images/top-panel/close.gif" alt="" /></a>
		<? if($this->Perm['edit']){ ?>
			<a class="create-item" href="<?= $this->ModuleInfo['href'] ?>?Event=New">Создать</a>
		<? } ?>
	</div></div>
	
	<div class="form-container">
		<div class="form" style="width:100%"><div class="bt"><div class="br"><div class="bb"><div class="bl"><div class="ctl"><div class="ctr"><div class="cbr"><div class="cbl">
			
			<? if($this->DisplayError){ ?>
				<p class="error">Ошибка, проверте выделенные поля</p>
			<? } ?>
			<?if($this->FileError){?>
				<p class="error"><?=$this->FileError?></p>
			<?}?>
			<form action="" method="post" id="form" enctype="multipart/form-data">
			
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td class="min-width"><label for="category">Родительская:</label></td>
						<td class="input">
							<?
								$selected = 0;
								if($_POST['parent_id:int']){
									$selected = $_POST['parent_id:int'];	
								}elseif($this->PageInfo['parent_id']){
									$selected = $this->PageInfo['parent_id'];
								}
								echo _generate_SELECT(array(
									'name' 			=> 'parent_id:int',
									'class'			=> 'small-width',
									'first_val' 	=> 0, 
									'first_title' 	=> 'Выберите родителя',
									'elements' 		=> $this->Pages, 
									'selected' 		=> $selected, 
									'val' 			=> array('val' => 'id', 'title' => 'title')
								));
							?>
						</td>
					</tr>
					
					<tr>
						<td class="min-width <?= $this->Errors['title'] ? 'error' : '' ?>"><label for="title">Название страницы:</label></td>
						<td class="input"><input type="text" id="title" name="title:str"  class="small-width" value="<?= $_POST['title:str'] ?  $_POST['title:str'] : $this->PageInfo['title'] ?>" /></td>
					</tr>
					<tr>
						<td class="min-width <?= $this->Errors['url'] ? 'error' : '' ?>"><label for="url">Ссылка:</label></td>
						<td class="input"><input type="text" id="url" name="url:str"  class="small-width" value="<?= $_POST['url:str'] ?  $_POST['url:str'] : $this->PageInfo['url'] ?>" /></td>
					</tr>
					<?	
						$checked = 0;
						if($_POST){
							
							if($_POST['publish:int'] == 1){ 
								$checked  = 1;
							}else{
								$checked  = 0;
							}
						}else{
							if($this->PageInfo){
								if($this->PageInfo['publish'] == 1){ 
									$checked  = 1;										
								}else{
									$checked  = 0;										
								}
							}
						}
					?>
					<tr>
						<td class="min-width"><label for="publish">Опубликована:</label></td>
						<td class="input publish">
							<input type="radio" style="width: auto;" name="publish:int" id="publish" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="publish">Да</label><br />
							<input type="radio" style="width: auto;" name="publish:int" id="publish_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="publish_no">Нет</label>
						</td>
					</tr>
					<?	
						$checked = 0;
						if($_POST){
							
							if($_POST['top_menu:int'] == 1){ 
								$checked  = 1;
							}else{
								$checked  = 0;
							}
						}else{
							if($this->PageInfo){
								if($this->PageInfo['top_menu'] == 1){ 
									$checked  = 1;										
								}else{
									$checked  = 0;										
								}
							}
						}
					?>
					<tr>
						<td class="min-width"><label for="publish">Верхнее меню:</label></td>
						<td class="input publish">
							<input type="radio" style="width: auto;" name="top_menu:int" id="top_menu" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="top_menu">Да</label><br />
							<input type="radio" style="width: auto;" name="top_menu:int" id="top_menu_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="top_menu_no">Нет</label>
						</td>
					</tr>
					<?	
						$checked = 0;
						if($_POST){
							
							if($_POST['right_menu:int'] == 1){ 
								$checked  = 1;
							}else{
								$checked  = 0;
							}
						}else{
							if($this->PageInfo){
								if($this->PageInfo['bottom_menu'] == 1){ 
									$checked  = 1;										
								}else{
									$checked  = 0;										
								}
							}
						}
					?><!--
					<tr>
						<td class="min-width"><label for="publish">Правое меню:</label></td>
						<td class="input publish">
							<input type="radio" style="width: auto;" name="right_menu:int" id="bottom_menu" <?=( $checked == 1 ) ? 'checked="checked"':'' ?> value="1" /><label for="bottom_menu">Да</label><br />
							<input type="radio" style="width: auto;" name="right_menu:int" id="bottom_menu_no" <?=( $checked == 0 ) ? 'checked="checked"':'' ?> value="0" /><label for="bottom_menu_no">Нет</label>
						</td>
					</tr>
					--><tr>
						<td class="min-width"><label for="publish">Сортировка:</label></td>
						<td class="input"><input type="text" id="url" name="order_by:int"  class="small-width" value="<?= $_POST['order_by:int'] ?  $_POST['order_by:int'] : $this->PageInfo['order_by'] ?>" /></td>
					</tr>
					
						<tr>
						<td><div >Для производителей:</div></td>
						<td><div class="hr">&nbsp;</div></td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
						<img width="100px" src="<?='/public/files/images/makers/thumb-'.$this->PageInfo['filename']?>" />
						</td>
					</tr>
					<tr>
						<td class="min-width "><label for="filename">Асоциативное фото: </label></td>
						<td class="input"><input type="file" id="filename" name="filename" /></td>
					</tr>	
					<tr>
						<td></td>
						<td><div class="hr">&nbsp;</div></td>
					</tr>	
					</tr>
					<tr>
						<td class="min-width va-top <?= $this->Errors['sklad'] ? 'error' : '' ?>"><label for="sklad">Ключевые слова:</label></td>
						<td class="input"><textarea cols="10"  class="small-width" rows="5" name="meta_keywords:str"><?= $_POST['meta_keywords:str'] ?  $_POST['meta_keywords:str'] : $this->PageInfo['meta_keywords'] ?></textarea></td>
					</tr>
					<tr>
						<td class="min-width va-top <?= $this->Errors['sklad'] ? 'error' : '' ?>"><label for="sklad">Мета описание:</label></td>
						<td class="input"><textarea cols="10"  class="small-width" rows="5" name="meta_description:str"><?= $_POST['meta_description:str'] ?  $_POST['meta_description:str'] : $this->PageInfo['meta_description'] ?></textarea></td>
					</tr>
					<tr>
						<td class="min-width va-top <?= $this->Errors['body'] ? 'error' : '' ?>"><label for="body">Содержание:</label></td>
						<td class="input"><textarea cols="29" rows="7" name="content:str" id="content"><?= $_POST['content:str'] ?  $_POST['content:str'] : $this->PageInfo['content'] ?></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><div class="hr">&nbsp;</div></td>
					</tr>
					<tr>	
						<td colspan="2">
							<button type="submit" id="submit_btn" class="save"><img src="<?= $this->RootUrl ?>public/images/buttons/save.gif" alt="" /></button>
							<button type="button" class="cancel" onclick="window.location='<?= $this->RootUrl ?>pages/?<?=$_REQUEST['lang']?'&amp;lang='.$_REQUEST['lang']:''?><?=$_REQUEST['cat']?'&amp;cat='.$_REQUEST['cat']:''?>'"><img src="<?= $this->RootUrl ?>public/images/buttons/cancel.gif" alt="" /></button>
							<? if($_GET['Event'] == 'New'){ ?>
								<input type="hidden" name="Event" value="Insert" />
							<? } ?>
							<? if($_GET['Event'] == 'Edit'){ ?>
								<input type="hidden" name="Event" value="Update" />
							<? } ?>
							<input type="hidden" name="redirect" value="" id="redirect" />
						</td>
					</tr>
				</table>
			
			</form>
		</div></div></div></div></div></div></div></div></div>
	</div>
</div>