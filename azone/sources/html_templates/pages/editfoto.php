<div class="categories">
	<div class="top-panel"><div class="tp-right">
		<a href="<?= $this->RootUrl ?>"><img class="close" src="<?= $this->RootUrl ?>public/images/top-panel/close.gif" alt="" /></a>
		<? if($this->Perm['edit']){ ?>
			<a class="create-item" href="<?= $this->ModuleInfo['href'] ?>?Event=New">Создать</a>
		<? } ?>
	</div></div>

	<div class="form-container">
		<div class="form engandsize"><div class="bt"><div class="br"><div class="bb"><div class="bl"><div class="ctl"><div class="ctr"><div class="cbr"><div class="cbl">
			<? if($this->DisplayError){ ?>
				<p class="error">Ошибка, проверте выделенные поля</p>
			<? } ?>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<form id="form-demo" action="/azone/imageuploader/?sid=<?= session_id() ?>&id=<?= $_GET['id']?>" method="post" enctype="multipart/form-data">
							<select style="visibility: hidden;" id="photo_type" name="photo_type">
								<option value="plan">План</option>
							</select>
							<fieldset id="demo-fallback" style="display: none>
								<label for="demo-photoupload" ">
									Upload :
									<input type="file" name="photoupload" id="demo-photoupload" />
								</label>
							</fieldset>
							<div id="demo-status" class="hide">
								<p class="ctrl">
									<a href="#" id="demo-browse-all" style="display: none">Browse Files</a>
									<a href="#" id="demo-browse-images">Просмотр</a>
									<a href="#" id="demo-clear">Очистить список</a>
									<a href="#" id="demo-upload">Загрузить</a>
								</p>
								<div>
									<strong class="overall-title">Загрузка списка</strong><br />
									<img src="/core/lib/fancy/bar.gif" class="progress overall-progress" />
								</div>
								<div>
									<strong class="current-title">Загрузка файла</strong><br />
									<img src="/core/lib/fancy/bar.gif" class="progress current-progress" />
								</div>
								<div class="current-text"></div>
							</div>
							<ul id="demo-list"></ul>
						 <input type="hidden" name="Event" value="<=$_GET['folder']>" />
						</form>
					</td>
				</tr>	
			</table>
			<form id="form" action="" method="post">
				<table>
					<tr>	
						<td colspan="2">
							<a href="#" onclick="window.location='<?= $this->ModuleInfo['href'] ?>?Event=Edit&amp;id=<?= $_GET['id'] ?>&page=<?= $_GET['page']?>&brand=<?= $_GET['brand']?>&sort=<?= $_GET['sort']?>&direct=<?= $_GET['direct']?>&cat=<?= $_GET['cat']?>&els=<?= $_GET['els']?>'" id="demo-upload">Назад</a>
						</td>
					</tr>
				</table>
			</form>

		</div></div></div></div></div></div></div></div></div>

	</div>

</div>