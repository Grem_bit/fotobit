
<table cellpadding="0" cellspacing="0">
	<tr>
		<td class="min-width <?= $this->Lang == 'ru' && $this->Errors['title_ru'] ? 'error' : '' ?>"><label for="title_<?= $this->Lang; ?>">Название страницы:</label></td>
		<td class="input"><input type="text" id="title_<?= $this->Lang; ?>" name="translation[<?= $this->Lang; ?>][title:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['title:str'] ?  $_POST['translation'][$this->Lang]['title:str'] : htmlspecialchars($this->PageInfo['translation'][$this->Lang]['title']) ?>" /></td>
	</tr>
	<?/*
	<tr>
		<td class="min-width <?= $this->Errors['title_menu'] ? 'error' : '' ?>"><label for="title_<?= $this->Lang; ?>">Название для меню:</label></td>
		<td class="input"><input type="text" id="title_text_<?= $this->Lang; ?>" name="translation[<?= $this->Lang; ?>][title_menu:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['title_menu:str'] ?  $_POST['translation'][$this->Lang]['title_menu:str'] : htmlspecialchars($this->PageInfo['translation'][$this->Lang]['title_menu']) ?>" /></td>
	</tr>
	*/?>
	<?/* <tr>
		<td class="min-width <?= $this->Errors['title_text'] ? 'error' : '' ?>"><label for="title_<?= $this->Lang; ?>">Текст в шапке:</label></td>
		<td class="input"><input type="text" id="title_text_<?= $this->Lang; ?>" name="translation[<?= $this->Lang; ?>][title_text:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['title_text:str'] ?  $_POST['translation'][$this->Lang]['title_text:str'] : htmlspecialchars($this->PageInfo['translation'][$this->Lang]['title_text']) ?>" /></td>
	</tr> */?>
	<tr>
		<td class="min-width <?= $this->Errors['page_title'] ? 'error' : '' ?>"><label for="page_title_<?= $this->Lang; ?>">Заголовок браузера:</label></td>
		<td class="input"><input type="text" id="page_title_<?= $this->Lang; ?>" name="translation[<?= $this->Lang; ?>][page_title:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['page_title:str'] ?  $_POST['translation'][$this->Lang]['page_title:str'] : htmlspecialchars($this->PageInfo['translation'][$this->Lang]['page_title']) ?>" /></td>
	</tr>
	<tr>
		<td class="min-width va-top <?= $this->Errors['sklad'] ? 'error' : '' ?>"><label for="sklad_<?= $this->Lang; ?>">Ключевые слова:</label></td>
		<td class="input"><textarea cols="10"  class="small-width" rows="5" name="translation[<?= $this->Lang; ?>][meta_keywords:str]"><?= $_POST['translation'][$this->Lang]['meta_keywords:str'] ?  $_POST['translation'][$this->Lang]['meta_keywords:str'] : $this->PageInfo['translation'][$this->Lang]['meta_keywords'] ?></textarea></td>
	</tr>
	
						
	<tr>
		<td class="min-width va-top <?= $this->Errors['sklad'] ? 'error' : '' ?>"><label for="sklad_<?= $this->Lang; ?>">Мета описание:</label></td>
		<td class="input"><textarea cols="10"  class="small-width" rows="5" name="translation[<?= $this->Lang; ?>][meta_description:str]"><?= $_POST['translation'][$this->Lang]['meta_description:str'] ?  $_POST['translation'][$this->Lang]['meta_description:str'] : $this->PageInfo['translation'][$this->Lang]['meta_description'] ?></textarea></td>
	</tr>
	<?/*
	<tr>
		<td class="min-width <?= $this->Lang == 'ru' && $this->Errors['intro_title_ru'] ? 'error' : '' ?>"><label for="intro_title_<?= $this->Lang; ?>">Заголовок интротекста:</label></td>
		<td class="input"><input type="text" id="intro_title_<?= $this->Lang; ?>" name="translation[<?= $this->Lang; ?>][intro_title:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['intro_title:str'] ?  $_POST['translation'][$this->Lang]['intro_title:str'] : htmlspecialchars($this->PageInfo['translation'][$this->Lang]['intro_title']) ?>" /></td>
	</tr>
	*/?>
	<tr>
		<td class="min-width va-top <?= $this->Errors['body'] ? 'error' : '' ?>"><label for="fron_text_<?= $this->Lang; ?>">Вступительный текст:</label></td>
		<td class="input"><textarea cols="29" rows="7" name="translation[<?= $this->Lang; ?>][fron_text:str]" id="fron_text_<?= $this->Lang; ?>"><?= $_POST['translation'][$this->Lang]['fron_text:str'] ?  $_POST['translation'][$this->Lang]['fron_text:str'] : $this->PageInfo['translation'][$this->Lang]['fron_text'] ?></textarea></td>
	</tr>
	
	<tr>
		<td class="min-width va-top <?= $this->Errors['body'] ? 'error' : '' ?>"><label for="body_<?= $this->Lang; ?>">Содержание:</label></td>
		<td class="input"><textarea cols="29" rows="7" name="translation[<?= $this->Lang; ?>][content:str]" id="content_<?= $this->Lang; ?>"><?= $_POST['translation'][$this->Lang]['content:str'] ?  $_POST['translation'][$this->Lang]['content:str'] : $this->PageInfo['translation'][$this->Lang]['content'] ?></textarea></td>
	</tr>
	
	<tr>
		<td colspan="2"><div class="hr">&nbsp;</div></td>
	</tr>
	<?/*
	<tr>
		<td class="min-width va-top <?= $this->Errors['body'] ? 'error' : '' ?>"><label for="right_column_<?= $this->Lang; ?>">Правая колонка:</label></td>
		<td class="input"><textarea cols="29" rows="7" name="translation[<?= $this->Lang; ?>][right_column:str]" id="right_column_<?= $this->Lang; ?>"><?= $_POST['translation'][$this->Lang]['right_column:str'] ?  $_POST['translation'][$this->Lang]['right_column:str'] : $this->PageInfo['translation'][$this->Lang]['right_column'] ?></textarea></td>
	</tr>
	<tr>
		<td class="min-width <?= $this->Errors['running_line'] ? 'error' : '' ?>"><label for="running_line">Бегущая строка:</label></td>
		<td class="input"><input type="text" id="running_line" name="translation[<?= $this->Lang; ?>][running_line:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['running_line:str'] ?  $_POST['translation'][$this->Lang]['running_line:str'] : $this->PageInfo['translation'][$this->Lang]['running_line'] ?>" /></td>
	</tr>
	<tr>
		<td class="min-width <?= $this->Errors['link_text'] ? 'error' : '' ?>"><label for="link_text">Подпись к ссылке:</label></td>
		<td class="input"><input type="text" id="link_text" name="translation[<?= $this->Lang; ?>][link_text:str]"  class="small-width" value="<?= $_POST['translation'][$this->Lang]['link_text:str'] ?  $_POST['translation'][$this->Lang]['link_text:str'] : $this->PageInfo['translation'][$this->Lang]['link_text'] ?>" /></td>
	</tr>*/?>
</table>