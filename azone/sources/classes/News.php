<?
	
	class News extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'news/';
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $Category					= array();
		protected $Category_sz				= 0;
		protected $SoftCategory				= array();
		protected $SoftCategory_sz			= 0;
		protected $SoftInfo					= array();
		protected $CatIdList				= array();
		protected $Articles					= array();
		protected $Articles_sz				= 0;
		protected $Soft					= array();
		protected $Soft_sz				= 0;
		protected $ArticlesInfo				= array();
		protected $Size			= array('small'=>array('width'=>111, 'height'=>111), 
										'medium'=>array('width'=>234, 'height'=>234), 
										'large'=>array('width'=>81, 'height'=>64));
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			
			$this->_init_grid();
		}
		
		public function OnNew() {
			
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/news.js');
			//$this->_load_category();
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert(){

			// _debug($_POST, 1);
			if(!$this->_check_data($_POST)){
				$this->OnNew();
			}
			else {
				$id = $this->_insert_articles($_POST);
				
				if ($_POST['subscribe'] == 1) {
					$this->_send_subscribe($_POST);
				}
				
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {			
			global $DB, $config;	

			$id = intval($_GET['id']);
						
			/*$sql = 'SELECT filename 
					FROM documents
					WHERE id	= \''.$id.'\'';
			
			$res = $DB->GetRow($sql); 
*/
			//@unlink($config['absolute-path'].'public/files/files/documents/'.$res['filename']);
	
			$sql = 'DELETE FROM news 
					WHERE id	= \''.$id.'\'';
			$DB->Execute($sql);
			
			$sql = 'DELETE FROM news_translation 
					WHERE obj_id	= \''.$id.'\'';
			$DB->Execute($sql);		
				
			$this->OnDefault();
		}
		private function _load_photos($id) {
			global $DB;
			
			$sql = "SELECT *
					FROM photos
					WHERE is_news = 1 AND product_id=".$id;
			
			$description = $DB->GetAll($sql);
			$description_sz = count($description);
		
			$this->AttachComponent('GridAll', $this->GridPhoto);

			$titles = array( 'Фото','По умолчанию','Описание/рус','Описание/укр','Описание/англ');
			
			$options['row_numbers'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=RemovePhoto&amp;id='.$id.'&amp;photo_id=%s&cat='.$_GET['cat'].'&page='.$_GET['page']);
			}
			$rows = array();
			
			for($i = 0; $i < $description_sz; $i++){				
					$row = array('<span style="text-align:center; display:block;"><img src="/public/files/gallery/thumb-'.$description[$i]['filename'].'" alt="" /></span>',
								'<span style="text-align:center; display:block;"><input onclick="SetDefault('.$description[$i]['product_id'].','.$description[$i]['id'].')" value="'.$description[$i]['id'].'" type="radio" '.($description[$i]['default'] ? 'checked="checked"' : '').' name="default"  /></span>',
									'<textarea name="text_ru['.$description[$i]['id'].']">'.$description[$i]['text_ru'].'</textarea>',
									'<textarea name="text_ua['.$description[$i]['id'].']">'.$description[$i]['text_ua'].'</textarea>',
									'<textarea name="text_en['.$description[$i]['id'].']">'.$description[$i]['text_en'].'</textarea>'
								);
					$rows[$description[$i]['id']] = $row;
					
			}

			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows, 'noform' => true);
			$this->GridPhoto->SetData($data);
		}
		
		
		
		public function OnDelFile() {
			global $DB, $config;			
			
			$this->_load_articles_info($_GET['id'], intval($_GET['lang_id']));
			
			@unlink($config['absolute-path'].'public/files/files/documents/'.$this->ArticlesInfo['filename']);
			
			$sql = 'UPDATE documents 
					SET filename	= \'\' 
					WHERE id=\''.$_GET['id'].'\'';

			$DB->Execute($sql);
			
			header('Location: '.$this->RootUrl.'documents/?Event=Edit&id='.$_GET['id']);
		}
		public function OnSetDefault() {
			$this->SendAjaxHeaders();
			$sql = 'UPDATE photos
					SET `default` = 0
					WHERE is_news = 1 AND product_id = \''.$_GET['id'].'\'';
			Execute($sql);
			
			$sql = 'UPDATE photos
					SET `default` = 1
					WHERE id = \''.$_GET['photo'].'\'';
			Execute($sql);
			exit;
		}
		
		
		protected function OnRemovePhoto(){
			global $DB,$config;						
			$sql = 'SELECT *
					FROM photos
					WHERE id = \''.(int)$_GET['photo_id'].'\'';
			$res = $DB->GetRow($sql);			
			@unlink($config['absolute-path'].'public/files/images/gallery/'.$res['filename']);			
			$sql = "DELETE FROM photos 
					WHERE id = '".(int)$_GET['photo_id']."'";
			$DB->Execute($sql);			
			header("Location: /azone/news/?Event=Edit&id=".(int)$_GET['id']);
		}
		public function OnAddPhoto() {
			$this->SetTemplate('editfoto.php');
			$this->AddJS('/core/lib/fancy/Swiff.Uploader.js');
			$this->AddJS('/core/lib/fancy/Fx.ProgressBar.js');
			$this->AddJS('/core/lib/fancy/FancyUpload2.js');
			$this->AddCSS('/core/lib/fancy/fancy.css');	
			$this->AddJS($this->RootUrl.'public/js/fotoobjects.js');
		}
		
		public function OnEdit() {
			$this->_load_photos(intval($_GET['id']));
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			
			$this->AddJS($this->RootUrl.'public/js/browse_product.js');
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
					
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/news.js');
			
			$this->_load_articles_info(intval($_GET['id']), intval($_GET['lang_id']));	
			$this->SetTemplate('edit.html');
		}
		
		public function OnView() {
			
			$this->AddCSS($this->RootUrl.'public/css/accordion.css');
			$this->AddJS($this->RootUrl.'public/js/accordion.js');
			$this->_load_category();
			
			$this->_load_articles_info(intval($_GET['id']));	
		
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_Articles(intval($_GET['id']), $_POST);
				
				if ($_POST['subscribe'] == 1) {
					$this->_send_subscribe($_POST);
				}
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.intval($_GET['id']));
				}
				else {
					
					$this->OnDefault();
				}
			}
		}

		/*
		 * Private methods
		 */

		private function _load_articles_info($id, $lang_id) {
			global $DB;			
			
			$lang_id = 0;
			
		 	 $sql = 'SELECT d.*
						FROM news d
						WHERE d.id = \''.$id.'\'';
			
			$this->ArticlesInfo = $DB->GetRow($sql);
			
			$this->ArticlesInfo['translation']['ru'] = GetRow('SELECT * FROM news_translation WHERE obj_id = '.$id.' AND lang = \'ru\''); 
			$this->ArticlesInfo['translation']['ua'] = GetRow('SELECT * FROM news_translation WHERE obj_id = '.$id.' AND lang = \'ua\'');
			$this->ArticlesInfo['translation']['en'] = GetRow('SELECT * FROM news_translation WHERE obj_id = '.$id.' AND lang = \'en\'');
		}
		
		protected function GetLangId($code) {
			global $DB;			
			
		 	$sql = 'SELECT id 
					FROM	languages
					WHERE 	code	= \''.$code.'\'';
			
			$res = $DB->GetRow($sql);
			
			return $res['id'];
		}
		
		private function DeleteFile($name) {
			global $DB, $config;			
			
			@unlink($config['absolute-path'].'public/files/files/documents/'.$name);
		}
		
			
		private function _update_articles($id, $data) {
			global $DB, $config;

					
			$sql = 'UPDATE news 
					SET published	= \''.($data['publish'] ? 1 : 0).'\',
						news	= \''.($data['news'] ? 1 : 0).'\',
						main	= \''.($data['main'] ? 1 : 0).'\',	
						filename	= \''.($data['filename']).'\',	
						filename2	= \''.($data['filename2']).'\',	
						url			= \''.$data['url'].'\',
						subscribe	= \''.$data['subscribe'].'\',
						module	= \''.$data['module'].'\',
						publish_ts	= \''.strtotime($data['publish_ts']).'\'
					WHERE id=\''.$id.'\'';

			$DB->Execute($sql);
			
			if( is_array( $data['translation'] ) && $data['translation'] ){
				foreach ( $data['translation'] as $key => &$value){
					
					$value['lang:enum'] = $key;
				
					if($this->_is_translation_exist($id, $key, 'news_translation')){
				
						$this->_update_translation($id, $value, 'news_translation', array(), $value['lang:enum']);
						
					}else{
						$value['obj_id:int'] = $id;
						$this->_insert($value, 'news_translation');
					}	
				}
			}	
			if($_POST['text_ru']){
				foreach($_POST['text_ru'] as $el=>$item){
					$sql = "UPDATE photos SET text_ru = '".s($item)."' WHERE id = '".$el."'";
					$DB->Execute($sql);
				}
			}
			if($_POST['text_ua']){
				foreach($_POST['text_ua'] as $el=>$item){
					$sql = "UPDATE photos SET text_ua = '".s($item)."' WHERE id = '".$el."'";
					$DB->Execute($sql);
				}
			}
			if($_POST['text_en']){
				foreach($_POST['text_en'] as $el=>$item){
					$sql = "UPDATE photos SET text_en = '".s($item)."' WHERE id = '".$el."'";
					$DB->Execute($sql);
				}
			}
		}
		
		
		private function _insert_articles($data) {
		
			global $DB;
			
			// _debug($data, 1, 'insert $data');
			$sql = 'INSERT INTO news (
							news,
							filename,
							filename2,
							published, 
							main,
							url, 
							subscribe, 
							module, 
							create_ts,
							publish_ts)
				 	VALUES (
							\''.( $data['news'] ? 1 : 0) .'\', 
							\''.$data['filename'].'\',
							\''.$data['filename2'].'\',
							\''.( $data['publish'] ? 1 : 0) .'\',
							\''.( $data['main'] ? 1 : 0) .'\',  
				 			\''.$data['url'].'\', 
				 			\''.$data['subscribe'].'\', 
				 			\''.$data['module'].'\', 
				 			\''.time().'\',
				 			\''.strtotime($data['publish_ts']).'\')';
			// _debug($sql, 1, 'insert $sql');
			
			if ($DB->Execute($sql)){
				$sql = 'SELECT MAX(id) AS id FROM news';
				$res = $DB->GetRow($sql);	
				
				if( is_array( $data['translation'] ) && $data['translation'] ){
					foreach ( $data['translation'] as $key => &$value){
						$value['lang:enum'] = $key;
						$value['obj_id:int'] = $res['id'];
	
						$this->_insert($value, 'news_translation');
					}
				}
			}
		}
				
		private function _check_data($data, $update = false) {
		
			// _debug($data, 1, '$data');
			$this->Errors['name_ru'] = $data['translation']['ru']['title:str'] ? false : true;
			$this->Errors['url'] = $data['url'] ? false : true;
			//$this->Errors['introtext'] = $data['introtext'] ? false : true;
			$this->Errors['fulltext_ru'] = $data['translation']['ru']['fulltext:str'] ? false : true;
					
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		
		
		private function _load_articles($cat_id = 0) {
			global $DB;
			
			$where = "WHERE `module` = '".$this->ModuleInfo['name']."'";
			
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM news '.$where;
			$res = $DB->GetRow($sql);
			
			$items_per_page = $this->_get_system_variable('admin_items_per_page');
			$max_pages_cnt = $this->_get_system_variable('admin_max_pages_cnt');;
			
			$page = intval($_GET['page']);
			
			if($page < 1){				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'news/', $page, '?page=%s');
			
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}else{
				$order_by = 'aa.create_ts DESC';
			}
			
			$sql = 'SELECT aa.*, dt.title as t_title 
					FROM news aa
						LEFT JOIN news_translation dt ON (aa.id = dt.obj_id AND dt.lang = \'ru\') 
					'.$where.'
					ORDER BY '.$order_by.'
					LIMIT '.$items_per_page.' OFFSET '.(($page - 1) * $items_per_page);

			$this->Articles = $DB->GetAll($sql);
			$this->Articles_sz = count($this->Articles);
		}
		
		private function _init_grid() {
			
			$this->_load_articles();
			
			$this->AttachComponent('GridAll', $this->Grid);
			
			$titles = array('Заголовок', 'Опубликовано','Дата публикации');
			
			
			$options['row_numbers'] = false;
			$options['sortable'] = array(	0 => array(	'up' => $this->RootUrl.'news/?sort=title&direct=ASC', 
														'down' => $this->RootUrl.'news/?sort=title&direct=DESC'),
											1 => array(	'up' => $this->RootUrl.'news/?sort=published&direct=ASC', 
														'down' => $this->RootUrl.'news/?sort=published&direct=DESC'
											
											),
											2 => array(	'up' => $this->RootUrl.'news/?sort=aa.publish_ts&direct=ASC', 
														'down' => $this->RootUrl.'news/?sort=aa.publish_ts&direct=DESC'
											
											)
										);						
			$options['multiply'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать','UnPublished'=> 'Скрыть','DeleteSelected'=> 'Удалить');
			
			if($_GET['page'])
				$options['page'] = $_GET['page'];
			else	
				$options['page'] = 1;
			
			if($this->Perm['edit']) {
				$options['controls'] = array(
												'edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s&page='.$_GET['page'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'], 
										     	'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s&page='.$_GET['page'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct']/*,
												'meta' => $this->RedirectUrl.'?Event=EditMeta&popup=show&amp;id=%s'
											*/);
			}
			
			
			$rows = array();
			
			
			
			
			for($i = 0; $i < $this->Articles_sz; $i++){
				
				$active = $this->Articles[$i]['published'] ? 
								'<a href="/azone/news/?Event=PublicPage&id='.$this->Articles[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/news/?Event=PublicPage&id='.$this->Articles[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';
				
				$row = array($this->Articles[$i]['t_title'],
							$active ,
							date('d/m/y H:i',$this->Articles[$i]['publish_ts']) 
							);
				$rows[$this->Articles[$i]['id']] = $row;
			}
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	
		private function _load_category($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT dc.id, dct.title as name 
					FROM heading dc
						LEFT JOIN heading_translations dct ON (dc.id = dct.object_id AND dct.lang_id = \''.$this->DefaultLangId.'\') 
					WHERE dc.parent_id=\''.$parent_id.'\'';
		
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['display_name'] = str_repeat('&nbsp;', $level*4).$cat[$i]['name'];
				$cat[$i]['level'] = $level;
				array_push($this->Category, $cat[$i]);
				$level++;
				$this->_load_category($cat[$i]['id']);
				$level--;
			}
			if($parent_id == 0) {
				
				$this->Category_sz = count($this->Category);
			}
		}

		
		private function _load_cataegories_id($parent_id) {
			
			global $DB;
			
			$sql = 'SELECT id 
					FROM heading 
					WHERE parent_id=\''.$parent_id.'\'';
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				array_push($this->CatIdList, $cat[$i]);
				$this->_load_cataegories_id($cat[$i]['id']);
			}
		}
	
	/* Extent */
		public function OnPublicPage(){
			global $DB;
			$sql = "UPDATE news SET published = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		
		public function OnSortUp() {
			global $DB;
			$sql='SELECT *
				  FROM model
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM model
					WHERE model.order < \''.$model['order'].'\'
					ORDER BY model.order DESC
					LIMIT 1';
			$model_2 = $DB->GetRow($sql);
			$sql='UPDATE model
				  SET model.order=\''.$model_2['order'].'\'
				  WHERE id=\''.$_GET['id'].'\'';
			$DB->Execute($sql);
			$sql='UPDATE model
				  SET model.order=\''.$model['order'].'\'
				  WHERE id=\''.$model_2['id'].'\'';
			$DB->Execute($sql);
			$this->OnDefault();
		}

		public function OnSortDown() {
			global $DB;
			$sql='SELECT *
				  FROM model
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM model
					WHERE model.order > \''.$model['order'].'\'
					ORDER BY model.order ASC
					LIMIT 1';
			$model_2 = $DB->GetRow($sql);
			$sql='UPDATE model
				  SET model.order=\''.$model_2['order'].'\'
				  WHERE id=\''.$_GET['id'].'\'';
			$DB->Execute($sql);
			$sql='UPDATE model
				  SET model.order=\''.$model['order'].'\'
				  WHERE id=\''.$model_2['id'].'\'';
			$DB->Execute($sql);
			$this->OnDefault();
		}

		public function OnPublished(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE news SET published = 1 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				
			}
			header('Location: /azone/news/?page='.$_GET['page']);
		}
		public function OnUnPublished(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE news SET published = 0 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
			}
			header('Location: /azone/news/?page='.$_GET['page']);
		}
		
		public function OnDeleteSelected(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "DELETE FROM news WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				$DB->Execute("DELETE FROM news_translation WHERE obj_id IN (".implode(',', $ids).")");
			}
			header('Location: /azone/news/?page='.$_GET['page']);
		}
		
		/* Edit Meta */
		protected function OnEditMeta(){
			$this->_load_articles_info(intval($_GET['id']),0);
			$this->SetTemplate('edit-meta.html');		
		}
		
		protected function OnUpdateMeta(){
			global $DB;
			$sql = "UPDATE news
					SET 
						url = '".mysql_escape_string($_REQUEST['url'])."'
					WHERE id = '".intval($_REQUEST['id'])."'
				";
			$DB->Execute($sql);
			$sql = "UPDATE news_translation
					SET 
						page_title = '".mysql_escape_string($_REQUEST['page_title'])."', 
						
						keywords = '".mysql_escape_string($_REQUEST['keywords'])."',
						description = '".mysql_escape_string($_REQUEST['description'])."'
					WHERE object_id = '".intval($_REQUEST['id'])."'
				";
		
			$DB->Execute($sql);
			echo '<script type="text/javascript">setTimeout("window.close()",3000)</script>';		
			exit();			
		}
		
		protected function _send_subscribe($data){
			global $config;
  
			$recipients = GetAll('SELECT * FROM users WHERE subscribe = 1');
			AttachLib('Mailer');
			$mail = new Mailer();
			
			$site_url = $config['site_url'];
			if($site_url{strlen($site_url)-1} == ';'){
				$site_url = substr($site_url,0,-1);
			}
			
			foreach($recipients as $email){
				if($email['lang'] == 'ru'){
					$mail->AddRecepient($email['email']);                    
					$mail->AddVars(array(
						'title'=> $data['translation']['ru']['title:str'],
						'url'=> $config['site_url'].'news/view/'.$data['url'].'.html',
						'filename'=> $site_url.$data['filename'],
						'introtext'=> $data['translation']['ru']['introtext:str'],
						'date'=>date('d:m:Y H:i',time())
					));
					$mail->Send(
						'subscribe-news.html',
						$this->_get_system_variable('news_subscribed_subject'),
						$this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name')
					);
					$mail->DelRecepient();
					$mail->DelVars();
				}else{
					$mail->AddRecepient($email['email']);                    
					$mail->AddVars(array(
						'title'=> $data['translation']['en']['title:str'],
						'url'=> $config['site_url'].'en/news/view-'.$data['url'],
						'filename'=> $site_url.$data['filename'],
						'introtext'=> $data['translation']['en']['introtext:str'],
						'date'=>date('d:m:Y H:i',time())
					));
					$mail->Send(
						'subscribe-news.html',
						$this->_get_system_variable('news_subscribed_subject'),
						$this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name')
					);
					$mail->DelRecepient();
					$mail->DelVars();
				}
			}
		}
		
		
	
	}
?>