<?

	class AdminPanel extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'admin-panel/';
		protected $TyreWidthList			= null;
		protected $TyreWidth				= array();
		protected $TyreWidth_sz				= 0;
		public 	  $TyreWidthInfo			= array();
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->SetTemplate('main.html');
			$this->_load_tyre_width_list();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
			$this->TyreWidth = $this->_get_tyre_width_list();
			$this->TyreWidth_sz = count($this->TyreWidth);
			
		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_tyre_width($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_tyre_width(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			
			$this->SetTemplate('edit.html');

			$sql = 'SELECT * FROM admin_table WHERE id=\''.intval($_GET['id']).'\'';
			$this->TyreWidthInfo = $DB->GetRow($sql);
		}
		
		public function OnUpdate() {
			
			global $DB;
			
			if(!$this->_chech_input($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_tyre_width(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		private function _update_tyre_width($id, $data){
		
			global $DB;
			$sql = 'UPDATE admin_table  
					SET name=\''.$data['tyre_width'].'\',
						state=\''.($data['publish'] ? 1 : 0).'\'						
					WHERE id=\''.$id.'\'
			';
			
			$DB->Execute($sql);
			
		}
		

		private function _remove_tyre_width($id) {			
			global $DB;
			$sql = 'DELETE FROM admin_table WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		
			private function _chech_input($data, $update = false) {
			
			$this->Errors['tyre_width'] = $data['tyre_width'] ? false : true;
					
			if(in_array(true, $this->Errors)) {				
				
				$this->DisplayError = true;
			}	
								
			return !$this->DisplayError;
		}
		
		private function _load_tyre_width_list() {			
			
			$list = $this->_get_tyre_width_list();
			$list_sz = count($list);			
			
			$this->AttachComponent('Grid', $this->CatList);	
					
			$titles = array('Опция', 'Включено');
			$options['row_numbers'] = true;			
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s');
			}			
			$rows = array();
			for($i = 0; $i < $list_sz; $i++){
				
				$row = array($list[$i]['name'],
				             $list[$i]['state'] ? 'да' : 'нет');
				$rows[$list[$i]['id']] = $row;
			}			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->CatList->SetData($data);
		}
		
		private function _get_tyre_width_list() {
			global $DB;
			$sql = 'SELECT * FROM admin_table';
			return $DB->GetAll($sql);
		}
	}

?>