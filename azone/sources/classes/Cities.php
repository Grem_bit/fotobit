<?
	class Cities extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'cities/';
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $Items					= array();
		protected $Items_sz					= 0;
		protected $ItemInfo					= array();
	
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');			
			$this->_init_grid();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_item($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {			
			
			global $DB;			
			
			$sql = 'DELETE FROM city 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
			$this->GoToUrl('/azone/cities/?page='.$_GET['page']);
		}
		
		public function OnEdit() {

			global $DB;
			
			$sql = 'SELECT * FROM city WHERE id=\''.intval($_GET['id']).'\'';
			$this->ItemInfo = $DB->GetRow($sql);
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, intval($_GET['id']))) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_item(intval($_GET['id']), $_POST);
				//$this->OnDefault();
				$this->GoToUrl('/azone/cities/?page='.intval($_GET['page']));
			}
		}

		/*
		 * Private methods
		 */

			private function _update_item($id, $data) {
				
				global $DB;
				
				$sql = 'UPDATE city 
						SET name=\''.$data['title'].'\',en_name=\''.$data['en_title'].'\',
							show_header=\''.($data['show_header'] ? 1:0).'\',
							capital = \''.intval($data['capital']).'\',
							selected = \''.intval($data['selected']).'\',

							country_id=\''.$data['country'].'\'
						WHERE id=\''.$id.'\'';
				
				$DB->Execute($sql);
		}
		
		private function _insert_item($data) {
			
			global $DB;
			$sql = "SELECT MAX(order_by) AS m_order_by 
					FROM city ";
			
			$res = $DB->GetOne($sql);
			$res++;
			$sql = 'INSERT INTO city  
					SET name=\''.$data['title'].'\',
					capital = \''.intval($data['capital']).'\',
					selected = \''.intval($data['selected']).'\',

					order_by = \''.$res.'\',
					en_name=\''.$data['en_title'].'\', country_id=\''.$data['country'].'\', show_header=\''.($data['show_header'] ? 1:0).'\'';
			$DB->Execute($sql);
		}
				
		private function _check_data($data, $id = 0) {
			
			$this->Errors['title'] = $data['title'] ? false : true;
			
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		private function _load_cities() {

			global $DB;

			$sql = 'SELECT COUNT(*) AS cnt 
					FROM city ';
			$res = $DB->GetRow($sql);
			$items_per_page = 25;
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){				
				$page = 1;
			}
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'cities/', $page, '?page=%s');
			$sql = 'SELECT * 
					FROM city
					ORDER BY order_by ASC 
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			$this->Items = $DB->GetAll($sql);
			$this->Items_sz = count($this->Items);
		}
		
		private function _init_grid($cat_id = 0) {
			
			$this->_load_cities();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('Название','English');
			$options['row_numbers'] = false;
			$options['multiply'] = true;			
			$options['multiply_events'] = array('DeleteSelected'=> 'Удалить');
			$options['multiply_info'] = array('table'=>'city','module'=>'cities','field'=>'active');
			$options['sort'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			$rows = array();
			for($i = 0; $i < $this->Items_sz; $i++){
				$row = array($this->Items[$i]['name'],$this->Items[$i]['en_name']);
				$rows[$this->Items[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}

		public function OnSortUp() {
			global $DB;
			$sql='SELECT *
				  FROM city
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM city
					WHERE city.order_by < \''.$model['order_by'].'\'
					ORDER BY city.order_by DESC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			
			if($model_2){
				$sql='UPDATE city
					  SET city.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE city
				  	SET city.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			
			}
			$this->GoToUrl('/azone/cities/?page='.$_GET['page']);
		}

		public function OnSortDown() {
			global $DB;
			$sql='SELECT *
				  FROM city
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM city
					WHERE city.order_by > \''.$model['order_by'].'\'
					ORDER BY city.order_by ASC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			if($model_2){
				$sql='UPDATE city
					  SET city.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE city
				  	SET city.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			}
			$this->GoToUrl('/azone/cities/?page='.$_GET['page']);
		}
		

	}
?>