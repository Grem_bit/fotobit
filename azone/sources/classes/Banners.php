<?
	class Banners extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'banners/';
		
		protected $TableName				= 'banners';
		protected $PublishField				= 'active';
		
		protected $BList					= null;
		protected $Brands					= array();
		protected $Brands_sz				= 0;
		protected $BrandInfo				= array();
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		protected $PageNavigator			= null;
		
		protected $LHeight1					= 120;
		protected $LHeight2					= 100;
		protected $LWidth1					= 70;
		protected $LWidth2					= 50;
		protected $MD5FileName				= '';
		protected $FileError				= '';
		protected $Positions				= array(
		'Сверху',
		'Слева',
		'Справа',
		'После рубрикатора',
		'В контенте',
		'В тексте',
		'Между свежими блоками'/*,
		'под первым блоком',
		'под вторым блоком',
		'под третьим блоком',
		'под четвертым блоком'*/
		
		);
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');
			$this->_init_brands_grid();
		}
		
		public function OnNew() {
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/banners.js');
			$this->SetTemplate('edit.html');
			$this->_load_cataegories();
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_brand($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;			
			$sql = 'DELETE FROM banners WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/banners.js');
			$sql = 'SELECT * FROM banners WHERE id=\''.intval($_GET['id']).'\'';
			$this->BrandInfo = $DB->GetRow($sql);
			$this->_load_cataegories();
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_brand(intval($_GET['id']), $_POST,$_FILES);
				$this->OnDefault();
			}
		}
		
		
		/*
		 * Private methods
		 */
		
		private function _update_brand($id, $data,$files) {
			
			global $DB;
			if($this->Perm['moderate']){			
				$sql = 'UPDATE banners 
					SET active=\''.intval($_POST['state']).'\'						
					WHERE id=\''.$id.'\'';
				$DB->Execute($sql);
				return;			
			}	
			$pos = strpos($data['url'],"http://");
			if($pos === FALSE){				
				$pos = strpos($data['url'],"https://");
				if($pos === FALSE){				
					$data['url'] = 'http://'.$data['url'];				
				}			
			}
			
			$sql = 'UPDATE banners
					SET 
						name=\''.$data['name'].'\',
						active=\''.$data['state'].'\',
						sidebar_r=\''.$data['sidebar_r'].'\',
						horizontal=\''.$data['horizontal'].'\',
						vertical=\''.$data['vertical'].'\',
						order_by=\''.intval($data['order_by']).'\',
						code = \''.htmlspecialchars(str_replace(array('<!--//<![CDATA[','//]]>-->','<!--','//-->','-->'),'',$data['code']), ENT_QUOTES).'\'
					WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
			/*code = \''.htmlentities(str_replace(array('<!--//<![CDATA[','//]]>-->'),'',$data['code']), ENT_QUOTES).'\',*/
		}
		
		private function _insert_brand($data) {
			
			global $DB;
			
			$pos = strpos($data['url'],"http://");
			if($pos === FALSE){				
				$pos = strpos($data['url'],"https://");
				if($pos === FALSE){				
					$data['url'] = 'http://'.$data['url'];				
				}			
			}
			
			$sql = 'INSERT INTO banners  
					SET 
						name=\''.$data['name'].'\',
						active=\''.$data['state'].'\',
						sidebar_r=\''.$data['sidebar_r'].'\',
						horizontal=\''.$data['horizontal'].'\',
						vertical=\''.$data['vertical'].'\',
						order_by=\''.intval($data['order_by']).'\',
						code = \''.htmlspecialchars(str_replace(array('<!--//<![CDATA[','//]]>-->','<!--','//-->','-->'),'',$data['code']), ENT_QUOTES).'\'';
		
			$DB->Execute($sql);
		}
				
		private function _check_data($data, $update = false) {		
			$this->Errors['name'] = trim($data['name']) ? false : true;			
			$this->Errors['code'] = trim(htmlentities($data['code'], ENT_QUOTES)) ? false : true;
			$this->ShowError = in_array(true, $this->Errors);			
			return !$this->ShowError;
		}
		
		private function _load_brands() {
			
			global $DB;
			
			$sql= 'SELECT COUNT(*) AS cnt FROM banners';
			$res = $DB->GetRow($sql);
			
			$items_per_page = $this->_get_system_variable('admin_items_per_page');
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){
				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'banners/', $page, '?page=%s');
		
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}else{
				$order_by = 'name DESC';
			}
			
			$limit = '	ORDER BY '.$order_by.'
					 	LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			
			
			$sql = 'SELECT banners.*
					FROM banners ';
		
			$sql.=$limit;
			$this->Brands = $DB->GetAll($sql);
			$this->Brands_sz = count($this->Brands);
		}
		
		private function _init_brands_grid() {
			
			$this->_load_brands();
			
			$this->AttachComponent('GridAll', $this->BList);
			$options['sortable'] = array(	0 => array(	'up' => $this->RootUrl.'banners/?sort=name&direct=ASC', 
														'down' => $this->RootUrl.'banners/?sort=name&direct=DESC'));
			$titles = array('Баннер','Опубликован');

			$options['multiply'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать', 'UnPublished'=> 'Скрыть', 'DeleteSelected'=> 'Удалить');
	
			if($this->Perm['moderate']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s');
			}elseif($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $this->Brands_sz; $i++){
				
				$active = $this->Brands[$i]['active'] ? '<div id="container'.$this->Brands[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->Brands[$i]['id'].', 0, \'banners\', \'active\')">Да</span></div>' : '<div id="container'.$this->Brands[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->Brands[$i]['id'].', 1, \'banners\', \'active\')">Нет</span></div>';
				
				$row = array($this->Brands[$i]['name'], 
							 $active
							 );
				$rows[$this->Brands[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->BList->SetData($data);
		}
	
	
	///////
		private function SaveLogo(){
			$bool = false;
			switch ($_FILES['logo']['type']){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':					
				case 'image/x-png':
				case 'image/png':
				case 'image/gif':
					break;
				default:
					return false;
				
		    }
			global $DB;
			
			$path = $this->GetConfigParam('absolute-path').'/public/files/brands/';
			@mkdir($path,0777);
			$ext = explode(".",$_FILES['logo']['name']);
			$md5 = md5($_FILES['logo']['name']+time()).'.'.end($ext);
			$this->MD5FileName = $md5;
			
			$file = $path.'/'.$md5;
			
			$file_small = $path.'/thumb-'.$md5;
			
			if(move_uploaded_file($_FILES['logo']['tmp_name'],$file)){
				$bool = $this->resize($file,$_FILES['logo']['type'],$this->LHeight1, $this->LWidth1);
			}
			if(copy($file,$file_small)){
				$bool1 = $this->resize($file_small,$_FILES['logo']['type'],$this->LHeight2, $this->LWidth2);
			}
			return $bool && $bool1;		
		}
		
		private function _load_cataegories($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT id, 
						   title 
					FROM categories 
					WHERE parent_id=\''.$parent_id.'\'';
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			$this->Categories = $cat;
			$this->Categories_sz = count($this->Categories);
		}
		
		
	
	
	
	}
?>