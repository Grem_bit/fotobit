<?
	class Shops extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'shops/';
		
		protected $TableName				= 'shops';
		protected $PublishField				= 'publish';
		
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $magazines						= array();
		protected $magazines_sz					= 0;
		protected $magazinesInfo					= array();
		
	
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');			
			$this->_init_grid();
		}
		
		public function OnNew() {
			
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			$this->_load_regions();
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$id = $this->_insert_magazines($_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.$id);
				}
				else {
					
					$this->OnDefault();
				}
			}
		}
		
		public function OnRemove() {			
			global $DB;			
			$sql = 'DELETE FROM shops 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			$this->_load_regions();
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			$this->_load_magazines_info(intval($_GET['id']));	
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_magazines(intval($_GET['id']), $_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.intval($_GET['id']));
				}
				else {
					
					$this->OnDefault();
				}
			}
		}

		/*
		 * Private methods
		 */

		private function _load_magazines_info($id) {
			global $DB;			
			$sql = 'SELECT * FROM shops WHERE id=\''.$id.'\'';
			$this->magazinesInfo = $DB->GetRow($sql);
		}

		private function _update_magazines($id, $data) {
			global $DB;
			
			if($data['type'] == 2) $data['parent_id'] = '';
			
			$sql = 'UPDATE shops 
					SET title=\''.$data['title'].'\', 
						url=\''.$data['url'].'\', 
						content=\''.$data['content'].'\', 
						city=\''.$data['city'].'\', 
						parent_id=\''.$data['parent_id'].'\', 
						type=\''.$data['type'].'\' 
					WHERE id=\''.$id.'\'';
			
			$DB->Execute($sql);
		}
		
		private function _insert_magazines($data) {
			global $DB;
			
			if($data['type'] == 2) $data['parent_id'] = '';
			
			$sql = 'INSERT INTO shops 
						SET title=\''.$data['title'].'\', 
						url=\''.$data['url'].'\', 
						content=\''.$data['content'].'\', 
						city=\''.$data['city'].'\', 
						parent_id=\''.$data['parent_id'].'\', 
						type=\''.$data['type'].'\'
					';
			
			$DB->Execute($sql);
			$sql = 'SELECT LAST_INSERT_ID() AS id FROM shops';
			$res = $DB->GetRow($sql);
			return $res['id'];
		}
				
		private function _check_data($data, $update = false) {
			$this->Errors['title'] = $data['title'] ? false : true;
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		private function _load_regions(){
			global $DB;
			$this->Regions = $DB->GetAll('SELECT * FROM country ORDER BY name');
			
		}
		
		private function _load_magazines() {
			global $DB;
			
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM shops'.$where;
			$res = $DB->GetRow($sql);
			$items_per_page = intval($this->_get_system_variable('admin_items_per_page'));
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'shops/', $page, '?page=%s', '', true, true);
			$sql = 'SELECT m.* , c.name
					FROM shops m 
					LEFT JOIN country c ON c.id = m.parent_id'.$where.'
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			$this->magazines = $DB->GetAll($sql);
			$this->magazines_sz = count($this->magazines);
		}
		
		private function _init_grid($cat_id = 0) {
			$this->_load_magazines();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('Заголовок'/*, 'Область'*/);
			$options['row_numbers'] = false;
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}

			$options['multiply'] = true;
			$options['multiply_events'] = array('DeleteSelected'=> 'Удалить');
	
			$rows = array();
			for($i = 0; $i < $this->magazines_sz; $i++){
				
				
				$row = array(
					$this->magazines[$i]['title']
					// ,$this->magazines[$i]['name']
				);
				$rows[$this->magazines[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	
		

		

	}
?>