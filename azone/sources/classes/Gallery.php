<?
	class Gallery extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'gallery/';
		
		protected $TableName				= 'gallery';
		protected $PublishField				= 'publish';
		
		protected $PageNavigator			= null;
		protected $Grid					= null;
		protected $Products				= array();
		protected $Products_sz				= 0;
		protected $ProdInfo				= array();
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		protected $CatIdList				= array();
		protected $DescriptionInfo			= array();
		
		
		protected $LHeight1				= 120;
		protected $LHeight2				= 260;
		protected $LWidth1				= 124;
		protected $LWidth2				= 260;
		protected $List_al				= array();
		
		
		protected $MD5FileName				= '';
		protected $FileError				= '';		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			//$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');
			
			$this->_init_grid();
		}
	
		public function OnRemove(){
			global $DB,$config;
			
			
			$sql = 'SELECT *
					FROM gallery
					WHERE id = \''.(int)$_GET['id'].'\'';
			$res = $DB->GetRow($sql);
			
			@unlink($config['absolute-path'].'public/files/images/gallery/'.$res['filename']);
			@unlink($config['absolute-path'].'public/files/images/gallery/middle-'.$res['filename']);
			
			$sql = "DELETE FROM gallery 
					WHERE id = '".(int)$_GET['id']."'";
			$DB->Execute($sql);
			
			header("Location: /azone/gallery/?page=".(int)$_GET['page'].'&els='.$_GET['els']);
		}
		
		public function OnDeleteSelected(){
			global $DB, $config;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$sql = 'SELECT *
							FROM gallery
							WHERE id = \''.(int)$value.'\'';
					$res = GetRow($sql);
					
					@unlink($config['absolute-path'].'public/files/images/gallery/'.$res['filename']);
					@unlink($config['absolute-path'].'public/files/images/gallery/middle-'.$res['filename']);
					
					$ids[] = intval($value);
				}
				$sql = "DELETE FROM ".$this->TableName." WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
			}
			header('Location: '.$_SERVER['REQUEST_URI']);
		}
		
		public function OnNew() {
			$this->_get_list_albom();
			$this->SetTemplate('edit.php');
			$this->AddJS('/core/lib/fancy/Swiff.Uploader.js');
			$this->AddJS('/core/lib/fancy/Fx.ProgressBar.js');
			$this->AddJS('/core/lib/fancy/FancyUpload2.js');
			$this->AddCSS('/core/lib/fancy/fancy.css');	
			$this->AddJS($this->RootUrl.'public/js/fotoobjects.js');
		}
		
		public function OnUpdate() {//_debug($_POST, 1);
			global $DB;
			
			if($_POST['photo']){
				foreach($_POST['photo'] as $el =>$item){
					$sql = "UPDATE gallery SET order_by = '".mysql_escape_string($item)."' WHERE id  = '".intval($el)."'";
					$DB->Execute($sql);
				}
			}
			
			if($_POST['ID']){
				foreach($_POST['ID'] as $id){
					$sql = "UPDATE `gallery` SET `url` = '".$_POST['url'][$id]."' WHERE `id` = $id";
					$DB->Execute($sql);
				}
			}
			
			//$this->GoTo('/azone/gallery/');
			header('Location: '.$_SERVER['REQUEST_URI']);
			exit();
		
		}
		
		/*
		 * Private methods
		 */
		
		
		private function _load_products($cat_id) {
			
			global $DB;
			
			$where = ' 1=1 ';
					
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM gallery 
					';
			$res = $DB->GetRow($sql);
			
			if($_GET['els'] == 'all'){
				$no_pager = true;
			}else{
				$items_per_page = intval($_GET['els']) ? intval($_GET['els']) : 20;
				$max_pages_cnt = 9;
				$page = intval($_GET['page']);
				if($page < 1){
					
					$page = 1;
				}
				
				AttachLib('PageNavigator');
				$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'gallery/', $page, '?page=%s', '', true, true);
			}
			
			$order = '';
			
			if($_GET['sort'] && $_GET['direct']){
				$order = 'ORDER BY '.mysql_escape_string($_GET['sort']).' '.mysql_escape_string($_GET['direct']);
			}
			
			$sql = 'SELECT 	p.*
					FROM gallery p
					ORDER BY id
					'.$order.' '.( $no_pager ? '' : 'LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page );
			
			$this->Products = $DB->GetAll($sql);
			$this->Products_sz = count($this->Products);
		}
	
		/* Extend methods */
		public function OnPublicPage(){
			global $DB;
			$sql = "UPDATE gallery SET publish = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		public function OnPublicFront(){
			global $DB;
			$sql = "UPDATE gallery SET front = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		
		private function _init_grid($cat_id = 0) {
			global $url_parts, $DB;
			
			$this->_load_products($cat_id);
			
			$this->AttachComponent('GridAll', $this->Grid);
			
			$titles = array('Фото', 'Ссылка', 'Порядок','Слайдер на главной');
			$options['row_numbers'] = false;
			
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s'.'&els='.$_GET['els'].'&page='.$_GET['page']);
			}
			
			$options['multiply'] = true;
			$options['multiply_events'] = array('Update'=> 'Сохранить изменения');
			
			$rows = array();
			for($i = 0; $i < $this->Products_sz; $i++){
				
				
				/*$public = $this->Products[$i]['publish'] ? 
								'<a href="/azone/gallery/?Event=PublicPage&id='.$this->Products[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/gallery/?Event=PublicPage&id='.$this->Products[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';
				*/
				$front = $this->Products[$i]['front'] ? 
								'<a href="/azone/gallery/?Event=PublicFront&id='.$this->Products[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/gallery/?Event=PublicFront&id='.$this->Products[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';
				$row = array(	'<span style="text-align:center; display:block;"><img src="/core/lib/phpThumb/phpThumb.php?w=300&f=png&src=/public/files/images/gallery/'.$this->Products[$i]['filename'].'" alt="" /></span>'	 ,
					'<input type="text" style="width: 250px;" name="url['.$this->Products[$i]['id'].']" value="'.$this->Products[$i]['url'].'" />',
					'<input type="text" style="width: 50px;" name="photo['.$this->Products[$i]['id'].']" value="'.$this->Products[$i]['order_by'].'" />',
					 /*$public,*/ $front
					//, $this->_get_albom($this->Products[$i]['product_id'])
				
							 );
				$rows[$this->Products[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
		
		protected function _get_albom ($id) {
			$sql = "
				SELECT p.title
				FROM projects_translations p
				WHERE p.id = ". $id ."
			";
			$ar = GetAll($sql);
			return $ar[0]['title'];						
		}
		
	protected function _get_ ($tit) {
			//_debug($_POST);
			$sql = '
			SELECT c.id 
			FROM categories c 
			WHERE c.title = \''. $tit .'\''
			;
			//echo $sql; exit;
			$ar = GetAll($sql);
			if ($ar)
			return $ar[0]['id'];
			return 0;									
		}
		
		protected function _get_list_albom(){
			
			$sql = 'SELECT aa.id, dt.title as t_title 
					FROM projects aa
						LEFT JOIN projects_translations dt ON (aa.id = dt.object_id)
			';
			//echo $sql; exit;
			$this->List_al = GetAll($sql);
		}
		
	}
?>
