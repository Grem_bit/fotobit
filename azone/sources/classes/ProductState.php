<?

	class ProductState extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'product-state/';
		protected $CatList					= null;
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		public $CategoryInfo				= array();
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			$this->_load_cat_list();
		}
		
		public function OnNew() {
			
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				
				$this->OnNew();
			}
			else {
				$this->_insert_category($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_tree(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
			
			$sql = 'SELECT * FROM product_status WHERE id=\''.intval($_GET['id']).'\'';
			$this->CategoryInfo = $DB->GetRow($sql);
		}
		
		public function OnUpdate() {
			
			global $DB;
			
			if(!$this->_chech_input($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_category(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		private function _generate_url($url){
		
			if($url == '/index.html' || $url == 'index.html' || $url == '/'){
				$url = '/';
			}else{
				if($url[0] == '/') $url = substr($url,1,strlen($url));
				if($url[strlen($url)-1] == '/'){ $url = substr($url,0,strlen($url)-1);}
				$url = '/'.$url.'/';
			}
			
			
			return $url;
		
		}
		private function _update_category($id, $data){
		
			global $DB;
			
			$sql = 'UPDATE product_status  
					SET 	title		= \''.$data['title'].'\', 
							img 		= \''.$data['img'].'\'
					WHERE 	id = \''.$id.'\'';
			$DB->Execute($sql);
		}
		

		private function _remove_tree($id) {
			
			global $DB;
			
			$sql = 'DELETE FROM product_status WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_category($data) {
			
			global $DB;
			
			$sql = 'INSERT INTO product_status  
					SET 	title		= \''.$data['title'].'\', 
							img 		= \''.$data['img'].'\'';
			
			$DB->Execute($sql);
		}
		
		private function _chech_input($data, $update = false) {
			
			global $DB;
			
			$this->Errors['title'] = $data['title'] ? false : true;
			
			if(in_array(true, $this->Errors)) {
				
				$this->DisplayError = true;
			}
			
			return !$this->DisplayError;
		}
		
		private function _load_cat_list() {
			
			$list = $this->_get_cat_list();
			$list_sz = count($list);
			
			$this->AttachComponent('GridAll', $this->CatList);
			
			$titles = array('Статус', 'Картинка');
			$options['row_numbers'] = true;
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $list_sz; $i++){
				
				$row = array($list[$i]['title'], 
							 '<img src="'.$list[$i]['img'].'" />',
							 );
				$rows[$list[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->CatList->SetData($data);
		}
		
		private function _get_cat_list($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			static $cat_list = array();
			
			$sql = 'SELECT * 
					FROM product_status';

			$categories = $DB->GetAll($sql);
	
			return $categories;
		}
	}

?>