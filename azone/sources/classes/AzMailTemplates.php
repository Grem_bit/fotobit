<?

	class AzMailTemplates extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'az-mail-templates/';
		protected $TemplatesList			= null;
		protected $MailTemplates			= array();
		protected $MailTemplates_sz			= 0;

		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			
			$this->SetTemplate('main.html');
			$this->ModuleInfo = $this->_get_module_info('az-mail-templates');
		}
		
		public function OnDefault() {
			
			$this->_load_list();
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$sql = 'DELETE FROM az_mail_templates WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);
			
			$this->_load_list();
		}
	
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
		}

		/*
		 * Private methods
		 */
		
		private function _load_list() {
			
			$this->_load_mail_templates();
			
			$this->AttachComponent('Grid', $this->TemplatesList);
			
			$titles = array('Шаблон');
			$options['row_numbers'] = true;
							
			if($this->Perm['edit']){
				
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
											 'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			for($i = 0; $i < $this->MailTemplates_sz; $i++){
				
				$row = array($this->MailTemplates[$i]['name']);
				$rows[$this->MailTemplates[$i]['id']] = $row;
			}
							
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->TemplatesList->SetData($data);
		}
		
		private function _load_mail_templates() {
			
			global $DB;
			
			$sql = 'SELECT id,
						   name, 
						   filename 
					FROM az_mail_templates';
			$this->MailTemplates = $DB->GetAll($sql);
			$this->MailTemplates_sz = count($this->MailTemplates);
		}
	}

?>