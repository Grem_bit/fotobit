<?
/*//////////////////////////////////////////////////////////////////////////////

        LanguageConstants.php
        --------
        Started On:     May 11, 2009
        Copyright:      (C) 2008-2009 FlyUpStudio.
        E-mail:         support@flyupstudio.com.ua

        $Id: LanguageConstants.php,v 1.1 2009/05/11 11:40:00 kde Exps $

//////////////////////////////////////////////////////////////////////////////*/
	class LanguageConstants extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'language-constants/';
		protected $Records					= null;
		protected $Record					= null;
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->SetTemplate('main.html');
			$this->_load_list($_GET['keyword']);
		}
		
		public function OnNew() {
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/editor.js');
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				$this->OnNew();
			} else {
				$this->_insert_lc($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_lc(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/editor.js');

			$this->SetTemplate('edit.html');
			$sql = 'SELECT * FROM language_constants WHERE id=\''.intval($_GET['id']).'\'';
			$this->Record = $DB->GetRow($sql);
			$this->Record['value'] = htmlspecialchars($this->Record['value']);
		}
		
		public function OnUpdate() {
			
			if(!$this->_chech_input($_POST, true)) {
				$this->OnEdit();
			}else {
				$this->_update_lc(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		private function _update_lc($id, $data){
		
			global $DB;
			$sql = 'UPDATE 
						language_constants  
					SET 
						name 	= \'' . $data['name'] . '\',
						value 	= \'' . ( !$data['html'] ? $data['html_value'] : $data['value'] ) . '\',
						html 	= \'' . ( $data['html'] ? 0 : 1 ) . '\'
					WHERE id=\''.$id.'\'
						';
			$DB->Execute($sql);
		}
		

		private function _remove_lc($id) {			
			
			global $DB;
			
			$sql = 'DELETE FROM language_constants WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_lc($data) {			
			
			global $DB;
					
			$sql = 'INSERT INTO 
						language_constants 
					SET 
						name = \'' . $data['name'] . '\',
						value 	= \'' . ( !$data['html'] ? $data['html_value'] : $data['value'] ) . '\',
						html 	= \'' . ( $data['html'] ? 0 : 1 ) . '\',
						lang 	= \'' . $data['lang'] . '\'
						';			
			$DB->Execute($sql);
		}
		
		private function _chech_input($data, $update = false) {
			global $DB;
			
			$this->Errors['name'] = $data['name'] ? false : true;
			
			if(!$data['html'])
				$this->Errors['html_value'] = $data['html_value'] ? false : true;
			else
				$this->Errors['value'] = $data['value'] ? false : true;
				
			if($data['name'] && $_GET['Event'] != 'Edit'){
				
				$sql = 'SELECT *
						FROM language_constants
						WHERE 	name = \''.$data['name'].'\''. ( $data['lang'] ? ' AND lang = \''.$data['lang'].'\'' : '' );
				$res = $DB->GetRow($sql);
				
				if($res){
					$this->Errors['name'] = true;
				}	
			}
			
			if(in_array(true, $this->Errors)) {
				$this->DisplayError = true;
			}	
								
			return !$this->DisplayError;
		}
		
		private function _load_list($keyword) {			
			global $DB;
			
			$where = ' WHERE 1=1 ';

			if($keyword){
				$where .= ' AND value LIKE \'%'.$keyword.'%\' or name LIKE \'%'.$keyword.'%\'';
			}

			if($_GET['lang']){
				$where .= ' AND lang = \''.$_GET['lang'].'\'';
			}
			
			$sql = 'SELECT 
						COUNT(*) AS cnt 
					FROM
						language_constants
					'.$where;
			
			$res = $DB->GetRow($sql);
			$cnt = $res['cnt'];

			$items_per_page = $this->_get_system_variable('admin_items_per_page');
			$max_pages_cnt = $this->_get_system_variable('admin_max_pages_cnt');
			
			$page = intval($_GET['page']);
			if($page < 1){
				$page = 1;
			}

			AttachLib('PageNavigator');
			
			$this->PageNavigator = new PageNavigator(
										$cnt, 
										$items_per_page, 
										$max_pages_cnt, 
										$this->RootUrl.'language-constants/', 
										$page, 
										'?page=%s&keyword='.$_GET['keyword'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'].($_GET['lang'] ? "&lang=$_GET[lang]" : '')
									);
									
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}else{
				$order_by = 'id ASC';
			}
			
			$sql = 'SELECT * 
					FROM 
						language_constants
					'.$where.' 
					ORDER BY 
						'.$order_by.' 
					LIMIT ' . (($page - 1) * $items_per_page).', '.$items_per_page;
			
			$this->Records = $DB->GetAll($sql);
			
			$this->AttachComponent('GridAll', $this->Grid);	
					
			$titles = array(
				'Название',
				'Значение',
				'Язык'
			);
			
			$options['sortable'] = array(	0 => array(	'up' => $this->RootUrl.'language-constants/?sort=name&direct=ASC&keyword='.$_GET['keyword'].'&lang='.$_GET['lang'], 
														'down' => $this->RootUrl.'language-constants/?sort=name&direct=DESC&keyword='.$_GET['keyword'].'&lang='.$_GET['lang']),
											1 => array(	'up' => $this->RootUrl.'language-constants/?sort=value&direct=ASC&keyword='.$_GET['keyword'].'&lang='.$_GET['lang'], 
														'down' => $this->RootUrl.'language-constants/?sort=value&direct=DESC&keyword='.$_GET['keyword'].'&lang='.$_GET['lang'])
										);
		
			$options['row_numbers'] = true;
			
			if($_GET['page'])
				$options['page'] = $_GET['page'];
			else	
				$options['page'] = 1;			
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s&keyword='.$_GET['keyword'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'], 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s&keyword='.$_GET['keyword'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct']);
			}			
			$rows = array();
			
			if(!$this->Records)
				$this->Records =  array();
				
			foreach($this->Records as $record){
				$rows[$record['id']] = array(
					$record['name'],
					($record['value']),
					$record['lang']
				);
			}			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
		
	}

?>