<?
	class reviews extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'reviews/';
		
		protected $TableName				= 'reviews';
		protected $PublishField				= 'publish';
		
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $News						= array();
		protected $News_sz					= 0;
		protected $NewsInfo					= array();
		protected $Categories				= array();
		protected $Categories_sz			= 0;
	
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');			
			$this->_init_grid();
		}
		
		public function OnNew() {
			$this->_load_cataegories();
			
			
			
			
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			
			$this->NewsInfo['publish_ts'] = $this->NewsInfo['create_ts'] = time();
			$_POST['publish_ts'] = $_POST['create_ts'] = time();
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS('/core/lib/ckeditor/adapters/jquery.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			
			//$this->_load_articles_info(intval($_GET['id']));			
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$id = $this->_insert_articles($_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.$id);
				}
				else {
					
					$this->OnDefault();
				}
			}
		}
		
		public function OnRemove() {			
			global $DB;			
			$sql = 'DELETE FROM reviews 
					 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			$this->_load_cataegories();
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS('/core/lib/ckeditor/adapters/jquery.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			$this->_load_articles_info(intval($_GET['id']));	
			if(!$this->NewsInfo){
			
				$this->NewsInfo['publish_ts'] = $this->NewsInfo['create_ts'] = time();
			
			}
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_articles(intval($_GET['id']), $_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.intval($_GET['id']));
				}
				else {
					
					$this->OnDefault();
				}
			}
		}

		/*
		 * Private methods
		 */

		private function _load_articles_info($id) {
			global $DB;			
			$sql = 'SELECT * FROM reviews WHERE id=\''.$id.'\'';
			$this->NewsInfo = $DB->GetRow($sql);
		}

		private function _update_articles($id, $data) {
			global $DB;
			
			$create_ts = $data['create_ts'];			
			$create_ts = explode(" ",$create_ts);
			$create_ts[0] = explode(".",$create_ts[0]);
			$create_ts[1] = explode(":",$create_ts[1]);
			
			$ct = mktime($create_ts[1][0],$create_ts[1][1],0,$create_ts[0][1],$create_ts[0][0],$create_ts[0][2]);
			
			$publish_ts = $data['publish_ts'];			
			$publish_ts = explode(" ",$publish_ts);
			$publish_ts[0] = explode(".",$publish_ts[0]);
			$publish_ts[1] = explode(":",$publish_ts[1]);
			
			$pt = mktime($publish_ts[1][0],$publish_ts[1][1],0,$publish_ts[0][1],$publish_ts[0][0],$publish_ts[0][2]);

			$sql = 'UPDATE reviews
					SET reviews.title=\''.$data['title'].'\', 
						reviews.fulltext=\''.$data['fulltext'].'\', 
						reviews.introtext=\''.$data['introtext'].'\', 
						reviews.filename=\''.$data['filename'].'\', 
						reviews.experience=\''.$data['experience'].'\', 
						reviews.create_ts=\''.$ct.''.'\',
						reviews.publish_ts=\''.$pt.''.'\',
						reviews.category_id = \''.$data['category'].'\',
						reviews.instr = \''.$data['instr'].'\',
						reviews.publish=\''.$data['publish'].'\' 
					WHERE id=\''.$id.'\'';
			
			$DB->Execute($sql);
			
		}
		
		private function _insert_articles($data) {
			global $DB;
			
			$create_ts = $data['create_ts'];			
			$create_ts = explode(" ",$create_ts);
			$create_ts[0] = explode(".",$create_ts[0]);
			$create_ts[1] = explode(":",$create_ts[1]);
			
			$ct = mktime($create_ts[1][0],$create_ts[1][1],0,$create_ts[0][1],$create_ts[0][0],$create_ts[0][2]);
			
			$publish_ts = $data['publish_ts'];			
			$publish_ts = explode(" ",$publish_ts);
			$publish_ts[0] = explode(".",$publish_ts[0]);
			$publish_ts[1] = explode(":",$publish_ts[1]);
			
			$pt = mktime($publish_ts[1][0],$publish_ts[1][1],0,$publish_ts[0][1],$publish_ts[0][0],$publish_ts[0][2]);

			
			$sql = 'INSERT INTO reviews 
					SET reviews.title=\''.$data['title'].'\', 
						reviews.fulltext=\''.$data['fulltext'].'\', 
						reviews.introtext=\''.$data['introtext'].'\',
						reviews.filename=\''.$data['filename'].'\',						
						reviews.experience=\''.$data['experience'].'\',						
						reviews.create_ts=\''.$ct.'\',
						reviews.publish_ts=\''.$pt.'\',
						reviews.category_id = \''.$data['category'].'\',
						reviews.instr = \''.$data['instr'].'\',
						reviews.publish=\''.$data['publish'].'\'';
			$DB->Execute($sql);
			$sql = 'SELECT LAST_INSERT_ID() AS id FROM reviews';
			$res = $DB->GetRow($sql);
			return $res['id'];
		}
				
		private function _check_data($data, $update = false) {
			$this->Errors['title'] = $data['title'] ? false : true;
			//$this->Errors['introtext'] = $data['introtext'] ? false : true;
			$this->Errors['fulltext'] = $data['fulltext'] ? false : true;
			//$this->Errors['create_ts'] = $data['create_ts'] ? false : true;
			//$this->Errors['publish_ts'] = $data['publish_ts'] ? false : true;
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		private function _load_articles() {
			global $DB;
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM reviews';
			$res = $DB->GetRow($sql);
			$items_per_page = intval($this->_get_system_variable('admin_items_per_page'));
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){				
				$page = 1;
			}
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'reviews/', $page, '?page=%s', '', true, true);
			$sql = 'SELECT * 
					FROM reviews 
					WHERE 1 = 1
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			//_debug($sql,1);
			$this->News = $DB->GetAll($sql);
			$this->News_sz = count($this->News);
		}
		
		private function _init_grid($cat_id = 0) {
			$this->_load_articles();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('Заголовок', 'Опубликовано');
			$options['row_numbers'] = false;
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}

			$options['multiply'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать', 'UnPublished'=> 'Скрыть', 'DeleteSelected'=> 'Удалить');
	
			$rows = array();
			for($i = 0; $i < $this->News_sz; $i++){
				
				$active = $this->News[$i]['publish'] ? '<div id="container'.$this->News[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->News[$i]['id'].', 0, \'reviews\', \'publish\')">Да</span></div>' : '<div id="container'.$this->News[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->News[$i]['id'].', 1, \'reviews\', \'publish\')">Нет</span></div>';
				
				$row = array($this->News[$i]['title'], 
							 //date("d.m.y H:i",$this->News[$i]['publish_ts']),
							 //date("d.m.y H:i",$this->News[$i]['create_ts']),
							 '<div style="text-align: center">'.$active.'</div>'
							);
				$rows[$this->News[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	private function _load_cataegories($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT id, 
						   title 
					FROM categories 
					WHERE parent_id=\''.$parent_id.'\'';
			$cat = GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['display_name'] = str_repeat('&nbsp;', $level*4).$cat[$i]['title'];
				$cat[$i]['level'] = $level;
				array_push($this->Categories, $cat[$i]);
				$level++;
				$this->_load_cataegories($cat[$i]['id']);
				$level--;
			}
			
			if($parent_id == 0) {
				
				$this->Categories_sz = count($this->Categories);
			}
		}
		

		

	}
?>