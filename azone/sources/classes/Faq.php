<?

	class Faq extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'faq/';
		
		protected $TableName				= 'faq';
		protected $PublishField				= 'publish';
		
		protected $CatList					= null;
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		public $CategoryInfo				= array();
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			$this->_load_cat_list();
		}
		
		public function OnNew() {
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_category($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_tree(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
			
			$sql = 'SELECT * FROM faq WHERE id=\''.intval($_GET['id']).'\'';
			$this->CategoryInfo = $DB->GetRow($sql);
		}
		
		public function OnUpdate() {
			
			global $DB;
			
			if(!$this->_chech_input($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_category(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		private function _generate_url($url){
		
			if($url == '/index.html' || $url == 'index.html' || $url == '/'){
				$url = '/';
			}else{
				if($url[0] == '/') $url = substr($url,1,strlen($url));
				if($url[strlen($url)-1] == '/'){ $url = substr($url,0,strlen($url)-1);}
				$url = '/'.$url.'/';
			}
			
			
			return $url;
		
		}
		
		private function _update_category($id, $data){
		
			global $DB;
			
			$sql = 'UPDATE faq  
					SET parent_id=\''.($data['parent_id'] ? $data['parent_id'] : 0).'\', 
						question=\''.$data['title'].'\', 
						order_by=\''.$data['order_by'].'\', 
						typeq=\''.$data['typeq'].'\', 
						publish = \''.($data['publish'] ? 1 : 0).'\',
						url=\''.$data['url'].'\',
						answer=\''.$data['answer'].'\' 
						WHERE id = \''.$id.'\'';
			
			$DB->Execute($sql);
		}
		

		private function _remove_tree($id) {
			
			global $DB;
			
			$sql = 'SELECT id FROM faq WHERE parent_id=\''.$id.'\'';
			$res = $DB->GetAll($sql);
			$res_sz = count($res);
			
			for($i = 0; $i < $res_sz; $i++) {
				
				$this->_remove_tree($res[$i]['id']);
			}
			
			$sql = 'DELETE FROM faq WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_category($data) {
			
			global $DB;
			$sql = "SELECT MAX(order_by) AS m_order_by 
					FROM faq WHERE parent_id = '".intval($data['parent_id'])."'";
			
			$res = $DB->GetOne($sql);
			$res++;
			
			$sql = 'INSERT INTO faq  
					SET parent_id=\''.($data['parent_id'] ? $data['parent_id'] : 0).'\', 
						question=\''.$data['title'].'\', 
						order_by=\''.$res.'\', 
						typeq=\''.$data['typeq'].'\', 

						publish = \''.($data['publish'] ? 1 : 0).'\',
						url=\''.$data['url'].'\',
						answer=\''.$data['answer'].'\'';

			$DB->Execute($sql);
		}
		
		private function _chech_input($data, $update = false) {
			
			global $DB;
			
			if($update) {
				
				$limit = 1;
			}
			else {
				
				$limit = 0;
			}
			
			$this->Errors['title'] = $data['title'] ? false : true;
			
			$this->Errors['url'] = $data['url'] ? false : true;
			
			
			if(!$this->Errors['url']) {
				
				$sql = 'SELECT COUNT(*) AS cnt FROM faq WHERE url=\''.$data['url'].'\' AND id !=\''.(int)$_GET['id'].'\'';
				
				if($_POST['type'] == 'question'){
					
					$sql.= ' AND parent_id=\''.$_POST['parent_id'].'\'';
				}else{
					$sql.= ' AND parent_id=\'0\'';
				}
				
				$res = $DB->GetRow($sql);
				
				if($res['cnt'] > $limit) {
					
					$this->Errors['url'] = true;
				}
				
			}
			
			if(!$this->Errors['url']) {				
				$sql = "SELECT url FROM az_disabled_urls WHERE url LIKE'".$url."'";
				$row = $DB->GetOne($sql);				
				if($row) {					
					$this->Errors['url'] = true;
				}				
			}
			
			if($_POST['type'] == 'question'){
				
				$this->Errors['answer'] = $data['answer'] ? false : true;
			
				$this->Errors['parent_id'] = $data['parent_id'] ? false : true;
			}
			
			if(in_array(true, $this->Errors)) {
				
				$this->DisplayError = true;
			}
			
			return !$this->DisplayError;
		}
		
		private function _load_cat_list() {
			
			$list = $this->_get_cat_list();
			$list_sz = count($list);
			
			$this->AttachComponent('GridAll', $this->CatList);
			$options['sort'] = true;
			$titles = array('Категория', 'Вопрос','Опубликован');
			$options['row_numbers'] = false;
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$options['multiply'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать', 'UnPublished'=> 'Скрыть', 'DeleteSelected'=> 'Удалить');
	
			$rows = array();
			for($i = 0; $i < $list_sz; $i++){
				$public = $list[$i]['publish'] ? 
								'<a href="/azone/faq/?Event=PublicPage&id='.$list[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/faq/?Event=PublicPage&id='.$list[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';			
				
				$row = array(( !$list[$i]['parent_id'] ? $list[$i]['question'] : ''), 
							 ( $list[$i]['parent_id'] ? $list[$i]['question'] : ''),
							 $public
							 );
				$rows[$list[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->CatList->SetData($data);
		}
		
		private function _get_cat_list($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			static $cat_list = array();

			$sql = 'SELECT * FROM faq WHERE parent_id=\''.$parent_id.'\' ORDER BY order_by ASC';
			
			$categories = $DB->GetAll($sql);
			$categories_sz = count($categories);
			
			for($i = 0; $i < $categories_sz; $i++) {

				$categories[$i]['level'] = $level;
				//$categories[$i]['display_title'] = str_repeat('&nbsp;', $level * 4).$categories[$i]['question'];
				
				array_push($cat_list, $categories[$i]);
				
				$level++;
				$this->_get_cat_list($categories[$i]['id']);
				$level--;
			}
			
			return $cat_list;
		}
	
		/* Extend */
		public function OnPublicPage(){
			global $DB;
			$sql = "UPDATE pages SET publish = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		public function OnSortUp() {
			global $DB;
			$sql='SELECT *
				  FROM faq
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM faq
					WHERE faq.order_by < \''.$model['order_by'].'\' AND faq.parent_id = \''.$model['parent_id'].'\'
					ORDER BY faq.order_by DESC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			
			if($model_2){
				$sql='UPDATE faq
					  SET faq.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE faq
				  	SET faq.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			
			}
			$this->OnDefault();
		}

		public function OnSortDown() {
			global $DB;
			$sql='SELECT *
				  FROM faq
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM faq
					WHERE faq.order_by > \''.$model['order_by'].'\'
					ORDER BY faq.order_by ASC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			if($model_2){
				$sql='UPDATE faq
					  SET faq.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE faq
				  	SET faq.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			}
			$this->OnDefault();
		}

	}

?>