<?

	class Urls extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'urls/';
		protected $CatList					= null;
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		public $CategoryInfo				= array();
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			$this->_load_cat_list();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_category($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_tree(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
			
			$sql = 'SELECT * FROM categories WHERE id=\''.intval($_GET['id']).'\'';
			$this->CategoryInfo = $DB->GetRow($sql);
		}
		
		public function OnUpdate() {
			
			global $DB;
			
			if(!$this->_chech_input($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_category(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		private function _update_category($id, $data){
		
			global $DB;
			
			$sql = 'UPDATE az_disabled_urls  
					SET url=\''.$data['url'].'\'						
						WHERE id = \''.$id.'\'
						';
			
			$DB->Execute($sql);
		}
		

		private function _remove_tree($id) {			
			global $DB;
			$sql = 'DELETE FROM az_disabled_urls WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_category($data) {			
			global $DB;			
			$sql = 'INSERT INTO az_disabled_urls  
					SET url=\''.$data['url'].'\'';			
			$DB->Execute($sql);
		}
		
		private function _chech_input($data, $update = false) {
			
			global $DB;
			$sql = 'SELECT COUNT(*) AS cnt FROM az_disabled_urls WHERE url=\''.$data['url'].'\'';
			$res = $DB->GetRow($sql);			
			if($res['cnt']) {			
				$this->Errors['url'] = true;
			}			
			if(in_array(true, $this->Errors)) {				
				$this->DisplayError = true;
			}			
			return !$this->DisplayError;
		}
		
		private function _load_cat_list() {			
			$list = $this->_get_cat_list();
			$list_sz = count($list);			
			$this->AttachComponent('Grid', $this->CatList);			
			$titles = array('Url');
			$options['row_numbers'] = true;			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}			
			$rows = array();
			for($i = 0; $i < $list_sz; $i++){
				
				$row = array($list[$i]['url']);
				$rows[$list[$i]['id']] = $row;
			}			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->CatList->SetData($data);
		}
		
		private function _get_cat_list($parent_id = 0) {
			global $DB;
			$sql = 'SELECT * FROM az_disabled_urls ORDER BY url ASC';
			$categories = $DB->GetAll($sql);
			return $categories;
		}
	}

?>