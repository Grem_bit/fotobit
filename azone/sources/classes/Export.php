<?
	class Export extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'export/';
		protected $BList					= null;
		
		protected $UrlParts					= array();
		protected $PageNavigator			= null;
		
		protected $excel_configurator 		= '';
		protected $file_excel_name 			= '';
		protected $UpdateTime 				= '';
		protected $UpdatePrd				= array();
		protected $UpdatePrd_sz				= 0;
		protected $Line						= 1;
		protected $ProductUrl				= array();
		
		protected $Log						= array();
		
		/*
		 * Public methods
		 */
		
	public function OnDefault() {
	
		$this->SetTemplate('main.html');
	}
		
	public function OnExport() {
	
		global $config;

		$this->FileName = $config['absolute-path'].'temp/catalog.xls';

		if($f = fopen($this->FileName, "w")){
			$this->_export($f);
		}
		
		$this->SetTemplate('main.html');
	}
	
	/*
	 * Private methods
	 */

	protected function _export( &$f ){
		
		$temp = GetAll("SHOW FULL COLUMNS FROM `products` ");
			//	_debug($temp);			
				if(is_for($temp)){
					foreach ($temp as $val){
						if($val['Comment'])
							$fields[$val['Comment']] = $val['Field'];  	
					}
				}
			//	_debug($fields);
				$str = '<table><tr><td>артикул</td><td>цена</td><td>категория</td><td>производитель</td><td>коллекция</td>';
				if(is_for($fields)){
					foreach ($fields as $key => $fl){
						if($key != 'описание' && $key != 'артикул' && $key != 'цена' && $key != 'купить'){
							$str .= '<td>'.$key.'</td>';
						}
					}
				}
				$str .= '<td>наличие</td><td>системный столбец</td><td>URL</td><td>купить</td></tr>';
				_debug($str,1);
		$products =	GetAll('SELECT p.* , c.title as category, b.title as maker, bb.title as collection, ph.`filename`  
							FROM products p
								LEFT JOIN categories c ON (c.id = p.category_id)
								LEFT JOIN pages b ON (b.id = p.maker_id)
								LEFT JOIN pages bb ON (bb.id = p.сollection_id)
								LEFT JOIN photos ph ON (p.id = ph.product_id AND ph.default = 1)');
		//_log($products);
		//_log(sss);
		if(is_array($products)){
			//$str = iconv("UTF-8", "CP1251", $str);
			fputs($f,$str);
			//fputs($f,mb_convert_encoding($str,"CP-1251", "UTF-8"));
			fputs($f,"\n");
			
			foreach ( $products as $p ){
				$this->ProductUrl = array();
				$this->getProdCategoryUrl($p['category_id']);
				$href = 'http://'.$_SERVER['HTTP_HOST'].'/catalog/'.implode('/',$this->ProductUrl).'/'.$p['url'].'.html';
				
				//_debug($href,1);				
				$s = '';		
					foreach ($fields as $key => $fl){
						if($key != 'описание' && $key != 'артикул' && $key != 'цена' && $key != 'купить'){
							$s .= '."<td>".$p["'.$fl.'"]."</td>"';
						}
					}  
				//_debug($s,1);
				$prise = ' fputs($f,	"<tr><td>".$p["code"]."</td>"
							."<td>".$p["price"]."</td>"
							."<td>".$p["category"]."</td>"
							."<td>".$p["maker"]."</td>"
							."<td>".$p["collection"]."</td>"
							'
							.$s.'
							."<td>".(($p["publish"]) ? "+" : "" )."</td>"
							."<td>".$p["filename"]."</td>"
							."<td>".$href."</td>"
							."<td>".$p["price_old"]."</td></tr>"
							);';
				//_log($prise,'a');
				eval($prise);
				fputs($f,"\n");
			}
			fputs($f,"</table>");	
		}
	}
	
	public function getProdCategoryUrl($id,$ext = 0){
			global $DB;
			
			$sql = "SELECT c.url, c.parent_id, c.title FROM categories c WHERE c.id = '".$id."'";
			//_debug($sql);
			$res = $DB->GetRow($sql);
			if($ext){
				array_unshift($this->ProductUrl,$res);
			}else{
				array_unshift($this->ProductUrl,$res['url']);
			}
			if($res['parent_id'] != 0){
				$this->getProdCategoryUrl($res['parent_id'],$ext);				
			}else{
				return;
			}
			return;
		}
			
}
?>
