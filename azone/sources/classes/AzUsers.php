<?

	class AzUsers extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'az-users/';
		protected $Users					= array();
		protected $Users_sz					= 0;
		protected $UsersList				= null;
		protected $UserInfo					= array();
		protected $PermTable				= null;
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			
			$this->AddJS($this->RootUrl.'public/js/az-users.js');
		}
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			$this->_load_users_list();
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$sql = 'DELETE FROM az_users WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);
			
			$this->OnDefault();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
			$this->_create_perm_table();
		}
		
		public function OnInsert() {

			$this->UserInfo = $_POST;
			
			if(!$this->_check_data($this->UserInfo)) {
				
				$this->OnNew();
			}
			else {
				
				$this->_create_user($this->UserInfo);
				$this->OnDefault();
			}
		}
		
		public function OnEdit() {
			
			$this->_load_user_info(intval($_GET['id']));
			$this->SetTemplate('edit.html');
			$this->_create_perm_table();
		}
		
		public function OnUpdate() {
			
			$this->UserInfo = $_POST;
			$this->UserInfo['id'] = intval($_GET['id']);
			
			if(!$this->_check_update_data($this->UserInfo)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_user($this->UserInfo);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		
		private function _save_perm($user_id, $data) {
			
			global $DB;
			
			$perm = serialize(array('show'=>$data['show'], 'edit'=>$data['edit'], 'view'=>$data['view']));
			
			$sql = 'UPDATE az_users 
					SET perm=\''.$perm.'\' 
					WHERE id=\''.$user_id.'\'';
			$DB->Execute($sql);
			
			$auth_data = $this->GetConfigParam('azone_auth');
			$_SESSION[$auth_data['session']]['perm'] = $perm;
		}
		
		private function _create_perm_table() {
			
			global $DB;
			
			$sql = 'SELECT * FROM az_modules WHERE `show` = 1';
			$modules = $DB->GetAll($sql);
			$modules_sz = count($modules);
			
			$this->AttachComponent('GridAll', $this->PermTable);
			
			$perm = unserialize($this->UserInfo['perm']);
			
			$titles = array('Модуль', 'Вывести на центральную', 'Изменение', 'Просмотр');
			$options = array('row_numbers' => true);
			
			$rows = array();
			
			for($i = 0; $i < $modules_sz; $i++){
				
				$show = $perm['show'][$modules[$i]['id']];
				$edit = $perm['edit'][$modules[$i]['id']];
				$view = $perm['view'][$modules[$i]['id']];
				
				$row = array($modules[$i]['title'], 
							 '<div style="text-align: center"><input type="checkbox" '.($show ? 'checked="checked"' : '').' name="show['.$modules[$i]['id'].']" value="1" class="perm-ch-fp" title="'.$modules[$i]['id'].'" id="fp'.$modules[$i]['id'].'" /></div>', 
							 '<div style="text-align: center"><input type="checkbox" '.($edit ? 'checked="checked"' : '').' name="edit['.$modules[$i]['id'].']" value="1" class="perm-ch-edit" id="edit'.$modules[$i]['id'].'" title="'.$modules[$i]['id'].'" /></div>', 
							 '<div style="text-align: center"><input type="checkbox" '.($view ? 'checked="checked"' : '').' name="view['.$modules[$i]['id'].']" value="1" class="perm-ch-view" id="view'.$modules[$i]['id'].'" title="'.$modules[$i]['id'].'" /></div>');
				array_push($rows, $row);
			}
							
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			
			$this->PermTable->SetData($data);
		}
		
		private function _update_user($data) {
			
			global $DB;
			
			$sql = 'UPDATE az_users 
					SET username=\''.$data['username'].'\',';
			
			if($data['password']) {
				
				$sql .= 'password=\''.$this->CryptPassword($data['password']).'\', ';
			}
			
			$sql .= 'firstname=\''.$data['firstname'].'\', 
					 lastname=\''.$data['lastname'].'\', 
					 occupation=\''.$data['occupation'].'\', 
					 email=\''.$data['email'].'\' 
				WHERE id=\''.$data['id'].'\'';
			$DB->Execute($sql);
			
			$this->_save_perm($data['id'], $data);
		}
		
		private function _load_user_info($id) {
			
			global $DB;
			
			$sql = 'SELECT * FROM az_users WHERE id=\''.$id.'\' LIMIT 1';
			$this->UserInfo = $DB->GetRow($sql);
		}
		
		private function _create_user($data) {
			
			global $DB;
			
			$sql = 'INSERT INTO az_users 
					SET username=\''.$data['username'].'\', 
						password=\''.$this->CryptPassword($data['password']).'\', 
						firstname=\''.$data['firstname'].'\', 
						lastname=\''.$data['lastname'].'\', 
						occupation=\''.$data['occupation'].'\', 
						email=\''.$data['email'].'\'';
			$DB->Execute($sql);
			
			$sql = 'SELECT LAST_INSERT_ID() AS id FROM az_users';
			$res = $DB->GetRow($sql);
			
			$this->_save_perm($res['id'], $data);
		}
		
		private function _check_update_data($data) {
			
			global $DB;
			
			$sql = 'SELECT username FROM az_users WHERE id=\''.$data['id'].'\'';
			$res = $DB->GetRow($sql);
			
			if(($this->_user_exists($data['username']) && $data['username'] != $res['username']) || !$data['username']) {
				
				$this->Errors['username'] = true;
			}
			
			if($data['password'] != $data['confirm']) {
				
				$this->Errors['password'] = true;
				$this->Errors['confirm'] = true;
			}
			
			$this->Errors['firstname'] = $data['firstname'] ? false : true;
			$this->Errors['lastname'] = $data['lastname'] ? false : true;
			$this->Errors['occupation'] = $data['occupation'] ? false : true;
			$this->Errors['email'] = !$this->ValidEmail($data['email']);
			
			$this->DisplayError = in_array(true, $this->Errors);
			
			return !$this->DisplayError;
		}
		
		private function _check_data($data) {
			
			if($this->_user_exists($data['username']) || !$data['username']) {
				
				$this->Errors['username'] = true;
			}
			
			if($data['password'] != $data['confirm'] || !$data['password']) {
				
				$this->Errors['password'] = true;
				$this->Errors['confirm'] = true;
			}
			
			$this->Errors['firstname'] = $data['firstname'] ? false : true;
			$this->Errors['lastname'] = $data['lastname'] ? false : true;
			$this->Errors['occupation'] = $data['occupation'] ? false : true;
			$this->Errors['email'] = !$this->ValidEmail($data['email']);
			
			$this->DisplayError = in_array(true, $this->Errors);
			
			return !$this->DisplayError;
		}
		
		private function _load_users() {
			
			global $DB;
			
			$sql = 'SELECT * FROM az_users';
			$this->Users = $DB->GetAll($sql);
			$this->Users_sz = count($this->Users);
		}
		
		private function _load_users_list() {
			
			$this->_load_users();
			$this->AttachComponent('Grid', $this->UsersList);
			
			$titles = array('Логин', 'Имя', 'Фамилия', 'Должность', 'E-mail');
			$options['row_numbers'] = true;
			
			if($this->Perm['edit']) {
				
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $this->Users_sz; $i++){
				
				$row = array($this->Users[$i]['username'], 
							 $this->Users[$i]['firstname'], 
							 $this->Users[$i]['lastname'], 
							 $this->Users[$i]['occupation'], 
							 $this->Users[$i]['email']);
				$rows[$this->Users[$i]['id']] = $row;
			}
							
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			
			$this->UsersList->SetData($data);
		}
	}

?>