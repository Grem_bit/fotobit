	<?
	class Orders extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'orders/';
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $Orders					= array();
		protected $Orders_sz				= 0;
		protected $OrderInfo				= array();
		public $Option_size					= array(1=>1,2=>6,3=>24,4=>25,5=>26,6=>27,7=>28,8=>29);
		public $Currencies					= array();
		public $MainCurrency					= array();
		
		
		/*
		 * Public methods
		 */
		public function OnCreate(){
			$cur_arr = $this->_load_content('course','','`default` DESC');
			foreach($cur_arr as $cur){
				$this->Currencies[$cur['id']] = $cur;
				if(intval($cur['corse'] == 1)){
					$this->MainCurrency = $cur;
				}
			}
		}


		public function OnDefault() {
			
			//$this->AddJS('/azone/public/js/refresh.js');
			$this->AddJS('/azone/public/js/calendar.js');
			$this->AddCSS('/azone/public/css/calendar.css');
			$this->SetTemplate('main.html');
			$this->_init_grid();
		}
		public function deleteDirectory($dir) {
			if (!file_exists($dir)) return true;
			if (!is_dir($dir) || is_link($dir)) return unlink($dir);
				foreach (scandir($dir) as $item) {
					if ($item == '.' || $item == '..') continue;
					if (!$this->deleteDirectory($dir . "/" . $item)) {
						chmod($dir . "/" . $item, 0777);
						if (!$this->deleteDirectory($dir . "/" . $item)) return false;
					};
			}
			return rmdir($dir);
		} 
		public function OnRemove() {
			global $config, $DB;
			$id = intval($_GET['id']);
			
			$dirPath = $config['absolute-path'].PATH_PUBLIC_FILES_IMAGES.'orders/'.$id;
			
			if (is_dir($dirPath)) {
				
				$this->deleteDirectory($dirPath);
			}
			$sql = 'SELECT  
						   o.*
					FROM  
						 orders o 
					WHERE  
						  o.id=\''.$id.'\'';
			$this->OrderInfo = $DB->GetRow($sql);
			
			$items = unserialize($this->OrderInfo['cart']);
			foreach($items as $k=>$item){
				
				$sql = 'DELETE FROM products_variants WHERE product_id=\''.$k.'\'';
				Execute($sql);
				$sql = 'DELETE FROM products WHERE id=\''.$k.'\'';
				Execute($sql);
			}
			$sql = 'DELETE FROM orders WHERE id=\''.$id.'\'';
			Execute($sql);
			$this->OnDefault();
		}
		
		public function OnEdit() {
			//ini_set('display_errors',1); error_reporting(E_ALL);
			//ini_set('display_errors',0); error_reporting(NULL);	
			$this->SetTemplate('edit.html');
			$this->_load_order_info(intval($_GET['id']));
		}
		
		public function OnUpdate() {
			
			global $DB;
			
			$id = intval($_GET['id']);
			$this->OrderInfo = $order = $this->_load_content_info('orders o','','o.id = '.$id);
			$changed = intval($order['changed']);
			$cart = array();
			$qty = $total = $discount = 0;
			if($_POST['redirect']) {
				header('Location: /azone/orders/?Event=AddToOrder&id='.$_GET['id']);
			}
			
			if(is_array($_POST['amount'])){
				
				$user = $this->_load_content_info('users','','id = '.$order['buyer_id']);
				$cart = unserialize($order['cart']);
				foreach($cart as &$product){
					$count = i($_POST['amount'][$product['id']]);
					if($count){
						if($product['count'] != $count) $changed = 1;
						$product['count'] = $count;
					}
					$qty += $product['count'];
				}
				unset($product);
			}
			
			$sql = 'UPDATE orders 
					SET 
						state=\''.$_POST['state'].'\',
						state_issuing=\''.$_POST['state_issuing'].'\',
						changed = 0 
						'.($cart ? ', cart = \''.serialize($cart).'\'':'').'
						'.($qty ? ', qty = \''.$qty.'\'':'').'
					WHERE id=\''.$id.'\'';
				
			$DB->Execute($sql);
			
			AttachLib('Mailer');	//Подключение класса для мыла
			require_once PATH_LIB."/SMSSender/smssender.php"; // Подключения класса для смс
			
			// Проверка и добавление в дисконт
			if( $this->OrderInfo['check_discount']==0 && ($_POST['state']=='printed' || $_POST['state_issuing']=='took' || $_POST['state_issuing']=='sentnpost' || $_POST['state_issuing']=='sent_courier' )){ 
				
				$sql=' SELECT photo_cnt FROM `users` WHERE id=\''.$this->OrderInfo['buyer_id'].'\'';
				$photo_cnt= $DB->GetOne($sql);
				$new_cnt=$photo_cnt+$this->OrderInfo['qty'];
				$sql=' UPDATE `users` SET
						photo_cnt=\''.$new_cnt.'\' WHERE id='.$this->OrderInfo['buyer_id'];
				$DB->Execute($sql);
				$sql=' UPDATE `orders` SET
						check_discount=1 WHERE id='.$this->OrderInfo['id'];
				
				$DB->Execute($sql);
			
				// установка второго прайса
				$sql='UPDATE `users` SET pricenamber=2 WHERE photo_cnt>='.$this->_get_system_variable('cnt_dicont').' AND id='.$this->OrderInfo['buyer_id'];	
				$DB->Execute($sql);
			}
			// конец Проверка и добавление в дисконт
			if($_POST['state']!=$this->OrderInfo['state'] ){
				$state_new = $this->_order_state($_POST['state']);
				$state_old = $this->_order_state($this->OrderInfo['state']);
				
				$mail3 = new Mailer();
				$mail3->AddRecepient($this->OrderInfo['email']);
				$mail3->AddVars(array(
								'order_id'		=> $order['id'],
								'ts'		=> $order['ts'],
								'state'		=> $state_new,
								'state_old'		=> $state_old,
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail3->Send(	'order_state_cheng.html',
								'Статус заказа  №'.$order['id'].' изменен',
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
			// Для отправки SMS				
				//if(true){
				if(i($this->OrderInfo['total'])>i($this->_get_system_variable('min_sms')) AND $this->OrderInfo['phone'] ){
					
					//require_once PATH_LIB."/SMSSender/smssender.php";
					$sms2 = new SMSSender();
					$result = mysql_query("SELECT * FROM `language_constants` where id in (257, 259)");
					$row =mysql_fetch_array($result);
					
					//TODO построение текста смс; //Статус заказа изменен с $state_old на $state. 
					$text = $row['value'].$order['id'].". ";
					$row =mysql_fetch_array($result);
					//$text .= $row['value'] .'"'. $this->_order_state($_POST['state']).'"';
					$text .= 'Статус изменен с '.$state_old.' на '.$state_new.'.';
					
					$sms2->registerSender('Foto-Servis','ua');
					$ph=$this->OrderInfo['phone'];

					$phd= str_replace('+38','',$ph);
					$phd=substr($phd,0,10);
					
					$phone =  "38".$phd;
					
					//if ($this->OrderInfo['buyer_id']!=7) $sms2->sendSMS($phone,$text);
				}
			
			}
			// Отправка оповещения об изменении статуса
			if( ($_POST['state_issuing']!=$this->OrderInfo['state_issuing']) &&($_POST['state_issuing']=='waiting' || $_POST['state_issuing']=='sent_courier' || $_POST['state_issuing']=='sentnpost') ){
				$tr=true;
			}else{
				$tr=false;
			}
			
			if( $tr && $order ){
				//ini_set('display_errors',1); error_reporting(E_ALL);
				
				//_debug($this->OrderInfo['email'],1);
				$state_new = $this->_order_state_issuing_mail($_POST['state_issuing']);
				//$state_old = $this->_order_state_issuing_mail($this->OrderInfo['state_issuing']);
				
				$mail4 = new Mailer();
				$mail4->AddRecepient($this->OrderInfo['email']);
				$mail4->AddVars(array(
								'order_id'		=> $order['id'],
								'state'		=> $state_new,
								'ts'		=> $order['ts'],
								
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail4->Send(	'order_state_is_cheng.html',
								'Статус заказа  №'.$order['id'].'изменен',
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
				// Для отправки SMS				
				
				//if($this->OrderInfo['buyer_id']==7){
				//if(true){
				
				if(i($this->OrderInfo['total'])>i($this->_get_system_variable('min_sms')) AND $this->OrderInfo['phone'] ){
					
					//require_once PATH_LIB."/SMSSender/smssender.php";
					
					$sms = new SMSSender();
					
					$result = mysql_query("SELECT * FROM `language_constants` where id in (257, 259)");
					$row =mysql_fetch_array($result);
					
					//TODO построение текста смс;
					$text = $row['value'].''.$order['id'].'.';
					$row =mysql_fetch_array($result);
					//$text .= $row['value'] .'"'. $this->_order_state($_POST['state']).'"';
					$text .= $state_new;
					
					$sms->registerSender('Foto-Servis','ua');
					$ph=$this->OrderInfo['phone'];
					
			
					$phd= str_replace('+38','',$ph);
					$phd=substr($phd,0,10);
					
					$phone =  "38".$phd;
					
					if ($this->OrderInfo['buyer_id']!=7) $sms->sendSMS($phone,$text);
				}
				//
			
			}
			//
			
			if($changed && $order){
				//AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				$user_data = '<p>'.
				'Имя пользователя:'.$order['name'].'<br />'.
				'Email: '.$order['email'].'<br />'.
				'Телефон: '.$order['phone'].'<br />'.
				'Адрес: '.$order['address'].'<br />'.
				'</p>';
				$cart_table = $this->generate_order_table(unserialize($order['cart']));
				$mail->AddVars(array(
									'order_id'		=> $order['id'],
									'user_data'		=> $user_data,
									'price_table' => $cart_table,
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'order.html',
								'Заказ отредактирован №'.$order['id'].' '.$username,//$this->_get_system_variables('order_email_subject')
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
						
				$mail2 = new Mailer();
				$mail2->AddRecepient($_POST['email']);
				$mail2->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' => $cart_table,
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail2->Send(	'order_consumer.html',
								'Ваш заказ отредактирован №'.$order['id'],
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
			}
			
			$this->GoToUrl("/azone/orders/");
		}
		
		public function OnAddToOrder() {
			
			$this->AddJS('/azone/public/js/search.js');
			
			$items_per_page = $this->_get_system_variable('search_items_per_page');
			$query = $_GET;
			unset($query['page']);
			$query = http_build_query($query);
			$order_by = ' t.code';
			$keyword = $_POST['code'];
			$this->Products = $this->_load_content_with_pn('products', 
								't.publish = 1 AND (t.code LIKE \''.$keyword.'%\' OR t.title LIKE \''.$keyword.'%\')',
								mysql_real_escape_string($order_by),
								'',
								'',
								 $items_per_page,9,$query
			);
			$this->AttachComponent('Grid', $this->Grid);
			
			$titles = array('Код','Название', 'Количество','Цена', 'Добавить');
			$options['row_numbers'] = true;
			
			$rows = array();
			$i = 0;
			
			foreach($this->Products['items'] as $item){			
				$row = array($item['code'],
							 $item['title'], 
							 '<input type="text" class="cartamount" name="'.$_GET['id'].'_'.$item['id'].'" value="1">', 
							 $item['price'].' y.e.',
							 '<a href="#" class="totheorder" rel="'.$_GET['id'].'_'.$item['id'].'"><img src="/azone/public/images/small-icons/add.png"/></a>'
							 );
				$rows[$i] = $row;
				$i++;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
			
			$this->SetTemplate('addorder.php');
			
		}
		
		public function OnAddProd(){
			$discount = 0;
			$count = intval($_POST['count']);
			$data = explode('_',$_POST['data']);
			$prod_id = i($data[1]);
			if(!$count) $count = 1;
			$product = $this->_load_content_info('products t',
												'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1',
												't.id = '.$prod_id,
												't.id, t.category_id, t.title, t.price, t.discount, t.currency_id, t.code, t.`new`, IF(t.url != \'\',t.url,t.id) as url, p.filename');
			if($product){
				$order = $this->_load_content_info('orders o','','o.id = '.i($data[0]));
				if($order && $order['state'] == 'new'){
					$cart = unserialize($order['cart']);
					if($cart[$product['id']]){
						$cart[$product['id']]['count'] += $count;
					}else{
						$user = $this->_load_content_info('users','','id = '.$order['buyer_id']);
						$product['discount'] = $this->calculate_discount($product,$user['discount']);
						$cart[$product['id']] = $product;
						$cart[$product['id']]['count'] = $count;
					}
					$sql = "UPDATE orders 
							SET 
								ts = '".time()."',
								qty = qty + ".i($count).",
								cart = '".serialize($cart)."',
								changed = 1 
							WHERE id = ".$order['id']."
					";
					Execute($sql);
				}
			}
			exit;
		}
		
		public function OnDelProd(){
			$discount = 0;
			$prod_id = i($_GET['prod_id']);
			$order = $this->_load_content_info('orders o', '', 'o.id = '.i($_GET['id']));
			if($order && $order['state'] == 'new'){
				$cart = unserialize($order['cart']);
				$qty = 0;
				
				foreach($cart as $id=>&$product){
					if($id == $prod_id){
						unset($cart[$id]);
					}else{
						$qty += $product['count'];
					}
				}
				unset($product);
				$sql = "UPDATE orders 
						SET 
							ts = '".time()."',
							qty = ".$qty.",
							cart = '".serialize($cart)."',
							changed = 1 
						WHERE id = ".$order['id']."
				";
				Execute($sql);
			}
			header('Location: /azone/orders/?Event=Edit&id='.$_GET['id']);
		}
		
		public function OnPrint(){
			$this->OrderInfo = $this->_load_content_info('orders o', '', 'o.id = '.i($_GET['id']));
			$this->Currency = $this->OrderInfo['currency_id'];
			$this->OrderInfo['date'] = date('d.m.Y H:i', $this->OrderInfo['ts']);
			$this->OrderInfo['total'] = number_format($this->OrderInfo['total'], 2, '.', ' ');
			$this->OrderInfo['state_label'] = $this->_order_state($this->OrderInfo['state']);
				$tf=false;
				$string = $this->_get_system_variable('delivery_methods');
				$delivery_methods = explodeMultiString($string);
				//_debug($delivery_methods,1);
				if($delivery_methods[$this->OrderInfo['delivery_way']]['options'][0]== 'newpost'){
					$deliv_option=$order['delivery_option'];
					if($this->OrderInfo['total']<200){ 
						
						$tf=true;
					}
				}
			//_debug($tf,1);
			$this->Cart = $this->generate_order_table(unserialize($this->OrderInfo['cart']),$tf);
			//$this->SetHeader('');
			$this->SetTemplate('print.php');
			//$this->SetFooter('');
		}
		public function OnPrintStic(){
			$this->OrderInfo = $this->_load_content_info('orders o', '', 'o.id = '.i($_GET['id']));
			$this->Currency = $this->OrderInfo['currency_id'];
			$this->OrderInfo['date'] = date('d.m.Y H:i', $this->OrderInfo['ts']);
			$this->OrderInfo['total'] = number_format($this->OrderInfo['total'], 2, '.', ' ');
			$this->OrderInfo['state_label'] = $this->_order_state($this->OrderInfo['state']);
			//$this->Cart = $this->generate_order_table(unserialize($this->OrderInfo['cart']));
			$this->Cart = $this->generate_order_tablestic(unserialize($this->OrderInfo['cart']));
			$this->Cart_str = $this->generate_order_option_str_stic(unserialize($this->OrderInfo['cart']));
			//$this->SetHeader('');
			$this->SetTemplate('printstic.php');
			//$this->SetFooter('');
		}
		
		public function OnDownload(){
		
			$file = $this->GetConfigParam('absolute-path').s($_GET['path']);
			$filename = s($_GET['name']);
			//_debug($filename);
			//_debug($file,1);
			header('Content-type: application/octet-stream');
			header("Content-Disposition: attachment; filename=".$filename);

			$handle = fopen($file, "rb");
			$contents = fread($handle, filesize($file));
			fclose($handle);
			echo $contents;
			
			exit;
		}
		
	
		/*
		 * Private methods
		 */
		
		private function _load_order_info($id) {
			
			global $DB;
			
			$sql = 'SELECT  
						   o.*
					FROM  
						 orders o 
					WHERE  
						  o.id=\''.$id.'\'';
			$this->OrderInfo = $DB->GetRow($sql);
			
			$this->OrderInfo['date'] = date('d.m.Y H:i', $this->OrderInfo['ts']);
			$this->OrderInfo['total'] = number_format($this->OrderInfo['total'], 2, '.', ' ');
			$this->OrderInfo['state_label'] = $this->_order_state($this->OrderInfo['state']);
			
			$order_id = $id;
			$items = unserialize($this->OrderInfo['cart']);
			
			$items_sz = count($items);
			$this->AttachComponent('Grid', $this->Grid);
			
			//$titles = array('Товар', 'Количество','Цена', 'Скидка', 'Сумма');
			$titles = array('Фото', 'Разрешение', 'Кол-во', 'Параметры', 'Качество', 'Цена', 'Сумма');
			$this->Currency = $this->OrderInfo['currency_id'];
			if($this->OrderInfo['state'] == 'new'){
				$titles[] = 'Удалить';
			}
			$options['row_numbers'] = true;
			
			$rows = array();
			$i = $discount = $this->OrderInfo['total'] = 0;
			global $config;
			
			foreach($items as $id=>$item){
				$price = $item['price'] * $item['count'];
				$item['discount'] *= $item['count'];

				$params = '';
				foreach($item['options'] as $opt_id => $opt_val) {
					
					
					//$opt_title = GetOne("SELECT title FROM product_options WHERE id='".$opt_id."' AND id<8");
					//$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$opt_val."' AND option_id<8 AND option_id='".$opt_id."'");
					
					$opt_title = GetOne("SELECT title FROM product_options WHERE id='".$opt_id."' ");
					$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$opt_val."'  AND option_id='".$opt_id."'");
					
					if($opt_title && $val_title  ){
						$params .= $opt_title.": ".$val_title."<br/>";
					}
					if(in_array($opt_id, $this->Option_size)){
					
					 if(isset($this->Opt_size_count[$val_title])){
						$this->Opt_size_count[$val_title]=$this->Opt_size_count[$val_title]+$item['count'];
					 }else{
						$this->Opt_size_count[$val_title]=$item['count'];
					 }
					
					 
					}
				
				}	
				
				$img_path = $_SERVER['DOCUMENT_ROOT'].'/'.$item['path'].$item['img'];
				// _debug($img_path, 1);
				if(file_exists($img_path)){
					//$img_path = $item['path'].$item['img'];
					$img_path = PATH_PUBLIC_FILES_IMAGES.'orders/'.$this->OrderInfo['id'].'/thumb/'.$item['img'];
				}else{
					$img_path = PATH_PUBLIC_FILES_IMAGES.'orders/'.$this->OrderInfo['id'].'/thumb/'.$item['img'];
				}
				
				$size = filesize ($config['absolute-path'].$img_path);
				
								
				//$str = 	'<img src="'.htmlspecialchars('/core/lib/phpThumb/phpThumb.php?w=100&h=100&src=').'/'.$img_path.'" alt="" />';
				$str = 	'<img width="100" height="100" src="/'.$img_path.'" alt="" />';
					
				// $row = array('<img src="/public/files/images/orders/'.$order_id.'/thumb/'.$item['img'].'" alt="'.$item['img'].'"></br>'.$item['img'], 
				$row = array(
					// '<a href="'.$this->GetConfigParam('site_url').'public/files/images/orders/'.$order_id.'/'.$item['img'].'"><img src="'.htmlspecialchars('/core/lib/phpThumb/phpThumb.php?w=100&src=').'/public/files/images/orders/'.$order_id.'/'.$item['img'].'" alt="" /></a><br /> '.$item['img'].' <br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path=public/files/images/orders/'.$order_id.'/'.$item['img'].'&name='.$item['img'].'">Скачать</a>',
					// '<a href="'.$this->GetConfigParam('site_url').'public/files/images/orders/'.$order_id.'/'.$item['img'].'"><img src="'.htmlspecialchars('/core/lib/phpThumb/phpThumb.php?w=100&src=').'/public/files/images/orders/'.$order_id.'/'.$item['img'].'" alt="" /></a><br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path=public/files/images/orders/'.$order_id.'/'.$item['img'].'&name='.$item['img'].'">'.$item['img'].'</a>',
					
					'<a target="_blank" href="/'.$img_path.'">'.$str.'</a><br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path='.$img_path.'&name='.$item['img'].'">'.$item['img'].'</a>',
					$item['image_size'][0].' '.$this->_get_system_variable('photo_size_separator').' '.$item['image_size'][1],
					$item['count'],
					$params,
					$item['quality'],
					$this->_show_price($item['price'],$item['currency_id']),
					//$this->_show_price($item['discount'],$item['currency_id']),
					$this->_show_price($price,$item['currency_id'])
				);
					
				if($this->OrderInfo['state'] == 'new'){
					$row[] = '<a href="/azone/orders/?Event=DelProd&id='.$this->OrderInfo['id'].'&prod_id='.$id.'" style="margin-left: 15px;"><img alt="Удалить" src="/components/office2007/GridAll/images/remove.gif"></a>';
				}
				
				$rows[$i] = $row;
				$i++;
				$this->OrderInfo['total'] += $price;
				$discount += $item['discount'];
			}
			
			$this->OrderInfo['total'] -= $discount;
			
			$this->OrderInfo['discount'] = $this->_show_price($discount);
			$this->OrderInfo['total'] = $this->_show_price($this->OrderInfo['total']);
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
					
			$this->Grid->SetData($data);
		}
		private function _order_state_issuing_mail($state) {

			switch($state) {
				
				default:
					return 'Статус Вышего заказа изменен';
				break;
				
				case 'waiting':
					return ' Ваш заказ ожидает Вас в пункте выдачи.';
				break;
				case 'sent_courier':
					return ' Ваш заказ передан в курьерскую службу.';
				break;
				case 'sentnpost':
					return ' Ваш заказ отправлен Новой Почтой.';
				break;
			}
		}
		private function _order_state_issuing($state) {
			
			switch($state) {
				
				default:
					return '-';
				break;
				
				case '-':
					return '-';
				break;
				case 'n_post':
					return ' Новой почтой';
				break;
				case 'lab':
					return ' Cамовывоз М,6';
				break;
				case 'waiting':
					return 'Ожидает в пункте выдачи';
				break;
				case 'printed':
					return 'Напечатан';
				break;
				case 'sentnpost':
					return 'Отправлен Новой почтой';
				break;
				case 'sent_courier':
					return 'Передан в курьерскую службу';
				break;
				case 'courier':
					return 'Курьер';
				break;
				case 'took':
					return 'Забрали';
				break;
				case 'sent':
					return 'Отправлен';
				break;
				case 'made':
					return 'Выполнен';
				break;
				case 'rejected':
					return 'Откланен';
				break;
				case 'rejected_user':
					return 'Откланен пользователем';
				break;
				case 'not_taken':
					return 'Не Забрали';
				break;
				case 'not_order':  
					return 'Не оформлен';
				break;
				
				case 'new':
					return 'Новый';
				break;

				case 'wait':
					return 'Ожидание';
				break;

				case 'cancelled':
					return 'Отменён';
				break;

				case 'paid':
					return 'Оплачен';
				break;
				case 'wait_pay':
					return 'Ожидает оплаты';
				break;
				
				case 'delivered':
					return 'Доставлен';
				break;
				     
				case 'inprint':
					return 'В печати';
				break;
				case 'dostavka':
					return 'Доставка';
				break;
				case 'dostavlen':
					return 'Доставлен';
				break;
				case 'dostavlen_punkt':
					return 'Доставлен в пункт выдачи';
				break;
				case 'dostavka_punkt':
					return 'Доставка в пункт выдачи';
				break;
				case 'dostavka_kyrer':
					return 'Доаставка курьером';
				break;
				case 'dostavka_newpost':
					return 'Доставка новой почтой';
				break;
				case 'accepted':
					return 'Принят';
				break;
			}
		}
		private function _order_state($state) {
			
			switch($state) {
				
				default:
					return 'Неизвестно';
				break;
				
				case 'sent':
					return 'Отправлен';
				break;
				case 'made':
					return 'Выполнен';
				break;
				case 'rejected':
					return 'Откланен';
				break;
				case 'rejected_user':
					return 'Откланен пользователем';
				break;
				case 'took':
					return 'Забрали';
				break;
				case 'not_taken':
					return 'Не Забрали';
				break;
				case 'not_order':
					return 'Не оформлен';
				break;
				case 'printed':
					return 'Напечатан';
				break;
				case 'new':
					return 'Новый';
				break;

				case 'wait':
					return 'Ожидание';
				break;

				case 'cancelled':
					return 'Отменён';
				break;

				case 'paid':
					return 'Оплачен';
				break;
				case 'wait_pay':
					return 'Ожидает оплаты';
				break;
				
				case 'delivered':
					return 'Доставлен';
				break;
				     
				case 'inprint':
					return 'В печати';
				break;
				case 'dostavka':
					return 'Доставка';
				break;
				case 'dostavlen':
					return 'Доставлен';
				break;
				case 'dostavlen_punkt':
					return 'Доставлен в пункт выдачи';
				break;
				case 'dostavka_punkt':
					return 'Доставка в пункт выдачи';
				break;
				case 'dostavka_kyrer':
					return 'Доаставка курьером';
				break;
				case 'dostavka_newpost':
					return 'Доставка новой почтой';
				break;
				case 'accepted':
					return 'Принят';
				break;
				
			}
		}
		private function _order_state_color($state) {
			
			switch($state) {
				
				default:
					return ' ';
				break;
				
				case 'sent':
					//return 'Отправлен';
					return ' ';
				break;
				case 'courier':
					//return 'Курьером';
					return 'FFDD50';
				break;
				case 'n_post':
					//return 'Новой почтой';
					return 'FF7300';
				break;
				case 'made':
					//return 'Выполнен';
					return ' ';
				break;
				case 'rejected':
					//return 'Откланен';
					return ' ';
				break;
				case 'rejected_user':
					//return 'Откланен пользователем';
					return ' ';
				break;
				case 'took':
					//return 'Забрали';
					return ' ';
				break;
				case 'not_taken':
					//return 'Не Забрали';
					return ' ';
				break;
				case 'not_order':
					return '00C4FF';
				break;
				case 'printed':
					//return 'Напечатан';
					return ' ';
				break;
				case 'new':
					//return 'Новый';
					return 'ff3a30';
				break;

				case 'wait':
					//return 'Ожидание';
					return ' ';
				break;

				case 'cancelled':
					//return 'Отменён';
					return ' ';
				break;

				case 'paid':
					//return 'Оплачен';
					return 'FF668C';
				break;
				case 'wait_pay':
					//return 'Ожидает оплаты';
					return 'FF87FF';
				break;
				
				case 'delivered':
					//return 'Доставлен';
					return ' ';
				break;
				     
				case 'inprint':
					//return 'В печати';
					return '8cff40';
				break;
				case 'lab':
					//return 'Самовывоз М6';
					return 'FFA500';
				break;
				case 'waiting':
					//return 'Ожидает в пункте выдачи';
					return 'ff3a30';
				break;
				case 'dostavka':
					//return 'Доставка';
					return ' ';
				break;
				case 'dostavlen':
					//return 'Доставлен';
					return ' ';
				break;
				case 'dostavlen_punkt':
					//return 'Доставлен в пункт выдачи';
					return ' ';
				break;
				case 'dostavka_punkt':
					//return 'Доставка в пункт выдачи';
					return ' ';
				break;
				case 'dostavka_kyrer':
					//return 'Доаставка курьером';
					return 'FFA570';
				break;
				case 'dostavka_newpost':
					//return 'Доставка новой почтой';
					return 'ff3a30';
				break;
				case 'accepted':
					//return 'Принят';
					return ' ';
				break;
				
			}
		}
		
		private function _load_orders() {
			
			global $DB;
			
			$where = ' WHERE 1 = 1 ';
			$url = '';
			if($_GET['order_id']){
				$url .= '&order_id='.$_GET['order_id'];
				$where .= ' AND id = \''.intval($_GET['order_id']).'\' ';
			}
			if($_GET['fio']){
				$url .= '&fio='.$_GET['fio'];
				$where .= ' AND name LIKE \'%'.$_GET['fio'].'%\' ';
			}
			if($_GET['date']){
				$url .= '&date='.$_GET['date'];
				$where .= ' AND ts > \''.strtotime($_GET['date']).'\' AND ts < \''.(strtotime($_GET['date'])+86400).'\' ';
			}
			if( $_GET['filtr']=='n'  ){
				$_SESSION['azone_account']['filtr']=false;
				if($_GET['state']){
					$url .= '&state='.$_GET['state'];
					$where .= ' AND `state` = \''.$_GET['state'].'\' ';
				}
			}
			
			if( $_GET['filtr']=='y' || $_SESSION['azone_account']['filtr'] ){
				
				$_SESSION['azone_account']['filtr']=true;
				
				//$url .= '&state='.$_GET['state'];
				$w_st=' (\'courier\',\'n_post\',\'not_order\',\'new\',\'paid\',\'wait_pay\',\'inprint\',\'lab\',\'waiting\',\'dostavka_kyrer\',\'dostavka_newpost\') ';
				$w_st_is= ' (\'courier\',\'n_post\',\'not_order\',\'new\',\'paid\',\'wait_pay\',\'inprint\',\'lab\',\'waiting\',\'dostavka_kyrer\',\'dostavka_newpost\') ';
				$where .= ' AND ( `state` IN  '.$w_st.'  OR `state_issuing`  IN  '.$w_st_is.' ) ';
			}
			
			
			$sql = 'SELECT COUNT(*) AS cnt FROM orders o'.$where;
			$res = $DB->GetRow($sql);

			$items_per_page = 30;
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){
				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'orders/', $page, '?page=%s'.$url);
			
			$sql = 'SELECT  
						   o.*
					FROM orders o
					'.$where.'
					ORDER BY o.ts DESC, o.id DESC 
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			//if($_GET['filtr']){  _debug($sql,1);}
			
			$this->Orders = $DB->GetAll($sql);
			$this->Orders_sz = count($this->Orders);
		}
		
		private function _init_grid() {
			
			$this->_load_orders();
			
			$this->AttachComponent('Grid', $this->Grid);
			
			$titles = array('Номер','ФИО заказчика', 'Дата', 'Общее к-во', 'Сумма', 'Состояние заказа', 'Состояние по выдаче');
			$options['row_numbers'] = false;
			
			if($this->Perm['edit']) {
				$options['controls'] = array(	'download' => $this->RedirectUrl.'?Event=DownloadOrder&amp;id=%s' 
								//,'edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
								//'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s'
								);
			}
			
			$rows = array();
			//_debug($this->Orders,1);
			for($i = 0; $i < $this->Orders_sz; $i++){
				
				$qty= $this->Orders[$i]['qty'] ;
				$total =$this->Orders[$i]['total'] ;
				
				$date = date('d.m.Y H:i', $this->Orders[$i]['ts']);
				$this->Currency = $this->Orders[$i]['currency_id'];
				$total = $this->_calc_order_price($this->Orders[$i]['cart']);
				$state = $this->_order_state($this->Orders[$i]['state']);
				$state_issuing = $this->_order_state_issuing($this->Orders[$i]['state_issuing']);
				//$state['info']=$state.($this->Orders[$i]['changed'] ? '<span style="color:red"> (изменен)</span>':'');
				$statearr['info']=$state;
				$statearr['color']= $this->_order_state_color($this->Orders[$i]['state']);
				
				$state_issuingarr['info']= $state_issuing;
				$state_issuingarr['color']= $this->_order_state_color($this->Orders[$i]['state_issuing']);
				
				$row = array('<a href="'.$this->RedirectUrl.'?Event=Edit&amp;id='.$this->Orders[$i]['id'].'">'.$this->Orders[$i]['id'].'</a>',
							'<a href="'.$this->RootUrl.'users/?Event=Edit&id='.$this->Orders[$i]['buyer_id'].'">'.($this->Orders[$i]['name'] ? $this->Orders[$i]['name'] : $this->Orders[$i]['email']).'</a>', 
								
							$date, 
							$qty, 
							 $this->_show_price($total), 
							
							 $statearr,
							 $state_issuingarr
						);//_debug($row, 0);
						 //$state.($this->Orders[$i]['changed'] ? '<span style="color:red"> (изменен)</span>':''),
				$rows[$this->Orders[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			//$data = array(false, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
		
		public function _show_price(&$price, $currency = 0){
			// _debug($this->Currencies, 1);
			if($currency && $this->Currency != $currency){
				if($currency == $this->MainCurrency){
					$price = floatval($price) * floatval($this->Currencies[$this->Currency][course]);
				}else{
					$price = money(floatval($price) * floatval($this->Currencies[$currency][course]) * floatval($this->Currencies[$this->Currency][course]));
				}
			}else{
				$price = floatval($price);
			}
			if($this->Currencies[$this->Currency]['sign_pos']){
				return money($price).' '.$this->Currencies[$this->Currency]['sign'];
			}else{
				// _debug($this->Currencies[$this->Currency]['sign'].money($price));
				return $this->Currencies[$this->Currency]['sign'].money($price);
			}
		}
		
		protected function _calc_order_price($cart){
			if(!is_array($cart)){
				$cart = unserialize($cart);
			}
			// _debug($cart, 1);
			$sum = 0;
			foreach ((array)$cart as $product) {
				$this->_show_price($product['price'],$product['currency_id']);
				$this->_show_price($product['discount'],$product['currency_id']);
				$sum += round(($product['price'] - $product['discount']) * $product['count'], 2) ;
			}
			return $sum;
		}
		
		public function calculate_discount($product,$user_discount = 0){
			$discount = 0;
			if(i($this->_get_system_variable('discount_action')) || !$product['new']){
				$discount_arr = $this->_get_category_discount($product['category_id']);
				$discount_arr[] = intval($product['discount']);
				$discount_arr[] = intval($user_discount);
				$discount = max($discount_arr);
				$discount = round($product['price'] * $discount/100, 2);
			}
			return $discount;
		}
		
		protected function _get_category_discount($parent_id, $discount = array()){
			$category = $this->_load_content_info('categories','','id = '.$parent_id,'');
			
			$discount[] = intval($category['discount']);
			if(intval($category['parent_id']) != 0){
				$discount = $this->_get_category_discount($category['parent_id'], $discount);
			}
			return $discount;
		}
		
		private function generate_order_table($cart,$add_np=false){
			
			$res ='
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<style type="text/css">
			.trash-list TD,
			.trash-list TH
			{
				padding:5px;
			}
			.trash-list P{
				margin:0;
			}
			</style>

			<table  cellpadding="0" cellspacing="0" class="trash-list" style="border-top:1px solid #000000;border-right:1px solid #000000;" width="100%">
					
					<tr class="table-title">
						<th style="background:#D0D0D0;width:70px;border-left:1px solid #000000;border-bottom:1px solid #000000;">Фото</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Разрешение</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Кол-во/шт.</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Параметры</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Качество</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Цена</th>
						
						
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;border-right:1px solid #000000" class="last">Стоимость</th>
					</tr> ';
			$count = $total = $price = $discount = 0;
			$items = $cart;
			foreach($items as $id=>$item){
				$price = $item['price'] * $item['count'];
				$item['discount'] *= $item['count'];

				$params = '';
				
				foreach($item['options'] as $opt_id => $opt_val) {
					//$opt_title = GetOne("SELECT title FROM product_options WHERE id='".$opt_id."' AND id<8");
					//$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$opt_val."' AND option_id<8 AND option_id='".$opt_id."' ");
					
					$opt_title = GetOne("SELECT title FROM product_options WHERE id='".$opt_id."' ");
					$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$opt_val."'  AND option_id='".$opt_id."' ");
					
					if($opt_title){
						$params .= $opt_title.": ".$val_title."<br/>";
					}
				}	
				
				$img_path = $_SERVER['DOCUMENT_ROOT'].'/'.$item['path'].$item['img'];
				// _debug($img_path, 1);
				if(file_exists($img_path)){
					$img_path = $item['path'].$item['img'];
				}else{
					$img_path = PATH_PUBLIC_FILES_IMAGES.'orders/'.$this->Order['id'].'/'.$item['img'];
				}
				$size = filesize ($config['absolute-path'].$img_path);
							
				$str = 	'<img src="'.htmlspecialchars('/thumb/?w=100&src=').$img_path.'" alt="" />';
							
				// $row = array('<img src="/public/files/images/orders/'.$order_id.'/thumb/'.$item['img'].'" alt="'.$item['img'].'"></br>'.$item['img'], 
				$row = array(
					// '<a href="'.$this->GetConfigParam('site_url').'public/files/images/orders/'.$order_id.'/'.$item['img'].'"><img src="'.htmlspecialchars('/core/lib/phpThumb/phpThumb.php?w=100&src=').'/public/files/images/orders/'.$order_id.'/'.$item['img'].'" alt="" /></a><br /> '.$item['img'].' <br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path=public/files/images/orders/'.$order_id.'/'.$item['img'].'&name='.$item['img'].'">Скачать</a>',
					// '<a href="'.$this->GetConfigParam('site_url').'public/files/images/orders/'.$order_id.'/'.$item['img'].'"><img src="'.htmlspecialchars('/core/lib/phpThumb/phpThumb.php?w=100&src=').'/public/files/images/orders/'.$order_id.'/'.$item['img'].'" alt="" /></a><br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path=public/files/images/orders/'.$order_id.'/'.$item['img'].'&name='.$item['img'].'">'.$item['img'].'</a>',
					
					'<a target="_blank" href="/'.$img_path.'">'.$str.'</a><br /><a href="'.$this->GetConfigParam('site_url').'azone/orders/?Event=Download&path='.$img_path.'&name='.$item['img'].'">'.$item['img'].'</a>',
					$item['image_size'][0].' '.$this->_get_system_variable('photo_size_separator').' '.$item['image_size'][1],
					$item['count'],
					$params,
					$item['quality'],
					$this->_show_price($item['price'],$item['currency_id']),
					//$this->_show_price($item['discount'],$item['currency_id']),
					$this->_show_price($price,$item['currency_id'])
				);
				if($this->OrderInfo['state'] == 'new'){
					$row[] = '<a href="/azone/orders/?Event=DelProd&id='.$this->OrderInfo['id'].'&prod_id='.$id.'" style="margin-left: 15px;"><img alt="Удалить" src="/components/office2007/GridAll/images/remove.gif"></a>';
				}
				$row['discount'] = $item['discount'];
				$rows[$i] = $row;
				$i++;
				
				$discount += $item['discount'];
			}
			
			foreach($rows as $prod){
				
				$res.='	<tr>
						<td class="image" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">'.$prod[0].'</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[1].'</p>
						</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[2].'</p>
						</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[3].'</p>
						</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[4].'</p>
						</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[5].'</p>
						</td>
						<td class="name" style="border-left:1px solid #000000;border-bottom:1px solid #000000;">
							<p>'.$prod[6].'</p>
						</td>
						
					</tr>';
				$count += $prod['count'];
				$total += $prod[6]-$prod['discount'];
				$discount += $prod['discount'];
			}
			
			$res.='	<tr class="summary">
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">Итого:</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">'.$this->OrderInfo['qty'].' шт.</th>
						<th style="background:#D0D0D0;border-left:1px solid #000000;border-bottom:1px solid #000000;">'.$this->_show_price($total).($add_np?' + стоимость доставки курьерской службой "Новая Почтя':'').'</th>
					</tr>';
			return $res;
		}
		// принт инфо print info
		private function generate_order_option_str_stic($cart){
			$count  = 0;
			$items = $cart;
			$opt_strs['0']['options']['ss']='2';
			
			foreach($items as $id=>$item){
				
				$est=false;
				$kkay=false;
				foreach($opt_strs as  $kay=>$opt_str ){
					if($opt_strs[$kay]['options']==$item['options']){
						$est=true;
						$kkay=$kay;
					}else{
						$est=false;
					}
				}
				if($est){
					$opt_strs[$kkay]['cnt']=$opt_strs[$kkay]['cnt']+$item['count'];
				}else{
					$m=array();
					$m['options'] = $item['options'];
					$m['cnt'] = $item['count'];
					$opt_strs[] = $m;
				}
			}
			
			array_shift($opt_strs);
			$retur=array();
			foreach($opt_strs as $kk => $opts) {
				$val_title_r=' ';
				foreach($opts['options'] as $key=>$val){
					$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$val."'");
					$val_title_r.=' '.$val_title;
				}
				$retur[$val_title_r]=$opt_strs[$kk];
			}
			
			return $retur;
		}
		private function generate_order_tablestic($cart){
			
			$count = $total = $price = $discount = 0;
			$items = $cart;
			//_debug($cart,1);		
			
			$options_format_all=array();
			
			foreach($items as $id=>$item){
				
				foreach($item['options'] as $opt_id => $opt_val) {
					foreach ($this->Optionsize as $key=>$val){
						//if($opt_id==1 || $opt_id==6 || $opt_id==7 ){
						if($opt_id==$val){
						 $options_format_all[]+= $opt_val;
						}
					}
				}	
			}
		
			$options_format=array_unique($options_format_all);
			$format_cunt=array();
			foreach($items as $id=>$item){
				foreach($item['options'] as $opt_id => $opt_val) {
					foreach ($this->Optionsize as $key=>$val){
					if($opt_id==$val ){
					//if($opt_id==1 || $opt_id==6 || $opt_id==7 ){
						 foreach($options_format as $aptfarmat){
						  if($aptfarmat == $opt_val ){ $format_cunt[$opt_val]+=$item['count'];}
						 }
						}
					}
				}
			
			}
				//_debug($format_cunt,1);
				foreach($format_cunt as $opt_vall => $opt_coun) {
					
					$val_title = GetOne("SELECT title FROM product_option_variants WHERE id = '".$opt_vall."'");
					$format_cunt['title'][]=$val_title.'-'.$opt_coun.'шт';
				}	
			
		//_debug($format_cunt,1);
			return $format_cunt;
		}
		
		
		
		
		
		
		//  end print infoпринт инфо
				
		protected function fillArrayWithFileNodes( DirectoryIterator $dir,&$zip ,$path ) {
		  $data = array();
		  foreach ( $dir as $node )
		  {
		    if ( $node->isDir() && !$node->isDot() )
		    {
			$zip->addFile($path.DIRECTORY_SEPARATOR.$node->getFilename()); 
		      $data[$node->getFilename()] = $this->fillArrayWithFileNodes( new DirectoryIterator( $node->getPathname() ), $zip, $path );
		      
		    }
		    else if ( $node->isFile() )
		    {
		      $data[] = $node->getFilename();
		      $zip->addFile($path.DIRECTORY_SEPARATOR.$node->getFilename()); 
		    }
		  }
		  return $data;
		}
		
		function Zip($source, $destination) {
			if (!extension_loaded('zip') || !file_exists($source)) {
				return false;
			}

			$zip = new ZipArchive();
			if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
				return false;
			}
			$order_id = intval($_GET['id']);
			$source = str_replace('\\', '/', realpath($source));

			if (is_dir($source) === true) {
				$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::CHILD_FIRST);

				foreach ($files as $file) {
					$file = str_replace('\\', '/', $file);			   
					if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
						continue;

						$file = realpath($file);

						if (is_dir($file) === true) {
							$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
						} else if (is_file($file) === true) {
							$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
						}
				}
			} else if (is_file($source) === true) {
				$zip->addFromString(basename($source), file_get_contents($source));
			}
			
			// close and save archive			
			$zip->addFile($path.'order-'.$order_id.'.html');      		
			$zip->close();
		}
		
		protected function OnDownloadOrder(){
			$order_id = intval($_GET['id']);
			
			ini_set('max_execution_time', 5000);
			$zipFile = $_SERVER['DOCUMENT_ROOT'].'/public/files/files/zip/'.$order_id.'.zip';
			$pathOrder = $_SERVER['DOCUMENT_ROOT']."/public/files/images/orders/";
			$path = $pathOrder.$order_id.'/';
			
			if(is_dir($path)){
				$zip = new ZipArchive();
				$zip->open($zipFile, ZipArchive::CREATE);
				$rdir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), TRUE);
					
				foreach ($rdir as $file) {
					if (is_dir($file) === true) {
						$zip->addEmptyDir(str_replace($pathOrder,"",$file));
					} else {
						$zip->addFile($file, str_replace($pathOrder,"",$file));
					}
					//echo str_replace($pathOrder,"",$file)."<br />";
				}
				
				$this->OrderInfo = $this->_load_content_info('orders o', '', 'o.id = '.i($_GET['id']));
				$this->Currency = $this->OrderInfo['currency_id'];
				$this->OrderInfo['date'] = date('d.m.Y H:i', $this->OrderInfo['ts']);
				$this->OrderInfo['total'] = number_format($this->OrderInfo['total'], 2, '.', ' ');
				$this->OrderInfo['state_label'] = $this->_order_state($this->OrderInfo['state']);
				$orderInfo = $this->generate_order_table(unserialize($this->OrderInfo['cart']));			
				$fp = fopen($path.'order-'.$order_id.'.html', 'w');
				fwrite($fp, $orderInfo);			
				fclose($fp);
				$zip->addFile($path.'order-'.$order_id.'.html','order-'.$order_id.'.html');      	
			
				$zip->close();
				header('Location: /public/files/files/zip/'.$order_id.'.zip');
				exit();
			}else{
				header('Location: /azone/orders/');
				exit();
			}
		}

	}
?>
