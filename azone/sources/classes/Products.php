<?
	class Products extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'products/';
		
		protected $TableName				= 'products';
		protected $PublishField				= 'publish';
		
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $Products					= array();
		protected $Products_sz				= 0;
		protected $ProdInfo					= array();
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		protected $CatIdList				= array();
		protected $DescriptionInfo			= array();
		
		
		protected $LHeight1					= 120;
		protected $LHeight2					= 260;
		protected $LWidth1					= 124;
		protected $LWidth2					= 260;
		
		
		protected $MD5FileName				= '';
		protected $FileError				= '';		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');
			$this->_load_cataegories();
			$this->_init_grid(intval($_GET['cat']));
		}
		
		public function OnNew() {
		
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->AddJS($this->RootUrl.'public/js/products.js');
			$sql = 'SELECT t.id, pt.title FROM products t LEFT JOIN products_translation pt ON pt.obj_id = t.id';
			$this->RecProd = GetAll($sql);
			$this->SetTemplate('edit.php');
			$this->_load_cataegories();
			$this->Currency = $this->_load_content('course');
			$this->Brands = GetAll('SELECT * FROM brands ORDER BY title ASC');
			$this->State = $this->_load_content('product_status');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$id = $this->_insert_product($_POST);
				if($_POST['redirect']) {
					header('Location: /azone/products/?Event=AddFoto&id='.$id.'&page='.$_GET['page']);
				}
				else {
					$this->OnDefault();
					// header('Location: /azone/products/?Event=Edit&id='.$id);
				}
			}
		}
		
		public function OnEdit() {
			
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->AddJS($this->RootUrl.'public/js/products.js');
			$this->_load_product_info(intval($_GET['id']));
			$sql = 'SELECT t.id, pt.title FROM products t LEFT JOIN products_translation pt ON pt.obj_id = t.id';
			$this->RecProd = GetAll($sql);
			$this->_load_cataegories();
			$this->_load_photos(intval($_GET['id']));
			$this->_load_video(intval($_GET['id']));
			$this->Currency = $this->_load_content('course');
			$this->Brands = GetAll('SELECT * FROM brands ORDER BY title ASC');
			$this->SetTemplate('edit.php');
			$this->State = $this->_load_content('product_status');
			$this->Chars = $this->_get_cat_options($this->ProdInfo['category_id']);
			// _debug($this->ProdInfo, 1);
			// _debug($this->Chars, 1);
		}
		
		public function OnSetDefault() {
			$this->SendAjaxHeaders();
			$sql = 'UPDATE photos
					SET `default` = 0
					WHERE product_id = \''.$_GET['id'].'\'';
			Execute($sql);
			
			$sql = 'UPDATE photos
					SET `default` = 1
					WHERE id = \''.$_GET['photo'].'\'';
			Execute($sql);
			exit;
		}
	public function OnSetDefaultVideo() {
			$this->SendAjaxHeaders();
			$sql = 'UPDATE video
					SET `default` = 0
					WHERE product_id = \''.$_GET['id'].'\'';
			Execute($sql);
			
			$sql = 'UPDATE video
					SET `default` = 1
					WHERE id = \''.$_GET['photo'].'\'';
			Execute($sql);
			exit;
		}
		
		public function OnSetPlan() {
			$this->SendAjaxHeaders();
			
			$plan = GetOne('SELECT plan
							FROM photos
							WHERE id = \''.$_GET['id'].'\'');
			
			$sql = 'UPDATE photos
					SET `plan` = '.( $plan ? 0 : 1 ).'
					WHERE id = \''.$_GET['id'].'\'';
			
			Execute($sql);
			exit;
		}
		public function OnSetPdf() {
			$this->SendAjaxHeaders();
			
			$plan = GetOne('SELECT garantie
							FROM photos
							WHERE id = \''.$_GET['id'].'\'');
			
			$sql = 'UPDATE photos
					SET `garantie` = '.( $plan ? 0 : 1 ).'
					WHERE id = \''.$_GET['id'].'\'';
			
			Execute($sql);
			exit;
		}
		public function OnSwitchPhotosField(){
		
			$this->SendAjaxHeaders();
			
			$id = $_GET['id'];
			$field = $_GET['field'];
			
			$state = GetOne("SELECT `$field` 
							FROM `photos` 
							WHERE `id` = '$id'");
			
			$sql = "UPDATE `photos` 
					SET `$field` = ".($state ? 0 : 1)." 
					WHERE `id` = '$id'";
			Execute($sql);
			
			exit;
		}
		
		public function OnUpdatePhotosField(){
		
			$this->SendAjaxHeaders();

			$id = $_GET['id'];
			$field = $_GET['field'];
			$value = $_GET['value'];
			
			$sql = "UPDATE `photos` 
					SET `$field` = '$value' 
					WHERE `id` = '$id'";
			Execute($sql);
			
			exit;
		}
		
	protected function OnRemoveVideo(){
			  global $DB,$config;
			
			
			$sql = 'SELECT *
					FROM video
					WHERE id = \''.(int)$_GET['photo_id'].'\'';
			$res = $DB->GetRow($sql);
			
			@unlink($config['absolute-path'].'public/files/images/products/'.$res['filename']);
			@unlink($config['absolute-path'].'public/files/images/products/middle-'.$res['filename']);
			
			$sql = "DELETE FROM video 
					WHERE id = '".(int)$_GET['photo_id']."'";
			$DB->Execute($sql);
			
			header("Location: /azone/products/?Event=Edit&id=".(int)$_GET['id']);
		}
		protected function OnRemovePhoto(){
			global $DB,$config;
			
			
			$sql = 'SELECT *
					FROM photos
					WHERE id = \''.(int)$_GET['photo_id'].'\'';
			$res = $DB->GetRow($sql);
			
			@unlink($config['absolute-path'].'public/files/images/products/'.$res['filename']);
			@unlink($config['absolute-path'].'public/files/images/products/middle-'.$res['filename']);
			@unlink($config['absolute-path'].'public/files/images/products/small-'.$res['filename']);
			// @unlink($config['absolute-path'].'public/files/images/products/thumb-'.$res['filename']);
			
			$sql = "DELETE FROM photos 
					WHERE id = '".(int)$_GET['photo_id']."'";
			$DB->Execute($sql);
			
			header("Location: /azone/products/?Event=Edit&id=".(int)$_GET['id']);
		}
		
		public function OnAddFoto() {
			$this->SetTemplate('editfoto.php');
			$this->AddJS('/core/lib/fancy/Swiff.Uploader.js');
			$this->AddJS('/core/lib/fancy/Fx.ProgressBar.js');
			$this->AddJS('/core/lib/fancy/FancyUpload2.js');
			$this->AddCSS('/core/lib/fancy/fancy.css');	
			$this->AddJS($this->RootUrl.'public/js/fotoobjects.js');
		}
		
		public function OnRemove() {
			
			global $DB, $config;
			
			$sql = 'DELETE FROM products 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);
			$sql = 'DELETE FROM products_translation 
					WHERE obj_id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);
			$photos = GetAll('	SELECT *
								FROM photos
								WHERE product_id = \''.intval($_GET['id']).'\'');
			
			if(is_array($photos)){
				foreach ($photos as $p){
					
					@unlink($config['absolute-path'].'public/files/images/products/'.$p['filename']);
					@unlink($config['absolute-path'].'public/files/images/products/middle-'.$p['filename']);
					@unlink($config['absolute-path'].'public/files/images/products/small-'.$p['filename']);
					// @unlink($config['absolute-path'].'public/files/images/products/thumb-'.$p['filename']);
				}
			}
			Execute('	DELETE FROM photos
						WHERE product_id = \''.intval($_GET['id']).'\'');
			$this->OnDefault();
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_product(intval($_GET['id']), $_POST);
				if($_POST['redirect']) {
					header('Location: /azone/products/?Event=AddFoto&id='.$_GET['id'].'&page='.$_GET['page'].'&brand='.$_GET['brand'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'].'&cat='.$_GET['cat'].'&video='.$_POST['video']);
				}
				else {
					$this->OnDefault();
						header('Location: /azone/products/?Event=Edit&id='.$_GET['id']);
					// header('Location: /azone/products/?Event=Edit&id='.$_GET['id']);
				}
			}
		}
		
		/*
		 * Private methods
		 */
		
		private function _load_product_info($id) {
			global $DB;
			$sql = 'SELECT * FROM products WHERE id=\''.$id.'\'';
			$this->ProdInfo = $DB->GetRow($sql);
			
			$this->ProdInfo['options'] = array();
			$sql = '	
				SELECT 
					pv.*, 
					po.title, 
					po.element,
					po.user_choice,
					pov.title as t_variant
				FROM 	
					products_variants pv
					LEFT JOIN product_options po ON pv.option_id	= po.id
					LEFT JOIN product_option_variants pov ON (pov.id = pv.variant_id AND pov.option_id = pv.option_id)	
				WHERE 	
					pv.product_id	= \''.$this->ProdInfo['id'].'\' ';	
			
			$arr = GetAll($sql);
			if(is_array($arr)){
				foreach ($arr as $key => $value){
					
					if($value['user_choice']){
						if(!$this->ProdInfo['options'][$value['option_id']]){
							$this->ProdInfo['options'][$value['option_id']] = $value;
							$this->ProdInfo['options'][$value['option_id']]['variants'] = array();
						}
						$this->ProdInfo['options'][$value['option_id']]['variants'][$value['variant_id']] = array(
							'addon'		=> $value['addon'],
							'default'	=> $value['default']
						);
					}else{
						if($value['element'] == 'select' || $value['element'] == 'radio'){
							$value['value'] = $value['variant_id'];
						}
						$this->ProdInfo['options'][$value['option_id']] = $value;
					}
					
				}
			}
			
			$this->PageInfo['translation']['ru'] = GetRow('SELECT * FROM products_translation WHERE obj_id = '.$this->ProdInfo['id'].' AND lang = \'ru\''); 
			$this->PageInfo['translation']['ua'] = GetRow('SELECT * FROM products_translation WHERE obj_id = '.$this->ProdInfo['id'].' AND lang = \'ua\'');
			$this->PageInfo['translation']['en'] = GetRow('SELECT * FROM products_translation WHERE obj_id = '.$this->ProdInfo['id'].' AND lang = \'en\'');
		}
		
		private function _update_product($id, $data) {
		
			if (!$data['url:str'])	{
				$data['url:str'] = _translit($data['translation']['ru']['title:str']);	
			}else{
				$data['url:str'] = str_replace(" ","_",$data['url:str']);
			}
		    // $data['url:str'] = str_replace("/","-",$data['url:str']);
			$data['url:str'] = $this->_make_correct_url($data['url:str']);
			
			if ($data['videofile']){
				foreach ($data['videofile'] as $ke=>$it){
					if ($it){
						$sq = 'UPDATE `video` SET `title` = \''.trim($it).'\' WHERE product_id = \''.$id.'\' AND id = \''.$ke.'\' ';
						Execute($sq);
					}
				}
			}
			// _debug($data, 1);
			$this->_update($id, $data, 'products', array('Event'));
			if( is_array( $data['translation'] ) && $data['translation'] ){
				foreach ( $data['translation'] as $key => &$value){
					$value['lang:enum'] = $key;
					if($this->_is_translation_exist($id, $key, 'products_translation')){
						$this->_update_translation($id, $value, 'products_translation', array(), $value['lang:enum']);
					}else{
						$value['obj_id:int'] = $id;
						$this->_insert($value, 'pages_translation');
					}	
				}
			}
			$this->_save_options($id, $data);
			
		}
		
		private function _insert_product($data) {
			
			if (!$data['url:str'])	{
				$data['url:str'] = _translit($data['translation']['ru']['title:str']);	
			}
			$data['create_ts:int'] = time();
			// $data['url:str'] = str_replace("/","-",$data['url:str']);
			$data['url:str'] = $this->_make_correct_url($data['url:str']);
			
			$id =  $this->_insert($data, 'products', array('Event'));
			if( is_array( $data['translation'] ) && $data['translation'] ){
					foreach ( $data['translation'] as $key => &$value){
						$value['lang:enum'] = $key;
						$value['obj_id:int'] = $id;
	
						$this->_insert($value, 'products_translation');
					}
			}
			$this->_save_options($id, $data);
			return $id;
			
		}
		
		protected function _get_cat_options($id, $group = true, $for_ver=false){
			/*
			if($for_ver) $add = ' AND po.`for_version` = \'1\' ';	
		
			$this->_get_parent_id($id);
			if($this->ParentId){
				 $id = $this->ParentId;
			} */
			
			global $DB;
			
			/*$sql = 'SELECT 
						po.*, 
						pog.title as group_title, 
						pog.id as group_id 
					FROM 
						product_option_type pot 
					LEFT JOIN product_options as po ON po.id = pot.option_id 
					LEFT JOIN product_options_groups pog ON pog.id = po.type_id 
					WHERE 
						pot.category_id = '.$id.$add.' AND 
						po.`title` not like \'%размер%\' 
					ORDER BY pog.sort_order, po.order_by
			';*/
			$sql = 'SELECT 
						po.*, 
						pog.title as group_title, 
						pog.id as group_id 
					FROM 
						product_option_type pot 
					LEFT JOIN product_options as po ON po.id = pot.option_id 
					LEFT JOIN product_options_groups pog ON pog.id = po.type_id 
					WHERE 
						pot.category_id = '.$id.$add.' 
					ORDER BY pog.sort_order, po.order_by
			';
			// _debug($sql,1);
			$res = $DB->GetAll($sql);
		//	_debug($res,1);
			if($res){
				foreach($res as $option){
					$sql = 'SELECT 
								* 
							FROM 
								product_option_variants
							WHERE 
								option_id = '.$option['id'].'
							ORDER BY
								sort
					';
					
					$option['variants'] = $DB->GetAll($sql);
					if($group){
						if($option['user_choice']){
							$options['user'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['user'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}else{
							$options['all'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['all'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}
					}else{
						$options[$option['id']] = $option;
					}
					
					
				}
			}

			return $options;
		}
		
		private function _save_options($id,$data){
			//_debug($data['option']);
			
			Execute("
				DELETE FROM 
					products_variants
				WHERE 
					product_id = $id AND `group`     != 'color-size'
			");
			
			$options = $this->_get_cat_options(intval($data['category_id:int']), false);
			
			
			if($data['option'] && is_array($data['option'])){
				
				foreach ($data['option'] as $key => $val){

					if($val){
						$sql = "
							INSERT INTO 
								products_variants
							SET 
								option_id 	= $key, 
								product_id	= $id 
						";
						// _debug($sql, 1);
						if($options[$key]['element'] == 'text' || $options[$key]['element'] == 'checkbox'){
							$sql .= ', value = \''.trim($val).'\'';
						}else{
							$sql .= ', variant_id = \''.$val.'\'';
						}
							
						Execute($sql);
					}	
				}
			}
			
			if($data['variants'] && is_array($data['variants'])){
				
				foreach ($data['variants'] as $key => $val){

					if($val && is_array($val)){
					
						foreach ($val as $variant => $addon){

							if($variant){

								$addon = floatval($addon);
								
								$default = $data['default_variant'][$key] == $variant ? 1 : 0; 
								
								$sql = "
									INSERT INTO 
										products_variants
									SET 
										option_id	= $key, 
										product_id	= $id,
										variant_id	= $variant,
										addon		= '$addon',
										`default`	= $default 
								";
								
								Execute($sql);
							}	
							
						}
					}
				}
			}
			// _debug($options,1);	
		}
				
		private function _check_data($data, $update = false) {
			
			
			/*$this->Errors['url'] = $data['url:str'] ? false : true;*/
			if($data['url:str']){
				$addQ = ($_GET['id']) ? ' `id` != \''.$_GET['id'].'\' AND ' : '';
				$sql = 'SELECT id FROM products WHERE '.$addQ.' url = \''.$data['url:str'].'\'';
				$id = GetOne($sql);
				if($id){
					$this->Errors['url'] = true;
				}
			}

			
			$this->ShowError = in_array(true, $this->Errors);
			
			return !$this->ShowError;
		}
		
		private function _load_products($cat_id) {
			
			global $DB;
			
			$where = ' 1=1 ';
			
			if($cat_id) {
				
				$where .= ' AND (p.category_id=\''.$cat_id.'\' ';
				
				$this->_load_cataegories_id($cat_id);
				foreach($this->CatIdList as $cat) {
					
					$where .= ' OR p.category_id=\''.$cat['id'].'\' ';
				}
				
				$where .= ' ) ';
			}
			
			
			
			if($_GET['code']){

				$where .= ' AND p.code = \''.$_GET['code'].'\'';
			}
			
			if($_GET['brand']){

				$where .= ' AND p.brand_id = \''.$_GET['brand'].'\'';
			}
			
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM products p 
					WHERE '.$where;
			
			$res = $DB->GetRow($sql);
			
			if($_GET['els'] == 'all'){
				$no_pager = true;
			}else{
				$items_per_page = intval($_GET['els']) ? intval($_GET['els']) : 20;
				$max_pages_cnt = 9;
				$page = intval($_GET['page']);
				if($page < 1){
					
					$page = 1;
				}
				
				AttachLib('PageNavigator');
				$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'products/?cat='.$_REQUEST['cat'].'&brand='.$_GET['brand'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'].'&els='.$_GET['els'], $page, '&page=%s', '', true, true);
			}
			
			$order = 'ORDER BY p.`publish` DESC, p.`new` DESC, p.`create_ts` DESC';
			if($_GET['sort'] && $_GET['direct']){
				$order = 'ORDER BY '.mysql_escape_string($_GET['sort']).' '.mysql_escape_string($_GET['direct']);
			}
			
			   $sql = 'SELECT 	p.*, pt.title as title,
							c.title as category
					FROM products p
						LEFT JOIN categories c ON (c.id = p.category_id)
						LEFT JOIN products_translation pt ON (p.id = pt.obj_id AND lang = \'ru\')
						
					WHERE '.$where.'
					'.$order.' '.( $no_pager ? '' : 'LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page );
			// if($_GET['alex']) _debug($sql, 1);
			$this->Products = $DB->GetAll($sql);
			$this->Products_sz = count($this->Products);
		}
		
		private function _init_grid($cat_id = 0) {
			
			$this->_load_products($cat_id);
			
			$this->AttachComponent('GridAll', $this->Grid);
			
			$titles = array( 'Заголовок',  'Категория', 'Опубликован'/*, 'На главной'*/);
			$options['row_numbers'] = false;
			$options['sortable'] = array(	
				0 => array(	'up' => $this->RootUrl.'products/?sort=pt.title&direct=ASC'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&page='.$_GET['page'].'&els='.$_GET['els'], 
							'down' => $this->RootUrl.'products/?sort=pt.title&direct=DESC'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&page='.$_GET['page'].'&els='.$_GET['els']),
												
				1 => array(	'up' => $this->RootUrl.'products/?sort=c.title&direct=ASC'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&page='.$_GET['page'].'&els='.$_GET['els'], 
							'down' => $this->RootUrl.'products/?sort=c.title&direct=DESC'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&page='.$_GET['page'].'&els='.$_GET['els'])
			);
			
			if($this->Perm['edit']) {
				$options['controls'] = array(
					'edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'].'&els='.$_GET['els'].'&page='.$_GET['page'], 
					'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s'.'&cat='.$_GET['cat'].'&brand='.$_GET['brand'].'&sort='.$_GET['sort'].'&direct='.$_GET['direct'].'&els='.$_GET['els'].'&page='.$_GET['page']);
			}
			
			$options['multiply'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать', 'UnPublished'=> 'Скрыть', 'DeleteSelected'=> 'Удалить');
			
			$rows = array();
			for($i = 0; $i < $this->Products_sz; $i++){
				
				$active = $this->Products[$i]['publish'] ? '<div id="container'.$this->Products[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->Products[$i]['id'].', 0, \'products\', \'publish\')">Да</span></div>' : '<div id="container'.$this->Products[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->Products[$i]['id'].', 1, \'products\', \'publish\')">Нет</span></div>';
				// $new = $this->Products[$i]['new'] ? '<div id="container'.$this->Products[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->Products[$i]['id'].', 0, \'products\', \'new\', \'container'.$this->Products[$i]['id'].'_'.$i.'\')">Да</span></div>' : '<div id="container'.$this->Products[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->Products[$i]['id'].', 1, \'products\', \'new\', \'container'.$this->Products[$i]['id'].'_'.$i.'\')">Нет</span></div>';
				
				$row = array(	
								$this->Products[$i]['title'], 
							 	$this->Products[$i]['category'],
							 	$active/*,
							 	$new*/
							 );
				$rows[$this->Products[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	
		private function _load_cataegories($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT id, 
						   title 
					FROM categories 
					WHERE parent_id=\''.$parent_id.'\'';
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['display_name'] = str_repeat('&nbsp;', $level*4).$cat[$i]['title'];
				$cat[$i]['level'] = $level;
				array_push($this->Categories, $cat[$i]);
				$level++;
				$this->_load_cataegories($cat[$i]['id']);
				$level--;
			}
			
			if($parent_id == 0) {
				
				$this->Categories_sz = count($this->Categories);
			}
		}

		private function _load_cataegories_id($parent_id) {
			
			global $DB;
			
			$sql = 'SELECT id 
					FROM categories 
					WHERE parent_id=\''.$parent_id.'\'';
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				array_push($this->CatIdList, $cat[$i]);
				$this->_load_cataegories_id($cat[$i]['id']);
			}
		}
		
		
		private function SaveImage(){
			$bool = false;
			switch ($_FILES['preview_image']['type']){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':					
				case 'image/x-png':
				case 'image/png':
				case 'image/gif':
					break;
				default:
					return false;
				
		    }
			global $DB;
			
			$path = $this->GetConfigParam('absolute-path').'/public/files/images/products/';
			@mkdir($path,0777);
			$ext = explode(".",$_FILES['preview_image']['name']);
			$md5 = md5($_FILES['preview_image']['name']+time()).'.'.end($ext);
			$this->MD5FileName = $md5;
			
			$file = $path.'/'.$md5;
			
			$file_small = $path.'/thumb-'.$md5;
			
			if(move_uploaded_file($_FILES['preview_image']['tmp_name'],$file)){
				$bool = $this->resize($file,$_FILES['preview_image']['type'],$this->LHeight2, $this->LWidth2);
			}
			if(copy($file,$file_small)){
				$bool1 = $this->resize($file_small,$_FILES['preview_image']['type'],$this->LHeight1, $this->LWidth1);
			}
			return $bool && $bool1;		
		}
		
	private function _load_photos($id) {
			global $DB;
			
			$sql = "SELECT * 
					FROM `photos` 
					WHERE product_id = '$id' 
					ORDER BY `order_by`
					";
			
			$description = $DB->GetAll($sql);
			$description_sz = count($description);
		
			$this->AttachComponent('Grid', $this->GridPhoto);

			$titles = array( 'Фото', 'Порядок', 'По умолчанию', 'Слайдер', 'Особенности', 'Название');
			
			$options['row_numbers'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=RemovePhoto&amp;id='.$id.'&amp;photo_id=%s&cat='.$_GET['cat'].'&page='.$_GET['page']);
			}
			$rows = array();
			
			for($i = 0; $i < $description_sz; $i++){
					$pdf1 = explode('.',$description[$i]['filename']);
					
					$row = array(($pdf1[1] == "pdf" || $pdf1[1] == "PDF") ? 
								'<a href="/public/files/images/products/'.$description[$i]['filename'].'"> Посмотреть файл </a>' :
								'<span style="text-align:center; display:block;"><img width="100px" src="/public/files/images/products/middle-'.$description[$i]['filename'].'" alt="" /></span>',
								'<span style="text-align:center; display:block;">
									<input onBlur="UpdatePhotosField('.$description[$i]['id'].', \'order_by\', $(this).value)" type="text" '.($description[$i]['order_by'] ? 'value="'.$description[$i]['order_by'].'"' : '').' name="order_by" />
								</span>',
								'<span style="text-align:center; display:block;"><input onclick="SetDefault('.$description[$i]['product_id'].','.$description[$i]['id'].',\''.$description[$i]['filename'].'\')" value="'.$description[$i]['id'].'" type="radio" '.($description[$i]['default'] ? 'checked="checked"' : '').' name="default"  /></span>',
								'<span style="text-align:center; display:block;">
									<input onclick="SwitchPhotosField('.$description[$i]['id'].', \'slider\')" type="checkbox" '.($description[$i]['slider'] ? 'checked="checked"' : '').' name="slider"  />
								</span>',
								'<span style="text-align:center; display:block;">
									<input onclick="SwitchPhotosField('.$description[$i]['id'].', \'features\')" type="checkbox" '.($description[$i]['features'] ? 'checked="checked"' : '').' name="features"  />
								</span>',
								'<span style="text-align:center; display:block;">
									<input onBlur="UpdatePhotosField('.$description[$i]['id'].', \'title\', $(this).value)" type="text" '.($description[$i]['title'] ? 'value="'.$description[$i]['title'].'"' : '').' name="title"  />
								</span>'
								);
								
				$tmp = '
								<span style="text-align:center; display:block;">
								<input onclick="SetPdf('.$description[$i]['id'].')" type="checkbox" '.($description[$i]['garantie'] ? 'checked="checked"' : '').' name="plan"  />
								</span>';
				
					$rows[$description[$i]['id']] = $row;
			}

			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows, 'noform' => true);
			$this->GridPhoto->SetData($data);
		}
		
		private function _load_video($id) {
			global $DB;
			
			$sql = "SELECT *
					FROM video
					WHERE product_id=".$id;
			
			$description = $DB->GetAll($sql);
			$description_sz = count($description);
		
			$this->AttachComponent('Grid', $this->GridVideo);

			$titles = array( 'Фото','По умолчанию',  'Код видео');
			
			$options['row_numbers'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=RemoveVideo&amp;id='.$id.'&amp;photo_id=%s&cat='.$_GET['cat'].'&page='.$_GET['page']);
			}
			$rows = array();
			
			for($i = 0; $i < $description_sz; $i++){
					$row = array(
								'<span style="text-align:center; display:block;"><img width="100px" src="/public/files/images/products/middle-'.$description[$i]['filename'].'" alt="" /></span>',
								'<span style="text-align:center; display:block;"><input onclick="SetDefaultVideo('.$description[$i]['product_id'].','.$description[$i]['id'].',\''.$description[$i]['filename'].'\')" value="'.$description[$i]['id'].'" type="radio" '.($description[$i]['default'] ? 'checked="checked"' : '').' name="defaultvideo"  /></span>',
								'<textarea rows="10" cols="40" name="videofile['.$description[$i]['id'].']">'.$description[$i]['title'].'</textarea>'
								);
					$rows[$description[$i]['id']] = $row;
			}

			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows, 'noform' => true);
			$this->GridVideo->SetData($data);
		}
	
	}
?>