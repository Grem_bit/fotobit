<?
	class Users extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir		= 'users/';
		protected $BList				= null;
		protected $UsersOrderName		= array('accepted'=>'Принят','dostavka_newpost'=>'Доставка новой почтой','dostavka_kyrer'=>'Доаставка курьером' ,'dostavka_punkt'=>'Доставка в пункт выдачи','dostavlen_punkt'=>'Доставлен в пункт выдачи' ,'dostavlen'=>'Доставлен','dostavka'=>'Доставка' ,'inprint'=>'В печати' ,'delivered'=>'Доставлен' ,'wait_pay'=>'Ожидает оплаты' ,'paid'=>'Оплачен' ,'cancelled'=>'Отменён' , 'wait'=>'Ожидание', 'new'=>'Новый' ,'printed'=>'Напечатан', 'not_order'=>'Не оформлен' , 'not_taken'=>'Не Забрали', 'took'=>'Забрали', 'rejected_user'=>'Откланен пользователем', 'rejected'=>'Откланен', 'sent'=>'Отправлен', 'made'=>'Выполнен');
		protected $Users				= array();
		protected $Users_sz				= 0;
		protected $UserInfo				= array();
		protected $PageNavigator		= null;
		
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->Cities = $this->getCities();
			$this->Cities_sz = count($this->Cities);
			$this->Shops = $this->getShops();
			
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			
			$this->SetTemplate('main.html');
			$this->_init_users_grid();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			$_POST['create_ts:str'] = time();
			$_POST['birthday:str'] = strtotime($_POST['birthday:str']);
			$_POST['date_purchase:str'] = strtotime($_POST['date_purchase:str']);
			
			if(!$this->_check_data($_POST)){
				$this->OnNew();
			}else{
				$this->_insert_user($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$sql = 'DELETE FROM users WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);
			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			
			$sql = 'SELECT * FROM users WHERE id=\''.intval($_GET['id']).'\'';
			$this->UserInfo = $DB->GetRow($sql);
			$sql=$sql = "SELECT state,total,ts FROM `orders` WHERE buyer_id='".intval($_GET['id'])."' ";
			$this->UserOrderInfo = $DB->GetAll($sql);
			$sql=$sql = "SELECT * FROM `order_info` WHERE user_id='".intval($_GET['id'])."' ";
			$this->UserOrderInfoDel = $DB->GetAll($sql);
			if($this->UserInfo['shop_id'] && !$this->UserInfo['shop_title']){
				$shop_id = $this->UserInfo['shop_id'];
				$sql = "SELECT `title` FROM `shops` WHERE `id` = '$shop_id'";
				$this->UserInfo['shop_title'] = GetOne($sql);
			}
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			$_POST['create_ts:str'] = strtotime($_POST['create_ts:str']);
			$_POST['birthday:str'] = strtotime($_POST['birthday:str']);
			$_POST['date_purchase:str'] = strtotime($_POST['date_purchase:str']);
			if(!$this->_check_data($_POST, true)) {
				$this->OnEdit();
			}else {
				$this->_update_user(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
	
		/*
		 * Private methods
		 */
		
		private function _update_user($id, $data) {
			
			$this->_update($id, $data, 'users');
		}
		
		private function _check_data($data, $update = false) {
			
			/*$this->Errors['login'] = trim($data['login:str']) ? false : true;
			
			if(!$this->Errors['login']) {
				
				global $DB;
				$sql = 'SELECT COUNT(*) AS cnt FROM users WHERE login=\''.$data['login:str'].'\'';
				$res = $DB->GetRow($sql);
				
				if($update && $res['cnt'] > 1) {
					
					$this->Errors['login'] = true;
				}
				elseif(!$update && $res['cnt'] > 0) {
					
					$this->Errors['login'] = true;
				}
			}*/
			
			/*if((!$update && !$data['password:pass']) || ($data['password:pass'] != $data['confirm'])) {
				
				$this->Errors['password']= true;
				$this->Errors['confirm']= true;
			}*/
			
			//$this->Errors['fio'] = trim($data['fio:str']) ? false : true;
			// $this->Errors['name'] = trim($data['name:str']) ? false : true;
			$this->Errors['email'] = !$this->ValidEmail($data['email:str']);
		
			$this->ShowError = in_array(true, $this->Errors);

			return !$this->ShowError;
		}
		
		private function getCities(){
		
			$sql = "SELECT DISTINCT `city` AS 'title' FROM `users` ORDER BY `title`";
			$cities = GetAll($sql);
			
			return $cities;
		}
		
		private function getShops(){
		
			$sql = "SELECT `title` AS 'title' FROM `shops` ORDER BY `title`";
			$res1 = GetAll($sql);
			
			$sql = "SELECT DISTINCT `shop_title` AS 'title' FROM `users` WHERE `shop_title` <> '' ORDER BY `title`";
			$res2 = GetAll($sql);
			
			$res = array_merge($res1, $res2);
			
			return $res;
		}
		
		private function _load_users(){
			global $DB;
			
			// _debug($_GET, 1);
			if($_GET){
				$where = '';
				foreach($_GET as $key => $item){
					if($item){
						if($key == 'Event' || $key == 'id' || $key == 'page'){
						
						}elseif($key == 'shop'){
							$sql = "SELECT `id` FROM `shops` WHERE `title` = '$item'";
							$shop_id = GetOne($sql);
							if($shop_id){
								$where .= " AND u.`shop_id` = '$shop_id'";
							}else{
								$where .= " AND u.`shop_title` = '$item'";
							}
						}elseif($key == 'date_purchase'){
							$ts1 = strtotime($item);
							$ts2 = $ts1 + 60*60*24 - 1;
							$where .= " AND u.`create_ts` BETWEEN '$ts1' AND '$ts2'";
						}elseif($key == 'create_ts_from'){
							$ts = strtotime($item);
							$where .= " AND u.`create_ts` >= '$ts'";
						}elseif($key == 'create_ts_to'){
							$ts = strtotime($item) + 60*60*24 - 1;
							$where .= " AND u.`create_ts` <= '$ts'";
						}else{
							$where .= " AND u.`$key` LIKE '%$item%'";
						}
					}
				}
			}
			
			$sql= 'SELECT COUNT(*) AS cnt FROM users  WHERE 1 = 1 '.$where.'';
			$res = $DB->GetRow($sql);
			$this->CntItems = $res['cnt'];
			$items_per_page = 20;
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){
				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'users/', $page, '?page=%s');
			
			$sql = "SELECT u.*, IF(u.`shop_id` != '', sh.`title`, u.`shop_title`) AS 'shop_title' 
					FROM users u 
					LEFT JOIN `shops` sh ON (sh.`id` = u.`shop_id`) 
					WHERE '1' = '1' $where 
					LIMIT ".(($page - 1) * $items_per_page).", ".$items_per_page;
			// _debug($sql, 1);
			$this->Users = $DB->GetAll($sql);
			$this->Users_sz = count($this->Users);
		}
		
		private function _init_users_grid(){
			
			$this->_load_users();
			
			$this->AttachComponent('Grid', $this->BList);
			
			$titles = array(/*'Логин', */'ФИО'/*, 'Город', 'Дата покупки', 'Дата регистрации', 'Место покупки'*/, 'E-mail', 'Активен'/*, 'Рассылка'*/, 'Примечания');
			$options['row_numbers'] = true;
			
			if($this->Perm['edit']) {
				$options['controls'] = array(
					'edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s&page='.$_GET['page'], 
					'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s&page='.$_GET['page']
				);
			}
			
			$rows = array();
			for($i = 0; $i < $this->Users_sz; $i++){
				
				$active = $this->Users[$i]['active'] ? 'Да' : 'Нет';
				$subscribe = $this->Users[$i]['subscribe'] ? 'Подтверждено' : 'Не подтверждено';
				
				$row = array(
					// $this->Users[$i]['login'],
					// $this->Users[$i]['fio'],
					$this->Users[$i]['surname'].' '.$this->Users[$i]['name'].' '.$this->Users[$i]['patronymic'],
					// $this->Users[$i]['city'],
					// date('d.m.Y', $this->Users[$i]['date_purchase']),
					// date('d.m.Y', $this->Users[$i]['create_ts']),
					// $this->Users[$i]['shop_title'],
					$this->Users[$i]['email'],
					$active,
					// $subscribe,
					$this->Users[$i]['note']
				);
				$rows[$this->Users[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->BList->SetData($data);
		}
	
		
		
	}
?>