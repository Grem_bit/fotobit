<?
class Pages extends ContentPage {
		protected $TemplatesBaseDir			= 'pages/';
		protected $TableName				= 'pages';
		protected $ModuleName				= 'pages';
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $GridComments				= null;
		protected $GridPhotos				= null;
		protected $Pages					= array();
		protected $Pages_sz					= 0;
		protected $PageInfo					= array();
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		protected $CatIdList				= array();
		protected $DescriptionInfo			= array();
		/*
		 * Public methods
		 */
		public function OnDefault() {
			$this->SetTemplate('main.html');
			$this->_init_grid();
			
		}

		public function OnNew() {
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->SetTemplate('edit.html');			
			//$_POST['videos'] = implode(",",$_POST['videos_ids']); 						
			
			$this->_load_pages();		
		}

		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				$this->OnNew();
			}
			else {
				$id = $this->_insert_page($_POST, $_FILES);
				if($_POST['redirect']) {
					header('Location: /azone/pages/?Event='.$_POST['redirect'].'&id='.$id);
				}else {
					$this->OnDefault();
				}
			}
		}

		public function OnRemove() {			
			global $DB;			
			$sql = 'DELETE FROM pages 					

					WHERE id=\''.intval($_GET['id']).'\'';

			$DB->Execute($sql);	

			$DB->Execute('DELETE FROM pages_translation WHERE obj_id = '.$_GET['id']);
			
			$this->OnDefault();

		}
		
		public function OnDelFile() {
			global $DB, $config;			
			
			$this->_load_articles_info($_GET['id'], intval($_GET['lang_id']));
			
			@unlink($config['absolute-path'].'public/files/files/documents/'.$this->ArticlesInfo['filename']);
			
			$sql = 'UPDATE documents 
					SET filename	= \'\' 
					WHERE id=\''.$_GET['id'].'\'';

			$DB->Execute($sql);
			
			header('Location: '.$this->RootUrl.'documents/?Event=Edit&id='.$_GET['id']);
		}
		public function OnSetDefault() {
			$this->SendAjaxHeaders();
			$sql = 'UPDATE photos
					SET `default` = 0
					WHERE is_news = 0 AND product_id = \''.$_GET['id'].'\'';
			Execute($sql);
			
			$sql = 'UPDATE photos
					SET `default` = 1
					WHERE id = \''.$_GET['photo'].'\'';
			Execute($sql);
			exit;
		}
		
		
		protected function OnRemovePhoto(){
			global $DB,$config;						
			$sql = 'SELECT *
					FROM photos
					WHERE id = \''.(int)$_GET['photo_id'].'\'';
			$res = $DB->GetRow($sql);			
			@unlink($config['absolute-path'].'public/files/images/gallery/'.$res['filename']);			
			$sql = "DELETE FROM photos
					WHERE id = '".(int)$_GET['photo_id']."'";
			$DB->Execute($sql);			
			header("Location: /azone/pages/?Event=Edit&id=".(int)$_GET['id']);
		}
		public function OnAddPhoto() {
			$this->SetTemplate('editfoto.php');
			$this->AddJS('/core/lib/fancy/Swiff.Uploader.js');
			$this->AddJS('/core/lib/fancy/Fx.ProgressBar.js');
			$this->AddJS('/core/lib/fancy/FancyUpload2.js');
			$this->AddCSS('/core/lib/fancy/fancy.css');	
			$this->AddJS($this->RootUrl.'public/js/fotoobjects.js');
		}
		private function DeleteFile($name) {
			global $DB, $config;			
			
			@unlink($config['absolute-path'].'public/files/files/documents/'.$name);
		}

		public function OnEdit() {
			$this->_load_photos(intval($_GET['id']));
			$this->_load_comments(intval($_GET['id']));
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->_load_page_info(intval($_GET['id']));
			$this->_load_pages();
			$this->SetTemplate('edit.html');
		}

		protected function OnGetPages(){
			global $DB;
			$sql = "SELECT pt.title, p.* 
					FROM pages p 
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = 'ru') 
					WHERE p.publish = 1";
			
			$man = $DB->GetAll($sql);			
			echo $this->json_safe_encode(array('pages' => $man, 'cnt' => count($man)));
			exit;
			
		}
		
		public function OnUpdate() {
			if(!$this->_check_data($_POST, true)) {
				$this->OnEdit();
			}
			else {
				$this->_update_page(intval($_GET['id']), $_POST, $_FILES);
				header('Location: /azone/pages/');				
			}
		}
		/*
		 * Private methods
		 */

		private function _load_page_info($id) {
			global $DB;
			$sql = 'SELECT * FROM pages WHERE id=\''.$id.'\'';
			$this->PageInfo = $DB->GetRow($sql);			
			
			$sql = 'SELECT m.* 
					FROM bindings b
					INNER JOIN video m ON (b.to_obj = \'page\' AND b.to_id = \''.$id.'\' AND b.from_obj = \'video\' AND m.id = b.from_id)	
					';		
			$videos = $DB->GetAll($sql);
			$m_id = array();
			if($videos){
				foreach($videos as $item=>$el){
					$m_id[] = $el['id'];			
				}
			}			
			$this->PageInfo['videos_ids'] = implode(",",$m_id); 			
			$this->PageInfo['videos'] = $videos; 
			
			//////
			$sql = 'SELECT m.* 
					FROM bindings b
					INNER JOIN video_alboms m ON (b.to_obj = \'page\' AND b.to_id = \''.$id.'\' AND b.from_obj = \'albums\' AND m.id = b.from_id)	
					';		
			$alboms = $DB->GetAll($sql);
			$a_id = array();
			if($alboms){
				foreach($alboms as $item=>$el){
					$a_id[] = $el['id'];			
				}
			}			
			$this->PageInfo['$alboms_ids'] = implode(",",$a_id); 			
			$this->PageInfo['alboms'] = $alboms; 
			//////

			
			$sql = 'SELECT p.*						
					FROM bindings b
					INNER JOIN pages p ON (b.to_obj = \'page\' AND b.to_id = \''.$id.'\' AND b.from_obj = \'page\' AND p.id = b.from_id)
					';
			$pages = $DB->GetAll($sql);
			$p_id = array();
			if($pages){
				foreach($pages as $item=>&$el){
					$p_id[] = $el['id'];	
					$el['create_ts'] = date("d/m/y H:i",$el['create_ts']);		
				}
			}			
			$this->PageInfo['pages_ids'] = implode(",",$m_id); 			
			$this->PageInfo['pages'] = $pages; 
			
			$this->PageInfo['translation']['ru'] = GetRow('SELECT * FROM pages_translation WHERE obj_id = '.$this->PageInfo['id'].' AND lang = \'ru\''); 
			$this->PageInfo['translation']['ua'] = GetRow('SELECT * FROM pages_translation WHERE obj_id = '.$this->PageInfo['id'].' AND lang = \'ua\'');
			$this->PageInfo['translation']['en'] = GetRow('SELECT * FROM pages_translation WHERE obj_id = '.$this->PageInfo['id'].' AND lang = \'en\'');
		}

		private function _update_page($id, $data, $file) {
			global $DB;
			//_debug($file, 1);
			if(intval($data['parent_id']) == 0 && intval($data['front_default']) == 1){
				$DB->Execute('UPDATE pages SET front_default = 0');
			}
			if ($file['filename']['tmp_name'] != ''){
			  $this->SaveImage('filename');
			  if($this->MD5FileName){
				  $data['filename'] = $this->MD5FileName;
				 
			  }
			}
			if ($file['filename_en']['tmp_name'] != ''){
			  $this->SaveImage('filename_en');
			  if($this->MD5FileName){
				  $data['filename_en'] = $this->MD5FileName;
				 
			  }
			}
			$publish_ts = $data['publish_ts'];			
			$publish_ts = explode(" ",$publish_ts);
			$publish_ts[0] = explode(".",$publish_ts[0]);
			$publish_ts[1] = explode(":",$publish_ts[1]);
			
			$pt = mktime($publish_ts[1][0],$publish_ts[1][1],0,$publish_ts[0][1],$publish_ts[0][0],$publish_ts[0][2]);
			
			$sql = 'UPDATE pages SET 
						parent_id=\''.intval($data['parent_id']).'\', 
						publish=\''.intval($data['publish']).'\', 
						show_in_map=\''.intval($data['show_in_map']).'\', 
						closed_page=\''.intval($data['closed_page']).'\',
						url=\''.s($data['url']).'\',
						code = \''.s($data['code']).'\', 
						icon = \''.s($data['icon']).'\', 
						'.($data['filename'] ? 'filename = \''.s($data['filename']).'\',': '').'
						'.($data['filename_en'] ? 'filename_en = \''.s($data['filename_en']).'\',': '').'
						filename_big = \''.s($data['filename_big']).'\',
						view_list  = \''.intval($data['view_list']).'\',
						view_album  = \''.intval($data['view_album']).'\',
						show_comments  = \''.intval($data['show_comments']).'\',
						publish_ts=\''.$pt.'\', 
						front_header_menu = \''.intval($data['front_header_menu']).'\',
						front_default = \''.intval($data['front_default']).'\',
						front_left = \''.intval($data['front_left']).'\',
						front_center = \''.intval($data['front_center']).'\',
						front_bottom = \''.intval($data['front_bottom']).'\',
						front_right = \''.intval($data['front_right']).'\',
						front_with_submenu = \''.intval($data['front_with_submenu']).'\',
						front_link_block = \''.intval($data['front_link_block']).'\',
						header_menu = \''.intval($data['header_menu']).'\',
						footer_menu = \''.intval($data['footer_menu']).'\',
						two_columns = \''.intval($data['two_columns']).'\',
						one_column = \''.intval($data['one_column']).'\',
						top_link = \''.s($data['top_link']).'\',
						top_link_img = \''.s($data['top_link_img']).'\',
						page_style = \''.s($data['page_style']).'\',
						red_link = \''.intval($data['red_link']).'\'
					WHERE id=\''.$id.'\'';					
			
			$DB->Execute($sql);
			
			
			if( is_array( $data['translation'] ) && $data['translation'] ){
				foreach ( $data['translation'] as $key => &$value){
					
					$value['lang:enum'] = $key;
					
					if($this->_is_translation_exist($id, $key, 'pages_translation')){
						$this->_update_translation($id, $value, 'pages_translation', array(), $value['lang:enum']);
						
					}else{
						
						$value['obj_id:int'] = $id;
						
						$this->_insert($value, 'pages_translation');
					}	
				}
			}
			//exit();
		}

		private function _insert_page($data,$file) {
			global $DB;
			if ($file['filename']['tmp_name'] != ''){
			  $this->SaveImage();
			  if($this->MD5FileName){
				  $data['filename'] = $this->MD5FileName;
			  }
			}
			if(intval($data['parent_id']) == 0 && intval($data['front_default']) == 1){
				$DB->Execute('UPDATE pages SET front_default = 0');
			}
			
			$sql = "SELECT MAX(order_by) AS m_order_by 
					FROM pages WHERE parent_id = '".intval($data['parent_id'])."'";
			$res = $DB->GetOne($sql);
			$res++;
			$publish_ts = $data['publish_ts'];			
			$publish_ts = explode(" ",$publish_ts);
			$publish_ts[0] = explode(".",$publish_ts[0]);
			$publish_ts[1] = explode(":",$publish_ts[1]);
			
			$pt = mktime($publish_ts[1][0],$publish_ts[1][1],0,$publish_ts[0][1],$publish_ts[0][0],$publish_ts[0][2]);
			$sql = 'INSERT INTO pages 
					SET 
						parent_id=\''.intval($data['parent_id']).'\', 
						publish=\''.intval($data['publish']).'\', 
						show_in_map=\''.intval($data['show_in_map']).'\', 
						closed_page=\''.intval($data['closed_page']).'\',
						order_by = \''.intval($res).'\', 
						create_ts=\''.time().'\', 
						publish_ts=\''.$pt.'\', 
						url=\''.s($data['url']).'\', 
						code = \''.s($data['code']).'\', 
						icon = \''.s($data['icon']).'\', 
						filename = \''.s($data['filename']).'\',
						filename_big = \''.s($data['filename_big']).'\',
						front_header_menu = \''.intval($data['front_header_menu']).'\',
						front_default = \''.intval($data['front_default']).'\',						
						view_list  = \''.intval($data['view_list']).'\',
						view_album  = \''.intval($data['view_album']).'\',
						show_comments  = \''.intval($data['show_comments']).'\',						
						front_left = \''.intval($data['front_left']).'\',
						front_center = \''.intval($data['front_center']).'\',
						front_bottom = \''.intval($data['front_bottom']).'\',
						front_right = \''.intval($data['front_right']).'\',
						front_with_submenu = \''.intval($data['front_with_submenu']).'\',
						front_link_block = \''.intval($data['front_link_block']).'\',
						header_menu = \''.intval($data['header_menu']).'\',
						footer_menu = \''.intval($data['footer_menu']).'\',
						two_columns = \''.intval($data['two_columns']).'\',
						one_column = \''.intval($data['one_column']).'\',
						top_link = \''.s($data['top_link']).'\',
						top_link_img = \''.s($data['top_link_img']).'\',
						page_style = \''.s($data['page_style']).'\',
						red_link = \''.intval($data['red_link']).'\'
						';
			
			$DB->Execute($sql);		
			$sql = 'SELECT LAST_INSERT_ID() AS id FROM pages';
			$res = $DB->GetRow($sql);			
			
				
			if( is_array( $data['translation'] ) && $data['translation'] ){
				foreach ( $data['translation'] as $key => &$value){
					$value['lang:enum'] = $key;
					$value['obj_id:int'] = $res['id'];

					$this->_insert($value, 'pages_translation');
				}
			}
			//print_r($_POST);
		//	exit();
			return $res['id'];
		}
		
		private function _check_data($data, $update = false) {
			$this->Errors['url'] = $data['url'] ? false : true;
			//$this->Errors['title_ru'] = $data['translation']['ru']['title:str'] ? false : true;
			 //$this->Errors['content'] = $data['content'] ? false : true;
			$this->ShowError = in_array(true, $this->Errors);
			
			return !$this->ShowError;

		}

		private function _init_grid() {
			$this->_load_pages();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('Заголовок','Вкл/Выкл');
			$options['row_numbers'] = false;
			$options['sortable'] = array(	0 => array(	'up' => $this->RootUrl.'pages/?sort=pt.title&direct=ASC', 
														'down' => $this->RootUrl.'pages/?sort=pt.title&direct=DESC'),
											1 => array(	'up' => $this->RootUrl.'pages/?sort=p.publish&direct=ASC', 
														'down' => $this->RootUrl.'pages/?sort=p.publish&direct=DESC'
											
											),
			);						
			$options['multiply'] = true;
			$options['sort'] = true;
			$options['multiply_events'] = array('Published'=>'Опубликовать','UnPublished'=> 'Скрыть','DeleteSelected'=> 'Удалить');
			
			if($this->Perm['edit']) {
				$options['controls'] = array(	'edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     	'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s'/*,
												'meta' => $this->RedirectUrl.'?Event=EditMeta&popup=show&amp;id=%s'*/
											);
			}
			
			$rows = array();
			
			
			for($i = 0; $i < $this->Pages_sz; $i++){
				$public = $this->Pages[$i]['publish'] ? 
								'<a href="/azone/pages/?Event=PublicPage&id='.$this->Pages[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/pages/?Event=PublicPage&id='.$this->Pages[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';			
				$row = array($this->Pages[$i]['title'],
							 	$public
							 );
				$rows[$this->Pages[$i]['id']] = $row;
			}
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}

		private function _load_pages($parent_id = 0) {
			global $DB;
			static $level = 0;		
			$order = ' ORDER BY order_by ASC';
			if($_GET['sort'] && $_GET['direct']){
				$order = ' ORDER BY '.mysql_escape_string($_GET['sort']).' '.mysql_escape_string($_GET['direct']);
			}
			$sql = 'SELECT p.id, p.publish, pt.title 
					FROM pages p  
						LEFT JOIN pages_translation pt ON (p.id = pt.obj_id AND lang = \'ru\')
					WHERE p.parent_id=\''.$parent_id.'\' AND p.id != '.i($_GET['id']).$order ;
			
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);			
			for($i = 0; $i < $cat_sz; $i++) {
				$cat[$i]['title'] = str_repeat('&nbsp;', $level*4).$cat[$i]['title'];
				$cat[$i]['level'] = $level;
				array_push($this->Pages, $cat[$i]);
				$level++;
				$this->_load_pages($cat[$i]['id']);
				$level--;
			}
			if($parent_id == 0) {
				$this->Pages_sz = count($this->Pages);
			}
		}

		/* Extend methods */
		public function OnPublicPage(){
			global $DB;
			$sql = "UPDATE pages SET publish = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		public function OnPublicComment(){
			global $DB;
			$sql = "UPDATE comments SET publish = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		
		public function OnShowInMap(){
			global $DB;
			$sql = "UPDATE pages SET show_in_map = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			$DB->Execute($sql);
			exit();		
		}
		
		public function OnPublished(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE pages SET publish = 1 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				
			}
			header('Location: /azone/pages/');
		}
		public function OnUnPublished(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE pages SET publish = 0 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
			}
			header('Location: /azone/pages/');
		}
		
		public function OnDeleteSelected(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "DELETE FROM pages WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				$DB->Execute("DELETE FROM pages_translation WHERE obj_id IN (".implode(',', $ids).")");
			}
			// header('Location: /azone/pages/');
			$this->OnDefault();
		}
		
		public function OnSortUp() {
			global $DB;
			$sql='SELECT *
				  FROM pages
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM pages
					WHERE pages.order_by < \''.$model['order_by'].'\' AND pages.parent_id = \''.$model['parent_id'].'\'
					ORDER BY pages.order_by DESC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			
			if($model_2){
				$sql='UPDATE pages
					  SET pages.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE pages
				  	SET pages.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			
			}
			// $this->OnDefault();
			header('Location: /azone/'.$this->ModuleName.'/');
		}

		public function OnSortDown() {
			global $DB;
			$sql='SELECT *
				  FROM pages
				  WHERE id=\''.$_GET['id'].'\'';
			$model = $DB->GetRow($sql);
			$sql = 'SELECT *
					FROM pages
					WHERE pages.order_by > \''.$model['order_by'].'\'
					ORDER BY pages.order_by ASC
					LIMIT 1';
			
			$model_2 = $DB->GetRow($sql);
			if($model_2){
				$sql='UPDATE pages
					  SET pages.order_by=\''.$model_2['order_by'].'\'
				  	WHERE id=\''.$_GET['id'].'\'';
				$DB->Execute($sql);
				$sql='UPDATE pages
				  	SET pages.order_by=\''.$model['order_by'].'\'
				  	WHERE id=\''.$model_2['id'].'\'';
				$DB->Execute($sql);
			}
			// $this->OnDefault();
			header('Location: /azone/'.$this->ModuleName.'/');
		}
		
		/* Edit Meta */
		protected function OnEditMeta(){
			$this->_load_page_info(intval($_GET['id']));
			$this->SetTemplate('edit-meta.html');		
		}
		protected function OnUpdateMeta(){
			global $DB;
			$sql = "UPDATE pages 
					SET 
						page_title = '".mysql_escape_string($_REQUEST['page_title'])."', 
						url = '".mysql_escape_string($_REQUEST['url'])."',
						meta_keywords = '".mysql_escape_string($_REQUEST['meta_keywords'])."',
						meta_description = '".mysql_escape_string($_REQUEST['meta_description'])."'
					WHERE id = '".intval($_REQUEST['id'])."'
						";
			
			$DB->Execute($sql);
			echo '<script type="text/javascript">setTimeout("window.close()",3000)</script>';		
			exit();			
		}
		
		
		private function _load_photos($id) {
			global $DB;
			
			$sql = "SELECT *
					FROM photos
					WHERE is_news = 0 AND product_id=".$id;
			
			$description = $DB->GetAll($sql);
			$description_sz = count($description);
		
			$this->AttachComponent('GridAll', $this->GridPhoto);

			$titles = array( 'Фото','По умолчанию','Описание/рус','Описание/укр','Описание/англ');
			
			$options['row_numbers'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=RemovePhoto&amp;id='.$id.'&amp;photo_id=%s&cat='.$_GET['cat'].'&page='.$_GET['page']);
			}
			$rows = array();
			
			for($i = 0; $i < $description_sz; $i++){				
					$row = array('<span style="text-align:center; display:block;"><img src="/public/files/gallery/thumb-'.$description[$i]['filename'].'" alt="" /></span>',
								'<span style="text-align:center; display:block;"><input onclick="SetDefault('.$description[$i]['product_id'].','.$description[$i]['id'].')" value="'.$description[$i]['id'].'" type="radio" '.($description[$i]['default'] ? 'checked="checked"' : '').' name="default"  /></span>',
									'<textarea name="text_ru['.$description[$i]['id'].']">'.$description[$i]['text_ru'].'</textarea>',
									'<textarea name="text_ua['.$description[$i]['id'].']">'.$description[$i]['text_ua'].'</textarea>',
									'<textarea name="text_en['.$description[$i]['id'].']">'.$description[$i]['text_en'].'</textarea>'
								);
					$rows[$description[$i]['id']] = $row;
					
			}

			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows, 'noform' => true);
			$this->GridPhoto->SetData($data);
		}
		
		
		private function _load_comments($id) {
			global $DB;
			
			$sql = "SELECT *
					FROM comments
					WHERE page_id=".$id;
			
			$description = $DB->GetAll($sql);
			$description_sz = count($description);
		
			$this->AttachComponent('GridAll', $this->GridComments);

			$titles = array( 'Комментарий','Отправитель','Опубликовано','Дата отправки');
			
			$options['row_numbers'] = true;
			if($this->Perm['edit']) {
				$options['controls'] = array('remove' => $this->RedirectUrl.'?Event=RemovePhoto&amp;id='.$id.'&amp;photo_id=%s&cat='.$_GET['cat'].'&page='.$_GET['page']);
			}
			$rows = array();
			
			for($i = 0; $i < $description_sz; $i++){				
					$public = $description[$i]['publish'] ? 
								'<a href="/azone/pages/?Event=PublicComment&id='.$description[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/pages/?Event=PublicComment&id='.$description[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';			
					$row = array(
									'<textarea name="comments['.$description[$i]['id'].']">'.$description[$i]['comment'].'</textarea>',
									$description[$i]['fio'],
									$public,
									$description[$i]['public_ts']
								);
					$rows[$description[$i]['id']] = $row;
					
			}

			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows, 'noform' => true);
			$this->GridComments->SetData($data);
		}


	      private function SaveImage($addPath=''){
			
			$bool = false;
			
			switch ($_FILES[$addPath]['type']){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':					
				case 'image/x-png':
				case 'image/png':
				case 'image/gif':
					break;
				default:
					return false;
				
		    }
			global $DB;
			
			$path = $this->GetConfigParam('absolute-path').'public/files/images/';
			@mkdir($path,0777);
			$ext = explode(".",$_FILES[$addPath]['name']);
			$md5 = md5($_FILES[$addPath]['name']+time()).'.'.end($ext);
			$this->MD5FileName = $md5;
			
			$file = $path.'/'.$md5;


			move_uploaded_file($_FILES[$addPath]['tmp_name'],$file);
				
			
			return $bool ;	
			
		}	
		
		
	}
?>
