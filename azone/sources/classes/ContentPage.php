<?
/*//////////////////////////////////////////////////////////////////////////////

        Languages.php
        --------
        Started On:     July 22, 2009
        Copyright:      (C) 2008-2009 FlyUpStudio.
        E-mail:         support@flyupstudio.com.ua

        $Id: Languages.php,v 1.1 2009/07/22 13:48:11 kde Exps $

//////////////////////////////////////////////////////////////////////////////*/

	abstract class ContentPage extends AbstractPage {
		
		/*
		 * Protected properties
		 */
		
		protected $NavPanel				= null;
		protected $NavPanelExp			= -1;
		protected $StartMenu			= null;
		protected $Modules				= array();
		protected $Modules_sz			= 0;
		protected $Modules_tree			= array();
		protected $Modules_tree_sz		= 0;
		protected $ModuleInfo			= array();
		protected $Perm					= array();
		protected $Errors				= array();
		protected $DisplayError			= false;
		public $LinkList 				= array();
		public $Price	 				= array();
		protected $PageContent			= '';
		protected $HeaderContent		= '';
		protected $FooterContent		= '';
		protected $TableName			= '';
		protected $TranslationTableName	= '';
		
		protected $Select				= 't.*';
		
		protected $SortField			= 'sort_order';
		protected $PublishField			= 'published';
		protected $DefaultField			= 'default';
		
		protected $SaveActions			= array();
		
		protected $WithSort				= false;
		
		protected $Records				= array();
		protected $Records_sz			= 0;

		public $DefaultLang				= 'rus';
		public $ToggleArr				= array(0 => 1, 1 => 0);
		public $DefaultLangId			= 0;
		public $Optionsize				= array(1=>1,2=>6,3=>24,4=>25,5=>26,6=>27,7=>28,8=>29);
		protected $Languages			= array();
		protected $News					= array();
		
		public $MonthNow				= array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
		
		/*
		 * Public methods
		 */
		final public function OnAfterLogin() {
			
			global $DB;
			
			$sql = 'UPDATE az_users 
					SET last_visit_ts=UNIX_TIMESTAMP(), 
						last_visit_ip=\''.$_SERVER['REMOTE_ADDR'].'\' 
					WHERE id=\''.$this->UserAccount['id'].'\'';
			$DB->Execute($sql);
		}
		
		final public function OnGetModules(){
			//print_r($this->Modules);
			$menuTree = array();
			$k = 0;
			
			global $DB,$url_parts;
			$break = false;
			$group = 0;
			
			$module_id = 0;
			foreach($this->Modules_tree as $el=>$item){
				for($i = 0, $c = count($item); $i < $c; $i++){
					if($item[$i]['name'] == $url_parts[2]){
						$break = true;
						$group = $item[$i]['group_id'];
						$module_id = $item[$i]['id'];
						break;
					}
				}
				if($break) break;
			}
			
			
			foreach($this->Modules_tree as $el=>$item){
				$module = array(
					'id' 	=> 0,
					'text'	=> $el,					
					'href'	=> '',					
					'expanded' => $item[0]['group_id'] == $group ? true : false,
					'leaf'	=> true,
					'iconCls' => 'group-ico-'.$item[0]['group_id']
				);
				
				
				
				if(is_array($item)){
					$module['leaf'] = false;
					for($i = 0, $c = count($item); $i < $c; $i++){
						$_module = array(
							'id' 	=> $item[$i]['id'],													
							'text'	=> $item[$i]['title'],
							'href'	=> $item[$i]['href'],
							'leaf'	=> true,
							'expanded' => $module_id == $item[$i]['id'] ? true : false,
							'iconCls' => 'group-nogo'
						);
						
						
						
						if($item[$i]['view']){
							$_module['leaf'] = false;
							$_module['iconCls'] = 'group-go';
							$_module['children'][] = array(
								'id' 	=> $item[$i]['id'] . '_view',
								'href'	=> $item[$i]['href'] . ($item[$i]['view']['event'] ? '?Event=' . $item[$i]['view']['event'] : ''),
								'text'	=> $item[$i]['view']['title'],
								'leaf'	=> true,
								'iconCls' => 'group-edit'						
							);
							
						}
						if($item[$i]['new']){
							
							$_module['iconCls'] = 'group-go';
							$_module['leaf'] = false;
							$_module['children'][] = array(
								'id' 	=> $item[$i]['id'] . '_new',
								'href'	=> $item[$i]['href'] . ($item[$i]['new']['event'] ? '?Event=' . $item[$i]['new']['event'] : ''),
								'text'	=> $item[$i]['new']['title'],
								'leaf'	=> true,
								'iconCls' => 'group-add'
							);
							
							
						}
						if($item[$i]['other']){
							for($k1 = 0, $kc = count($item[$i]['other']); $k1 < $kc; $k1++){
								$_module['iconCls'] = 'group-go';
								$_module['leaf'] = false;
								$_module['children'][] = array(
									'id' 	=> ($item[$i]['id'] .$item[$i]['other'][$k1]['id']. '_other'),
									'href'	=> $item[$i]['href'].($item[$i]['other'][$k1]['event'] ? '?Event=' . $item[$i]['other'][$k1]['event'] : ''),
									'text'	=> $item[$i]['other'][$k1]['title'],
									'leaf'	=> true,
									'iconCls' => 'group-add'
								);
														
							}
						}
						$module['children'][] = $_module;						
					}
					
					
									
				}				
				$menuTree[$k] = $module;
				$k++;
				
				
			}
		
			
			echo $this->json_safe_encode($menuTree);
			exit;
		}
		
		final public function OnBeforeCreate() {
			global $url_parts;
			
			//_debug($this->_get_html_form('products'),1);
			
			$this->UrlParts = $url_parts;
			array_shift($this->UrlParts);
			
			$this->ModuleInfo = $this->_get_module_info($this->UrlParts[1]);
			
			if(!$this->IsAuthUser) {
				
				$this->SetHeader('');
				$this->SetFooter('');
			}else {
				
				$this->_load_default_lang();
				$this->_load_modules_info();
				
				if($_GET['popup']){
					$this->SetHeader('header-popup.html');
					$this->SetFooter('footer-popup.html');
				}
				
				/*
				 * Attach Common CSS
				 */
				$this->AddCSS($this->RootUrl.'public/css/main.css');
				$this->AddCSS('/core/lib/extjs/resources/css/ext-all.css');
				
				/*
				 * Attach Common JavaScript
				 */
				
				$this->AddJS('/core/lib/extjs/ext-base.js');
				$this->AddJS('/core/lib/extjs/ext-all.js');
				
				if($_GET['popup']){
					$this->AddJS('/core/lib/extjs/components/MainWinPopup.js');
				}else{
					$this->AddJS('/core/lib/extjs/components/MainWin.js');
				}
				
				$this->AddJS('/core/lib/extjs/components/Module.js');				
				$this->AddJS('/core/lib/extjs/SortColumn.js');
			
				if(($url_parts[count($url_parts)-2] == 'resume' && $_REQUEST['Event'] == 'Edit' ) ||
					($url_parts[count($url_parts)-2] == 'products' && $_REQUEST['Event'] == 'Edit' ) ||
					($url_parts[count($url_parts)-2] == 'options' && $_REQUEST['Event'] == 'New' ) ||
					($url_parts[count($url_parts)-2] == 'options' && $_REQUEST['Event'] == 'Edit' ) ||
					($url_parts[count($url_parts)-2] == 'vacancy' && $_REQUEST['Event'] == 'Edit' )){
					// $this->AddJS($this->RootUrl.'public/js/jquery-1.2.6.pack.js');							
					$this->AddJS($this->RootUrl.'public/js/jquery-1.7.2.min.js');							
				
				}else{
					$this->AddJS('/core/lib/fancy/mootools-trunk.js');
				}
				
				/*
				 * Attach Common Components
				 */
				//$this->AttachComponent('NavigationPanel', $this->NavPanel);
				
				$data = array('modules' => $this->Modules, 'modules_sz' => $this->Modules_sz);
				//$this->NavPanel->SetData($data);
				//$this->NavPanel->SetExpandedItem($this->NavPanelExp);
				
				//$this->AttachComponent('StartMenu', $this->StartMenu);
				
				$this->_check_perm();
				
				if($_REQUEST['Event'] == 'Edit' || $_REQUEST['Event'] == 'Update' || $_REQUEST['Event'] == 'New' || $_REQUEST['Event'] == 'Insert' || $_REQUEST['Event'] == 'Remove') {
					
					if(!$this->Perm['edit']) {
						
						$this->GoTo404();
					}
				}
				
				$this->UserAccount['last_visit_date'] = date('d.m.Y', $this->UserAccount['last_visit_ts']);
				$this->UserAccount['last_visit_time'] = date('H:i', $this->UserAccount['last_visit_ts']);
			}
		}

		public function OnDisplay() {
			global $config;
			$this->AuthData = $config['azone_auth'];
			ob_start();
			if($this->Header) 
				require_once PATH_HTML_TEMPLATES.$this->Header;
			$this->HeaderContent = ob_get_contents();
			ob_end_clean();
			
			ob_start();
			require_once PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.$this->Template;
			$this->PageContent = ob_get_contents();
			ob_end_clean();
			
			ob_start();
			if($this->Footer)
				require_once PATH_HTML_TEMPLATES.$this->Footer;
			$this->FooterContent = ob_get_contents();
			ob_end_clean();

			$script = array();
			$scr = explode('<!--script',$this->PageContent,2);
			
			if(count($scr) > 1){
				$script = explode('/script-->',$scr[1]);
				$this->PageContent = end($script);
			}

			if($this->AuthData['template'] == $this->Template){
				echo $this->HeaderContent . $this->PageContent . $this->FooterContent;
			}else{
				echo $this->HeaderContent;
				if($_GET['popup']){
					echo $this->PageContent;
				}else{
					echo '<script type="text/javascript">var pageHTML = "'.str_replace(array("\t","\n","\r"), "", addslashes($this->PageContent)).'"</script>';
				}
				
				if($script) echo '<script type="text/javascript">'.reset($script).'</script>';
				echo $this->FooterContent;
			}
		}

		
		//----------------- Ajax responses[BEGIN] -----------------------
		public function OnGetModuleStructure(){
			echo $this->json_safe_encode(
				array(
					'component' => false
				)
			);
			exit;			
		}
		
		public function OnFill(){
			
			$sql = 'UPDATE '.$_REQUEST['table'].'
					SET `'.$_REQUEST['field'].'` = \''.$_REQUEST['value'].'\'
					WHERE id = \''.$_REQUEST['id'].'\'';
			Execute($sql);
			echo $_REQUEST['value'] ? '<span style="cursor:pointer; color: green; text-decoration: underline;" onclick="fill('.$_REQUEST['id'].', 0, \''.$_REQUEST['table'].'\', \''.$_REQUEST['field'].'\', \''.$_REQUEST['cont'].'\')">Да</span>' : '<span style="cursor:pointer;  text-decoration: underline;" onclick="fill('.$_REQUEST['id'].', 1, \''.$_REQUEST['table'].'\', \''.$_REQUEST['field'].'\', \''.$_REQUEST['cont'].'\')">Нет</span>';
			
			exit;
		}

		public function OnPublished(){
			global $DB;

			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE ".$this->TableName." SET ".$this->PublishField." = 1 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				
			}
			header('Location: '.$_SERVER['REQUEST_URI']);
		}
		
		public function OnUnPublished(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "UPDATE ".$this->TableName." SET ".$this->PublishField." = 0 WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
			}
			header('Location: '.$_SERVER['REQUEST_URI']);
		}
		
		public function OnDeleteSelected(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "DELETE FROM ".$this->TableName." WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
			}
			header('Location: '.$_SERVER['REQUEST_URI']);
		}
		public function OnGetEditForm(){
			echo '{"noedit":false}';
			exit;
		}	

		public function OnGetLanguages(){
			global $DB;
			
			$sql = 'SELECT name, id
					FROM languages 
			';
			$res = $DB->GetAll($sql);
			
			echo $this->json_safe_encode(
				array(
					'data' => $res,
					'totalCount' => count($res)
				)
			);
			exit;	
		}	

		public function OnLoadRecord(){
			global $DB;
 
			if($this->TableName != ''){
				
				$select = 't.*';
				$from = $this->TableName . ' t';
				$left_join = '';
				
				foreach($this->LeftJoin as $key=>$val){
					$left_join .= ' LEFT JOIN '.$val['table'].' t_'.$key.' ON '.$val['on'];	
					$select .= ', '.$val['select'];
				}
				
				$sql = 'SELECT '.$select.' 
						FROM '.$from.' 
							 '.$left_join.'	 
						WHERE t.id = \''.intval($_REQUEST['id']).'\'';
				
				$res = $DB->GetRow($sql);

				if($this->TranslationTableName){
					$lang_id = intval($_REQUEST['lang_id']) ? intval($_REQUEST['lang_id']) : $this->DefaultLangId;
					
					$sql = "SELECT 
								* 
							FROM 
								$this->TranslationTableName 
							WHERE 
								object_id = '$res[id]' AND lang_id = $lang_id 
							";

					$translation = $DB->GetRow($sql);
//					if(!$translation){
//						$sql = "SELECT 
//									* 
//								FROM 
//									$this->TranslationTableName tt 
//									INNER JOIN
//									languages l
//									ON tt.lang_id = l.id
//								WHERE 
//									tt.object_id = '$res[id]'
//								ORDER BY
//									l.sort_order  
//								";
//						$translation = $DB->GetRow($sql);
//					}
					$res = array_merge($translation, $res);
				};
					
				echo $this->json_safe_encode(
					array(
						'success'	=> true,
						'data'		=> $res
					)
				);
					
			}else{
				echo $this->json_safe_encode(
					array(
						'success'	=> false,
						'data'		=> array(),
						'reason'	=> 'пїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ!'
					)
				);
			}
			exit;
		}
		
		public function OnGetRecords(){
			$cnt = $this->_get_records();
			echo json_encode(array(
				'records' => $this->Records,
				'cnt' => $cnt
			));
			exit;
		}

		public function OnGetSimpleTreeRecords(){
	    	
			echo json_encode($this->_get_tree_records());
			exit;
	    }
		
		public function OnSaveRecord(){
			global $DB;
			$res = false;
			if(intval($_REQUEST['data']['id'])){
				if(isset($_REQUEST['data']) && is_array($_REQUEST['data'])){
					
					if($_REQUEST['data']['url']){
						
						$this->_check_data_url();
					}
					
					if($this->WithSort && $_REQUEST['data'][$this->SortField]){
						$sql = 'SELECT * FROM `' . $this->TableName . '` WHERE `' . $this->SortField . '` = (SELECT MAX(' . $this->SortField . ') FROM `' . $this->TableName . '` )';
						$record = $DB->GetOne($sql);
						$sortValues = array(
							intval($_REQUEST['data']['id'])	=> $_REQUEST['data'][$this->SortField],
							$record['id']	=> $record[$this->SortField]
						);
						
						$this->_save_sort($sortValues);
						unset($_REQUEST['data'][$this->SortField]);
					}

					$setArr = array();
					foreach($_REQUEST['data'] as $key=>$val){
						if($this->SaveActions[$key]){
							$fn_name = $this->SaveActions[$key];
							$val = $this->$fn_name($val);
						}
						
						$setArr[] = "tn.`$key` = '$val'";
					}
					if($this->TranslationTableName){
						if($_REQUEST['data_translation']['object_id']){
							
							foreach($_REQUEST['data_translation'] as $key=>$val){
								if(($_REQUEST['data_translation']['lang_id'] && $key == 'lang_id') || $key != 'lang_id'){
									$setArr[] = "tt.`$key` = '$val'";
								}	
							}
							
							$sql = "
									UPDATE 
										$this->TableName tn
										LEFT JOIN 
										$this->TranslationTableName tt
										ON 
										tt.object_id = tn.id ".( $_REQUEST['data_translation']['lang_id'] ? " AND tt.lang_id = " . intval($_REQUEST['data_translation']['lang_id']) : "" ). "
									SET 
										" . implode(", ", $setArr) . "
									WHERE tn.id = " . intval($_REQUEST['data']['id']) . "
							";
							$res = $DB->Execute($sql);
							
						}else{
							$sql = 'UPDATE ' . $this->TableName . ' tn SET ' . implode(", ", $setArr) . ' WHERE id = ' . intval($_REQUEST['data']['id']);
							$res = $DB->Execute($sql);

							$_REQUEST['data_translation']['object_id'] = intval($_REQUEST['data']['id']);
							
							$setArr = array();
							foreach($_REQUEST['data_translation'] as $key=>$val){
								$setArr[] = "`$key` = '$val'";
							}
							
							$sql = "INSERT INTO $this->TranslationTableName SET " . implode(", ", $setArr);
							$res &= $DB->Execute($sql);
						}
					}else{
						$sql = 'UPDATE ' . $this->TableName . ' tn SET ' . implode(", ", $setArr) . ' WHERE id = ' . intval($_REQUEST['data']['id']);
						$res = $DB->Execute($sql);
					}
					
					if($res && $_REQUEST['data'][$this->DefaultField]){
						$this->_make_default(intval($_REQUEST['data']['id']));
					}
				}
			}else{
				if(isset($_REQUEST['data']) && is_array($_REQUEST['data'])){
					
					if($_REQUEST['data']['url']){
						
						$this->_check_data_url();
					}
					
					$setArr = array();
					foreach($_REQUEST['data'] as $key=>$val){
						$setArr[] = "`$key` = '$val'";
					}
										
					if($this->TranslationTableName){
						$sql = 'INSERT INTO ' . $this->TableName . ' SET ' . implode(", ", $setArr);
						$res = $DB->Execute($sql);
						
						$sql = 'SELECT LAST_INSERT_ID() as id';
						
						$res_id = $DB->GetRow($sql);
						
						$_REQUEST['data_translation']['lang_id'] = $_REQUEST['data_translation']['lang_id'] == 'default' ? $this->DefaultLangId : intval($_REQUEST['data_translation']['lang_id']);
						$_REQUEST['data_translation']['object_id'] = $res_id['id'];
						
						$setArr = array();
						foreach($_REQUEST['data_translation'] as $key=>$val){
							$setArr[] = "`$key` = '$val'";
						}
						
						$sql = "INSERT INTO $this->TranslationTableName SET " . implode(", ", $setArr);
						$res &= $DB->Execute($sql);
					}else{					
						$sql = 'INSERT INTO ' . $this->TableName . ' SET ' . implode(", ", $setArr);
						$res = $DB->Execute($sql);
					}
					
					$sql = 'SELECT LAST_INSERT_ID() as id';
					$res_id = $DB->GetRow($sql);
					
					if($this->WithSort && $_REQUEST['data'][$this->SortField]){
						$sql = 'SELECT * FROM `' . $this->TableName . '` WHERE `' . $this->SortField . '` = (SELECT MAX(' . $this->SortField . ') FROM `' . $this->TableName . '` )';
						$record = $DB->GetOne($sql);
						
						$sortValues = array(
							intval($res_id['id'])	=> $_REQUEST['data'][$this->SortField],
							$record['id']	=> $record[$this->SortField]
						);
						
						$this->_save_sort($sortValues);
					}
					
					if($res && $_REQUEST['data'][$this->DefaultField]){
						$this->_make_default(intval($res_id['id']));
					}
					
				}
			}
			
			echo $this->json_safe_encode(
				array(
					'success'	=> $res
				)
			);
			exit;
			
		}
		
		public function OnToggleField(){
			global $DB;
			
			if($_REQUEST['fieldName'] && $_REQUEST['id']){
				
				$sql = 'UPDATE '.$this->TableName.'
						SET '.$_REQUEST['fieldName'].' = '.$this->ToggleArr[$_REQUEST['fieldValue']].'
						WHERE id = '.$_REQUEST['id'];
				$DB->Execute($sql);
			}
			
			exit;
		}
		
		public function OnSaveData(){
			global $DB;
			
			$res = false;
			
			if($_REQUEST['field'] == $this->DefaultField){
				$res = $this->_make_default($id);
			}elseif($this->WithSort && $_REQUEST['field'] == $this->SortField){
				$sql = 'SELECT * FROM `' . $this->TableName . '` WHERE `' . $this->SortField . '` = (SELECT MAX(' . $this->SortField . ') FROM `' . $this->TableName . '` ) LIMIT 1';
				$record = $DB->GetOne($sql);
				$sortValues = array(
					intval($_REQUEST['id'])	=> $_REQUEST['field'],
					$record['id']			=> $record[$this->SortField]
				);
				$res = $this->_save_sort($sortValues);
			}else{
				$res = $this->_update_record($_REQUEST['id'], $_REQUEST['field'], $_REQUEST['value'], $_REQUEST['lang_id']);
			}
			
			echo $this->json_safe_encode(array('success' => $res));
			exit;
			
		}
		
		public function OnTreeDD(){
			global $DB;

			if($_REQUEST['target_id'] == 'source') $_REQUEST['target_id'] = 0;
				
			$sql = 'SELECT url, parent_id
					FROM '.$this->TableName.' 
					WHERE id	= \''.$_REQUEST['drop_id'].'\'';
			$url = $DB->GetRow($sql);
			
			
			if($url['parent_id'] != $_REQUEST['target_id']){
				
				$sql = 'SELECT url 
						FROM '.$this->TableName.' 
						WHERE 	parent_id	= \''.$_REQUEST['target_id'].'\' AND
								url			= \''.$url['url'].'\'';
				$res = $DB->GetRow($sql);
			
				$sql = 'UPDATE '.$this->TableName.'
						SET 	parent_id	= \''.$_REQUEST['target_id'].'\',
								url			= \''.( $res ? $url['url'].'_'.rand() : $url['url'] ).'\'
					 	WHERE 	id			= \''.$_REQUEST['drop_id'].'\'';
				
				$res = $DB->Execute($sql);
			}	

			echo $this->json_safe_encode(array('success' => $res));
			exit;
		}
		
		public function OnChangeSort(){
			if($this->WithSort){
				$id = intval($_REQUEST['id']);
				$direction = $_REQUEST['direction'] == 'up' ? 'up' : 'down';
				
				$this->_get_records();
				//print_r($this->Records);
				for($i = 0;$i < $this->Records_sz; $i++){
					if($this->Records[$i]['id'] == $id && $direction == 'up'){
						//list($this->Records[$i], $this->Records[$i-1]) = array($this->Records[$i-1],$this->Records[$i]);
						$this->_update_record($id, $this->SortField, $this->Records[$i-1][$this->SortField]);
						$this->_update_record($this->Records[$i-1]['id'], $this->SortField, $this->Records[$i][$this->SortField]);
					}elseif($this->Records[$i]['id'] == $id){
						//list($this->Records[$i], $this->Records[$i+1]) = array($this->Records[$i+1],$this->Records[$i]);
						$this->_update_record($id, $this->SortField, $this->Records[$i+1][$this->SortField]);
						$this->_update_record($this->Records[$i+1]['id'], $this->SortField, $this->Records[$i][$this->SortField]);
					}
				}
			}
			exit;
		}
		
		public function OnSaveSort($oneRecord = false){
			global $DB, $sf;
			
			if($this->WithSort){
				$sortValues = isset($_REQUEST['sortValues']) && is_array($_REQUEST['sortValues']) ? $_REQUEST['sortValues'] : false;
				$this->_save_sort($sortValues);
			}
			exit;
		}

		public function OnMakeDefault(){
			$this->_make_default(intval($_REQUEST['id']));
			exit;
		}
		
		public function OnPublish(){
			$this->_publish(intval($_REQUEST['id']));
			exit;
		}

		public function OnUnpublish(){
			$this->_unpublish(intval($_REQUEST['id']));
			exit;
		}
		
		public function OnRemove() {
			
			global $DB;
			
			echo $this->json_safe_encode(array('success' => $this->_remove(intval($_REQUEST['id']))));
			exit;
		}
		
		public function OnRemoveTreeNode() {
			
			global $DB;
			
			echo $this->json_safe_encode(array('success' => $this->_remove_tree_node(intval($_REQUEST['id']))));
			exit;
		}
				
		//----------------- Ajax responses[END] -------------------------
		
		
		public function CreateModuleUrl($name) {
			
			return $this->RootUrl.$name.'/';
		}
		
		/*
		 * Protected methods
		 */
		protected function _user_exists($login) {
			
			global $DB;
			
			$sql = 'SELECT COUNT(*) AS cnt FROM az_users WHERE username=\''.$login.'\'';
			$res = $DB->GetRow($sql);
			
			if($res['cnt'] == 0){
				
				return false;
			}
			
			return true;
		}
		
		protected function _check_data_url() {
			
			global $DB;
			
			$sql = 'SELECT url, id
					FROM '.$this->TableName.'
					WHERE url	= \''.$_REQUEST['data']['url'].'\'';
			
			$where = '';
			if($_REQUEST['data']['parent_id']) $where = ' AND parent_id = \''.$_REQUEST['data']['parent_id'].'\'';
				
			$res = $DB->GetRow($sql.$where);
			
			if($res){
				if($res['id'] != $_REQUEST['data']['id']){
					echo $this->json_safe_encode(
						array(
							'success'	=> false
						)
					);
					exit;
				}	
			}
		}
		
		protected function _load_modules_info() {
			
			global $DB;
			
			$perm = (unserialize($this->UserAccount['perm']));

			$where = '';
			
			if(is_array($perm['edit'])){

				foreach($perm['edit'] as $id => $val) {
					
					$where .= '(az_modules.id=\''.$id.'\') OR ';
				}
			}	
			
			if(is_array($perm['view'] )){
				
				foreach($perm['view'] as $id => $val) {
					
					$where .= '(az_modules.id=\''.$id.'\') OR ';
				}
			}	
			
			$where = substr($where, 0, strlen($where) - 3);
			
			$sql = 'SELECT * FROM az_modules 
					WHERE '.$where.'
					ORDER BY title ASC';
			$this->Modules = $DB->GetAll($sql);
			$this->Modules_sz = count($this->Modules);
			
			$sql = 'SELECT az_modules.*,az_modules_group.name AS azmg_name FROM az_modules,az_modules_group WHERE az_modules.group_id = az_modules_group.id AND az_modules.show = 1 ORDER BY az_modules_group.order_by ASC, az_modules.order_by ASC ';
			$data = $DB->GetAll($sql);
		
			foreach($data as $el=>$item){
				if(array_key_exists($item['id'],$perm['view']) || array_key_exists($item['id'],$perm['edit']) ){
					$this->Modules_tree[$item['azmg_name']][] = $item;
				}
			
			}
			
			
			
			$url_parts = explode('/', $this->RedirectUrl);
			$url_parts_sz = count($url_parts);
			
			if(!$url_parts[$url_parts_sz - 1]) {
				
				$module_url = $url_parts[$url_parts_sz - 2];
			}
			else {
				
				$module_url = $url_parts[$url_parts_sz - 1];
			}
			
			foreach($this->Modules_tree as $el=>&$item){
				for($j = 0, $jc = count ($item); $j < $jc; $j++){
				
					$sql = 'SELECT * FROM az_menu WHERE module_id=\''.$item[$j]['id'].'\'';
					$res = $DB->GetAll($sql);
					$res_sz = count($res);
					
					$menu = array();
					
					for($k = 0; $k < $res_sz; $k++) {
					
						if($res[$k]['name'] != 'other'){
							
							$menu[$res[$k]['name']] = $res[$k];
						}
					}
					for($k = 0; $k < $res_sz; $k++) {
						if($res[$k]['name'] == 'other'){
							$item[$j]['other'][] = $res[$k];
						}
					}
					
					$item[$j]['href'] = $this->CreateModuleUrl($item[$j]['name']);
					$item[$j]['show'] = $perm['show'][$item[$j]['id']];

					
					if($perm['view'][$item[$j]['id']] && $menu['view']) {						
						$item[$j]['view'] = $menu['view'];
					}

					if($perm['edit'][$item[$j]['id']] && $menu['new']) {	
						
						$item[$j]['new'] = $menu['new'];
					}
				
				}								
			}
			//_debug($this->Modules_tree,1);
			
			for($i = 0; $i < $this->Modules_sz; $i++) {
				
				$sql = 'SELECT * FROM az_menu WHERE module_id=\''.$this->Modules[$i]['id'].'\'';
				$res = $DB->GetAll($sql);
				$res_sz = count($res);
				
				$menu = array();
				for($j = 0; $j < $res_sz; $j++) {
					
					$menu[$res[$j]['name']] = $res[$j];
				}
				
				$this->Modules[$i]['href'] = $this->CreateModuleUrl($this->Modules[$i]['name']);
				$this->Modules[$i]['show'] = $perm['show'][$this->Modules[$i]['id']];

				
				if($perm['view'][$this->Modules[$i]['id']] && $menu['view']) {
					
					$this->Modules[$i]['view'] = $menu['view'];
				}

				if($perm['edit'][$this->Modules[$i]['id']] && $menu['new']) {
					
					$this->Modules[$i]['new'] = $menu['new'];
				}
				
				if($this->Modules[$i]['name'] == $module_url) {
					
					$this->NavPanelExp = $i;
				}
			}
		}
		
		protected function _get_module_info($name) {
			
			global $DB;
			
			$sql = 'SELECT * FROM az_modules WHERE name=\''.$name.'\' LIMIT 1';
			$module = $DB->GetRow($sql);
			$module['href'] = $this->CreateModuleUrl($name);
						
			return $module;
		}
		
		protected function _get_form_structure($name, $conf, $positions = array(), $fields_conf = array()) {
			global $DB;
			
			$sql = 'SHOW FULL COLUMNS FROM ' . $name;

			$table = $DB->GetAll($sql);

			$result = array();
			$i = 0;
			foreach($table as $row){
				$tmp_row = array();
				if(@in_array($row['Field'], $this->HiddenFields))
					continue;
				switch($row['Field']){
					case 'id':
						
						$tmp_row['name'] = 'data[' . $row['Field'] . ']';
						$tmp_row['dataindex'] = $row['Field'];
						$tmp_row['id'] = $row['Field'];
						$tmp_row['xtype'] = 'hidden';
						$tmp_row['sort_order'] = $i++;
						$result[$row['Field']] = $tmp_row;
						
						break;
						
					default:
						$tmp_arr = explode('(', $row['Type']);
						$field_type = reset($tmp_arr);
						$tmp_row['name'] = 'data[' . $row['Field'] . ']';
						$tmp_row['dataindex'] = $row['Field'];
						$tmp_row['id'] = $row['Field'];
						$tmp_row['xtype'] = 'textfield';
						$tmp_row['fieldLabel'] = $row['Field'];
						$tmp_row['allowBlank'] = $row['Null'] == 'YES';
						
						
						if($row['Comment'])
							$tmp_row['fieldLabel'] = $row['Comment'];
							
						switch($field_type){
							case 'varchar':
								$size = intval(reset(explode(')', end($tmp_arr))));
								if($size > 255){
									$tmp_row['xtype'] = 'textarea';
								}
								break;
							
							case 'tinyint':
								
								$hidden['name'] = 'data[' . $row['Field'] . ']';
								$hidden['xtype'] = 'hidden';
								$hidden['value'] = 0;
								$hidden['sort_order'] = -1;
								$result[$row['Field'].'hidden'] = $hidden;
														
								$tmp_row['xtype'] = 'checkbox';
								$tmp_row['inputValue'] = 1;
								break;
							
							case 'text':
								$tmp_row['xtype'] = 'textarea';
								break;
							
							case 'int':
								$tmp_row['xtype'] = 'numberfield';
								break;
							
							case 'datetime':
								$tmp_row['xtype'] = 'datetime';
								break;
							
							case 'date':
								$tmp_row['xtype'] = 'datefield';
								break;
							
							case 'time':
								$tmp_row['xtype'] = 'timefield';
								break;
							
							case 'enum':
								$tmp_str = substr($row['Type'],0,-2);
								$tmp_arr = explode('(\'', $tmp_str);
								$opt_arr = explode('\',\'', end($tmp_arr));
								$options = array();
								if($opt_arr)
									foreach($opt_arr as $value)
										$options[] = array($value);
								$tmp_row['options'] = $options;
								$tmp_row['options'] = $options;
								$tmp_row['xtype'] = 'combo';
								break;
								
							default:;
						}
						$tmp_row['sort_order'] = $i++;
						
						
						//$tmp_row['name'] = $fields_conf[$row['Field']]['name'] ? $fields_conf[$row['Field']]['name'] : $tmp_row['name'];
						//$tmp_row['dataindex'] = $row['Field'];
						//$tmp_row['id'] = $row['Field'];
						if($fields_conf[$row['Field']] && is_array($fields_conf[$row['Field']])){
							foreach($fields_conf[$row['Field']] as $key=>$_field_conf){
								$tmp_row[$key] = $_field_conf;
							}
						}
						//$tmp_row['fieldLabel'] = $row['Field'];
						//$tmp_row['allowBlank'] = $row['Null'] == 'YES';
						
						$result[$row['Field']] = $tmp_row;
				}
			}
			
			if($this->TranslationTableName){
				
				$sql = 'SHOW FULL COLUMNS FROM ' . $this->TranslationTableName;
	
				$table = $DB->GetAll($sql);
	
				foreach($table as $row){
					$tmp_row = array();
					
					if(@in_array($row['Field'], $this->HiddenFields))
						continue;
					
					switch($row['Field']){
						case 'id':
							break;
						case 'object_id':
						case 'lang_id':
							
							$tmp_row['name'] = 'data_translation[' . $row['Field'] . ']';
							$tmp_row['dataindex'] = $row['Field'];
							$tmp_row['id'] = $row['Field'];
							$tmp_row['xtype'] = 'hidden';
							$tmp_row['sort_order'] = $i++;
							$result[$row['Field']] = $tmp_row;
							
							break;
							
						default:
							$tmp_arr = explode('(', $row['Type']);
							$field_type = reset($tmp_arr);
							
							$tmp_row['name'] = 'data_translation[' . $row['Field'] . ']';
							$tmp_row['dataindex'] = $row['Field'];
							$tmp_row['id'] = $row['Field'];
							$tmp_row['xtype'] = 'textfield';
							$tmp_row['fieldLabel'] = $row['Field'];
							$tmp_row['allowBlank'] = $row['Null'] == 'YES';
							
							
							if($row['Comment'])
								$tmp_row['fieldLabel'] = $row['Comment'];
								
							switch($field_type){
								case 'char':
								case 'varchar':
								case 'varchar':
									$size = intval(reset(explode(')', end($tmp_arr))));
									if($size > 255){
										$tmp_row['xtype'] = 'textarea';
									}
									break;
									
								case 'tinytext':
								case 'mediumtext':
								case 'longtext':
								case 'text':
									$tmp_row['xtype'] = 'textarea';
									break;
								
								default:
									continue;
									break;
							}
							$tmp_row['sort_order'] = $i++;
							
							if($fields_conf[$row['Field']] && is_array($fields_conf[$row['Field']])){
								foreach($fields_conf[$row['Field']] as $key=>$_field_conf){
									$tmp_row[$key] = $_field_conf;
								}
							}
														
							$result[$row['Field']] = $tmp_row;
					}
				}				
				
			}
			
			if($positions){
				foreach($positions as $key=>$val){
					$result[$key]['sort_order'] = $val;
				}
			}
			
			function fn($a, $b){ return $a['sort_order'] == $b['sort_order'] ? 0 : ($a['sort_order'] < $b['sort_order'] ? -1 : 1);}
			usort($result, "fn");
			
			$fields = array();
			foreach($result as $field){
				$fields[] = $field;
			}
			
			$win_params = array(
				'title'			=> $conf['title'] ? $conf['title'] : 'Form',
				'width'			=> $conf['width'] ? $conf['width'] : 500,
				'height'		=> $conf['height'] ? $conf['height'] : 500,
				'layout'		=> $conf['layout'] ? $conf['layout'] : 'fit',
				'plain'			=> $conf['plain'] ? $conf['plain'] : true,
				'bodyStyle'		=> $conf['bodyStyle'] ? $conf['bodyStyle'] : 'padding:5px;',
				'buttonAlign' 	=> $conf['buttonAlign'] ? $conf['buttonAlign'] : 'center'
			);
			$form_params = array(
				'baseCls'		=> $conf['baseCls'] ? $conf['baseCls'] : 'x-plain',
				'labelWidth'	=> $conf['labelWidth'] ? $conf['labelWidth'] : 130,
				'fileUpload'	=> $conf['fileUpload'] ? $conf['fileUpload'] : false,
				'url'			=> $conf['url'] ? $conf['url'] : $url,
				'autoScroll'	=> $conf['autoScroll'] ? $conf['autoScroll'] : true,
				'defaults'		=> $conf['defaults'] ? $conf['defaults'] : array(
					'anchor'	=> '95%'
				)
			);
			
			return array(
				'fields'		=> $fields,
				'form_params'	=> $form_params,
				'win_params'	=> $win_params
			);			
			
		}
		
		protected function _get_records() {			
			global $DB;
			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : false;
			$limit = $_REQUEST['limit'] ? $_REQUEST['limit'] : false;
			$search = $_REQUEST['search'] ? $_REQUEST['search'] : false;
			
			$where = '';

			if($start !== false && $limit !== false){
				$limitStr = " LIMIT $start,$limit "; 
			}
			
			if($this->TableName){
				
				$orderStr = '';
				if($this->WithSort && $this->SortField){
					 $orderStr = ' ORDER BY ' . $this->SortField . ' ASC ';
				}
				
				$select = 't.*';
				$from = $this->TableName . ' t';
				$left_join = '';
				
				
				if($this->TableName == 'companies'){				
					$select .= ', IF(t.type = \'direct\',\'РџСЂСЏРјРѕР№ СЂР°Р±РѕС‚РѕРґР°С‚РµР»СЊ\',\'РђРіРµРЅСЃС‚РІРѕ\') AS type';					
				}
				
				foreach($this->LeftJoin as $key=>$val){
					$left_join .= ' LEFT JOIN '.$val['table'].' t_'.$key.' ON '.$val['on'];	
					$select .= ', '.$val['select'];
				}
				
				$sql = 'SELECT SQL_CALC_FOUND_ROWS '.$select.' 
						FROM '.$from.' 
							 '.$left_join.' ';
				
				if($search){
					
					if($this->TranslationTableName) 
						$sql .= ', ' . $this->TranslationTableName . ' tt ';

					$sql .= ' WHERE ';
					
/*TODO
 * 
 * */					$this->TranslationTableName ? $sql .= ' tt.title like \'%'.$search.'%\' AND 
															tt.lang_id = \''.$this->DefaultLangId.'\' AND
															tt.object_id = t.id' 
												: $sql .= ' t.title like \'%'.$search.'%\'' ;
				}
				
				$sql .= $orderStr . $limitStr;
				
				$this->Records = $DB->GetAll($sql);
				$this->Records_sz = count($this->Records);

				$sql = 'SELECT FOUND_ROWS() as cnt';
				$res = $DB->GetOne($sql);
				$cnt = $res;
					
				if($this->TranslationTableName){
					foreach($this->Records as &$item){
/*TODO
 * add list of required fields
 * */
						$sql = "SELECT 
									* 
								FROM 
									$this->TranslationTableName 
								WHERE 
									object_id = '$item[id]' AND lang_id = '$this->DefaultLangId' 
								";

						$translation = $DB->GetRow($sql);
						if(!$translation){
							$sql = "SELECT 
										* 
									FROM 
										$this->TranslationTableName tt 
										INNER JOIN
										languages l
										ON tt.lang_id = l.id
									WHERE 
										tt.object_id = '$item[id]'
									ORDER BY
										l.sort_order  
									";
							$translation = $DB->GetRow($sql);
						}
						$item = array_merge($translation, $item);
					};
					unset($item);
				}
				return $cnt;
			}else{
				$this->Records = array();
				$this->Records_sz = 0;
				return 0;
			}
		}

		protected function _get_tree_records() {			
			global $DB;
	    	
			if($_REQUEST['node'] == 'source'){
				
				$res = $this->_get_chidrens_by_parent_id(0);

			}elseif(is_numeric($_REQUEST['node'])){
				
				$res = $this->_get_chidrens_by_parent_id($_REQUEST['node']);
			}
			
			return $res;
		}
		
		protected function _get_chidrens_by_parent_id($parent_id = 0) {
			global $DB;
			
			$result = array();
			
			$select = '';
			$left_join = '';
			
			$sql = 'SELECT t.id as node_id ';

			if($this->TranslationTableName){
				
				$select .= ', tt.*';
				
				$left_join .= 'LEFT JOIN ' . $this->TranslationTableName . ' tt ON (t.id = tt.object_id AND lang_id = \''.$this->DefaultLangId.'\')';
			}	
			
			$sql .= $select . '
					FROM ' . $this->TableName . ' t ' . $left_join . '
					WHERE t.parent_id = \''.$parent_id.'\'
					ORDER BY t.id DESC';
			
			$res = $DB->GetAll($sql);
			
			foreach($res as $r){
				
				$node = array(
					'id'	=> $r['node_id'],
					'text'	=> $r['title'],
					'leaf'	=> $this->_has_chidrens($r['node_id']),
					'isTarget' => true 
				);
				
				array_push($result, $node);
			}
			
			return $result;
		}
		
		protected function _has_chidrens($parent_id = 0) {
			global $DB;
			
			$sql = 'SELECT id
					FROM '. $this->TableName . '
					WHERE parent_id	= \''.$parent_id.'\'';
			
			$res = $DB->GetAll($sql);
			
			if($res)
				return false;
			else
				return true;	
		}
		
		protected function _update_record($id, $field, $value, $lang_id = 0) {
			global $DB;
			if($this->TableName){
				if($this->TranslationTableName){
					$sql = "
							UPDATE 
								$this->TableName tn
								LEFT JOIN 
								$this->TranslationTableName tt
								ON 
								tt.object_id = tn.id AND tt.lang_id = $lang_id
							SET 
								$field  = '$value'
							WHERE tn.id = $id
					";
				}else{
					$sql = 'UPDATE ' . $this->TableName . ' SET ' . $field . ' = \'' . $value . '\' WHERE id = ' . $id;
				}

				return $DB->Execute($sql);
			}
			return false;
		}

		protected function _get_system_variable($name){
			global $DB;

			$sql = "SELECT var_value FROM sys_vars WHERE var_name = '$name' ";			
			return $DB->GetOne($sql);			
		}

		public function _get_essential_variable($name){
			global $DB;

			$sql = "SELECT var_value FROM essentials WHERE var_name = '$name' ";			
			return $DB->GetOne($sql);			
		}

		private function _make_default($id){
			global $DB;
			
			$sql = 'UPDATE ' . $this->TableName . ' SET `' . $this->DefaultField . '` = \'0\'';
			$DB->Execute($sql);

			$sql = 'UPDATE ' . $this->TableName . ' SET `' . $this->DefaultField . '` = \'1\' WHERE id=\''.$id.'\'';
			return $DB->Execute($sql);
		}

		private function _publish($id){
			global $DB;
			
			$sql = 'UPDATE ' . $this->TableName . ' SET `' . $this->PublishField . '` = \'1\' WHERE id=\''.$id.'\'';
			return $DB->Execute($sql);
		}		

		private function _unpublish($id){
			global $DB;
			
			$sql = 'UPDATE ' . $this->TableName . ' SET `' . $this->PublishField . '` = \'0\' WHERE id=\''.$id.'\'';
			return $DB->Execute($sql);
		}		
		
		private function _remove($id) {			
			global $DB;
			
			$sql = 'DELETE FROM ' . $this->TableName . ' WHERE id=\''.$id.'\'';
			$res = $DB->Execute($sql);
			
			if($this->TranslationTableName){
				$sql = 'DELETE FROM ' . $this->TranslationTableName . ' WHERE object_id = \''.$id.'\'';
				$res &= $DB->Execute($sql);
			}
			return $res;
		}
		
		private function _remove_tree_node($id) {			
			global $DB;
			
			$sql = 'SELECT id 
					FROM ' . $this->TableName . ' 
					WHERE parent_id=\''.$id.'\'';
			
			$res = $DB->GetAll($sql);
			$res_sz = count($res);
			
			for($i = 0; $i < $res_sz; $i++) {
				
				$this->_remove_tree_node($res[$i]['id']);
			}
			
			$sql = 'DELETE 
					FROM ' . $this->TableName . ' 
					WHERE id=\''.$id.'\'';
			$res = $DB->Execute($sql);
			
			if($this->TranslationTableName){
				$sql = 'DELETE 
						FROM ' . $this->TranslationTableName . ' 
						WHERE object_id = \''.$id.'\'';
				$res &= $DB->Execute($sql);
			}
			
			return $res;
		}

		private function _save_sort($sortValues){
			global $DB, $sf;
			
			if($sortValues){
				$sortIDs = array();
				foreach($sortValues as $key=>$val)
					$sortIDs[] = $key;
				
				$sql = 'SELECT ' . $this->SortField . ' FROM ' . $this->TableName . ' WHERE id IN(\'' . implode("','", $sortIDs) . "')";
				$records = $DB->GetAll($sql);
				if($records){
					$minSort = intval($records[0][$this->SortField]);
					$maxSort = 0;
					foreach($records as $record){
						if(intval($record[$this->SortField]) < $minSort){
							$minSort = intval($record[$this->SortField]);
						}
						if(intval($record[$this->SortField]) > $maxSort){
							$maxSort = intval($record[$this->SortField]);
						}
						
					}
					foreach($sortValues as $record){
						if(intval($record) < $minSort){
							$minSort = intval($record);
						}
						if(intval($record) > $maxSort){
							$maxSort = intval($record);
						}
						
					}


					$sql = 'SELECT id, ' . $this->SortField . ' FROM ' . $this->TableName . ' WHERE ' . $this->SortField . ' <= \'' . $maxSort . "' AND " . $this->SortField . " >= '" . $minSort . "'";
					$records = $DB->GetAll($sql);
					if($records){
						foreach($records as &$record){
							if(isset($sortValues[$record['id']])){
								//if($sortValues[$record['id']] < $record[$this->SortField])
									$record[$this->SortField] = $sortValues[$record['id']];
								//elseif($sortValues[$record['id']] > $record[$this->SortField])
									//$record[$this->SortField] = $sortValues[$record['id']] + 0.5;
							}
						}
						unset($record);
						$sf = $this->SortField;
						function cmp($a, $b){global $sf; return $a[$sf] == $b[$sf] ? 0 : ($a[$sf] < $b[$sf] ? -1 : 1);}
						usort($records, "cmp");
						$val = $minSort > 0 ? $minSort : 1;
						foreach($records as $record){
							$this->_update_record($record['id'], $this->SortField, $val++);
						}
						return true;
					}
					
				}
			}
			return false;
		}		
		/*
		 * Private methods
		 */
		protected function _check_url($url){
			global $DB;
			
			if(!$url) return true;
			$sql = "SELECT url FROM az_disabled_urls WHERE url LIKE'".$url."'";
			$row = $DB->GetOne($sql);
			return $row;
			
		}
		
		final private function _check_perm() {
			
			global $url_parts, $DB;
			
			$module = $this->_get_module_info($url_parts[2]);
			$perm = unserialize($this->UserAccount['perm']);
			
			$this->Perm['view'] = $perm['view'][$module['id']];
			$this->Perm['edit'] = $perm['edit'][$module['id']];

			if(!$this->Perm['view'] && !$this->Perm['edit'] && $module['id']) {
				
				$this->GoTo404();
			}
		}

		protected function _load_default_lang(){
			global $DB;

			$sql = 'SELECT 
						* 
					FROM 
						languages 
					WHERE 
						`default`=\'1\'';
			$res = $DB->GetRow($sql);
			//echo $sql;
			if($res){
				$this->DefaultLang = $res['code'];
				$this->DefaultLangId = $res['id'];
			}
		}
		
		final public function resize($file, $type, $height, $width){
			
		    $img = false;
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = @imagecreatefromjpeg($file);
					break;
				case 'image/x-png':
				case 'image/png':
					$img = @imagecreatefrompng($file);
					break;
				case 'image/gif':
					$img = @imagecreatefromgif($file);
					break;
		    }
		    if(!$img){
				return false;
		    }
		    
		    $curr = @getimagesize($file);
		    
		    $perc_w = $width / $curr[0];
		    $perc_h = $height / $curr[1];
		    
		    if(($width > $curr[0]))/* || ($height > $curr[1]))*/{
				return;
		    }
		    
		    if($perc_h > $perc_w){
				$width = $width;
				$height = round($curr[1] * $perc_w);
		    } else {
				$height = $height;
				$width = round($curr[0] * $perc_h);
		    }
		    
		  
		    $nwimg = @imagecreatetruecolor($width, $height);
		    @imagecopyresampled($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[1]);
		    
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					@imagejpeg($nwimg, $file);
					break;
				case 'image/x-png':
				case 'image/png':
					@imagepng($nwimg, $file);
					break;
				case 'image/gif':
					@imagegif($nwimg, $file);
					break;
		    }
		    
		    @imagedestroy($nwimg);
		    @imagedestroy($img);
			return true;
		}	
	
		protected function GetLangId($code) {
			global $DB;			
			
		 	$sql = 'SELECT id 
					FROM	languages
					WHERE 	code	= \''.$code.'\'';
			
			$res = $DB->GetRow($sql);
			
			return $res['id'];
		}
		
		final public function is_picture($type){
			
			switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = true;
					break;
				case 'image/x-png':
				case 'image/png':
					$img = true;
					break;
				case 'image/gif':
					$img = true;
					break;
		    }
		    
		    if(!$img) $img = false;
		    
		    return $img;
		}
		
		final public function check_size($file_size, $max_size){
			
			if($file_size <= $max_size){
				
				return true;
				
			}else{
				
				return false;
			}
		}
		
		protected function _save_logo($file, $p, $height, $width) {
			
			global $DB,$config;

			$path = $config['absolute-path'].$p;
			
			$ext = $this->_get_file_ext($file['name']);
			$large = md5($file['tmp_name']).'-'.time().'.'.$ext;
	
			@move_uploaded_file($file['tmp_name'], $path.$large);
			@chmod($path.$large,0777);
			
			$file['type']."<br>".$path.$large."<br>".$height."<br>".$width;
			
			$this->resize($path.$large, $file['type'], $height, $width);
			
			return $large;
		}
		
		protected function _get_file_ext($filename) {
			
			return end(explode('.', $filename));
		}
		
		protected function _get_system_variable_array($name){
			
			return explode('|',GetOne("SELECT var_value FROM sys_vars WHERE var_name = '$name' "));			
		}
		
		protected function _create_arr_for_sql($data, $diff_arr){
			global $DB;
			
			$sql_arr = array();
			if(is_array($data)){
				foreach($data as $key => $val){
					if(!in_array($key, $diff_arr)){
						
						$field_type = end(explode(':', $key));
						$field = current(explode(':', $key));
					
						switch ($field_type){
							case 'str':
								if(!trim($val))
									$sql_arr[] = '`'.$field.'` = \'\'';	
								else
									$sql_arr[] = '`'.$field.'` = \''.s($val).'\'';
							break;
							case 'int':
								if(!i(trim($val)))
									$sql_arr[] = '`'.$field.'` = 0';	
								else
									$sql_arr[] = '`'.$field.'` = \''.intval($val).'\'';
							break;
							case 'float':
								if(!floatval(trim($val)))
									$sql_arr[] = '`'.$field.'` = 0';	
								else
									$sql_arr[] = '`'.$field.'` = \''.floatval($val).'\'';
							break;
							case 'pass':
								$sql_arr[] = '`'.$field.'` = \''.$this->CryptPassword($val).'\'';
							break;
							case 'enum':
								if(trim($val))
									$sql_arr[] = '`'.$field.'` = \''.s($val).'\'';
							break;
						}
					}
				}
			}	
			
			return $sql_arr;
		}
		
		protected function _load_content($table, $where = '', $order_by = '', $select='', $left_join = '', $debug = 0){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			
			$sql = 'SELECT t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$order_by;
			
			if($debug) echo $sql;
			return GetAll($sql);
		}
		
		protected function _load_content_assoc($table, $where = '', $order_by = '', $select='', $left_join = '', $debug = 0){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			
			$sql = 'SELECT t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$order_by;
			
			if($debug) echo $sql;
			return GetAssoc($sql);
		}
		
		protected function getFilterGET($filter_name = null, $prev = '&')
		{
		   if(!(is_array($filter_name))) $filter_name = array($filter_name);
		   
		   $GET = array();
		   foreach($_GET as $key => $item){
			if(!in_array($key, $filter_name)) $GET[] = $key.'='.$item;
		   }
		   
		   if($GET){
			return $prev.implode('&', $GET);
		   }else{
			return null;
		   }
		}
		protected function _load_content_info($table, $left_join = '', $where = '', $select = '', $debug = 0){ 
			
			if($where) $where = ' WHERE '.$where;
			if(!$select) $select = '* ';
		
			$sql = 'SELECT '.$select.'
					FROM '.$table.' '.$left_join.' 
					'.$where;
			if($debug) echo $sql;
			return GetRow($sql);
		}
		
		protected function _load_content_with_pn($table, $where, $order_by, $select='', $left_join = '', $it = 0, $pc = 9, $req_arr = '', $page_url = 'page', $group_by = ''){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			if($group_by) $group_by = ' GROUP BY '.$group_by;
			
			$page = intval($_GET[$page_url]);
			if($page < 1){		
				$page = 1;
				$url = explode(".",$_SERVER['REDIRECT_URL']);
				$u = $url[0]; 
				$p = '?'.$page_url.'=%s';
			}else{
				$u = $_SERVER['REDIRECT_URL'];
				$p = '?'.$page_url.'=%s';
			}
			
			AttachLib('PageNavigator');
			
			$sql = 'SELECT SQL_CALC_FOUND_ROWS t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$group_by.'
					'.$order_by.
					($it ? ' LIMIT '.(($page - 1) * $it).', '.$it:'');
			$result = GetAll($sql);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			
			if(!$it) $it = $res['total'];
			$pn = new PageNavigator($res['total'], $it, $pc, $u, $page, $p, '&'.$req_arr, true, true);
			return array('items' => $result, 'pn' => $pn, 'cnt' => $res['total']);
		}
		
		protected function _in_datetime($birthday){
			
			return implode('-', $birthday);
		}
	
		protected function _in_birthday($birthday){
			
			$birthday = explode('-', $birthday);											
			$day = explode(' ', $birthday[2]);
			return array('year' => $birthday[0], 'month' => $birthday[1], 'day' => $day[0]);
		}
		
		protected function _check_email($email){
			
			$sql = 'SELECT e.id, e.email, e.user_id
					FROM emails e
					WHERE e.email = \''.s($email).'\'';

			return GetRow($sql);
		}
		
		protected function _insert_email($email, $type, $user_id){
			
			$sql = 'INSERT INTO emails (email, type, user_id)
					VALUES (\''.$email.'\', \''.$type.'\', \''.$user_id.'\')';
			Execute($sql);
			
			$sql = 'SELECT max(id) as id FROM emails';
			return GetOne($sql);
		}
		
		protected function _delete_content($table, $where){
			
			if($where) $where = ' WHERE '.$where;
		
			$sql = 'DELETE
					FROM '.$table.'
					'.$where;
			
			return Execute($sql);
		}
		
		protected function _get_binded_items($id, $type, $what, $sort = NULL, $limit = NULL){
			global $DB;
			
			$tbl = '';
			$id_field = '';

			switch($what){
					
				case 'logo':
					$tbl = 'pictures';
					$id_field = 'id';
					break;
				
				default:
					return array();
			};
			
			$select = '';
			
			$sql = "SELECT 
						tb.*
					FROM 
						bindings b 
						INNER JOIN $tbl tb ON (b.to_obj = '$what' AND b.to_id = tb.$id_field)
					WHERE 
						b.from_obj = '$type' AND b.from_id = $id".($sort != NULL ? " ORDER BY create_ts ".$sort : "").($limit != NULL ? " LIMIT ".$limit : "");
			return GetAll($sql);
		}
		
		public function _save_image($file, $path, $type_arr) {
			
			global $config;

			if(is_array($type_arr)){
								
				$type_arr = $this->_get_img_size_arr($type_arr);

				$bool = false;
				$path = $config['absolute-path'].$path;
				
				$ext = end(explode('.', $file['name']));
				$this->MD5Filename = md5($file['tmp_name'].time()).'.'.$ext;

				foreach($type_arr as $t){
					
					if(mb_strlen($t[2]) > 8)
						$name = $t[2].'.'.$ext;
					else
						$name = $t[2].'-'.$this->MD5Filename; 
					$size = explode('x',$t[0]);

					copy($file['tmp_name'], $path.$name);
					@chmod($path.$name,0777);

					if ($size[0] && $size[1])
						$bool = $this->resize($path.$name, $file['type'], $size[0], $size[1]);
				}		
				return $bool;
			}	
		}
		public function _is_translation_exist($id, $lang, $table) {
			global $DB;
			
			$sql = 'SELECT *
					FROM '.$table.' 
					WHERE 	obj_id	= \''.$id.'\' AND
							lang	= \''.$lang.'\'';
			$res = $DB->GetRow($sql);
			
			if($res)
				return true;
			else
				return false;
		}	
		public function _get_img_size_arr($arr){
			if(is_array($arr)){
				foreach($arr as &$a){
					$a = explode(':', $a);
				}
				return $arr;
			}else
				return false;	
		}
		
	/**
		* Remove FORUM User
		*/
		private function forum_user_delete($mode, $user_id, $post_username = false)
		{
			$sql = 'SELECT *
					FROM phpbb_users
					WHERE username = \''.$user_id.'\'';
			$user_row = GetRow($sql);
		
			if (!$user_row){
				return false;
			}else{
				$user_id = intval($user_row['user_id']);
			}
		
			// Before we begin, we will remove the reports the user issued.
			$sql = 'SELECT r.post_id, p.topic_id
					FROM phpbb_reports r, phpbb_posts p
					WHERE r.user_id = '.$user_id.'
					AND p.post_id = r.post_id';
			$result = GetAll($sql);
		
			$report_posts = $report_topics = array();
			if($result)
				foreach ($result as $row){
					$report_posts[] = $row['post_id'];
					$report_topics[] = $row['topic_id'];
				}
		
			if (sizeof($report_posts))
			{
				$report_posts = array_unique($report_posts);
				$report_topics = array_unique($report_topics);
		
				// Get a list of topics that still contain reported posts
				$sql = 'SELECT DISTINCT topic_id
						FROM phpbb_posts
						WHERE 
							topic_id IN ('.implode(',', $report_topics).') AND
							post_reported = 1 AND 
							post_id IN ('.implode(',', $report_posts).')';
				$result = GetAll($sql);
		
				$keep_report_topics = array();
				if($result)
					foreach ($result as $row){
						$keep_report_topics[] = $row['topic_id'];
					}
		
				if (sizeof($keep_report_topics))
				{
					$report_topics = array_diff($report_topics, $keep_report_topics);
				}
				unset($keep_report_topics);
		
				// Now set the flags back
				$sql = 'UPDATE phpbb_posts
						SET post_reported = 0
						WHERE post_id IN ('.implode(',', $report_posts).')';
				Execute($sql);
		
				if (sizeof($report_topics))
				{
					$sql = 'UPDATE phpbb_topics
							SET topic_reported = 0
							WHERE topic_id IN ('.implode(',', $report_topics).')';
					Execute($sql);
				}
			}
		
			// Remove reports
			Execute('DELETE FROM phpbb_reports WHERE user_id = '.$user_id);
		/*
			if ($user_row['user_avatar'] && $user_row['user_avatar_type'] == 1)
				avatar_delete('user', $user_row);
		*/
			switch ($mode)
			{
				case 'retain':
					if ($post_username === false)
						$post_username = 'Р“РѕСЃС‚СЊ';
		
					// If the user is inactive and newly registered we assume no posts from this user being there...
					if ($user_row['user_type'] == 1 && $user_row['user_inactive_reason'] == 1 && !$user_row['user_posts'])
					{
					}
					else
					{
						$sql = 'UPDATE phpbb_forums
								SET forum_last_poster_id = \'1\', 
									forum_last_poster_name = \''.addslashes($post_username)."', 
									forum_last_poster_colour = ''
								WHERE forum_last_poster_id = $user_id";
						Execute($sql);
		
						$sql = 'UPDATE phpbb_posts
								SET poster_id = \'1\', 
									post_username = \''.addslashes($post_username)."'
								WHERE poster_id = $user_id";
						Execute($sql);
		
						$sql = 'UPDATE phpbb_posts
							SET post_edit_user = \'1\'
							WHERE post_edit_user = '.$user_id;
						Execute($sql);
		
						$sql = 'UPDATE phpbb_topics
							SET topic_poster = \'1\', 
								topic_first_poster_name = \''.addslashes($post_username) . "', 
								topic_first_poster_colour = ''
							WHERE topic_poster = $user_id";
						Execute($sql);
		
						$sql = 'UPDATE phpbb_topics
							SET topic_last_poster_id = \'1\', 
								topic_last_poster_name = \''.addslashes($post_username) . "', 
								topic_last_poster_colour = ''
							WHERE topic_last_poster_id = $user_id";
						Execute($sql);
		
						// Since we change every post by this author, we need to count this amount towards the anonymous user
		
						// Update the post count for the anonymous user
						if ($user_row['user_posts'])
						{
							$sql = 'UPDATE phpbb_users
								SET user_posts = user_posts + ' . $user_row['user_posts'] . '
								WHERE user_id = \'1\'';
							Execute($sql);
						}
					}
				break;
		
				case 'remove':
					/*if (!function_exists('delete_posts')){
						include($phpbb_root_path . 'includes/functions_admin.' . $phpEx);
					}*/
		
					$sql = 'SELECT topic_id, COUNT(post_id) AS total_posts
						FROM phpbb_posts
						WHERE poster_id = '.$user_id.' 
						GROUP BY topic_id';
					$result = GetAll($sql);
		
					$topic_id_ary = array();
					if($result)
						foreach ($result as $row){
							$topic_id_ary[$row['topic_id']] = $row['total_posts'];
						}
		
					if (sizeof($topic_id_ary))
					{
						$sql = 'SELECT topic_id, topic_replies, topic_replies_real
								FROM phpbb_topics
								WHERE topic_id IN ('.implode(',',array_keys($topic_id_ary)).')';
						$result = GetAll($sql);
		
						$del_topic_ary = array();
						if($result)
							foreach ($result as $row){
								if (max($row['topic_replies'], $row['topic_replies_real']) + 1 == $topic_id_ary[$row['topic_id']]){
									$del_topic_ary[] = $row['topic_id'];
								}
							}
		
						if (sizeof($del_topic_ary)){
							$sql = 'DELETE FROM phpbb_topics
									WHERE topic_id IN ('.implode(',', $del_topic_ary).')';
							Execute($sql);
						}
					}
		
					// Delete posts, attachments, etc.
					//delete_posts('poster_id', $user_id);
		
				break;
			}
		
			$table_ary = array('phpbb_users', 'phpbb_user_group', 'phpbb_topics_watch', 'phpbb_forums_watch', 'phpbb_acl_users', 'phpbb_topics_track', 'phpbb_topics_posted', 'phpbb_forums_track', 'phpbb_profile_fields_data', 'phpbb_moderator_cache', 'phpbb_drafts', 'phpbb_bookmarks', 'phpbb_sessions_keys');
		
			foreach ($table_ary as $table)
			{
				$sql = "DELETE FROM $table
					WHERE user_id = $user_id";
				Execute($sql);
			}
		
			//$cache->destroy('sql', MODERATOR_CACHE_TABLE);
		
			// Delete the user_id from the banlist
			$sql = 'DELETE FROM phpbb_banlist
					WHERE ban_userid = ' . $user_id;
			Execute($sql);
		
			// Delete the user_id from the session table
			$sql = 'DELETE FROM phpbb_sessions
					WHERE session_user_id = ' . $user_id;
			Execute($sql);
		
			// Remove any undelivered mails...
			$sql = 'SELECT msg_id, user_id
					FROM phpbb_privmsgs_to
					WHERE author_id = ' . $user_id . ' AND 
						folder_id = -3';
			$result = GetAll($sql);
		
			$undelivered_msg = $undelivered_user = array();
			if($result)
				foreach ($result as $row){
					$undelivered_msg[] = $row['msg_id'];
					$undelivered_user[$row['user_id']][] = true;
				}
		
			if (sizeof($undelivered_msg))
			{
				$sql = 'DELETE FROM phpbb_privmsgs
						WHERE msg_id IN ('.implode(',', $undelivered_msg).')';
				Execute($sql);
			}
		
			$sql = 'DELETE FROM phpbb_privmsgs_to
					WHERE author_id = '.$user_id.' AND 
						folder_id = -3';
			Execute($sql);
		
			// Delete all to-information
			$sql = 'DELETE FROM phpbb_privmsgs_to
					WHERE user_id = ' . $user_id;
			Execute($sql);
		
			// Set the remaining author id to anonymous - this way users are still able to read messages from users being removed
			$sql = 'UPDATE phpbb_privmsgs_to
					SET author_id = 1 
					WHERE author_id = ' . $user_id;
			Execute($sql);
		
			$sql = 'UPDATE phpbb_privmsgs
				SET author_id = 1 
				WHERE author_id = ' . $user_id;
			Execute($sql);
		
			foreach ($undelivered_user as $_user_id => $ary)
			{
				if ($_user_id == $user_id)
				{
					continue;
				}
		
				$sql = 'UPDATE phpbb_users
						SET user_new_privmsg = user_new_privmsg - ' . sizeof($ary) . ',
							user_unread_privmsg = user_unread_privmsg - ' . sizeof($ary) . '
						WHERE user_id = ' . $_user_id;
				Execute($sql);
			}
		
			// Reset newest user info if appropriate
			if ($config['newest_user_id'] == $user_id){
				$sql = 'SELECT user_id, username, user_colour
						FROM phpbb_users
						WHERE user_type IN (\'0\', \'3\')
						ORDER BY user_id DESC';
				$row = GetRow($sql);
				if ($row){
					$sql = 'UPDATE phpbb_config	
							SET config_value = \'newest_user_id\'
							WHERE config_name = \''.$row['user_id'].'\'';
					Execute($sql);
					$sql = 'UPDATE phpbb_config	
							SET config_value = \'newest_username\'
							WHERE config_name = \''.$row['username'].'\'';
					Execute($sql);
					$sql = 'UPDATE phpbb_config	
							SET config_value = \'newest_user_colour\'
							WHERE config_name = \''.$row['user_colour'].'\'';
					Execute($sql);
				}
			}
		
			// Decrement number of users if this user is active
			if ($user_row['user_type'] != USER_INACTIVE && $user_row['user_type'] != USER_IGNORE){
				Execute('UPDATE phpbb_config SET config_value = config_value - 1 WHERE config_name = \'num_users\'');
			}
		
			return false;
		}
		
		protected function GetCompanies(){
			global $DB;
			$sql = "SELECT u.*,c.name AS c_name,c.id AS c_id FROM users u, companies c WHERE u.type = 'company' AND u.active = '0' AND c.published = 0 ORDER BY u.create_ts ASC";
			return $DB->GetAll($sql);
		}
		
		public function _update($id, $data, $table, $diff_arr = array()) {
			global $DB;
			
			$query = $this->_create_arr_for_sql($data, $diff_arr);
			
			$sql = 'UPDATE '.$table.' 
					SET	'.implode(', ', $query).' 
					WHERE id=\''.$id.'\'';
			// _debug($sql, 1);
			$DB->Execute($sql);
		}
			
		public function _insert($data, $table, $diff_arr = array()) {
			
			global $DB;
			
			$query = $this->_create_arr_for_sql($data, $diff_arr);
			
			$sql = 'INSERT INTO '.$table.'  
					SET '.implode(', ', $query);
			
			if($DB->Execute($sql)){
				return GetOne('SELECT LAST_INSERT_ID() FROM '.$table);		
			}else{
				return false;
			}
		}
		public function _update_translation($id, $data, $table, $diff_arr, $lang = 'rus') {
			global $DB;
			
			$query = $this->_create_arr_for_sql($data, $diff_arr);
			
			$sql = 'UPDATE '.$table.' 
					SET	'.implode(', ', $query).' 
					WHERE	obj_id	= \''.$id.'\' AND
							lang	= \''.$lang.'\'';
//echo $sql;

			$DB->Execute($sql);
		}
		public function _get_lang_html($lang){
		
		$this->Lang = $lang;
		include(PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'content.php');
		}
		protected function translit($st){
		
			$st=strtr($st,"абвгдеёзийклмнопрстуфхъыэ_",
			"abvgdeeziyklmnoprstufh'iei");
			$st=strtr($st,"АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ_",
			"ABVGDEEZIYKLMNOPRSTUFH'IEI");
			$st=strtr($st, 
							array(
								"ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", 
								"щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
								"Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH", 
								"Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
								"ї"=>"i", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye"
								)
					 );
		
			return $st;
		}
		
		protected function _get_html_form($name, $fields_conf = array()) {
			global $DB;
			
			$sql = 'SHOW FULL COLUMNS FROM ' . $name;
			$table = $DB->GetAll($sql);
			$form = '';
			$result = array();
			$i = 0;
			foreach($table as $row){
				$tmp_row = array();
				if(@in_array($row['Field'], $this->HiddenFields))
					continue;
				switch($row['Field']){
					case 'id':
						$tmp_row['name'] = $row['Field'];
						$tmp_row['xtype'] = 'hidden';
						$tmp_row['sort_order'] = $i++;
						$result[$row['Field']] = $tmp_row;
					break;
						
					default:
						$tmp_arr = explode('(', $row['Type']);
						$field_type = reset($tmp_arr);
						$tmp_row['name'] = $row['Field'];
						$tmp_row['xtype'] = 'textfield';
						$tmp_row['fieldLabel'] = $row['Field'];
						$tmp_row['allowBlank'] = $row['Null'] == 'YES';
						if($row['Comment'])
							$tmp_row['fieldLabel'] = $row['Comment'];
							
						switch($field_type){
							case 'varchar':
								$size = intval(reset(explode(')', end($tmp_arr))));
								if($size > 255){
									$tmp_row['xtype'] = 'textarea';
								}
								break;
							
							case 'tinyint':
								$size = intval(reset(explode(')', end($tmp_arr))));
								if($size > 1){
									$tmp_row['xtype'] = 'checkbox';
									$tmp_row['inputValue'] = 1;
								}
								break;
							
							case 'text':
								$tmp_row['xtype'] = 'textarea';
								break;
							
							case 'datetime':
								$tmp_row['xtype'] = 'datetime';
								break;
							
							case 'date':
								$tmp_row['xtype'] = 'datefield';
								break;
							
							case 'time':
								$tmp_row['xtype'] = 'timefield';
								break;
							
							case 'enum':
								$tmp_str = substr($row['Type'],0,-2);
								$tmp_arr = explode('(\'', $tmp_str);
								$opt_arr = explode('\',\'', end($tmp_arr));
								$options = array();
								if($opt_arr)
									foreach($opt_arr as $value)
										$options[] = array($value);
								$tmp_row['options'] = $options;
								$tmp_row['options'] = $options;
								$tmp_row['xtype'] = 'combo';
								break;
								
							default:;
						}
						$tmp_row['sort_order'] = $i++;
						
						if($fields_conf[$row['Field']] && is_array($fields_conf[$row['Field']])){
							foreach($fields_conf[$row['Field']] as $key=>$_field_conf){
								$tmp_row[$key] = $_field_conf;
							}
						}
						
						$result[$row['Field']] = $tmp_row;
					break;
				}
			}
			return $result;
		}
		
		public function _get_country_list() {
				global $DB;
			
			$sql = 'SELECT * FROM country';
			return $DB->GetAll($sql);
		}
		public function _get_city_list() {
				global $DB;
			
			$sql = 'SELECT * FROM city';
			return $DB->GetAll($sql);
		}
		
		public function _make_correct_url($url){
		
			$url = str_replace(array(' ', '/'), '_', trim($url));
			return $url;
		}
	}

?>