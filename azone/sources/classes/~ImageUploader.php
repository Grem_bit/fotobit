<?
	class ImageUploader {
	
		protected $Sys_Vars				= array();
		private $ImagesDir		= '/public/files/ob-photos/';
		private $Size			= array('small'=>array('width'=>112, 'height'=>112), 
										'medium'=>array('width'=>195, 'height'=>127), 
										'middle'=>array('width'=>181, 'height'=>98), 
										'large'=>array('width'=>600, 'height'=>600));
		
		public function __construct() {
			
			$this->OnPhotoUpload();
		}
		
		public function OnPhotoUpload() {
			header('Content-Type: text/html; charset=windows-1251');
			$result = array();
			
			$this->_get_sys_vars();
			
			if (isset($_FILES['photoupload']) ) {
				
				$file = $_FILES['photoupload']['tmp_name'];
				$error = false;
				$size = false;
			 
				if (!is_uploaded_file($file) || ($_FILES['photoupload']['size'] > 2 * 1024 * 1024) ) {
					
					$error = '!';
				}
				
				if (!$error && !($size = @getimagesize($file) ) ) {
					
					$error = '!';
				}
			 
				if ($error) {
					
					$result['result'] = 'failed';
					$result['error'] = $error;
				}
				else {
					
				
					
					$result['result'] = 'success';
					$result['size'] = " ({$size['mime']}) -  {$size[0]}px/{$size[1]}px.";
					header('Content-type: application/json');
					echo $result['size'];
					
					$this->_save_image($_FILES['photoupload'], $size['mime']);
					
					exit;
				}
			}
			else {
				
				$result['result'] = 'error';
				$result['error'] = 'Missing file or internal error!';
			}
			 
			if (!headers_sent() ) {
				
				header('Content-type: application/json');
			}
		
			echo $result[1];
			exit;
		}
		
		private function _get_file_ext($filename) {
			
			return end(explode('.', $filename));
		}
		
		private function _save_image($file, $sid) {
			
			global $DB,$config;
			
			
			$path = $config['absolute-path'].'public/files/images/products/';

			$ext = $this->_get_file_ext($file['name']);
			
			$large = md5($file['tmp_name'].time()).'.'.$ext;
			$thumb = 'thumb-'.$large;
			$middle = 'middle-'.$large;
			move_uploaded_file($file['tmp_name'], $path.$large);
			/*@chmod($path.$large,0777);*/
			//copy($path.$large, $path.$thumb);
			//copy($path.$large, $path.$middle);
			/*@chmod($path.$thumb,0777);*/
				
			//$this->resize($path.$thumb, $sid, 55, 50);
			//$this->resize($path.$middle, $sid, 179, 183);
			/*$this->InsertLogo($path.$large, $path.$large, 100, 4, true, $sid);   
			$this->InsertLogo($path.$thumb, $path.$thumb, 100, 4, false, $sid);
			$this->InsertLogo($path.$tiny, $path.$tiny, 100, 4, false, $sid);
			$this->InsertLogo($path.$middle, $path.$middle, 100, 4, false, $sid);*/
			
			
			$sql = "INSERT INTO photos (product_id, filename)
					VALUES ('".$_GET['id']."', '".$large."')";
			$DB->Execute($sql);
		}
		
		final public function resize($file, $type, $height, $width){
			
		    $img = false;
			
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = @imagecreatefromjpeg($file);
					break;
				case 'image/x-png':
				case 'image/png':
					$img = @imagecreatefrompng($file);
					break;
				case 'image/gif':
					$img = @imagecreatefromgif($file);
					break;
		    }
		    if(!$img){
				return false;
		    }
				
		    
		    $curr = @getimagesize($file);
		    
		    $perc_w = $width / $curr[0];
		    $perc_h = $height / $curr[1];
		    
		    if(($width > $curr[0]) && ($height > $curr[1])){
				return true;
		    }
		    
		    if($perc_h > $perc_w){
				$width = $width;
				$height = round($curr[1] * $perc_w);
		    } else {
				$height = $height;
				$width = round($curr[0] * $perc_h);
		    }
		    
		  
		    $nwimg = @imagecreatetruecolor($width, $height);
		    @imagecopyresampled($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[1]);
		    
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					@imagejpeg($nwimg, $file);
					break;
				case 'image/x-png':
				case 'image/png':
					@imagepng($nwimg, $file);
					break;
				case 'image/gif':
					@imagegif($nwimg, $file);
					break;
		    }
		    
		    @imagedestroy($nwimg);
		    @imagedestroy($img);
			return true;
		}		
		
		protected function _get_sys_vars() {
			
			global $DB;
			
			$sql = 'SELECT var_name, var_value FROM sys_vars';
			$this->Sys_Vars = $DB->GetAssoc($sql);
		}
		
		
		function InsertLogo($file_from,$file_to,$q=100,$place=1,$big = true, $type) {

				$path = $_SERVER['DOCUMENT_ROOT'];
				if($path[strlen($path)-1] != "/")$path .= "/";
				
			 	if($big){
					$path_logo = $path."public/images/watermark.png";
				}else{
					$path_logo = $path."public/images/watermark-small.png";
				}
			
				$logo = @ImageCreateFromPNG($path_logo); 
				$a1= @GetImageSize($path_logo); 
				
		        $logo_places = array(
								1=>array(text => "���� - �����", x => 0, y => 0),  
								2=>array(text => "���� - ������", x => 1, y => 0),
								3=>array(text => "��� - �����", x => 0, y => 1),
								4=>array(text => "��� - ������", x => 1, y => 1),
				); 
				
		        $kx = $logo_places[$place][x];
		        $ky = $logo_places[$place][y];
		        $logo = @ImageCreateFromPNG($path_logo);
		        $a1= @GetImageSize($path_logo);
		        $a2 = @GetImageSize($file_from);
		        
				switch ($type){
					case 'image/jpeg':
					case 'image/jpg':
					case 'image/pjpeg':
						$fr = @imagecreatefromjpeg($file_from);
						break;
					case 'image/x-png':
					case 'image/png':
						$fr = @imagecreatefrompng($file_from);
						break;
					case 'image/gif':
						$fr = @imagecreatefromgif($file_from);
						break;
				}
		        $f=@ImageCreateTrueColor(imagesx($fr),imagesy($fr));
		        @ImageCopy($f,$fr,0,0,0,0,imagesx($fr),imagesy($fr));
		        @ImageDestroy($fr);
		        @ImageAlphaBlending($f, 1);
		        @ImageAlphaBlending($logo, 1);
		        @ImageCopy($f, $logo, $kx * ($a2[0] - $a1[0]) + 2 * (0.5 - $kx), $ky * ($a2[1] - $a1[1]) + 2 * (0.5 - $ky), 0, 0, $a1[0], $a1[1]);
		        if(file_exists($file_to))unlink($file_to);
				switch ($type){
					case 'image/jpeg':
					case 'image/jpg':
					case 'image/pjpeg':
						@imagejpeg($f,$file_to,$q);
						break;
					case 'image/x-png':
					case 'image/png':
						@imagepng($f, $file_to);
						break;
					case 'image/gif':
						@imagegif($f, $file_to);
						break;
				}

			   
		        chmod($file_to,0777);
		} 
		
		
		
	}
?>