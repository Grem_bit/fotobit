<?
	class Options extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'options/';		protected $PageNavigator			= null;		protected $Grid						= null;		protected $questions				= array();		protected $questions_sz				= 0;		protected $ProdInfo					= array();		protected $Categories				= array();		protected $Categories_sz			= 0;		protected $CatIdList				= array();
		protected $HTMLelements				= array(
													array(
														'id'	=> 'text',
														'title'	=> 'Текст(TEXT)'
													),
													array(
														'id'	=> 'select',
														'title'	=> 'Выбор из нескольких вариантов(SELECT)'
													),
													array(
														'id'	=> 'checkbox',
														'title'	=> 'Да/Нет(CHECKBOX)'
													),
													array(
														'id'	=> 'radio',
														'title'	=> 'Выбор из нескольких вариантов(RADIO)'
													)
											);		protected $DescriptionInfo			= array();
		protected $LHeight1					= 120;		protected $LHeight2					= 260;		protected $LWidth1					= 124;		protected $LWidth2					= 170;
		protected $MD5FileName				= '';		protected $FileError				= '';		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
					$this->SetTemplate('main.html');
			$this->_load_categories();			$this->_init_grid();
			$this->AddJS($this->RootUrl.'public/js/fill.js');		}				public function OnNew() {
					$this->SetTemplate('edit.html');

			$sql = 'SELECT id, title
					FROM product_options_groups';
			$this->Chars = GetAll($sql);
			
			$this->_load_prod_description(0);			$this->_load_categories();		}
		public function OnInsert() {			if(!$this->_check_data($_POST)){				$this->OnNew();			} else {				$id = $this->_insert_question($_POST);				header('Location: /azone/options/');			}		}				public function OnRemove() {
			global $DB;
			
			$delete = GetOne('SELECT `delete` FROM product_options WHERE id = '.intval($_GET['id']));
			
			if( !$delete ){
				$sql = "delete from products_variants where option_id ='".intval($_GET['id'])."'";
				$DB->Execute($sql);
				
				$sql = "DELETE FROM product_option_type WHERE option_id ='".intval($_GET['id'])."'";
				$DB->Execute($sql);
				
				$sql = "delete from product_options where id ='".intval($_GET['id'])."'";
				$DB->Execute($sql);
				
				$sql = "delete from product_option_variants where option_id ='".intval($_GET['id'])."'";
				$DB->Execute($sql);		
			}			$this->OnDefault();		}
		public function OnEdit() {
			
			$sql = 'SELECT id, title
					FROM product_options_groups';
			$this->Chars = GetAll($sql);
						$this->_load_option_info(intval($_GET['id']));
						$this->_load_prod_description(intval($_GET['id']));
			
			$this->_load_categories();
						$this->SetTemplate('edit.html');		}
		public function OnUpdate() {			if(!$this->_check_data($_POST, true)) {				$this->OnEdit();			}			else {				$this->_update_question(intval($_GET['id']), $_POST);				if($_POST['redirect']) {					header('Location: '.$_POST['redirect'].'&id='.intval($_GET['id']));				}				else {					$this->OnDefault();				}			}		}
		public function OnAddDescription() {
					$this->SetTemplate('description.html');		}
		public function OnInsertDescription() {			if(!$this->_check_data($_POST)) {
							$this->OnAddDescription();			}			else {				$this->_insert_description($_POST, $_GET['id']);				header('Location: '.$this->RootUrl.'options/?Event=Edit&id='.$_GET['id']);			}		}
		public function OnEditDescription() {			$this->SetTemplate('description.html');			
			$this->ProdInfo = GetRow('SELECT * FROM product_option_variants WHERE id = '.$_GET['desc_id']);		}
		public function OnUpdateDescription() {			global $DB;
						$this->_update($_GET['desc_id'], $_POST, 'product_option_variants', array('Event'));
						header('Location: '.$this->RootUrl.'options/?Event=Edit&id='.$_GET['id']);		}
		public function OnRemoveDescription() {			global $DB;
			
			Execute('DELETE FROM product_option_variants WHERE id = '.$_GET['desc_id']);						header('Location: '.$this->RootUrl.'options/?Event=Edit&id='.$_GET['id']);
		}
		
		
		/*		 * Private methods		 */

		private function _insert_description($data, $prod_id){			
			return $id = $this->_insert($data, 'product_option_variants', array('Event'));
		}
		private function _load_prod_description($id) {			global $DB;
						$sql = "SELECT 
						* 
					FROM 
						product_option_variants
					WHERE 
						option_id		= ".intval($id)."
					ORDER BY 
						sort
			";
						$this->Description = $DB->GetAll($sql);			$this->Description_sz = count($this->Description);
			
			$this->AttachComponent('GridAllQ', $this->Grid);			/*$this->Description_sz = $description_sz;
			$titles = array('Название/Значение');			
			$options['row_numbers'] = true;
						if($this->Perm['edit']) {				$options['controls'] = array('remove' => 'javascript:;');			}			$rows = array();			
			for($i = 0; $i < $description_sz; $i++){
									$row = array(
						'<input type="text" value="'.$description[$i]['title'].'" name="opt['.$i.']"/>'
					);
								 					$rows[$description[$i]['id']] = $row;			}
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);			$this->Grid->SetData($data);*/		}
		private function _load_option_info($id) {			global $DB;
						$sql = 'SELECT 
						* 
					FROM 
						product_options 
					WHERE 
						id	=\''.$id.'\'';			$this->ProdInfo = $DB->GetRow($sql);
			
			$sql = "SELECT 
						COUNT(id) 
					FROM 
						product_option_variants
					WHERE 
						option_id = ".$id;
			
			$this->ProdInfo['option_cnt'] = GetOne($sql);
			
			$this->Types = GetCol('SELECT category_id FROM product_option_type WHERE option_id=\''.intval($_GET['id']).'\'');
			//_log('SELECT category_id FROM product_option_type WHERE option_id=\''.intval($_GET['id']).'\'');
			
		}
		private function _update_question($id, $data) {//_debug($data, 1);			global $DB;			
			if(!$data['error:str'])
				$data['error:str'] = 'Заполните поле!';
			
			$sql = 'SELECT * 
					FROM product_options 
					WHERE id	=\''.$id.'\'';
			
			$this->ProdInfo = $DB->GetRow($sql);	

			if($this->ProdInfo['delete']){
				unset($data['title:str']);
			}
						$this->_update($id, $data, 'product_options', array('Event', 'categories'));
			
			
			if(is_array($data['opt']) && $data['element:str'] != 'text' && $data['element:str'] != 'checkbox'){
				$ids = array(0);
				
				foreach($data['opt'] as $key=>$opt){
					if($opt){
						if($data['opt_id'][$key] == 'new'){
							$ids []= $this->_insert(
								array(
									'title:str'		=> $opt,
									'price:float'	=> $data['price'][$key],
									'price2:float'	=> $data['price2'][$key],
									'price3:float'	=> $data['price3'][$key],
									'side1:int'	=> $data['side1'][$key],
									'side2:int'	=> $data['side2'][$key],
									'option_id:int'	=> $id,
									'sort:int'		=> $key
								),
								'product_option_variants',
								array('Event')
							);
						}elseif($data['opt_id'][$key]){
							$ids []= intval($data['opt_id'][$key]);
							$this->_update(
								intval($data['opt_id'][$key]), 
								array(
									'title:str'		=> $opt,
									'price:float'	=> $data['price'][$key],
									'price2:float'	=> $data['price2'][$key],
									'price3:float'	=> $data['price3'][$key],
									'side1:int'	=> $data['side1'][$key],
									'side2:int'	=> $data['side2'][$key],
									'option_id:int'	=> $id,
									'sort:int'		=> $key
								), 
								'product_option_variants', 
								array('Event')
							);
							
						}
					}
				}

				$sql = 'DELETE 
						FROM product_option_variants
						WHERE option_id	= \''.$id.'\' AND id NOT IN(\'' . implode("','", $ids) . '\')';
				$DB->Execute($sql);
				
			}
			
			$sql = 'DELETE 
					FROM product_option_type
					WHERE option_id	= \''.$id.'\'';
			$DB->Execute($sql);

			if(is_array($data['type'])){
				
				foreach ($data['type'] as $c){
					
					$sql = 'INSERT INTO product_option_type (
								option_id,
								category_id)
							VALUES	(
								\''.$id.'\',
								\''.$c.'\')';
					
					$DB->Execute($sql);
				}
			}
		}
		private function _insert_question($data) {			
			if(!$data['error:str'])
				$data['error:str'] = 'Заполните поле!';
							$id = $this->_insert($data, 'product_options', array('Event', 'categories'));
			
			if(is_array($data['opt']) && $data['element:str'] != 'text' && $data['element:str'] != 'checkbox'){
				foreach($data['opt'] as $key=>$opt){
					if($opt)
						$this->_insert(
							array(
								'title:str'		=> $opt,
								'option_id:int'	=> $id,
								'sort:int'		=> $key
							),
							'product_option_variants',
							array('Event')
						);
				}
			}
			
			if(is_array($data['type'])){
				
				foreach ($data['type'] as $c){
					
					$sql = 'INSERT INTO product_option_type (
								option_id,
								category_id)
							VALUES	(
								\''.$id.'\',
								\''.$c.'\')';
					
					Execute($sql);
				}
			}
			
			return $id;
					}
		private function _check_data($data, $update = false) {
					$this->Errors['title'] = $data['title:str'] ? false : true;
			
			if($data['Event'] == 'Insert' || $data['Event'] == 'Update'){
				$this->Errors['element'] = $data['element:str'] ? false : true;
			}						$this->ShowError = in_array(true, $this->Errors);			return !$this->ShowError;		}
		private function _load_options() {
					global $DB;
			
			$order = 'ORDER BY ps.order_by ASC';
			
			if($_GET['sort'] && $_GET['direct']){
				$order = 'ORDER BY '.mysql_escape_string($_GET['sort']).' '.mysql_escape_string($_GET['direct']);
			}
			
			$sql = 'SELECT ps.id, 
						   ps.title, 
						   ps.element,
						   ps.main,
						   ps.fill, 
						   ps.order_by, 
						   ps.user_choice, 
						   COUNT(povv.id) as count_variants, 
						   c.title as cat_title, 
						   pog.title as group_title 
					FROM 
						product_options as ps
						LEFT JOIN 
						product_option_variants as povv 
						ON ps.id = povv.id
						LEFT JOIN 
						product_option_type pot 
						ON ps.id = pot.option_id
						LEFT JOIN 
						categories c
						ON c.id = pot.category_id
						LEFT JOIN 
						product_options_groups pog 
						ON ps.type_id = pog.id 
					' . ( intval($_GET['cat']) ? 'WHERE c.id = ' . intval($_GET['cat']) : '' ) . '
				#	WHERE ps.`user_choice` != 1    # убрать при надобности !!!!!!!!!!!!
					GROUP BY ps.id 
					'.$order;		//	_debug($sql);			$this->questions = $DB->GetAll($sql);
			$this->questions_sz = count($this->questions);
		}
		private function _init_grid() {
					$this->_load_options();
						$this->AttachComponent('GridAll', $this->Grid);
						//$titles = array('ID', 'Опция', 'Категория', 'Группа', 'HTML элемент', 'Выбор польз-ля', 'Обяз. заполн.', 'Сорт.');
			//$titles = array('ID', 'Опция', 'Категория', 'Группа', 'HTML элемент', 'Обяз. заполн.', 'Сорт.');
			$titles = array('ID', 'Опция',  'HTML элемент', 'Обяз. заполн.', 'Сорт.');
			
			$options['row_numbers'] = false;			if($this->Perm['edit']) {				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');			}
			
			$options['sortable'] = array(	
				1 => array(	
					'up' => $this->RootUrl.'options/?sort=ps.title&direct=ASC', 
					'down' => $this->RootUrl.'options/?sort=ps.title&direct=DESC'
				),
				2 => array(	
					'up' => $this->RootUrl.'options/?sort=cat_title&direct=ASC', 
					'down' => $this->RootUrl.'options/?sort=cat_title&direct=DESC'
				),
				3 => array(	
					'up' => $this->RootUrl.'options/?sort=group_title&direct=ASC', 
					'down' => $this->RootUrl.'options/?sort=group_title&direct=DESC'
				)
			);	
						$rows = array();
						for($i = 0; $i < $this->questions_sz; $i++){
				
				$user = $this->questions[$i]['user_choice'] ? '<div id="container'.$this->questions[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->questions[$i]['id'].', 0, \'product_options\', \'user_choice\', \'container'.$this->questions[$i]['id'].'_'.$i.'\')">Да</span></div>' : '<div id="container'.$this->questions[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->questions[$i]['id'].', 1, \'product_options\', \'user_choice\', \'container'.$this->questions[$i]['id'].'_'.$i.'\')">Нет</span></div>';
				$user = $this->questions[$i]['element'] != 'text' && $this->questions[$i]['element'] != 'checkbox' ? $user : '';
				
				$fill = $this->questions[$i]['fill'] ? '<div id="containerfill_'.$this->questions[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->questions[$i]['id'].', 0, \'product_options\', \'fill\', \'containerfill_'.$this->questions[$i]['id'].'_'.$i.'\')">Да</span></div>' : '<div id="containerfill_'.$this->questions[$i]['id'].'_'.$i.'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->questions[$i]['id'].', 1, \'product_options\', \'fill\', \'containerfill_'.$this->questions[$i]['id'].'_'.$i.'\')">Нет</span></div>';
								$row = array(
					$this->questions[$i]['id'],
					$this->questions[$i]['title'],
					//$this->questions[$i]['cat_title'],
					//$this->questions[$i]['group_title'] ? $this->questions[$i]['group_title'] : 'Без группы',
					$this->questions[$i]['element'],
					//$user,
					$fill,
					$this->questions[$i]['order_by']
				);
							  				$rows[$this->questions[$i]['id']] = $row;			}
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);			$this->Grid->SetData($data);		}
		
		private function _load_categories($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT 
						id, 
						title 
					FROM 
						categories 
					WHERE 
						parent_id=\''.$parent_id.'\'
			';
			
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['title'] = str_repeat('&nbsp;', $level*4).$cat[$i]['title'];
				$cat[$i]['level'] = $level;
				array_push($this->Categories, $cat[$i]);
				$level++;
				$this->_load_categories($cat[$i]['id']);
				$level--;
			}
			
			if($parent_id == 0) {
				
				$this->Categories_sz = count($this->Categories);
			}
		}
	}

?>