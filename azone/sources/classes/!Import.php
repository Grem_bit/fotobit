<?
	class Import extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'import/';
		protected $BList					= null;
		
		protected $UrlParts					= array();
		protected $PageNavigator			= null;
		
		protected $excel_configurator 		= '';
		protected $file_excel_name 			= '';
		protected $UpdateTime 				= '';
		protected $UpdatePrd				= array();
		protected $UpdatePrd_sz				= 0;
		protected $Line						= 1;
		
		protected $ErLog					= array();
		protected $ImpCnt					= array('brand' => 0, 'model' => 0, 'ins' => 0, 'upd' => 0, 'prod' => 0);
		protected $Del_arr 					= array('под шип', 'шипы');
		public 	  $FatalErrors			= array();
		private   $MakerCode				= '';
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			global $url_parts, $DB;

			$this->AddJS($this->RootUrl.'public/js/pages.js');
			
			$this->UrlParts = $url_parts;

			$this->SetTemplate('main.html');
		}
		
	function detect_my_utf($s){
		 $s=urlencode($s); // в некоторых случаях - лишняя операция (закоментируйте)
		 $res='0';
		 $j=strlen($s);
		
		 $s2=strtoupper($s);
		 $s2=str_replace("%D0",'',$s2);
		 $s2=str_replace("%D1",'',$s2);
		 $k=strlen($s2);
		
		 $m=1;
		 if ($k>0){
		  $m=$j/$k;
		  if (($m>1.2)&&($m<2.2)){ $res='1'; }
		 }
		 return $res;
		}
		
		
		public function OnImport() {
			$this->AddJS($this->RootUrl.'public/js/pages.js');	
			global $config, $url_parts, $DB;
			
			$this->UrlParts = $url_parts;
			
			if (isset($_FILES["csv"]) && $_FILES["csv"]["name"]){
				if(strtolower(_fgetex($_FILES["csv"]["name"])) != 'csv'){
					$this->FatalErrors[] = "Импортируются данные только CSV формата. Импортируйте ваши данные в CSV формат.";
				} else {
					$this->file_excel_name = $config['absolute-path']."temp/file".date("-d-m-Y_H-i-s").".csv";
					$res = move_uploaded_file($_FILES["csv"]["tmp_name"], $this->file_excel_name);
				}
			}else{
				$this->FatalErrors[] = "Укажите файл CSV формата для импорта продукции.";
			} 

			if (isset($res) && $res){

				$this->_setRightsToUploadedFile( $this->file_excel_name );

				$data = $this->_myfgetcsv($this->file_excel_name, ';');
			
				if($data){
					
					$data = $this->_check_data($data);
					if($data){
						
						if(!$this->detect_my_utf($data[0][0])){
							foreach ($data as &$import){
								foreach ($import as &$val){
									$val=iconv("CP1251", "UTF-8", $val);	
								}
							}					
						}
						//_debug($data) ;exit;
						$this->_import($data);
					}
				
				}
			}
			
			$this->SetTemplate('main.html');
		}
		
		/*
		 * Private methods
		 */
		
		protected function _import($data){
			$titleColumns = $data[0];
			array_shift($data);
			$makerField = array_search('производитель',$titleColumns);
			$parent_id_maker = GetOne("SELECT `id` FROM `pages` WHERE TRIM(`title`) = 'Производители'");
			$catField = array_search('категория',$titleColumns);
			$collField = array_search('коллекция',$titleColumns); 
			$parent_id_coll = GetOne("SELECT `id` FROM `pages` WHERE TRIM(`title`) = 'Коллекции'");
			$temp = GetAll("SHOW FULL COLUMNS	FROM `products` ");
			$defaultCourse =GetOne("SELECT `id` FROM `course` WHERE `default` = 1");
			$atrtiklField = array_search('артикул',$titleColumns);
			$publishFields = array_search('наличие',$titleColumns);
			//$photoFields   = array_search('системный столбец',$titleColumns);
			$photoFields   = array_search('артикул',$titleColumns);
			$photoAddFields   = array_search('системный столбец',$titleColumns);
			$ExImg = array('.jpg', '.JPG', '.png', '.PNG');
			
			if($_POST['update']=='remove'){		// удаляем
				foreach ($data as $import){
					if($import[$atrtiklField]){
						$import[$atrtiklField] = str_replace("/","-",$import[$atrtiklField]);
						$import[$atrtiklField] = str_replace(".","",$import[$atrtiklField]);
						$idPr = GetOne("SELECT `id` FROM `products` WHERE `code` = '".$import[$atrtiklField]."'");
						if($idPr){
							if(Execute("DELETE FROM `products` WHERE `code` = '".$import[$atrtiklField]."'")){
								$this->DeleteFile($idPr);
								$this->ImpCnt['remove']++;
							}
						}
					}else{
						$this->ErLog[] = 'Не обнаружено артикула в строке - '.implode(';',$import);
					}
				}
			}else{								// добавляем или обновляем
			
			//_log($titleColumns);
			
			if(is_for($temp)){
				foreach ($temp as $val){
					if($val['Comment'])
						$fields[$val['Comment']] = $val['Field'];  	
				}
			}

			if(is_for($titleColumns)){
				foreach ($titleColumns as &$tit){
					if(array_key_exists($tit,$fields)){
						$tit = $fields[$tit];
					}else{
						$tit = '';
					}
				}
			}
		
			foreach ($data as $import){
					$q = '';
				if($import[$atrtiklField]){
					$import[$atrtiklField] = str_replace("/","-",$import[$atrtiklField]);
					$import[$atrtiklField] = str_replace(".","",$import[$atrtiklField]);
					for ($i = 0, $cn = count($import); $i < $cn; $i++) {
						if($import[$i] != '' && $titleColumns[$i] != ''){
							$add = ',';
							//if($i == $cn - 1) $add = '';
							$q .= " `".$titleColumns[$i]."` = '".m(trim($import[$i]))."'".$add;
							
						}	
					}
					
					//_cut_str($q);
					
					// производители
					if($import[$makerField]){
									$brandi = $this->_get_brand_id($import[$makerField]);
								if($brandi){
									$add = ($q) ? ', ' : '';
									$q .=  $add." `maker_id` = '".$brandi."' ";  
								}else{
									$sql = 'INSERT INTO `pages`
											SET title 		= \''.$import[$makerField].'\',
												url 		= \''.$import[$makerField].'\', 
												`parent_id`	= \''.$parent_id_maker.'\',
												`publish`	= \'1\',	
												`create_ts` = NOW()'
									;
									if(Execute($sql)){
										$brandi = GetOne('SELECT LAST_INSERT_ID() FROM pages');
										if($brandi){
											$add = ($q) ? ', ' : '';
											$q .=  $add." `maker_id` = '".$brandi."' ";
										}
									}
								}
					}
					////////////////////////////////////
					// категории
				if($import[$catField]){
									$catid = $this->_get_cat_id($import[$catField]);
								if($catid){
									$add = ($q) ? ', ' : '';
									$q .=  $add." `category_id` = '".$catid."' ";  
								}else{

								$sql = 'INSERT INTO categories
									SET title 		= \''.$import[$catField].'\',
										parent_id	= 0,
										onFront		= 1,
										publish		= 1,
										url 		= \''._translit($import[$catField]).'\'';
								
									if(Execute($sql)){
										$catid = GetOne('SELECT LAST_INSERT_ID() FROM pages');
										if($catid){
											$add = ($q) ? ', ' : '';
											$q .=  $add." `category_id` = '".$catid."' ";
										}
									}
								}
					}
					// коллекции
					//_debug($q);
					////_cut_str($q);
				if($import[$collField]){
									$collid = $this->_get_coll_id($import[$collField]);
								if($collid){
									$add = ($q) ? ', ' : '';
									$q .=  $add." `сollection_id` = '".$collid."' ";  
								}else{
									$sql = 'INSERT INTO `pages`
											SET title 		= \''.$import[$collField].'\',
												url 		= \''.$import[$collField].'\', 
												`parent_id`	= \''.$parent_id_coll.'\',
												`publish`	= \'1\',	
												`create_ts` = NOW()'
									;
									if(Execute($sql)){
										$collid = GetOne('SELECT LAST_INSERT_ID() FROM pages');
										if($collid){
											$add = ($q) ? ', ' : '';
											$q .=  $add." `сollection_id` = '".$collid."' ";
										}
									}
								}
					}
						
						_cut_str($q);
						//_debug($q);
						// сам запрос импорта
						$sql='';$qw ='';
						if($q){
						
						$publish = ($import[$publishFields]) ? ' `publish` = \'1\' ' : ' `publish` = \'0\' ';
						$idProduct = '';
						$qw = "SELECT `id` FROM `products` WHERE `code` = '".($import[$atrtiklField])."'"; 
						$position = GetOne($qw);
							if($position){
								$sql = "UPDATE `products` SET `create_ts` = '".time()."', `url` = '".str_replace(" ","-", $import[2].'-'.$import[$atrtiklField])."', `currency_id` = '".$defaultCourse."', 
										".$publish.", `new` = '1', `title` = '".$import[2].' '.$import[$atrtiklField]."', ".$q." WHERE `id` = '".$position."'";
							//	_log($sql,'a');
								$this->ImpCnt['upd']++;
							}else{
								$sql = "INSERT INTO `products` SET `create_ts` = '".time()."', `url` = '".str_replace(" ","-", $import[2].'-'.$import[$atrtiklField])."', `currency_id` = '".$defaultCourse."',
										".$publish.", `new` = '1', `title` = '".$import[2].' '.$import[$atrtiklField]."', ".$q;
								$this->ImpCnt['ins']++;
								//_debug($qw);
								//_debug($sql,1);
								//_log($sql,'a');
							}
						}
						if($sql) Execute($sql);
					
					// фото
					$idProduct = GetOne($qw);
					if($idProduct){
							
							$file ='';
							$this->DeleteFile($idProduct);
							
							$def = 1;
								/*foreach ($ExImg as $val){
								//	_log($import[$photoFields].$val,'a');
									$file = $this->SaveImage($import[$photoFields].$val);
									
									if($file){
										$sql = "INSERT INTO photos SET
													`filename` = '".$file."',
													`product_id` = '".$idProduct."',
													`default` = '".$def."'			
													";	
	//									_log($qw.' ###'. $import[$atrtiklField].' ### '. $sql,'a');
										Execute($sql);
										$def = 0;
									}
								}*/
								
								$photos = explode('|', $import[$photoAddFields]);
								if(is_for($photos)){
									foreach ($photos as $val){
										$ph = GetOne("SELECT `filename` FROM `photos` WHERE `product_id` = '".$idProduct."' AND `filename` = '".$val."'");
										
										if(!$ph){
											//$file = $this->SaveImage($val);
											$file = $val;
											if($file){
												$sql = "INSERT INTO photos SET
															`filename` = '".$file."',
															`product_id` = '".$idProduct."',
															`default` = '".$def."'			
															";	

												Execute($sql);
												$def = 0;
											}
										}
									}								
								}								
						//	}
						}
						
					}else{
						$this->ErLog[] = 'Не обнаружено артикула в строке - '.implode(';',$import);
					}
					
								
						
				}
			}
			
			unset($tyre);
			unset($_SESSION['brand']);
			unset($_SESSION['brandid']);
			unset($_SESSION['cat']);
			unset($_SESSION['catid']);
		}
		
		private function SaveImage($img){
		global $config;	
			$bool = false;
			
		$source_file = $config['absolute-path'].'import/'.$img;
		
		if( file_exists ($source_file)){
					_log($source_file,'a');
					$ext = end(explode(".",$img));
					
					switch ($ext){
						case 'jpeg':
						case 'jpg':
						case 'pjpeg':					
						case 'x-png':
						case 'png':
						case 'gif':
							break;
						default:
							return false;
						
				    }
					global $DB;
					
					$path = $this->GetConfigParam('absolute-path').'public/files/images/products';
					
					@mkdir($path,0777);
					
					$md5 = md5($img.time()).'.'.$ext;
					
					$file = $path.'/'.$md5;
					
					if(rename($source_file,$file)){
						
						$bool = $this->resize($file,'image/'.$ext,600, 800);
						$this->_setRightsToUploadedFile($file);
						//_log($source_file,'a');	
						//_log($file,'a');
						return $md5;
					}
		}
				
			
		}	
		
	private function _check_data($data){
		
			if(is_for($data)){
				return $data;
			}
		}
		
	protected function _setRightsToUploadedFile( $file_name ){
		
		@chmod( $file_name, 0777);
	}
	
	protected function _myfgetcsv($fname, $del){
	
		setlocale(LC_ALL, "ru_RU");
	
		$filesize	= filesize( $fname );
		$f			= fopen( $fname, "r" );
		$res		= array();
		$this->Line = 1;
		
		while( $row = $this->fgetcsv_club($f, $filesize, $del)) {
			
			$this->Line++;
			if ($this->Line > 1){ 

				$res[] = $row; 
			}
		}
		
		fclose($f);

		return $res;
	}
	
	protected function fgetcsv_club($f_handle, $length, $delimiter=',', $enclosure='"'){
		
	    if (!strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	        return fgetcsv($f_handle, $length, $delimiter, $enclosure);
	    if (!$f_handle || feof($f_handle))
	        return false;
	           
	    if (strlen($delimiter) > 1)
	        $delimiter = substr($delimiter, 0, 1);
	    elseif (!strlen($delimiter))          // There _MUST_ be a delimiter
	        return false;
	                       
	    if (strlen($enclosure) > 1)         // There _MAY_ be an enclosure
	        $enclosure = substr($enclosure, 0, 1);
	                       
	    $line = fgets($f_handle, $length);
	    if (!$line)
	        return false;
	    $result = array();
	    $csv_fields = explode($delimiter, trim($line));
	    $csv_field_count = count($csv_fields);
	    $encl_len = strlen($enclosure);
	    for ($i=0; $i<$csv_field_count; $i++)
	    {
	        // Removing possible enclosures$tyre
	        if ($encl_len && $csv_fields[$i]{0} == $enclosure)
	            $csv_fields[$i] = substr($csv_fields[$i], 1);
	        if ($encl_len && $csv_fields[$i]{strlen($csv_fields[$i])-1} == $enclosure)
	            $csv_fields[$i] = substr($csv_fields[$i], 0, strlen($csv_fields[$i])-1);
	        // Double enclosures are just original symbols
	        $csv_fields[$i] = str_replace($enclosure.$enclosure, $enclosure, $csv_fields[$i]);
	        $result[] = $csv_fields[$i];
	    }
	    return $result;
	}
	
	private function _get_brand_id($brand){
		if ($_SESSION['brand'] != $brand || !$_SESSION['brandid']){
			$sql = 'SELECT `id`
								FROM `pages`
								WHERE TRIM(`title`) = \''.trim($brand).'\' AND `parent_id` = \'92\'';
			$_SESSION['brand'] = $brand;
			$_SESSION['brandid'] = GetOne($sql);
		}
			return $_SESSION['brandid'];
	}
	
	private function _get_coll_id($coll){
		if ($_SESSION['coll'] != $coll || !$_SESSION['collid']){
			$sql = 'SELECT `id`
								FROM `pages`
								WHERE TRIM(`title`) = \''.trim($coll).'\' AND `parent_id` = \'96\' ';
			$_SESSION['coll'] = $coll;
			$_SESSION['collid'] = GetOne($sql);
		}
			return $_SESSION['collid'];
	}
	
	private function _get_cat_id($cat){
		if ($_SESSION['cat'] != $cat || !$_SESSION['catid']){
			$sql = 'SELECT `id`
								FROM `categories`
								WHERE `title` = \''.$cat.'\' ';
			$_SESSION['cat'] = $cat;
			$_SESSION['catid'] = GetOne($sql);
		}
			return $_SESSION['catid'];
	}

	
	
		
	private function _operate_tyre($data){
		$code_pr = $this->MakerCode . "-" . trim($data[0]) . "-" . trim($data[1]);
		$url = $this->MakerCode .  trim($data[0]) . trim($data[1]);
		$category_id = trim($data[0]);
		$title = m($data[2]);
		$price = _strtoint($data[4]);	
	
		$sql = 'SELECT * 
				FROM products
				WHERE code = \''.$code_pr.'\'';
		
		$res = GetRow($sql);
		
		if($res){
		
					$sql = 'UPDATE products
								SET create_ts		= \''.time().'\',
							publish			= \'1\',
							code			= \''.$code_pr.'\',
							url				= \''.$url.'\',
							brand_id		= \''.$data['brand_id'].'\',
							price			= \''.ereg_replace(",",".",$price).'\',
							category_id		= \''.$data['category_id'].'\',
							serach_field	= \''.$data['search_filed'].'\',
							title			= \''.$title.'\'
							WHERE 	id		= \''.$res['id'].'\'';
					
					if(Execute($sql)){
						$this->ImpCnt['upd'] += 1;
						$this->checkFile($res['id'],trim($data[1]), true);
					}
			
		} else {

			$sql = 'INSERT INTO products
					SET create_ts		= \''.time().'\',
						publish			= \'1\',
						code			= \''.$code_pr.'\',
						url				= \''.$url.'\',
						brand_id		= \''.$data['brand_id'].'\',
						price			= \''.ereg_replace(",",".",$price).'\',
						category_id		= \''.$data['category_id'].'\',
						serach_field	= \''.$data['search_filed'].'\',
						title			= \''.$title.'\'
						';
			if(Execute($sql)){
				$this->ImpCnt['ins']++;
				$id = GetOne("SELECT LAST_INSERT_ID() as ID FROM products");			
				$this->checkFile($id,trim($data[1]),false);
			}	
		}
	}
	
	protected function _save_img($id,$line,$up,$filename,$source_file){
	global $config;
	$dest_name = $config['absolute-path'].'public/files/products/'.$filename;
			$file_medium = $config['absolute-path'].'public/files/products/medium-'.$filename;
			$file_small	 = $config['absolute-path'].'public/files/products/thumb-'.$filename; 
			
			copy($source_file,$dest_name);
			
			if(copy($source_file,$file_small)){
				$this->resize($file_small,'image/jpg',85, 85);
			}
			if(copy($source_file,$file_medium)){
				$this->resize($file_medium,'image/jpg',168, 168);
			}
			
			//echo $dest_name;
			if ($up){
				$this->DeleteFile($id);
			}
			if ($_POST['delfiles']){
				@unlink($source_file);
			}
				$sql = "INSERT INTO photos SET
				`filename` = '".$filename."',
				`product_id` = '".$id."',
				`default` = 1			
				";	
				Execute($sql);
	}
	
	protected function checkFile($id,$line,$up){
		global $config;
		$source_file = $config['absolute-path'].'import/'.$line.'.jpg';
		
		
		
		if( file_exists ($source_file)){							
			$filename = md5(time().'-'.$line).'.jpg';			
			$this->_save_img($id,$line,$up,$filename,$source_file);
			
		}else{
			$source_file = $config['absolute-path'].'import/'.$line.'.JPEG';
			$source_file1 = $config['absolute-path'].'import/'.$line.'.jpeg';
			if( file_exists ($source_file) ){
				$filename = md5(time().'-'.$line).'.JPEG';			
				$this->_save_img($id,$line,$up,$filename,$source_file);
			}
			if( file_exists ($source_file1)){
				$filename = md5(time().'-'.$line).'.jpeg';			
				$this->_save_img($id,$line,$up,$filename,$source_file1);
			}
			$source_file = $config['absolute-path'].'import/'.$line.'.JPG';
			if( file_exists ($source_file) ){
				$filename = md5(time().'-'.$line).'.JPG';			
				$this->_save_img($id,$line,$up,$filename,$source_file);
			}
		}
	
	}
	
	protected function checkFile_old($id,$line,$up){
		global $config;
		$source_file = $config['absolute-path'].'import/'.$line.'.jpg';
		
		
		if( file_exists ($source_file)){							
			$filename = md5(time().'-'.$line).'.jpg';			
			$dest_name = $config['absolute-path'].'public/files/products/'.$filename;
			$file_medium = $config['absolute-path'].'public/files/products/medium-'.$filename;
			$file_small	 = $config['absolute-path'].'public/files/products/thumb-'.$filename; 
			
			copy($source_file,$dest_name);
			sleep(1);
			$thumb = PhpThumbFactory::create($source_file);
			$thumb->resize(168,156);
			$thumb->save($file_medium);
			$thumb->resize(84,73);
			$thumb->save($file_small);
			unset($thumb);
			//echo $dest_name;
			if ($up){
				$this->DeleteFile($id);
			}
				Execute("INSERT INTO photos SET
				`filename` = '".$filename."',
				`product_id` = '".$id."',
				`default` = 1			
				");
			
		}
	
	}
	private function DeleteFile($id){
		global $DB;
		$sql = 'SELECT filename 
						FROM `photos`
						WHERE `product_id` = \''.$id.'\'';
				$res = $DB->GetAll($sql); 
				if($res){
					for ($i = 0, $cn = count($res); $i < $cn; $i++) { 
						@unlink($this->GetConfigParam('absolute-path').'public/files/images/products/'.$res[$i]['filename']);
						//_log($this->GetConfigParam('absolute-path').'public/files/images/products/'.$res[$i]['filename'],'a');
					//	@unlink($this->GetConfigParam('absolute-path').'public/files/products/medium-'.$res[$i]['filename']);
					//	@unlink($this->GetConfigParam('absolute-path').'public/files/products/thumb-'.$res[$i]['filename']);
					}
					$sql = 'DELETE	FROM `photos`
							WHERE `product_id` = \''.$id.'\'';
					$res = $DB->Execute($sql);
				}
		}	
	
	
		
	
	function Ruslat ($string){

    	$string = ereg_replace("ж","zh",$string);
		$string = ereg_replace("ё","yo",$string);
		$string = ereg_replace("й","i",$string);
		$string = ereg_replace("ю","yu",$string);
		$string = ereg_replace("ь","",$string);
		$string = ereg_replace("ч","ch",$string);
		$string = ereg_replace("щ","sh",$string);
		$string = ereg_replace("ц","c",$string);
		$string = ereg_replace("у","u",$string);
		$string = ereg_replace("к","k",$string);
		$string = ereg_replace("е","e",$string);
		$string = ereg_replace("н","n",$string);
		$string = ereg_replace("г","g",$string);
		$string = ereg_replace("ш","sh",$string);
		$string = ereg_replace("з","z",$string);
		$string = ereg_replace("х","h",$string);
		$string = ereg_replace("ъ","",$string);
		$string = ereg_replace("ф","f",$string);
		$string = ereg_replace("ы","y",$string);
		$string = ereg_replace("в","v",$string);
		$string = ereg_replace("а","a",$string);
		$string = ereg_replace("п","p",$string);
		$string = ereg_replace("р","r",$string);
		$string = ereg_replace("о","o",$string);
		$string = ereg_replace("л","l",$string);
		$string = ereg_replace("д","d",$string);
		$string = ereg_replace("э","yе",$string);
		$string = ereg_replace("я","jа",$string);
		$string = ereg_replace("с","s",$string);
		$string = ereg_replace("м","m",$string);
		$string = ereg_replace("и","i",$string);
		$string = ereg_replace("т","t",$string);
		$string = ereg_replace("б","b",$string);
		$string = ereg_replace("Ё","yo",$string);
		$string = ereg_replace("Й","I",$string);
		$string = ereg_replace("Ю","YU",$string);
		$string = ereg_replace("Ч","CH",$string);
		$string = ereg_replace("Ь","",$string);
		$string = ereg_replace("Щ","SH'",$string);
		$string = ereg_replace("Ц","C",$string);
		$string = ereg_replace("У","U",$string);
		$string = ereg_replace("К","K",$string);
		$string = ereg_replace("Е","E",$string);
		$string = ereg_replace("Н","N",$string);
		$string = ereg_replace("Г","G",$string);
		$string = ereg_replace("Ш","SH",$string);
		$string = ereg_replace("З","Z",$string);
		$string = ereg_replace("Х","H",$string);
		$string = ereg_replace("Ъ","",$string);
		$string = ereg_replace("Ф","F",$string);
		$string = ereg_replace("Ы","Y",$string);
		$string = ereg_replace("В","V",$string);
		$string = ereg_replace("А","A",$string);
		$string = ereg_replace("П","P",$string);
		$string = ereg_replace("Р","R",$string);
		$string = ereg_replace("О","O",$string);
		$string = ereg_replace("Л","L",$string);
		$string = ereg_replace("Д","D",$string);
		$string = ereg_replace("Ж","Zh",$string);
		$string = ereg_replace("Э","Ye",$string);
		$string = ereg_replace("Я","Ja",$string);
		$string = ereg_replace("С","S",$string);
		$string = ereg_replace("М","M",$string);
		$string = ereg_replace("И","I",$string);
		$string = ereg_replace("Т","T",$string);
		$string = ereg_replace("Б","B",$string);
		$string = ereg_replace("&","AND",$string);
		//$string = ereg_replace(".","",$string);
		$string = ereg_replace(" ","_",$string);
		$string = ereg_replace("/","_",$string);
		return $string;
	}		
	
}
?>
