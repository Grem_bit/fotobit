<?

	class Categories extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'categories/';
		protected $TableName				= 'categories';
		protected $CatList					= null;
		protected $Categories				= array();
		protected $Categories_sz			= 0;
		protected $CategoryInfo				= array();
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');
			$this->_load_cat_list();
		}
		
		public function OnNew() {
			
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);

		}
		
		public function OnInsert() {
			
			if(!$this->_chech_input($_POST)) {
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_category($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->_remove_tree(intval($_GET['id']));
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			
			$this->SetTemplate('edit.html');
			$this->Categories = $this->_get_cat_list();
			$this->Categories_sz = count($this->Categories);
			
			$sql = 'SELECT * FROM categories WHERE id=\''.intval($_GET['id']).'\'';
			$this->CategoryInfo = $DB->GetRow($sql);

		}
		
		public function OnUpdate() {
			
			global $DB;
			
			if(!$this->_chech_input($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_category(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		/*
		 * Private methods
		 */
		
		private function _update_category($id, $data) {
			
			global $DB;
			
			$this->_update($id, $data, $this->TableName);
			
		}

		private function _remove_tree($id) {
			
			global $DB;
			
			$sql = 'SELECT id FROM categories WHERE parent_id=\''.$id.'\'';
			$res = $DB->GetAll($sql);
			$res_sz = count($res);
			
			for($i = 0; $i < $res_sz; $i++) {
				
				$this->_remove_tree($res[$i]['id']);
			}
			
			$sql = 'DELETE FROM categories WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_category($data) {
			
			$this->_insert($data,$this->TableName);
		}
		
		private function _chech_input($data, $update = false) {
			
			global $DB;
			
			if($update) {
				
				$limit = 1;
			}
			else {
				
				$limit = 0;
			}
			
			$this->Errors['title'] = $data['title:str'] ? false : true;
			$this->Errors['url'] = ereg("[@]|[?]|[ ]|[!]|[.]|`]|[']|[а-я]|[А-Я]", $data['url:str']);
			
			/*if(!$this->Errors['title']) {
				
				$sql = 'SELECT COUNT(*) AS cnt FROM categories WHERE title=\''.$data['title:str'].'\'';
				$res = $DB->GetRow($sql);
				
				if($res['cnt'] > $limit) {
					
					$this->Errors['title'] = true;
				}
			}*/
			
			if(!$this->Errors['url']) {
				
				$sql = 'SELECT COUNT(*) AS cnt FROM categories 
						WHERE parent_id = '.i($data['parent_id:int']).' AND url=\''.$data['url:str'].'\'';
				$res = $DB->GetRow($sql);
				
				if($res['cnt'] > $limit) {
					
					$this->Errors['url'] = true;
				}
				
			}
			
			if($update && $_GET['id'] == $data['parent_id:int']) {
				
				$this->Errors['parent_id'] = true;
			}
			
			if(in_array(true, $this->Errors)) {
				
				$this->DisplayError = true;
			}
			
			return !$this->DisplayError;
		}
		
		private function _load_cat_list() {
			
			$list = $this->_get_cat_list();
			$list_sz = count($list);
			
			$this->AttachComponent('GridAll', $this->CatList);
			
			$titles = array('Категория', 'Url', 'Опубликована','На главной');
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $list_sz; $i++){
				$active = $list[$i]['publish'] ? '<div id="container'.$list[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 0, \'categories\', \'publish\',\'container'.$list[$i]['id'].'\')">Да</span></div>' : 
													'<div id="container'.$list[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 1, \'categories\', \'publish\',\'container'.$list[$i]['id'].'\')">Нет</span></div>';
				$right = $list[$i]['right'] ? '<div id="right'.$list[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 0, \'categories\', \'right\',\'right'.$list[$i]['id'].'\')">Да</span></div>' : 
													'<div id="right'.$list[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 1, \'categories\', \'right\',\'right'.$list[$i]['id'].'\')">Нет</span></div>';
				$onFront = $list[$i]['onFront'] ? '<div id="onFront'.$list[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 0, \'categories\', \'onFront\',\'onFront'.$list[$i]['id'].'\')">Да</span></div>' : 
													'<div id="onFront'.$list[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$list[$i]['id'].', 1, \'categories\', \'onFront\',\'onFront'.$list[$i]['id'].'\')">Нет</span></div>';
				$row = array(
						$list[$i]['display_title'], 
						$list[$i]['url'],
						$active,
						$onFront
					);
				$rows[$list[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->CatList->SetData($data);
		}
		
		private function _get_cat_list($parent_id = 0) {
			
			global $DB;
			static $level = 0;
			static $cat_list = array();
			
			$sql = 'SELECT * FROM categories WHERE parent_id=\''.$parent_id.'\' ORDER BY order_by ASC';
			$categories = $DB->GetAll($sql);
			$categories_sz = count($categories);
			
			for($i = 0; $i < $categories_sz; $i++) {

				$categories[$i]['level'] = $level;
				$categories[$i]['display_title'] = str_repeat('&nbsp;', $level * 4).$categories[$i]['title'];
				
				array_push($cat_list, $categories[$i]);
				
				$level++;
				$this->_get_cat_list($categories[$i]['id']);
				$level--;
			}
			
			return $cat_list;
		}
	}

?>