<?
	
	class Brands extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'brands/';
		
		protected $TableName				= 'brands';
		protected $PublishField				= 'active';
		
		protected $BList					= null;
		protected $Brands					= array();
		protected $Brands_sz				= 0;
		protected $BrandInfo				= array();
		protected $PageNavigator			= null;
		protected $MD5FileName				= '';
		protected $FileError				= '';
		
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');
			$this->_init_brands_grid();
		}
		
		public function OnNew() {
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_brand($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			$this->_remove(intval($_GET['id']));		
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/pages.js');
			$sql = 'SELECT * FROM '.$this->TableName.' WHERE id=\''.intval($_GET['id']).'\'';
			$this->BrandInfo = $DB->GetRow($sql);
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				$this->OnEdit();
			}
			else {
				$this->_update_brand(intval($_GET['id']), $_POST, $_FILES);
				$this->OnDefault();
			}
		}
		
		
		/*
		 * Private methods
		 */
		
		private function _update_brand($id, $data,$files) {
			
			$this->_update($id, $data, $this->TableName);
		}
		
		private function _insert_brand($data) {
			
			$this->_insert($data,$this->TableName);
		}
				
		private function _check_data($data, $update = false) {		
			$this->Errors['title'] = trim($data['title:str']) ? false : true;			
			//$this->Errors['url'] = trim($data['url:str']) ? false : true;		
			$this->ShowError = in_array(true, $this->Errors);			
			return !$this->ShowError;
		}
		
		private function _load_brands() {
			
			global $DB;
			
			$sql= 'SELECT COUNT(*) AS cnt FROM '.$this->TableName.'';
			$res = $DB->GetRow($sql);
			
			$items_per_page = $this->_get_system_variable('admin_items_per_page');
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){
				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'brands/', $page, '?page=%s');
		
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}else{
				$order_by = 'title DESC';
			}
			
			$limit = '	ORDER BY '.$order_by.'
					 	LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			
			
			$sql = 'SELECT '.$this->TableName.'.*
					FROM '.$this->TableName.' ';
		
			$sql.=$limit;
			$this->Brands = $DB->GetAll($sql);
			$this->Brands_sz = count($this->Brands);
		}
		
		private function _init_brands_grid() {
			
			$this->_load_brands();
			
			$this->AttachComponent('GridAll', $this->BList);
			$options['sortable'] = array(	0 => array(	'up' => $this->RootUrl.'brands/?sort=name&direct=ASC', 
														'down' => $this->RootUrl.'brands/?sort=name&direct=DESC'));
			$titles = array('Бренд');

			$options['multiply'] = false;
			$options['multiply_events'] = array('Published'=>'Опубликовать', 'UnPublished'=> 'Скрыть', 'DeleteSelected'=> 'Удалить');
	
			if($this->Perm['moderate']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s');
			}elseif($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $this->Brands_sz; $i++){
				
				$active = $this->Brands[$i]['active'] ? '<div id="container'.$this->Brands[$i]['id'].'" ><span style="cursor:pointer; color: green;  text-decoration: underline;" onclick="fill('.$this->Brands[$i]['id'].', 0, \'brands\', \'active\')">Да</span></div>' : '<div id="container'.$this->Brands[$i]['id'].'" ><span style="cursor:pointer; text-decoration: underline;" onclick="fill('.$this->Brands[$i]['id'].', 1, \'brands\', \'active\')">Нет</span></div>';
				
				$row = array($this->Brands[$i]['title']
							 );
				$rows[$this->Brands[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->BList->SetData($data);
		}
	
	}
?>