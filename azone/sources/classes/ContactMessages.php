<?
	class ContactMessages extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'contact-messages/';
		
		protected $TableName				= 'contact_messages';
		protected $PublishField				= 'publish';
		
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $News						= array();
		protected $News_sz					= 0;
		protected $NewsInfo					= array();
		
	
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			$this->AddJS($this->RootUrl.'public/js/fill.js');
			$this->SetTemplate('main.html');			
			$this->_init_grid();
		}
		
		public function OnNew() {
			
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			
			//$this->_load_news_info(intval($_GET['id']));			
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$id = $this->_insert_news($_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.$id);
				}
				else {
					
					$this->OnDefault();
				}
			}
		}
		
		public function OnRemove() {			
			global $DB;			
			$sql = 'DELETE FROM `contact_messages` 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			$this->AddCSS($this->RootUrl.'public/css/calendar.css');
			$this->AddJS($this->RootUrl.'public/js/calendar.js');
			$this->AddJS('/core/lib/fckeditor/fckeditor.js');
			$this->AddJS($this->RootUrl.'/public/js/pages.js');
			$this->_load_news_info(intval($_GET['id']));	
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_news(intval($_GET['id']), $_POST);
				
				if($_POST['redirect']) {
					
					header('Location: '.$_POST['redirect'].'&id='.intval($_GET['id']));
				}
				else {
					
					$this->OnDefault();
				}
			}
		}

		/*
		 * Private methods
		 */

		private function _load_news_info($id) {
			global $DB;			
			$sql = 'SELECT * FROM `contact_messages` WHERE id=\''.$id.'\'';
			$this->NewsInfo = $DB->GetRow($sql);
			
		}

		private function _update_news($id, $data) {//_debug($data);_debug($_FILES, 1);
			global $DB;
			ini_set('display_errors',1);
			$create_ts = $data['create_ts'];			
			$create_ts = explode(" ",$create_ts);
			$create_ts[0] = explode(".",$create_ts[0]);
			$create_ts[1] = explode(":",$create_ts[1]);
			
			$ct = mktime($create_ts[1][0],$create_ts[1][1],0,$create_ts[0][1],$create_ts[0][0],$create_ts[0][2]);
			
			
			$sql = 'UPDATE contact_messages 
					SET name = \''.$data['name'].'\', 
						message = \''.$data['message'].'\', 
						'.($data['new'] ? 'answer = \''.$data['answer'].'\', ' : '').'
						'.($_FILES['filename']['name'] ? 'attach_answ = \'1\', ' : '').'
						notes = \''.$data['notes'].'\', 
						email = \''.$data['email'].'\', 
						'.($data['answer'] ? '`new` = 0,' : '').' 
						create_ts = \''.$ct.''.'\' 
					WHERE id=\''.$id.'\'';
			// _debug($sql, 1);
			$DB->Execute($sql);
			
			if ($data['answer'] && $data['new']){
	
				AttachLib('Mailer');
				$mail = new Mailer();
				$mail->AddRecepient($data['email']);                    
				$mail->AddVars(array(
									'name'		=> $data['name'],
									'email'		=> $data['email'],
									'message'	=> $data['answer'],
									'date'		=> date('d:m:Y H:i',time())								
				));
				if($_FILES['filename']['name']){
					$file['name'] = $_FILES['filename']['name'];
					$file['path'] = $_FILES['filename']['tmp_name'];
					$mail->AddFile($file);
				}
				$mail->Send(	'contact.html',
								$this->_get_system_variable('contact_email_subject'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
				);
			}
			
			
		}
		
		private function _insert_news($data) {
			global $DB;
	
			$create_ts = $data['create_ts'];			
			$create_ts = explode(" ",$create_ts);
			$create_ts[0] = explode(".",$create_ts[0]);
			$create_ts[1] = explode(":",$create_ts[1]);
			
			$ct = mktime($create_ts[1][0],$create_ts[1][1],0,$create_ts[0][1],$create_ts[0][0],$create_ts[0][2]);
			
			$sql = 'INSERT INTO contact_messages 
					SET SET name=\''.$data['name'].'\', 
						message=\''.$data['message'].'\', 
						answer=\''.$data['answer'].'\', 
						notes=\''.$data['notes'].'\', 
						email=\''.$data['email'].'\', 
						create_ts=\''.$ct.''.'\' ';
			$DB->Execute($sql);
			$sql = 'SELECT LAST_INSERT_ID() AS id FROM contact_messages';
			$res = $DB->GetRow($sql);
			return $res['id'];
		}
				
		private function _check_data($data, $update = false) {
			$this->Errors['name'] = $data['name'] ? false : true;
			$this->Errors['email'] = $data['email'] ? false : true;
			$this->Errors['create_ts'] = $data['create_ts'] ? false : true;
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		private function _load_news() {
			global $DB;
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM `contact_messages`';
			$res = $DB->GetRow($sql);
			$items_per_page = intval($this->_get_system_variable('admin_items_per_page'));
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){				
				$page = 1;
			}
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.$this->TemplatesBaseDir, $page, '?page=%s', '', true, true);
			$sql = 'SELECT * 
					FROM `contact_messages` 
					WHERE 1 = 1 
					ORDER BY `create_ts` DESC 
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			$this->News = $DB->GetAll($sql);
			$this->News_sz = count($this->News);
		}
		
		private function _init_grid($cat_id = 0) {
			$this->_load_news();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('ФИО отправителя',  'Дата создания',  'Сообщение',  'Ответ', 'Отвечено');
			$options['row_numbers'] = false;
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}

			$options['multiply'] = true;
			$options['multiply_events'] = array( 'DeleteSelected'=> 'Удалить');
	
			$rows = array();
			for($i = 0; $i < $this->News_sz; $i++){
				
				// $question = !$this->News[$i]['new'] ? '<div id="container'.$this->News[$i]['id'].'" ><span style="color: green;" >Да</span></div>' : '<div id="container'.$this->News[$i]['id'].'" ><span>Нет</span></div>';
				$active = !$this->News[$i]['new'] ? '<div id="container'.$this->News[$i]['id'].'" ><span style="color: green;" >Да</span></div>' : '<div id="container'.$this->News[$i]['id'].'" ><span>Нет</span></div>';
				
				$row = array($this->News[$i]['name'],
							 date("d.m.y H:i",$this->News[$i]['create_ts']),
							 $this->News[$i]['message'].($this->News[$i]['attach_mess'] ? '<p><img src="/azone/public/images/small-icons/attachment.png" style="padding-top: 4px;"></p>' : ''),
							 $this->News[$i]['answer'].($this->News[$i]['attach_answ'] ? '<img src="/azone/public/images/small-icons/attachment.png" style="padding-top: 4px;">' : ''),
							 $active
							);
				$rows[$this->News[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	
		/* Extend methods */
		public function OnDeleteSelected(){
			global $DB;
			$ids = array();
			if($_POST['ID']){
				foreach($_POST['ID'] as $el=>$value){
					$ids[] = intval($value);
				}
				$sql = "DELETE FROM `".$this->TableName."` WHERE id IN(".implode(',',$ids).")";
				$DB->Execute($sql);
				$DB->Execute("DELETE FROM pages_translation WHERE obj_id IN (".implode(',', $ids).")");
			}
			// header('Location: /azone/pages/');
			$this->OnDefault();
		}
		

		

	}
?>