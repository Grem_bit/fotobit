<?
	class Emails extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'emails/';
		protected $BList					= null;
		protected $Buyers					= array();
		protected $Buyers_sz				= 0;
		protected $BuyerInfo				= array();
		protected $PageNavigator			= null;
		
		
		/*
		 * Public methods
		 */
		
		protected function OnGetManagers(){
			global $DB;
			$sql = "SELECT * FROM manager_emails";
			$man = $DB->GetAll($sql);			
			echo $this->json_safe_encode(array('managers' => $man, 'cnt' => count($man)));
			exit;
		}
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');
			$this->_init_buyers_grid();
		}
		
		public function OnNew() {

			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/emails.js');
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_buyer($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {
			
			global $DB;
			
			$this->SetTemplate('delete.html');
			$sql = 'SELECT id, name
					FROM manager_emails
					WHERE id != '.intval($_GET['id']);
			$this->Managers = GetAll($sql);
			
		}
		
		public function OnDelete() {
			
			$sql = 'UPDATE products
					SET manager_id	= \''.i($_POST['manager_id']).'\'
					WHERE manager_id = '.intval($_GET['id']);
			Execute($sql);
			
			$sql = 'SELECT user_id FROM manager_emails WHERE id=\''.intval($_GET['id']).'\'';
			$user_id = GetOne($sql);
			
			$sql = 'DELETE FROM az_users WHERE id=\''.$user_id.'\'';
			Execute($sql);
			
			$sql = 'DELETE FROM manager_emails WHERE id=\''.intval($_GET['id']).'\'';
			Execute($sql);
			
			$this->OnDefault();
		}
		
		public function OnEdit() {
			
			global $DB;
			$this->AddJS($this->RootUrl.PATH_LIB.'ckfinder/ckfinder.js');
			$this->AddJS($this->RootUrl.'public/js/emails.js');
			$sql = 'SELECT * FROM manager_emails WHERE id=\''.intval($_GET['id']).'\'';
			$this->BuyerInfo = $DB->GetRow($sql);
			
			$this->UserInfo = GetRow('SELECT * FROM az_users WHERE id=\''.$this->BuyerInfo['user_id'].'\'');
			$this->SetTemplate('edit.html');
			
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, true)) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_buyer(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}
		
		
		/*
		 * Private methods
		 */
		
		private function _update_buyer($id, $data) {
			
			global $DB;

			$sql = 'UPDATE manager_emails 
					SET email=\''.$data['email'].'\', 
						name=\''.$data['name'].'\', 
						phone=\''.$data['phone'].'\',
						contact=\''.intval($data['contact']).'\',
						online_order=\''.intval($data['online_order']).'\' 
					WHERE id=\''.$id.'\'';
			$DB->Execute($sql);
		}
		
		private function _insert_buyer($data) {
			
			global $DB;
			
			$sql = 'INSERT INTO manager_emails 
					SET email=\''.$data['email'].'\', 
						name=\''.$data['name'].'\', 
						phone=\''.$data['phone'].'\',
						contact=\''.intval($data['contact']).'\',
						online_order=\''.intval($data['online_order']).'\'									
						';	
			$DB->Execute($sql);
			
			$id = GetOne('SELECT LAST_INSERT_ID() FROM manager_emails');
		}
				
		private function _check_data($data) {
			
			$this->Errors['email'] = !$this->ValidEmail($data['email']);
			
			$this->ShowError = in_array(true, $this->Errors);
			
			return !$this->ShowError;
		}
		
		private function _load_buyers() {
			
			global $DB;
			
			$sql= 'SELECT COUNT(*) AS cnt FROM manager_emails';
			$res = $DB->GetRow($sql);
			
			$items_per_page = 20;
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){
				
				$page = 1;
			}
			
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'emails/', $page, '?page=%s');
			
			$sql = 'SELECT * FROM manager_emails LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			$this->Buyers = $DB->GetAll($sql);
			$this->Buyers_sz = count($this->Buyers);
		}
		
		private function _init_buyers_grid() {
			
			$this->_load_buyers();
			
			$this->AttachComponent('GridAll', $this->BList);
			
			$titles = array('Имя','e-mail','Телефон','С контактной формы','Онлайн заявка');
			$options['row_numbers'] = true;
			
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			
			$rows = array();
			for($i = 0; $i < $this->Buyers_sz; $i++){
				$contant = $this->Buyers[$i]['contact'] ? 
								'<a href="/azone/emails/?field=contact&Event=PublicPage&id='.$this->Buyers[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/emails/?field=contact&Event=PublicPage&id='.$this->Buyers[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';			
				$online = $this->Buyers[$i]['online_order'] ? 
								'<a href="/azone/emails/?field=online_order&Event=PublicPage&id='.$this->Buyers[$i]['id'].'" rel="status=0" class="ajax-request on"><img src="/azone/public/images/public.gif" alt="" /></a>':
								'<a href="/azone/emails/?field=online_order&Event=PublicPage&id='.$this->Buyers[$i]['id'].'" rel="status=1" class="ajax-request off"><img src="/azone/public/images/unpublic.gif" alt="" /></a>';			
				
				$row = array(
							$this->Buyers[$i]['name'],
							$this->Buyers[$i]['email'],
							$this->Buyers[$i]['phone'],
							$contant,
							$online
							);
				$rows[$this->Buyers[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->BList->SetData($data);
		}
		
		public function OnPublicPage(){
			global $DB;
			$sql = "UPDATE manager_emails SET ".$_GET['field']." = '".intval($_REQUEST['status'])."' WHERE id = '".intval($_REQUEST['id'])."'";
			echo $sql;
			$DB->Execute($sql);
			exit();		
		}
		
		private function _save_perm($user_id) {
			
			global $DB;

			$perm = serialize(array('show'=>Array('92' => 1), 'edit'=>Array('92' => 1), 'view'=>Array('92' => 1)));
			
			$sql = 'UPDATE az_users 
					SET perm=\''.$perm.'\' 
					WHERE id=\''.$user_id.'\'';
			$DB->Execute($sql);
		}
		
		private function _product_count($status = '', $manager_id) {
			
			return GetOne('SELECT COUNT(*) FROM products WHERE type = \'pass\''.( $status ? ' AND state = \''.$status.'\'' : ''). ' AND manager_id = \''.$manager_id.'\'');
		
		}
		
		
	}
?>