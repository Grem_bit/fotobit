<?
	class Country extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'country/';
		protected $PageNavigator			= null;
		protected $Grid						= null;
		protected $Items					= array();
		protected $Items_sz					= 0;
		protected $ItemInfo					= array();
	
		
		/*
		 * Public methods
		 */
		
		public function OnDefault() {
			
			$this->SetTemplate('main.html');			
			$this->_init_grid();
		}
		
		public function OnNew() {
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnInsert() {
			
			if(!$this->_check_data($_POST)){
				
				$this->OnNew();
			}
			else {
				
				$this->_insert_item($_POST);
				$this->OnDefault();
			}
		}
		
		public function OnRemove() {			
			
			global $DB;			
			
			$sql = 'DELETE FROM country 
					WHERE id=\''.intval($_GET['id']).'\'';
			$DB->Execute($sql);			
			$this->OnDefault();
		}
		
		public function OnEdit() {

			global $DB;
			
			$sql = 'SELECT * FROM country WHERE id=\''.intval($_GET['id']).'\'';
			$this->ItemInfo = $DB->GetRow($sql);
			
			$this->SetTemplate('edit.html');
		}
		
		public function OnUpdate() {
			
			if(!$this->_check_data($_POST, intval($_GET['id']))) {
				
				$this->OnEdit();
			}
			else {
				
				$this->_update_item(intval($_GET['id']), $_POST);
				$this->OnDefault();
			}
		}

		/*
		 * Private methods
		 */

			private function _update_item($id, $data) {
				
				global $DB;
				
				$sql = 'UPDATE country 
						SET name=\''.$data['title'].'\',
							en_name=\''.$data['en_title'].'\', 
							coordinates=\''.$data['coordinates'].'\', 
							`default`=\''.$data['default'].'\' 
						WHERE id=\''.$id.'\'';
				// _debug($sql, 1);
				$DB->Execute($sql);
		}
		
		private function _insert_item($data) {
			
			global $DB;
			
			$sql = 'INSERT INTO country  
					SET name=\''.$data['title'].'\',
						en_name=\''.$data['en_title'].'\', 
						coordinates=\''.$data['coordinates'].'\', 
						`default`=\''.$data['default'].'\' 
					';
			$DB->Execute($sql);
		}
				
		private function _check_data($data, $id = 0) {
			
			$this->Errors['title'] = $data['title'] ? false : true;
			
			$this->ShowError = in_array(true, $this->Errors);
			return !$this->ShowError;
		}
		
		private function _load_cities() {

			global $DB;
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM country';
			$res = $DB->GetRow($sql);
			$items_per_page = 25;
			$max_pages_cnt = 9;
			$page = intval($_GET['page']);
			if($page < 1){				
				$page = 1;
			}
			AttachLib('PageNavigator');
			$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'countries/', $page, '?page=%s');
			$sql = 'SELECT * 
					FROM country 
					ORDER BY `name` 
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			$this->Items = $DB->GetAll($sql);
			$this->Items_sz = count($this->Items);
		}
		
		private function _init_grid($cat_id = 0) {
			
			$this->_load_cities();
			$this->AttachComponent('GridAll', $this->Grid);
			$titles = array('Название','English');
			
			$options['multiply'] = true;			
			$options['multiply_events'] = array('DeleteSelected'=> 'Удалить');
			$options['multiply_info'] = array('table'=>'country','module'=>'country','field'=>'active');
			
			
			$options['row_numbers'] = false;
			if($this->Perm['edit']) {
				$options['controls'] = array('edit' => $this->RedirectUrl.'?Event=Edit&amp;id=%s', 
										     'remove' => $this->RedirectUrl.'?Event=Remove&amp;id=%s');
			}
			$rows = array();
			for($i = 0; $i < $this->Items_sz; $i++){
				$row = array($this->Items[$i]['name'],$this->Items[$i]['en_name']);
				$rows[$this->Items[$i]['id']] = $row;
			}
			
			$data = array('options' => $options, 'titles' => $titles, 'rows' => $rows);
			$this->Grid->SetData($data);
		}
	}
?>