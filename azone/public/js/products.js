var oldWinRendered = WinRendered;

WinRendered = function(){
	if(oldWinRendered)
		oldWinRendered();	
	var sBasePath = '/core/lib/fckeditor/';

	if( $('tab_el_main') && $('tab_el_ru') && $('tab_el_ua') && $('tab_el_en') ){

		var tabs = new Ext.TabPanel({
		        renderTo: 'tabs1',
		        width:'100%',
				height: '100%',
		        activeTab: 0,
				border: false,
				plain:true,
		        defaults:{
		        	autoHeight	: true,
		        	frame		: true
		        },
		        items:[
						{contentEl:'tab_el_main', title: 'Основная информация', a: true},		
						{contentEl:'tab_el_ru', title: 'Русский', lang: 'ru', a: false},		
						//{contentEl:'tab_el_ua', title: 'Украинский', lang: 'ua', a: false},	           
						{contentEl:'tab_el_en', title: 'Английский', lang: 'en', a: false},
						//{contentEl:'tab_el_photo_gallery', title: 'Фотографии', lang: 'en', a: false}
		        ],
		        listeners: {
		        	
		        	tabchange: function(tp, p){
		        	
		        		if (!p.a) {
			        		if($('fulltext_'+p.lang)){    
								var conception = new FCKeditor('fulltext_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							if($('introtext_'+p.lang)){    
								var conception = new FCKeditor('introtext_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 200 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							p.a = true;
						}	
		        	}
		        }
		    });
	}
							
};


function SetDefault(id, photo, filename){				
	
	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SetDefault&id='+id+'&photo='+photo+'&filename='+filename);
}

function SwitchPhotosField(id, field){

	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SwitchPhotosField&id='+id+'&field='+field);
}

function UpdatePhotosField(id, field, value){

	var ajax = new Request.HTML();
	
	ajax.get('/azone/products/?Event=UpdatePhotosField&id='+id+'&field='+field+'&value='+value);
}