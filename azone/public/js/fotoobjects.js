WinRendered = function(){

	var swiffy = new FancyUpload2($('demo-status'), $('demo-list'), {
		'url': $('form-demo').action,
		'fieldName': 'photoupload',
		'path': '/core/lib/fancy/Swiff.Uploader.swf',
		'onLoad': function() {
			$('demo-status').removeClass('hide');
			$('demo-fallback').destroy();
		},
		target: 'demo-browse-images' // the element for the overlay (Flash 10 only)
	});

 	/**
	 * Various interactions
	 */
 
	$('demo-browse-all').addEvent('click', function() {
		swiffy.browse();
		return false;
	});
 
	$('demo-browse-images').addEvent('click', function() {
		var filter = null;
		//swiffy.browse({'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'});
		swiffy.options.typeFilter = {'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'};
		swiffy.browse();
		return false;
	});
 
	$('demo-clear').addEvent('click', function() {
		swiffy.removeFile();
		return false;
	});
 
	$('demo-upload').addEvent('click', function() {
		swiffy.options.data = {photo_type:$('photo_type').value};
		swiffy.upload();
		return false;
	});
 
};