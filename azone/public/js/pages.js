WinRendered = function(){
	var sBasePath = '/core/lib/fckeditor/';
	var content = $('content');
	var content_ua = $('content_ua');
	var content_en = $('content_en');
	
	if( $('tab_el_main') && $('tab_el_ru')/* && $('tab_el_ua')*//* && $('tab_el_en')*/){

		var tabs = new Ext.TabPanel({
		        renderTo: 'tabs1',
		        width:'100%',
				height: '100%',
		        activeTab: 0,
				border: false,
				plain:true,
		        defaults:{
		        	autoHeight	: true,
		        	frame		: true
		        },
		        items:[
						{contentEl:'tab_el_main', title: 'Основная информация', a: true},		
						{contentEl:'tab_el_ru', title: 'Русский', lang: 'ru', a: false},		
						// {contentEl:'tab_el_ua', title: 'Украинский', lang: 'ua', a: false},	           
						// {contentEl:'tab_el_en', title: 'Английский', lang: 'en', a: false}
						//{contentEl:'tab_el_photo_gallery', title: 'Фото галлерея', lang: 'en', a: false}
						//{contentEl:'tab_el_video', title: 'Видео/Аудио списки страниц (при выборе арт сообщества)', lang: 'en', a: false}/*,
						//{contentEl:'tab_el_comments', title: 'Комментарии', lang: 'en', a: false}*/
						
						
		        ],
		        listeners: {
		        	
		        	tabchange: function(tp, p){
		        		if(p.contentEl = 'tab_el_main' && p.a){
						//newsGrid = showNewsGrid('pages_links1', relevant_news);
					}
		        		if (!p.a) {
			        		if($('content_'+p.lang)){    
								var conception = new FCKeditor('content_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							if($('fron_text_'+p.lang)){    
								var conception = new FCKeditor('fron_text_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							if($('catalog_description_'+p.lang)){    
								var conception = new FCKeditor('catalog_description_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							
							if($('right_column_'+p.lang)){   
								var conception = new FCKeditor('right_column_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
							if($('warranty_'+p.lang)){   
								var conception = new FCKeditor('warranty_'+p.lang) ;	
								conception.BasePath	= sBasePath ;
								conception.Height = 500 ;
								conception.AutoDetectLanguage = false ;
								conception.DefaultLanguage = "ru" ;	
								conception.ReplaceTextarea() ;    
							}
						}
						p.a = true;	
		        	}
		        }
		    });
	
	}else{
		if(content){
			var oFCKeditor = new FCKeditor('content') ;
			oFCKeditor.BasePath	= sBasePath ;
			oFCKeditor.Height = 500 ;
			oFCKeditor.ReplaceTextarea() ;
		}
		if(content_ua){
			var oFCKeditor = new FCKeditor('content_ua') ;
			oFCKeditor.BasePath	= sBasePath ;
			oFCKeditor.Height = 500 ;
			oFCKeditor.ReplaceTextarea() ;
		}
		if(content_en){
			var oFCKeditor = new FCKeditor('content_en') ;
			oFCKeditor.BasePath	= sBasePath ;
			oFCKeditor.Height = 500 ;
			oFCKeditor.ReplaceTextarea() ;
		}
	
	}
	
	var submit_btn = Ext.get('submit_btn');
	if(submit_btn){
		submit_btn.on('click', function(){
			var ids = [];
			Ext.each(
				charsGrid.store.data.items,
				function(item, index, all){
					ids[index] = item.id;
					
				}
			);			
			$('videos_ids').value = ids.join(',');
			///////////
			$('alboms_ids').value = ids.join(',');
			//////////
		});
	}
	
	var current_input = null;
	
	var c_inputs = $$("img.browse");
	
	c_inputs.each(function(p,i){
		p.addEvent('click',function(e){
			new Event(e).stop();
			BrowseServer();	
		});
	});
	
	/* $('yandex').addEvent('click',function(e){
			new Event(e).stop();
			$('loader').setStyle('visibility', 'visible');
			var ajax = new Request({onComplete: function(){
				$('loader').setStyle('visibility', 'hidden');
				alert('Файл обновлён')
			}});
			ajax.get('/yandex-market.php');
	}); */
};

function BrowseServer( startupPath, functionData ){
	var finder = new CKFinder() ;
	finder.BasePath = '/core/lib/ckfinder/' ;	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.startupPath = startupPath;
	finder.selectActionFunction = SetFileField;
	finder.selectActionData = functionData;
	finder.popup();
}

function SetFileField( fileUrl, data )
{
	if($(data["selectActionData"])){
		$(data["selectActionData"]).value = fileUrl ;
	}
}

function SetDefault(id,photo,filename){				
	
	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SetDefault&id='+id+'&photo='+photo+'&filename='+filename);
}
function SetDefaultVideo(id,photo,filename){				
	
	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SetDefaultVideo&id='+id+'&photo='+photo+'&filename='+filename);
}

function SetPlan(id){				
	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SetPlan&id='+id);
}
function SetPdf(id){				
	var ajax = new Request.HTML();
	ajax.get('/azone/products/?Event=SetPdf&id='+id);
}