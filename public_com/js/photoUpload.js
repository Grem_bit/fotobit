$(document).ready(function(){
	var uploader = new qq.FileUploader({
	   // pass the dom node (ex. $(selector)[0] for jQuery users)
	   element: document.getElementById('file-uploader'),
	   // path to server-side upload script
	   action: '?Event=LoadPhotos',
	   debug: false,
		allowedExtensions: [],
		
		
	});
	
	$('.photo-params input, .photo-params select').change(function() {
		
		var pid = $(this).parents('.photo-params').children('input[name=pid]').val();
		$.ajax({
		  url: "?Event=SaveParams&"+$(this).parents('.photo-params').serialize(),
		  dataType:  'json'
		}).done(function(data) {
			//console.log(data);
			//alert(data);
			if(data.photo_priceall_arr){
					var pp = data.photo_priceall_arr
					$.each(pp, function(index, element) {
						$('#ph-price-'+index).html("Итого: "+element);
						
					}); 
				}
				if(data.photo_priceone_arr){
					var pp = data.photo_priceone_arr
					$.each(pp, function(index, element) {
						$('#ph-prc-'+index).html("Стоимость: "+element);//add
					});
				}
			$('#ph-quality-'+pid).html("Качество: "+data.photo_quality);
			$('#ph-prc-'+pid).html("Стоимость: "+data.photo_price);
 			$('#ph-price-'+pid).html("Итого: "+data.photo_price_cnt);
 			$('#sum-prices').html("Общая стоимость: "+data.sum_photo_price);
			paymentFormRefresh(data.sum_photo_price);
		});
	});
	
	$('#params4all input, #params4all select').change(function(){
		var option = $(this).attr('name'); 
		var val = $(this).val();
		if(option=='kolvodvs') return false;
		$.ajax({
		  url: "?Event=SaveParams&"+option+'='+val,
		  dataType:  'json'
		}).done(function(data) {
				console.log(data);
				$('#ph-quality').html("Качество: "+data.quality);
				if(data.photo_price > 0)$('.ph-price').html("Стоимость: "+data.photo_price);
				
				for(key in data.photo_priceall_arr) {
					$('#ph-price-'+key).html("Итого: "+data.photo_priceall_arr[key]); 
				}
				for(key in data.photo_quality_arr) {
					$('#ph-quality-'+key).html("Качество: "+data.photo_quality_arr[key]);
				}
			var n = $('.photo-params input[name="'+option+'"][value="'+val+'"]').length;
			if(n>0){
				$('.photo-params input[name="'+option+'"][value="'+val+'"]').attr("checked", true);
			}else{
				$('.photo-params select[name="'+option+'"] option[value="'+val+'"]').attr("selected","selected");  
			}
			
			
				$('#sum-prices').html("Общая стоимость: "+data.sum_photo_price);
				paymentFormRefresh(data.sum_photo_price);
		});
	});
		
	$('.ph-count').click(function() {
		if($(this).val() == 1) $(this).val('');
	});

	$('#kdvs').change(function(e) {
		e.preventDefault();
		var kdi=$(this);
		if(Number(kdi.val()) < 1) kdi.val('1');
		var curm=Number(kdi.val());
		$.ajax({
		  url: "?Event=SaveParams&"+kdi.attr('name')+'='+curm, 
		  dataType:  'json'
		}).done(function(data) {
				console.log(data);
				$('#ph-quality').html("Качество: "+data.quality);
				if(data.photo_price > 0)$('.ph-price').html("Стоимость: "+data.photo_price);
				
				for(key in data.photo_priceall_arr) {
					$('#ph-price-'+key).html("Итого: "+data.photo_priceall_arr[key]); 
				}
				for(key in data.photo_quality_arr) {
					$('#ph-quality-'+key).html("Качество: "+data.photo_quality_arr[key]);
				}
			        $('.ph-count').val(curm);
			
				$('#sum-prices').html("Общая стоимость: "+data.sum_photo_price);
				paymentFormRefresh(data.sum_photo_price);
		});
	});
	
	$('.ph-count').keyup(function() {
		var pid = $(this).parent().parent().children('.params').find('input[name=pid]').val();
		
		var count = $(this).val();
		
		$.ajax({
			url: "?Event=ChangeCount",
			data: { pid: pid, count: count },
			dataType:  'json'
		}).done(function(data) {
 			 //console.log(data);
			if(data) {
 				if(data.photo_priceall_arr){
					var pp = data.photo_priceall_arr
					$.each(pp, function(index, element) {
						$('#ph-price-'+index).html("Итого: "+element);
					});
				}
				if(data.photo_priceone_arr){
					var pp = data.photo_priceone_arr
					$.each(pp, function(index, element) {
						$('#ph-prc-'+index).html("Стоимость: "+element);//add
					});
				}
				$('#ph-price-'+pid).html("Итого: "+data.photo_price);
 				$('#sum-prices').html("Общая стоимость: "+data.sum_photo_price);
				$('#ph-prc-'+pid).html("Стоимость: "+data.price_one);//add
				paymentFormRefresh(data.sum_photo_price);
 			}
		});
	});
})

function removeProd(product_id) {
	if(confirm('Удалить фото с заказа')){
		$.ajax({
		  url: "?Event=RemovePhoto",
		  data: { pid: product_id },
		  dataType:  'json'
		}).done(function(data) { 
		  $('#p_'+product_id).remove();
		  if(data) {
			  if(data.hide) {
					$('#ik_payment').hide();
					$('#ik_payment-submit').hide();
					$('#params4all').hide();	
			  }
			  if(data.sum > 0) {
					$('#sum-prices').html('Общая стоимость: '+data.sum);
					paymentFormRefresh(data.sum);
			  } else {
					$('#sum-prices').hide();
			  }
		  }
		  location.reload();
		});
	}
}
/*
function makeOrder() {
	$.ajax({
	  url: "?Event=OrderPhoto"
	}).done(function() {
		payOrder();
	  $('.qq-upload-list').html('');
	});
}
*/