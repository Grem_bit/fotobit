$(document).ready(function() {

	if ($(document).height() >= 1500){
		$('.enter').css('position','fixed').css('left','50%').css('margin-left','338px');
	}
	
	$("a.order_image").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	true,
		'overlayOpacity':	0.6,
		'overlayColor'	:	'#000000',
		'titleShow'		:	true
	});
	
	$('.photo div:eq(0)').animate({rotate:'-=15'},0);
	$('.photo div:eq(1)').animate({rotate:'+=15'},0);
	$('.photo div:eq(2)').animate({rotate:'+=0'},0);

	$('.enter span').click(function(){
		$('.bg').fadeIn(250);
		$('.form-box').fadeIn(250);
		$('.form').removeAttr('style').animate({top:140},1000);
		$('.form-slider').removeAttr('style');
		$('.form').removeClass('error');
	});

	$('.close-win, .bg').click(function(){
		if(!$('.bg').hasClass('filesList')){
			$('.bg').fadeOut(250);
			$('.form-box').delay(750).fadeOut(250);
			$('.form').animate({top:-1200},1000);
		
		}
	});
	
	$(document).keypress(function(e){
		if (e.keyCode == 27){
			$('.bg').fadeOut(250);
			$('.form-box').delay(750).fadeOut(250);
			$('.form').animate({top:-1200},1000);
		}
	});

	$('.remember').click(function(){
		$('.form-slider').animate({top:-460},750);
	});

	$('.backward').click(function(){
		$('.form-slider').animate({top:-230},750);
	});

	$('.reg span.button').click(function(){
		$('.form-slider').animate({top:0},750);
	});

	$('.forgotten span.but').click(function(){
		$('.form').addClass('error');
	});
	
	$('.validation-form').submit(function(e){
		
		e.preventDefault();
		
		var validation = $(".valid", this);
		var error = false;
		
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					validRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
					strEmail = el.value;
					if(strEmail.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					} 
				}else if($(this).hasClass('phone')){
					validRegExp = /^\d{10,13}$/i;
					strPhone = el.value;
					if (strPhone.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					}
				}else if($(this).hasClass('rules')){
					if(!el.checked){
						error = true;
						alert('Для регистрации необходимо принять условия использования сайта.');
					}
				}else if($(this).hasClass('day') || $(this).hasClass('month') || $(this).hasClass('year')){
					if(isNaN(el.value)){
						error = true;
						$('#err').attr('src', '/public/images/error.gif');
					}
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');							
					}						
				}					
			});
		}
		if(!error){
			var form = $(this);
			if(form.length){
				var action = form.attr('action');
				if(action == '') action = '/';
				var str = form.serialize();
				$.post(action, str+'&ajax=true', function(response){
					var params;
					//eval("params = " + response);
					params = response;
					if (params.success){
						$(".validateTipsTitle", form).hide();
						$(".validateTips",form).html('');
						// $('.form_content',form).hide();
						/*offset = form.offset();
						$(document).scrollTop(offset.top);*/
						if(params.text){
							$('.thanks', form).html(params.text);
						}
						if (form.hasClass('contacts')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Ваше сообщение отправлено</div><div class="message_thanks">Ожидайте ответ в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('comment')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за комментарий</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('report')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за отзыв</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('coupon')){
							$.fancybox(params.coupon, {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('ajax-login')){
							$('.enter').html(params.user_info);
							$('.bg').fadeOut(250);
							$('.form-box').delay(750).fadeOut(250);
							$('.form').animate({top:-1200},1000);
						}else if(form.hasClass('ajax-registration')){
							$('.form').removeClass('error');
							$('.registeren').html(params.text);
							setTimeout(function() {
								$('.bg').fadeOut(250);
								$('.form-box').delay(750).fadeOut(250);
								$('.form').animate({top:-1200},1000);
								window.location = '/';
							}, 10000);
						}else if(form.hasClass('ajax-forgot-pass')){
							$('.form').removeClass('error');
							$('.forgotten').html(params.text);
							setTimeout(function() {
								$('.bg').fadeOut(250);
								$('.form-box').delay(750).fadeOut(250);
								$('.form').animate({top:-1200},1000);
							}, 10000);
						}
		
						setTimeout(function() {
							// parent.$.fancybox.close();
						}, 5000);
						
						// $('.thanks',form).show();
						if(params.callback){
							if (eval("typeof " + params.callback + " == 'function'")) {
								eval(params.callback + '();');
							}
						}
						if(params.redirect){
							location = params.redirect;
						}
						
					}else{
						$(params.errors).each(function(ind, el){$("input[name='"+el+"']", form).addClass('error-ico');});
						// $(".validateTipsTitle", form).show();
						// $(".validateTips", form).html(params.errortext);
						
						if(params.text){
							$('.form').addClass('error');
							$('.error_msg', form).html(params.text);
							$('.error_msg', form).css('visibility', 'visible');
						}
					}
				}, 'json');
			}
		}else{
			//$('#captcha').load('/registration.html?Event=ChangeCaptcha');
		}
		return false;
	});
	
	$('.del-order').click(function(){
		order_id = $(this).attr('rel');
		if(confirm('Удалить заказ № '+order_id+'?')){
			return true;
		}else{
			return false;
		}
	});
	
});