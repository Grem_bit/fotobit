$(document).ready(function(){

	$('a[rel="fb"]').fancybox({
		arrows: false
	});
	
	$('.nav > li.active').find('.sub').css('display','block');
	$('.nav > li.main > a').click(function(e){
		e.preventDefault();
		if (!$(this).parent().hasClass('active')){
			$('.nav ul').slideUp(250, function(){$('.nav > li').removeClass('active')});
		}
		$(this).next().slideToggle(250, function(){$(this).parent().toggleClass('active')});
	});
	$('.validation-form').submit(function(e){
		
		e.preventDefault();
		
		var validation = $(".valid", this);
		var error = false;
		
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					validRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
					strEmail = el.value;
					if(strEmail.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					} 
				}else if($(this).hasClass('phone')){
					validRegExp = /^\d{10,13}$/i;
					strPhone = el.value;
					if (strPhone.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					}
				}else if($(this).hasClass('rules')){
					if(!el.checked){
						error = true;
						alert('Для регистрации необходимо принять условия использования сайта.');
					}
				}else if($(this).hasClass('day') || $(this).hasClass('month') || $(this).hasClass('year')){
					if(isNaN(el.value)){
						error = true;
						$('#err').attr('src', '/public/images/error.gif');
					}
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');							
					}						
				}					
			});
		}
		if(!error){
			var form = $(this);
			if(form.length){
				var action = form.attr('action');
				if(action == '') action = '/';
				var str = form.serialize();
				$.post(action, str+'&ajax=true', function(response){
					var params;
					//eval("params = " + response);
					params = response;
					if (params.success){
						$(".validateTipsTitle", form).hide();
						$(".validateTips",form).html('');
						// $('.form_content',form).hide();
						/*offset = form.offset();
						$(document).scrollTop(offset.top);*/
						if(params.text){
							$('.thanks', form).html(params.text);
						}
						if (form.hasClass('contacts')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Ваше сообщение отправлено</div><div class="message_thanks">Ожидайте ответ в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('comment')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за комментарий</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('report')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за отзыв</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('coupon')){
							$.fancybox(params.coupon, {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('ajax-login')){
							$('.enter').html(params.user_info);
							$.fancybox.close();	
						}else if(form.hasClass('ajax-registration')){
							
							$('.registeren').attr('title',$('.registeren').html());	
							$('.registeren').html(params.text);
							setTimeout(function() {								
								$('.registeren').html($('.registeren').attr('title'));
								$.fancybox.close();	
								window.location = '/';
							}, 3000);
						}else if(form.hasClass('ajax-forgot-pass')){
							
							$('.forgotten').attr('title',$('.forgotten').html());	
							$('.forgotten').removeClass('error_msg');								
							$('.forgotten').html(params.text);
							setTimeout(function() {								
								$('.forgotten').html($('.forgotten').attr('title'));			
								$('.forgotten').addClass('error_msg');
								$.fancybox.close();								
							}, 3000);
						}
		
						setTimeout(function() {
							// parent.$.fancybox.close();
						}, 5000);
						
						// $('.thanks',form).show();
						if(params.callback){
							if (eval("typeof " + params.callback + " == 'function'")) {
								eval(params.callback + '();');
							}
						}
						if(params.redirect){
							location = params.redirect;
						}
					}else{
						$(params.errors).each(function(ind, el){$("input[name='"+el+"']", form).addClass('error-ico');});
						// $(".validateTipsTitle", form).show();
						// $(".validateTips", form).html(params.errortext);
						if(form.hasClass('ajax-registration')){							
							$('.registeren').attr('title',$('.registeren').html());	
							$('.registeren').html(params.text);
							
						}
						if(params.text){
							$(form).parent().addClass('error');
							$('.error_msg', form).html(params.text);
							$('.error_msg', form).css('visibility', 'visible');
						}
					}
				}, 'json');
			}
		}else{
			//$('#captcha').load('/registration.html?Event=ChangeCaptcha');
		}
		return false;
	});
	
	
	
	
	
	
});

























//eof