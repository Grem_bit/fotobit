<?
	require_once('mail/class.phpmailer.php');
	class Mailer
	{
		var $Recepients			= array();
		var $Type				= 'text/html';
		var $Charset			= 'UTF-8';
		var $MessageContent		= '';
		var $TemplateVars		= array();
		var $RootUrl			= '';

		function AddRecepient($email)
		{
			array_push($this->Recepients, $email);
		}
		
		function DelRecepient()
		{
			unset($this->Recepients);
			$this->Recepients = array();
		}
		
		function AddVars($vars)
		{
			$this->TemplateVars = $vars;
		}
		
		function DelVars()
		{
			unset($this->TemplateVars);
			$this->TemplateVars	= array();
		}
		
		function Send($template, $subject= '', $sender_email = '', $sender_name = '')
		{
			global $config;
			$this->RootUrl = $config['site_url'];
			$this->_load_template($template);
			$headers  = "MIME-Version: 1.0\n";
			$headers .= 'Content-Type: '.$this->Type.'; charset='.$this->Charset."\n";
			if($this->Charset == 'UTF-8'){
				$subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
			}
			if($sender_email)
			{
				$from = $sender_name.' <'.$sender_email.'>';
				$headers .= 'From: '.$from."\n";
				$headers .= 'Reply-To: '.$from."\n";
			}
			
			$recepients_sz = count($this->Recepients);
			for($i = 0; $i < $recepients_sz; $i++)			
			{
				@mail($this->Recepients[$i], $subject, $this->MessageContent, $headers);
			}
			/*
			$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

			$mail->IsSMTP(); // telling the class to use SMTP

			try {
			  $mail->Host       = "91.206.30.40"; // SMTP server
			  $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
			  $mail->SMTPAuth   = true;                  // enable SMTP authentication
			  $mail->Host       = "91.206.30.40"; // sets the SMTP server
			  $mail->Port       = 250;                    // set the SMTP port for the GMAIL server
			  $mail->Username   = "no-replay@flyupstudio.com"; // SMTP account username
			  $mail->Password   = "tR8zFmOI";        // SMTP account password
			  $mail->AddReplyTo('no-replay@flyupstudio.com', 'First Last');
			  $mail->AddAddress('alexflyus@gmail.com', 'John Doe');
			  $mail->SetFrom('no-replay@flyupstudio.com', 'First Last');
			  $mail->AddReplyTo('no-replay@flyupstudio.com', 'First Last');
			  $mail->Subject = $subject;
			  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
			  // $mail->MsgHTML(file_get_contents('contents.html'));
			  $mail->MsgHTML('test тест');
			  $mail->AddAttachment('public/files/coupons/coupon_81.pdf');      // attachment
			  // $mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
			  $mail->Send();
			  echo "Message Sent OK</p>\n";
			} catch (phpmailerException $e) {
			  echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
			  echo $e->getMessage(); //Boring error messages from anything else!
			}
			*/
/*			


			echo '<hr>';
			echo $this->MessageContent;
			echo '<hr>';*/
		}
		
		function _load_template($template)
		{
			foreach($this->TemplateVars as $key => $val)
			{
				$code = 'global $'.$key.'; $'.$key.' = "'.htmlspecialchars  ($val,ENT_QUOTES ).'";';
				
				eval($code);
			}
			
			ob_start();
			include PATH_MAIL_TEMPLATES.$template;
			$this->MessageContent = ob_get_contents();
			ob_end_clean();
		}
	}
?>