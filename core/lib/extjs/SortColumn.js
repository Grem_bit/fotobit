
Ext.ns('Ext.ux.grid');

Ext.ux.grid.SortColumn = function(config){
    Ext.apply(this, config);
	this.header += '<img class="ux-sort-save-icon" src="/azone/public/images/small-icons/save.gif" />';
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.ux.grid.SortColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        
		this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
		
		this.grid.getView().addListener(
			'refresh',
			function(view){
				var records = view.grid.store.data.items;
				Ext.each(
					records,
					function(item, index, all){
						item.sorter = new Ext.form.NumberField({
							width		: 30,
							height		: 19,
							value		: item.data[this.sort_field],
							renderTo	: 'sc-'+this.id+'-sort-field-'+item.id
						})
					},
					this
				);
			},
			this
		);
    },

    onMouseDown : function(e, t){
//        if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
//            e.stopEvent();
//            var index = this.grid.getView().findRowIndex(t);
//            var record = this.grid.store.getAt(index);
//            record.set(this.dataIndex, !record.data[this.dataIndex]);
//            e.record = record;
//            this.grid.fireEvent('afteredit', e);
//        }
    },

    renderer : function(value, metaData, record, rowIndex, colIndex, store){
		var str = '';
		str += rowIndex != 0 ? '<div class="ux-grid-icon arrow-up-icon ux-cell-act-move-up">&#160;</div>' : '<div class="ux-grid-empty-icon">&#160;</div>';
		str += rowIndex + 1	 != store.data.items.length ? '<div class="ux-grid-icon arrow-down-icon ux-cell-act-move-down">&#160;</div>' : '<div class="ux-grid-empty-icon">&#160;</div>';
		
        return str + '<div id="sc-'+this.id+'-sort-field-'+record.id+'"></div>';
    }
};

// register ptype
Ext.preg('sortcolumn', Ext.ux.grid.SortColumn);

// backwards compat
Ext.grid.SortColumn = Ext.ux.grid.SortColumn;
