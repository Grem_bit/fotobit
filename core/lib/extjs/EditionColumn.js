/*!
 * Ext JS Library 3.0.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.ns('Ext.ux.grid');

Ext.ux.grid.EditionColumn = function(config){
    Ext.apply(this, config);
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};



Ext.ux.grid.EditionColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('click', this.onClick, this);
        }, this);
       
    },

    onClick : function(e, t){
    	var parent = t.parentNode;
    	
    	switch (parent.rel)
    	{	case 'delete':
    			this.onDelete();
    			break
    		case 'edit':
    			this.onEdit();
    			break
    	}
    	
    },
    
    onDelete : function(){
    				
    				var grid = this.grid;
    				var index = grid.getSelectionModel().getSelectedCell();
    				
    				Ext.Msg.show({
					   title	: 'Удаление элемента!',
					   msg		: 'Вы действительно хотите удалить выбранный элемент?',
					   buttons	: Ext.Msg.YESNOCANCEL,
					   fn		: function(btn){
										if(btn == 'yes')
											var rec = grid.store.getAt(index[0]);
											Ext.Ajax.request({
				                    			url: 'index.php',
				                    			success: function(){
													grid.store.reload();
				                    			},
				                    			failure: function(){
				                    				grid.store.reload();
				                    			},
				                    			params: { 
				                    				Event		: 'DeleteData',
				                    				id			: rec.data.id
				                    			}
				                    		});
									},
					   animEl: 'grid-win',
					   icon: Ext.MessageBox.QUESTION
					});    
					Ext.Msg.getDialog().setZIndex(Ext.WindowMgr.zseed+100);
				},
	onEdit : function () {			
				
				var grid = this.grid;
				
				grid.plugins[2].onRowClick;
				
				//console.log(MyDesktop);	
			},
    renderer : function(value, p, record){
      
    	return '<a class="ux-del-grid-icon" rel="delete" href="javascript:;"><img src="/public/images/delete.png" alt="Удалить" /></a><a class="ux-del-grid-icon" rel="edit" href="javascript:;"><img src="/public/images/delete.png" alt="Удалить" /></a>';
    }
};

//register ptype
Ext.preg('editioncolumn', Ext.ux.grid.EditionColumn);

// backwards compat
Ext.grid.EditionColumn = Ext.ux.grid.EditionColumn;
