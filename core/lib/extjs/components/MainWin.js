Ext.MainWin = Ext.extend(Ext.Container, {
    initComponent : function() {
        Ext.MainWin.superclass.initComponent.call(this);
		this.layout = 'border';
		this.createEnviroment();
        this.el = Ext.get(this.renderTo);
        this.el.setHeight = Ext.emptyFn;
        this.el.setWidth = Ext.emptyFn;
        this.el.setSize = Ext.emptyFn;
        this.el.dom.scroll = 'no';
        this.allowDomMove = false;
        this.autoWidth = true;
        Ext.EventManager.onWindowResize(this.fireResize, this);
        this.renderTo = this.el;
    },
    fireResize : function(w, h){
        this.fireEvent('resize', this, w, h, w, h);
    },
	createEnviroment : function(){
		this.modulesTree = new Ext.tree.TreePanel({
			autoScroll	: true,
			animate		: true,
			containerScroll: true,
			border		: false,
			id			: 'modules_tree',
			listeners: {
				click: function(n) {
					return true;
				}
			},
			loader: new Ext.tree.TreeLoader({
				dataUrl	: '?Event=GetModules'
			}),
			root: {
				expanded:true,
				nodeType: 'async',
				text: 'Главная',
				id: 'source',
				href:'/azone/'
			}
		
		});
		
		this.modulesTreePanel = new Ext.Panel({
			bodyStyle:{
			},
			title		: 'Панель навигации',
		    region		: 'west',
			layout		: 'fit',
			collapsible	: true,
		    split		: true,
			width		: 250,
			margins		: '5 0 5 5',
			cmargins	: '5 5 5 5',
			items		: this.modulesTree
		});
		
		this.contentArea = new Ext.Panel({
			autoScroll:true,
		    region	: 'center',
			layout	: 'fit',
		    split	: true,
			//html	: pageHTML,
			margins	: '5 5 5 0'
		});
		
		this.add(this.modulesTreePanel);
		this.add(this.contentArea);
	},
	addHTML : function(html){
		this.contentArea.add(
			new Ext.Panel({
				autoScroll	: true,
				border		: false,
				html		: html,
				listeners	: {
					afterrender : WinRendered
				}
			})
		);
		this.contentArea.doLayout();
	},
	addModule : function(module){
		this.contentArea.add(module);
		this.contentArea.doLayout();
	}	
});





WinRendered = Ext.emptyFn;

Ext.onReady(function(){
	MainWin = new Ext.MainWin({
		renderTo: 'main_container'	
	});
	
	

	
})


