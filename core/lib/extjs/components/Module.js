timeVar = null;
Ext.onReady(function(){
	Ext.Ajax.request({
		url:'?_tv='+new Date().getTime(),
		success: function(response, opts){
			var result;
			eval('result = ' + response.responseText);
			
			if(result.component === false){
				MainWin.addHTML(pageHTML ? pageHTML : '');
			}else{
				successHandler(result);
			}
		},
		failure: function(response, opts){
			
		},
		callback: function(){
			
		},
		params: {
			Event : 'GetModuleStructure'
		}
	});
})

function successHandler(record){
	var module;
	var moduleUrl = record.url;
	switch(record.component){
		default:
		case 'grid':
			var storeConfig = {
				baseParams	: Ext.apply({}, record.params),
				autoLoad	: record.autoLoad,
				reader		: new Ext.data.JsonReader(
					{
						totalProperty: 'cnt',
						root: 'records',
						id: 'id'
					},
					Ext.data.Record.create(record.structure)
				),
				proxy: new Ext.data.HttpProxy({
					url: moduleUrl
				}),
				listeners:{
					beforeload:function(){
						if(srchField)
							this.baseParams.search = srchField.getValue();
						//win.loadMask.hide();
					}
				}
			};
			
			var listeners = {}
			var viewConfig = {
				forceFit		: true
			}			
			
			if(record.grouping && record.grouping !== false){
				storeConfig.sortInfo = {
					field		: record.grouping.sortField,
					direction	: "ASC"
				};
				storeConfig.groupField = record.grouping.groupField;
				
				
				var store = new Ext.data.GroupingStore(storeConfig);
			}else{
				var store = new Ext.data.Store(storeConfig);
			}
			
			if(record.grouping && record.grouping !== false){
				viewConfig.groupTextTpl = record.grouping.groupTextTpl;
				viewConfig.startCollapsed = record.grouping.startCollapsed;
				
				var view = new Ext.grid.GroupingView(viewConfig);
			}else{
				var view = new Ext.grid.GridView(viewConfig);
			}
			
			var options = {
				title		: record.title,
				border		: false,
				store		: store,
				view		: view,
				loadMask	: {msg:"Загрузка данных..."},
				animCollapse: false
			}
			if(record.perPage && record.perPage > 0 ){
				options.bbar = getPagingBar(store, record.perPage, 'Записи');
			}
			
//******************************* Create a ToolBar *************************************************
			
			if(!record.withoutNew || record.searchable){
				options.tbar = new Ext.Toolbar({
					items: []
				});
			}	
			
            if(!record.withoutNew){
				buttonAddMenu = new Ext.Toolbar.Button( {
					text	: 'Добавить',
					handler	: function(){
						module.onNew('default')
					},
					iconCls	: 'add-icon'
				});
				
				options.tbar.add(buttonAddMenu);
				options.tbar.add('-');
		    }
			
            if(record.searchable){
            	var srchField = new Ext.form.TextField({
		            xtype: 'textfield',
		            name: 'searchTxt',
		            emptyText: 'введите текст для поиска',
	    			id:'searchTxt',
	    			selectOnFocus:true,
	    			width:200,
	    			enableKeyEvents:true,
	    			listeners:{
	    				keypress:function(input, e){
	    					if(e.getKey()) {
	    						clearTimeout(timeVar);
	    						timeVar = setTimeout(
	    							function(){
	    								
	    								if(input.getValue() != ''){input.addClass('not-empty');}else{input.removeClass('not-empty');}
	    								store.load({params:{start:0, limit:store.limit, search: srchField.getValue()}});
	    							},
	    							1000);
	    					}
	    				}
	    			}
		        });
            	options.tbar.add('Поиск:');
            	options.tbar.add(srchField);
            }	
			
			//***************************Column Model************************************
			var cm = [];
			Ext.each(
				record.colModel,
				function(item, index, all){
					
					
					
					if(item.renderer){
						eval('item.renderer = '+item.renderer);
					}
					if(record.editable){
						switch(item.editor){
							case 'numberfield':
								item.editor = new Ext.form.NumberField({
									allowBlank: false,
									allowNegative: false
								});										
								break;
							
							case 'datefield':
								item.editor = new Ext.form.DateField({
									format:'Y-m-d'
								});
								break;
		
							case 'datetime':
								item.editor = new Ext.ux.form.DateTime({
									timeFormat	: 'H:i:s',
									dateFormat	: 'Y-m-d',
									timeWidth	: 100,
									timePosition: 'bellow'
								});
								item.editor.df.addListener('show', function(){
									this.setWidth(this.width);
								})
		
								break;
								
							case 'timefield':
								item.editor = new Ext.form.TimeField({
									format:'H:i:s'
								});
								break;
									
							default:
								item.editor = new Ext.form.TextField({});
								break;
						}
		
					}
					if(record.sortColumn && record.sortColumn !== false){
						item.sortable = false;
					}
					cm.push(item);
					
				}
			)
			
			var plugins = [];
			var actions = [];
						
			Ext.each(
				record.serv_columns,
				function(item, index, all){
					switch(item.type){
						case 'process_col':
							var conf = {
								header		: 'Process',
								width		: 12,
								dataIndex	: item.data_index,
								id			: item.data_index,
								renderer	: function (value, p, record){
									return '<span class="ux-grid-icon ux-grid-icon-middle ' + (value ? 'accept-icon' : 'no-accept-icon') + ' ux-cell-act-process_col">&nbsp;</span>'; 
								}
							};
							cm.push(Ext.apply(conf, item.config));						
							break;
						
						case 'default':
							var conf = {
								header		: 'По умолчанию',
								width		: 12,
								dataIndex	: 'default',
								id			: 'default',
								renderer	: function (value, p, record){
									return '<span class="ux-grid-icon ux-grid-icon-middle ' + (value ? 'accept-icon' : 'ux-cell-act-make_default no-accept-icon') + '">&nbsp;</span>'; 
								}
							};
							cm.push(Ext.apply(conf, item.config));
							break;

						case 'published':
							var conf = {
								header		: 'Опубликовано',
								width		: 12,
								dataIndex	: 'published',
								id			: 'published',
								renderer	: function (value, p, record){
									return '<span class="ux-grid-icon ux-grid-icon-middle ' + (value ? 'accept-icon ux-cell-act-unpublish' : 'ux-cell-act-publish no-accept-icon') + '">&nbsp;</span>'; 
								}
							};
							cm.push(Ext.apply(conf, item.config));						
							break;

						case 'languages':
							var conf = {
								header		: 'Переводы',
								width		: 12,
								dataIndex	: 'id',
								id			: 'id',
								languages	: item.languages
							};
							conf.renderer = function (value, p, record){
								var txt = '';
								Ext.each(
									this.languages,
									function(el, index, all){
										txt += '<span class="ux-cell-act-change-lang '+(all.length == 1 ? 'ux-grid-icon-middle' : '')+' ux-edit-lang-'+el.id+' lang-'+el.code+' ux-grid-icon">&nbsp;</span>'
									}
								)
								
								return txt; 
							}							
							cm.push(Ext.apply(conf, item.config));						
							break;

						case 'control':
						
							if(item.icons){
								var conf = {
									header		: 'Управление',
									width		: 12,
									icons		: item.icons,
									dataIndex	: 'id',
									id			: 'id'
								};
								
								conf.renderer = function (value, p, record){
									var txt = '';
									Ext.each(
										this.icons,
										function(el, index, all){
											txt += '<span class="ux-cell-act-'+el+' '+el+'-icon ux-grid-icon">&nbsp;</span>'
										}
									)
									
									return txt; 
								}
								
								cm.push(Ext.apply(conf, item.config));	
							}						
							break;

						default:
							var conf = {
								header		: item.type,
								width		: 12,
								actions		: item.actions,
								dataIndex	: 'id',
								id			: 'id'
							};
							conf.renderer = function (value, p, record){
								var txt = '';
								Ext.each(
									this.actions,
									function(el, index, all){
										txt += '<span class="ux-cell-act-'+el.name+' '+(all.length == 1 ? 'ux-grid-icon-middle' : '')+' '+el.name+'-icon ux-grid-icon">&nbsp;</span>'
									}
								)
								
								return txt; 
							}
							
							cm.push(Ext.apply(conf, item.config));

							
							Ext.each(
								item.actions,
								function(el, index, all){
									actions.push(el.name);
									eval('options.on' + el.name + ' = ' + el['function']);
								}
							);							
							
							break;
					}
				}
			);
			
			
			
			if(record.sortColumn && record.sortColumn !== false){
				var conf = {
					header		: 'Сортировка',
					dataIndex	: 'id',
					width		: 14,
					sort_field	: 'sort_order'
				};
				Ext.apply(conf, record.sortColumn.config)
				
				listeners.headerclick = function(grid, columnIndex, e){
					var action = e.target.className;
					if(action == 'ux-sort-save-icon'){
						this.onSaveSort();
					}
				}
				
				var sortColumn = new Ext.grid.SortColumn(conf);
				plugins.push(sortColumn);
				
				if(record.sortColumn.sort_order > 0 && record.sortColumn.sort_order < cm.length){
					cm.splice(record.sortColumn.sort_order, 0, sortColumn);
				}else{
					cm.push(sortColumn);
				}
			}
			
			if(record.withNumberer)
				cm.unshift(new Ext.grid.RowNumberer({width: 25}));
				
			options.columns = cm;
			options.plugins = plugins;			
			//***************************************************************			
			
			if(!record.editable){
				listeners.celldblclick = function(grid, rowIndex, columnIndex, e){
				}
			}else{
				listeners.celldblclick = function(grid, rowIndex, columnIndex, e){
					
				}
				listeners.afteredit = function(e, a,b,c){
					Ext.Ajax.request({
						url		: moduleUrl,
						scope	: this,
						success : function(result){
							var success;
							eval('success = ' + result.responseText);
							if(!success.success){
								Ext.Msg.show({
								   title:'Ошибка!',
								   msg: 'Не удалось сохранить данные!',
								   buttons: Ext.Msg.OK,
								   icon: Ext.MessageBox.ERROR
								});										
							}
							store.reload();
						},
						failure: function(){
							Ext.Msg.show({
							   title:'Ошибка!',
							   msg: 'Не удалось сохранить данные!',
							   buttons: Ext.Msg.OK,
							   icon: Ext.MessageBox.ERROR
							});
							store.reload();
						},
						params: {
							Event	: 'SaveData',
							id		: e.record.id,
							value	: e.value,
							field	: e.field,
							lang_id	: e.record.data.lang_id
						}
					});						
				}
			}
			
			listeners.render = function(){
				Ext.QuickTips.init();
			}
			
			options.onSaveSort = function(id){
				var params = {};
				Ext.each(
					this.store.data.items,
					function(item, index, all){
						params['sortValues['+item.id+']'] = item.sorter.getValue();
					}
				);
				params.Event = 'SaveSort';

				Ext.Ajax.request({
					url : moduleUrl,
					scope	: this,
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params : params
				});
				
			}
			
			options.onChangeSort = function(id, direction){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this,
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params:{
						Event		: 'ChangeSort',
						id			: id,
						direction	: direction
					}
				});
				
			}
			
			options.onNew = function(lang_id){
				Ext.Ajax.request({
					url : moduleUrl,
					success: function(response, opts){
						var result;
						eval('result = ' + response.responseText);
						if(result.noedit !== false){
							result.form_params.items = [];
							Ext.each(
								result.fields,
								function(item, index, all){
									if(item.xtype == 'combo'){
										var combo_options = Ext.apply(item,{
										    typeAhead		: true,
										    triggerAction	: 'all',
										    lazyRender		: true,
										    mode			: 'local',
										    store: new Ext.data.ArrayStore({
										        id		: 0,
										        fields	: [
										            'value'
										        ],
										        data: item.options
										    }),
										    valueField: 'value',
										    displayField: 'value'
										});
										
										var combo = new Ext.form.ComboBox(combo_options);

										result.form_params.items.push(combo);
									}else{
										result.form_params.items.push(item);
									}
								}
							)
							result.form_params.listeners = {
								render : function(form){
									var field = form.getForm().findField('lang_id');
									if(field){
										field.setValue(lang_id);
									}
								}
							}
							var form = new Ext.form.FormPanel(result.form_params);
							result.win_params.items = form;
	
							result.win_params.modal = true;
							result.win_params.buttons = [
								{
									text	: 'Сохранить',
									handler : function(){
										if(form.getForm().isValid()){
											form.getForm().submit({
												url		: form.url,
												params	: {
													Event : 'SaveRecord'
												},
												success	: function(response, opts){
													editWindow.close();
													store.reload();
												},
												failure	: function(response, opts){
													Ext.Msg.show({
													   title:'Ошибка!',
													   msg: 'Не удалось сохранить данные!',
													   buttons: Ext.Msg.OK,
													   icon: Ext.MessageBox.ERROR
													});												
													editWindow.close();
													store.reload();
												}
											})
										}else{
											Ext.Msg.show({
											   title:'Ошибка!',
											   msg: 'Данные введены с ошибками!',
											   buttons: Ext.Msg.OK,
											   icon: Ext.MessageBox.ERROR
											});												
										}
									}
								},{
									text	: 'Отмена',
									handler	: function(){
										editWindow.close();
									}
								}
							];
							
							var editWindow = new Ext.Window(result.win_params);
							editWindow.show();
						}else{
							Ext.Msg.show({
								title	:'Ошибка!',
								msg		: 'Модуль не поддерживает редактирование!',
								buttons	: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
						}
						
					},
					failure: function(response, opts){
						Ext.Msg.show({
							title	:'Ошибка!',
							msg		: 'Не удалось загрузить форму добавления!',
							buttons	: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
					},
					params : {
						Event	: 'GetEditForm'
					}
				});
				
			}
			
			options.onEdit = function(record, lang_id){
				Ext.Ajax.request({
					url : moduleUrl,
					success: function(response, opts){
						var result;
						eval('result = ' + response.responseText);
						if(result.noedit !== false){
							result.form_params.items = [];
							Ext.each(
								result.fields,
								function(item, index, all){
									if(item.xtype == 'combo'){
										var combo_options = Ext.apply(item,{
										    typeAhead		: true,
										    triggerAction	: 'all',
										    lazyRender		: true,
										    mode			: 'local',
										    store: new Ext.data.ArrayStore({
										        id		: 0,
										        fields	: [
										            'value'
										        ],
										        data: item.options
										    }),
										    valueField: 'value',
										    displayField: 'value'
										});
										
										var combo = new Ext.form.ComboBox(combo_options);

										result.form_params.items.push(combo);
									}else{
										result.form_params.items.push(item);
									}
								}
							)
							
							var form = new Ext.form.FormPanel(result.form_params);
							result.win_params.items = form;
	
							result.win_params.modal = true;
							result.win_params.buttons = [
								{
									text	: 'Сохранить',
									handler : function(){
										if(form.getForm().isValid()){
											form.getForm().submit({
												url		: form.url,
												params	: {
													Event : 'SaveRecord'
												},
												success	: function(response, opts){
													editWindow.close();
													store.reload();
												},
												failure	: function(response, opts){
													Ext.Msg.show({
													   title:'Ошибка!',
													   msg: 'Не удалось сохранить данные!',
													   buttons: Ext.Msg.OK,
													   icon: Ext.MessageBox.ERROR
													});												
													editWindow.close();
													store.reload();
												}
											})
										}else{
											Ext.Msg.show({
											   title:'Ошибка!',
											   msg: 'Данные введены с ошибками!',
											   buttons: Ext.Msg.OK,
											   icon: Ext.MessageBox.ERROR
											});												
											
										}
									}
								},{
									text	: 'Отмена',
									handler	: function(){
										editWindow.close();
									}
								}
							];
							
							var editWindow = new Ext.Window(result.win_params);
							editWindow.show();
							
							form.getForm().load({
								url		: form.url,
								params	: {
									Event	: 'LoadRecord',
									id		: record.id,
									lang_id	: lang_id
								},
								success	: function(){
									var field = form.getForm().findField('lang_id');
									if(field){
										field.setValue(lang_id);
									}									
								},
								failure	: function(){
									Ext.Msg.show({
										title	:'Ошибка!',
										msg		: 'Данные не загружены!',
										buttons	: Ext.Msg.OK,
										icon: Ext.MessageBox.ERROR
									});									
								}
							})
							
						}else{
							Ext.Msg.show({
								title	:'Ошибка!',
								msg		: 'Модуль не поддерживает редактирование!',
								buttons	: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
						}
						
					},
					failure: function(response, opts){
						Ext.Msg.show({
							title	:'Ошибка!',
							msg		: 'Не удалось загрузить форму редактирования!',
							buttons	: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
					},
					params : {
						Event : 'GetEditForm'
					}
				});
				
			}
			
			options.onDelete = function(id){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this,
					success: function(response, opts){
						var result;
						eval('result = ' + response.responseText);
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params: {
						id		: id,
						Event	: 'Remove'
					}
				});
			}
			
			options.onMakeDefault = function(id){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this, 
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params: {
						id		: id,
						Event	: 'MakeDefault'
					}
				});
			}
			
			options.onPublish = function(id){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this,
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params: {
						id		: id,
						Event	: 'Publish'
					}
				});
			}
			
			options.onUnpublish = function(id){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this,
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params: {
						id		: id,
						Event	: 'Unpublish'
					}
				});
			}

			options.onProcessCol = function(fieldName, fieldValue, id){
				Ext.Ajax.request({
					url		: moduleUrl,
					scope	: this,
					success: function(response, opts){
						this.store.reload();
					},
					failure: function(response, opts){
						
					},
					callback: function(){
						
					},
					params: {
						id			: id,
						fieldName	: fieldName,
						fieldValue	: fieldValue,
						Event		: 'ToggleField'
					}
				});
			}
			
			listeners.cellclick = function(grid, rowIndex, columnIndex, e){
				var action = e.target.className.split('ux-cell-act-').pop().split(' ').shift();
				switch(action){
					case 'process_col':
						var index = grid.getColumnModel().config[columnIndex].dataIndex;
						this.onProcessCol(index, this.store.data.items[rowIndex].data[index], this.store.data.items[rowIndex].id);
						break;
						
					case 'change-lang':
						var lang = e.target.className.split('ux-edit-lang-').pop().split(' ').shift();
						this.onEdit(this.store.data.items[rowIndex], lang);
						break;
					
					case 'edit':
						this.onEdit(this.store.data.items[rowIndex], this.store.data.items[rowIndex].data.lang_id);
						break;
						
					case 'delete':
						Ext.Msg.show({
							   title	: 'Удаление элемента!',
							   msg		: 'Вы действительно хотите удалить выбранный элемент?',
							   buttons	: Ext.Msg.YESNOCANCEL,
							   scope	: this,
							   fn		: function(btn){
												if(btn == 'yes'){
													this.onDelete(this.store.data.items[rowIndex].id);
						                    	}
											},
							   icon: Ext.MessageBox.QUESTION
							});    
							Ext.Msg.getDialog().setZIndex(Ext.WindowMgr.zseed+100);
						break;
						
					case 'make_default':
						this.onMakeDefault(this.store.data.items[rowIndex].id);
						break;
						
					case 'publish':
						this.onPublish(this.store.data.items[rowIndex].id);
						break;
					
					case 'unpublish':
						this.onUnpublish(this.store.data.items[rowIndex].id);
						break;

					case 'move-up':
						this.onChangeSort(this.store.data.items[rowIndex].id, 'up');
						break;

					case 'move-down':
						this.onChangeSort(this.store.data.items[rowIndex].id, 'down');
						break;
						
					default:
						if(actions.in_array(action)){
							eval('this.on' + action + '()');
						}
					;

				}
			}
			
			options.listeners = listeners;
			
			if(record.editable){
				module = new Ext.grid.EditorGridPanel(options);
			}else{
				module = new Ext.grid.GridPanel(options);
			}
			
			break;
			
		case 'dataview':
			break;
			
		case 'tree':
			
			var module = new Ext.tree.TreePanel({
				loadMask	: {msg:"Загрузка данных..."},
				animCollapse: false,
				id			: 'tree-panel',
		    	title		: record.title,
		        autoScroll	: true,
		        enableDD	: true,
		        rootVisible	: true,
	        	//ddGroup			: 'treeDDGroup',
		        dropConfig	: {
		        	appendOnly		: true
		        	,
					getDropPoint	: function(e, n, dd){
						var tn = n.node;
						if(tn.isRoot){
							return tn.allowChildren !== false ? "append" : false; // always append for root
						}
						var dragEl = n.ddel;
						var t = Ext.lib.Dom.getY(dragEl), b = t + dragEl.offsetHeight;
						var y = Ext.lib.Event.getPageY(e);
						var noAppend = tn.allowChildren === false;// || tn.isLeaf();
						return noAppend ? false : "append";
					}
		        },
		        border		: false,
				animate		: true,
				tbar		: tbar = new Ext.Toolbar({
					items: [
						new Ext.Toolbar.Button({
							text: 'Добавить Категорию',
							iconCls: 'add-icon',
							handler: function(){
								
								if(!module.selModel.selNode){
									
									Ext.Msg.show({
										   title	: 'Не выбран узел!',
										   msg		: 'Вы не выбрали узел, в который будет добавлен элемент!',
										   buttons	: Ext.Msg.OK,
										   icon: Ext.MessageBox.WARNING
										});    
									Ext.Msg.getDialog().setZIndex(Ext.WindowMgr.zseed+100);
								}else{
									
									if(langsCombo)
										module.onNew(langsCombo.getValue());
									else
										module.onNew(record.defLangId);	
								}
							}
						}),
						new Ext.Toolbar.Button({
							text: 'Удалить Категорию',
							iconCls: 'delete-icon',
							handler: function(){
								Ext.Msg.show({
									   title	: 'Удаление элемента!',
									   msg		: 'Вы действительно хотите удалить выбранный элемент?',
									   buttons	: Ext.Msg.YESNOCANCEL,
									   scope	: this,
									   fn		: function(btn){
														if(btn == 'yes'){
															module.onDelete(module.selModel.selNode.id);
															module.selModel.selNode.parentNode.collapse();
															module.selModel.selNode.parentNode.loaded = false;
															module.selModel.selNode.parentNode.expand();
								                    	}
													},
									   icon: Ext.MessageBox.QUESTION
									});    
									Ext.Msg.getDialog().setZIndex(Ext.WindowMgr.zseed+100);
							}		
						})						
					]
				}),	
				loader: treeloader = new Ext.tree.TreeLoader({
					dataUrl		: moduleUrl,
					baseParams	: {
						Event	: 'GetSimpleTreeRecords'
					}
		        }),
		        root: new Ext.tree.AsyncTreeNode({
					id		: 'source',
					text	: record.rootTitle 	
		        }),
				listeners:{
					dblclick: function(n, e){
						
						if(n.id != 'source'){
							
							if(langsCombo)
								this.onEdit(n, langsCombo.getValue());
							else
								this.onEdit(n, record.defLangId);
						}
					},
					beforenodedrop: function(e){
						console.log(e);
						Ext.Ajax.request({
							url		: moduleUrl,
							scope	: this,
							success: function(response, opts){
								
							},
							failure: function(response, opts){
								
							},
							callback : function(){
								var path = e.target.getPath();
								module.loader.load(
									e.target.parentNode,
									function(){
										module.expandPath(path);
									}
								);				
							},
							params: {
								point		: e.point,
								target_id	: e.target.id,
								drop_id		: e.dropNode.id,
								Event		: 'TreeDD'
							}
						});
					}
				},
				onDelete : function(id){
					Ext.Ajax.request({
						url		: moduleUrl,
						scope	: this,
						success: function(response, opts){
							var result;
							eval('result = ' + response.responseText);
						},
						failure: function(response, opts){
							
						},
						callback: function(){
							
						},
						params: {
							id		: id,
							Event	: 'RemoveTreeNode'
						}
					});
				
				},
				onNew : function(lang_id){
					Ext.Ajax.request({
						url : moduleUrl,
						success: function(response, opts){
							var result;
							eval('result = ' + response.responseText);
							if(result.noedit !== false){
								result.form_params.items = [];
								Ext.each(
									result.fields,
									function(item, index, all){
										result.form_params.items.push(item);
									}
								)
								result.form_params.listeners = {
									render : function(form){
										var field = form.getForm().findField('lang_id');
										if(field){
											field.setValue(lang_id);
										}
									}
								}
								var form = new Ext.form.FormPanel(result.form_params);
								result.win_params.items = form;
		
								result.win_params.modal = true;
								result.win_params.buttons = [
									{
										text	: 'Сохранить',
										handler : function(){
											if(form.getForm().isValid()){
												form.getForm().submit({
													url		: form.url,
													params	: {
														Event : 'SaveRecord',
														parent_id: module.selModel.selNode.id
													},
													success	: function(response, opts){
														editWindow.close();
														
														console.log(module.selModel.selNode);
														
														if(		module.selModel.selNode.id == 'source' || 
																module.selModel.selNode.firstChild		){
															
															module.selModel.selNode.loaded = false;
															module.selModel.selNode.expand();
														}else{
														
															module.selModel.selNode.parentNode.loaded = false;
															module.selModel.selNode.parentNode.expand();
														}
													},
													failure	: function(response, opts){
														Ext.Msg.show({
														   title:'Ошибка!',
														   msg: 'Не удалось сохранить данные!',
														   buttons: Ext.Msg.OK,
														   icon: Ext.MessageBox.ERROR
														});												
														editWindow.close();
														
													}
												})
											}else{
												Ext.Msg.show({
												   title:'Ошибка!',
												   msg: 'Данные введены с ошибками!',
												   buttons: Ext.Msg.OK,
												   icon: Ext.MessageBox.ERROR
												});												
											}
										}
									},{
										text	: 'Отмена',
										handler	: function(){
											editWindow.close();
										}
									}
								];
								
								var editWindow = new Ext.Window(result.win_params);
								editWindow.show();
							}else{
								Ext.Msg.show({
									title	:'Ошибка!',
									msg		: 'Модуль не поддерживает редактирование!',
									buttons	: Ext.Msg.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
							
						},
						failure: function(response, opts){
							Ext.Msg.show({
								title	:'Ошибка!',
								msg		: 'Не удалось загрузить форму добавления!',
								buttons	: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						params : {
							Event	: 'GetEditForm'
						}
					});
				},	
				onEdit : function(record, lang_id){
					Ext.Ajax.request({
						url : moduleUrl,
						success: function(response, opts){
							var result;
							eval('result = ' + response.responseText);
							if(result.noedit !== false){
								result.form_params.items = [];
								Ext.each(
									result.fields,
									function(item, index, all){
		
										result.form_params.items.push(item);
									}
								)
								
								var form = new Ext.form.FormPanel(result.form_params);
								result.win_params.items = form;
		
								result.win_params.modal = true;
								result.win_params.buttons = [
									{
										text	: 'Сохранить',
										handler : function(){
										
										
											if(form.getForm().isValid()){
												form.getForm().submit({
													url		: form.url,
													params	: {
														Event : 'SaveRecord'
													},
													success	: function(response, opts){
														editWindow.close();
														record.parentNode.collapse();
														record.parentNode.loaded = false;
														record.parentNode.expand();

													},
													failure	: function(response, opts){
														Ext.Msg.show({
														   title:'Ошибка!',
														   msg: 'Не удалось сохранить данные!',
														   buttons: Ext.Msg.OK,
														   icon: Ext.MessageBox.ERROR
														});												
														editWindow.close();
														record.parentNode.collapse();
														record.parentNode.loaded = false;
														record.parentNode.expand();

													}
												})
											}else{
												Ext.Msg.show({
												   title:'Ошибка!',
												   msg: 'Данные введены с ошибками!',
												   buttons: Ext.Msg.OK,
												   icon: Ext.MessageBox.ERROR
												});												
												
											}
										}
									},{
										text	: 'Отмена',
										handler	: function(){
											editWindow.close();
										}
									}
								];
								
								var editWindow = new Ext.Window(result.win_params);
								editWindow.show();
								
								form.getForm().load({
									url		: form.url,
									params	: {
										Event	: 'LoadRecord',
										id		: record.id,
										lang_id	: lang_id
									},
									success	: function(){
										var field = form.getForm().findField('lang_id');
										if(field){
											field.setValue(lang_id);
										}									
									},
									failure	: function(){
										Ext.Msg.show({
											title	:'Ошибка!',
											msg		: 'Данные не загружены!',
											buttons	: Ext.Msg.OK,
											icon: Ext.MessageBox.ERROR
										});									
									}
								})
								
							}else{
								Ext.Msg.show({
									title	:'Ошибка!',
									msg		: 'Модуль не поддерживает редактирование!',
									buttons	: Ext.Msg.OK,
									icon: Ext.MessageBox.ERROR
								});
							}
							
						},
						failure: function(response, opts){
							Ext.Msg.show({
								title	:'Ошибка!',
								msg		: 'Не удалось загрузить форму редактирования!',
								buttons	: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
						},
						params : {
							Event : 'GetEditForm'
						}
					});
				}
				
		    });

			if(record.langCombo){
				var langStoreConfig = {
						baseParams	: {
							Event	: 'GetLanguages'
						},
						autoLoad	: true,
						reader		: new Ext.data.JsonReader(
							{
								root: 'data',
					            totalProperty: 'totalCount',
					            id: 'id'
							},
							Ext.data.Record.create([
								{name:'id', type: 'int'},	
								{name:'name'}
							])
						),
						proxy: new Ext.data.HttpProxy({
							url: moduleUrl
						}),
						listeners	: {
							load	: function(){
								langsCombo.setValue(record.defLangId);
							}
						}
					};
				
				var langStore = new Ext.data.Store(langStoreConfig);
				
				var langsCombo = new Ext.form.ComboBox({
				    triggerAction	: 'all',
				    store			: langStore,
				    valueField		: 'id',
				    displayField	: 'name'
				});

				tbar.add('-');
				tbar.add('Язык:');
				tbar.add(langsCombo);
				tbar.add('-');
				tbar.add('Для редактирования кликните на узле 2 раза!');
				
			}	
			
			break;	
	}

	MainWin.addModule(module);
}


//-----------------------------------------------------------------------------

function getPagingBar(store, setupValue, text){
	store.limit = setupValue;
	var pagingBar = new Ext.PagingToolbar({
		pageSize: setupValue,
		store: store,
		displayInfo: true,
		displayMsg: text + ' {0} - {1} из {2}',
		emptyMsg: text + ' отсутствуют.',
		items:[
		    '-',
			'Покзывать:',
			new Ext.form.NumberField({
				emptyText:'',
				selectOnFocus:true,
				width:30,
				value:setupValue,
				listeners:{
					change:function(input, newValue, oldValue){
						pagingBar.pageSize = newValue;
						store.limit = newValue;
						if(store.lastOptions.params){
							store.lastOptions.params.start = 0;
							store.lastOptions.params.limit = newValue;
						}
						store.baseParams.start = 0;
						store.baseParams.limit = newValue;
						store.reload();
					}
				}
			}),
			'записей'
		]							
	});

	return pagingBar;
}



Array.prototype.in_array = function (needle, argStrict) {
	var haystack = this;
    var key = '', strict = !!argStrict;
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }
    return false;
}
