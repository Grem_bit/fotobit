<?php

class LiqpayClass{
	/**
	 * Создает ХЕШ от xml файла для параметра запроса operation_xml
	 * @param string $merchant_id - айди мерчанта
	 * @param string $result_url - сюда вернется клиент после оплаты
	 * @param string $server_url - сюда придет ответ от сервера
	 * @param string $order_id - айди заказа
	 * @param string $amount - стоимость
	 * @param string $currency - валюта (UAH)
	 * @param string $description - описание 
	 * @param string $default_phone - телефон введенный при оплате
	 * @param string $pay_way - способ оплаты (card | etc)
	 * @param string $goods_id - количество покупки товара
	 * @param string $exmp_time - таймаут для оплаты на терминале
	 * @return (xml) string
	 */
	public function operation_xml($merchant_id, $result_url, $server_url, 
		$order_id, $amount, $currency, $description, 
		$default_phone, $pay_way, $goods_id, $exmp_time){
		
		$xml = simplexml_load_file(PATH_LIB.'private/xmlTemplate/operation_xml.xml');
		
		$xml->merchant_id = $merchant_id;
		$xml->result_url = $result_url;
		$xml->server_url = $server_url;
		$xml->order_id = $order_id;
		$xml->amount = $amount;
		$xml->currency = $currency;
		$xml->description = $description;
		$xml->default_phone = $default_phone;
		$xml->pay_way = $pay_way;
		$xml->goods_id = $goods_id;
		$xml->exmp_time = $exmp_time;
		
		return $xml->asXML();
	}
	/**
	 * Создает подпись продавца
	 * @param string $merc_sign - пароль мерчанта 
	 * @param (xml) string $xml - не упакованный xml запроса
	 * @return string - хеш от $merc_sign.$xml.$passwd;
	 */
	public function signature($merc_sign, $xml){
		return base64_encode(sha1($merc_sign.$xml.$merc_sign,1));
	}
		
	/**
	 * Упаковывает XML запроса
	 * @param string $xml
	 * @return string хеш
	 */
	public function encodeXML($xml){
		return base64_encode($xml);
	}
	
	/**
	 * Конвертирует SimpleXMLElement в ассоциативный массив
	 * @param SimpleXMLElement $simple_xml_obj обьект с xml
	 * @return array асоциативка
	 */
	public function responseXmlAsArray($simple_xml_obj){
		$json = json_encode($simple_xml_obj);
		return json_decode($json,true);		
	}
	
	/** не используется
	 * Возвращает статус из полученого xml
	 * @param XMLString $xml
	 */
	public function getResponseStatus($xml){
		return $xml->status;
	}
		

}

?>