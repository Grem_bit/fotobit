<?php
	/*
	На выходе строка урла
	На вход обязательно	: 	1) id 										(int)
							2) таблица									(string)		
							3) fronttext, текст перед урлом				(string)	'' 
	не обязательно		:	3) sometext, иногда нужно например '/view'	(string)	'' 
							4) строить урл по полю id или url 			(boolean)   true
							5) в конце приставка .html 					(boolean)	true
							6) используется внутри ф-ции при рекурсии 	(string)	''
	*/
		function _fullurl($id, $table, $fronttext='', $sometext = '', $idview = false, $html = false, $url = '', $parent_field = 'parent_id', $debug = false){
			global $DB;
			$sql = "SELECT `id`, `url`, `".$parent_field."` FROM `".$table."` WHERE id='".$id."'";
			if ($debug) { echo $sql."<br />"; }
			$res = array();
			//$res = $DB->GetRow($sql);
			$res = $DB->CacheGetRow($sql);
			
			if ($idview) {
				$url =  '/'.$res['id'] . $url;
			} else {
				 $url = '/'. (($res['url']) ? $res['url'] : $res['id']) . $url;
			}
			
			if($res['parent_id'] != 0 ){			
				return _fullurl($res['parent_id'], $table, $fronttext, $sometext, $idview, $html, $url);		
			} else {
				if ($sometext) {
					$url = substr_replace($url, $sometext, strpos(substr($url,1), '/') + 1, 0);
				}	
				if ($url) {
					return   $fronttext .  $url  . (($html) ? '.html' : '');
				} else {
					return  '';
				}
				
			}
		}
		/*
		Хлебные крошки
		На выходе массив 		['title'] => ['url']
		На вход обязательно	: 	1) id 										(int)
								2) таблица									(string)		
		не обязательно		:	3) имя класса, первый элемент урла			(string)	'' 
								4) строить урл по полю id или url 			(boolean)   true
								5) в конце приставка .html 					(boolean)	true
								6) используется внутри ф-ции при рекурсии 	(array)	''
								6) используется внутри ф-ции при рекурсии 	(int)	''
		*/
		function _breadcrumbs($id, $table, $fronttext='', $classname = '', $idview = false, $html = true, $crumbs = array(), $i = 0){
			global $DB;
			$sql = "SELECT `id`, `title`, `parent_id` FROM `".$table."` WHERE id='".$id."'";
			$res = array();
			//$res = $DB->GetRow($sql);
			$res = $DB->CacheGetRow($sql);
			$crumbs[$i]['title'] = $res['title'];
			$crumbs[$i]['url']	 =  _fullurl($res['id'], $table, $fronttext, $classname, $idview, $html);	
			$i++;			
			if($res['parent_id'] != 0 ){
				return _breadcrumbs($res['parent_id'], $table, $fronttext, $classname, $idview, $html, $crumbs, $i);		
			} else {
				return array_reverse($crumbs);
			}
		}
		
		function _echo_br($br){
			if($br){
			$res =  '<div class="CurrentPage">
								<ul>';
								$res .=	'<li><a href="/catalog.html">Каталог товаров</a></li>';	
								$res .=	'<li><img style="padding: 0 5px" src="/public/images/two-arrows.png"></li>';
								 for($i = 0, $cn = sizeof($br); $i<$cn; $i++) {
								$res .=	'<li><a title="" href="'.$br[$i]['url'].'">'.$br[$i]['title'].'</a></li>';
									 if($i != $cn - 1){
									$res .=	'<li><img alt="" src="/public/images/two-arrows.png"></li>';
									 }?>
									<?}
						$res .='	</ul>
							</div>';
				return $res;		
			}	
		 }
		
		 function _print_breadcr($br) { ?>
       	<div class="CurantPage">
       	<ul>
			<li><a href="/" title="">Главная</a></li>
			<? for($i = 0, $cn = sizeof($br); $i<$cn; $i++) { 
          		$url = ($i == $cn - 1) ? $br[$i]['title'] : '<a href="'.$br[$i]['url'].'">'.$br[$i]['title'].'</a>'; 	
          		$class = ($i == $cn - 1) ? 'class="curent"' : ''; ?>
          		<li><span <?=$class?> ><?=$url?></span></li>
          <?} ?>
				
       	</ul>
       	</div>
       <? }
	
//	Обрезает текст до заданной длинны Utf8
	function _trim_word($text, $counttext = 10, $maxlen = 0, $sep = ' ') {
    		$words = split($sep, $text);
     		if ( count($words) > $counttext )
        	 $text = join($sep, array_slice($words, 0, $counttext));
        	 if ($maxlen) {
        	 	if (strlen($text) < $maxlen) {
	        	 	for($i = $counttext; $i < $counttext + 100; $i++) {
		        	 	if ( mb_strlen($text,'UTF-8') < $maxlen) {
		    	    	       	 	$text = join($sep, array_slice($words, 0, $i)); 
		        	 	} else {
		    	    	 	return $text;
		    	    	}
	        		}
        	 	}
        	 }
     		return $text.' ...';
 		}
// выделяет из строки число 
	function _strtoint($str){
		return 	ereg_replace("[^0-9]", "", $str);
	} 		
 		
//	подготовка переменных для последующего юзанья в БД 			
	function var_smart ( $value ) {
			if ( get_magic_quotes_gpc ()) {
				      $value = stripslashes ( $value );
	 		    }
		    if ( ! is_numeric ( $value )) {
				      $value =  mysql_real_escape_string ( $value ) ;
			    }
				    return $value ;
		}	
		
//	преобразование символов всего массива 	
	function _hsc($data) {
 			if (is_array($data)) {
 			foreach ($data as $index => $val) {
 			 	$data[$index] = htmlspecialchars($val);
 				}
 			}
 			return $data;
 		}	
		
//	запись в файл, для дебага
	function _log($s, $m = '', $file = '', $date = true) {
       $s =($s) ? $s : 'ZERO_STRING!!!';
       $m =($m) ? $m : 'wb+';
		$file = ($file) ? $_SERVER['DOCUMENT_ROOT'].$file : $_SERVER['DOCUMENT_ROOT'].'/_log.txt'; 	

       if (!file_exists($file)) {
       		@file_put_contents($file,'');
       		@chmod($file, 0777);	       
       }
		if(is_array($s)) {
			$s = var_export($s,true);
		}
		
		if ($date) {
			$s =  date("-------------------------- d-m-y H:i:s ------------------------------ \n") . $s;
		}
       if ($f = @ fopen ($file, $m))
	     	{
    			@ flock ($f, LOCK_EX);
		    	@ fputs ($f, $s."\n\n");
    			@ flock ($f, LOCK_UN);
		    	@ fclose ($f);
    		}
    }
		
		function _time($exit=0){
			echo "<pre>";
			echo time();
			echo "</pre>";
			if($exit) exit();
		}
		
	
	/* adodb подсчет кол-ва запросов */	
		function CountExecs($conn, $sql, $inputarray) {
		   global $EXECS;
		   $EXECS++;
		}
		
		function CountCachedExecs($conn, $secs2cache, $sql, $inputarray) {
		   global $CACHED;
		   $CACHED++;
		}
		// вывод на экран
		function _get_count_q($title){
			static $x;
			$x += 105;
			global $EXECS,$CACHED; 
			$res = $EXECS + $CACHED;  ?>
			<div style="position: absolute;   top: <?= $x ?>px; left: 100px; background: #FFFFFF; width: 350px; heigth: 100px; "
			<div style="padding: 15px">
				<?= ($title) ? '<p><b>Point: </b>'.$title.'</p>' : '' ?>
   				<p>Всего запросов к базе данных: <b><?=($res)?$res:'0'?></b></p>
	   			<p>Из них взято из кеша : <b><?=($CACHED)? $CACHED : '0'?></b></p>
	   			</div>
	   		</div>	
	  <? }
	  /* end */
	  // функция превода текста с кириллицы в траскрипт
		function _translit($str){
			if($str){
		   	 $cirilica = array("а", "б", "в", "г", "д", "е", "ё", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ъ", "ы", "э", "_", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ъ", "Ы", "Э", "_", "ж",   "ц",  "ч",  "ш",    "щ", "ь", "ю", "я",   "Ж",  "Ц",  "Ч",  "Ш",    "Щ","Ь",  "Ю", "Я", " ", ",", ".","!","?");
			 $latinica = array("a", "b", "v", "g", "d", "e", "e", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h",  "", "i", "e", "_", "A", "B", "V", "G", "D", "E", "E", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H",  "", "I", "E", "I",  "zh", "ts", "ch", "sh", "shch", "", "yu", "ya", "ZH", "TS", "CH", "SH", "SHCH", "", "YU", "YA", "-", "", "","","");
		     	return str_replace($cirilica, $latinica, $str);
			}else{
				return '';
			} 
		}
	 		
	  // возвращает расширение файла
	   function _fgetex($filename) {
	    	 return end(explode(".", $filename));
	   }
	   
	   // возвращает права доступа к папке или файлу
	   function _fgetperm($path){
			$fileperms =  substr ( decoct (  fileperms ( $path ) ), 2, 6 );
			if ( strlen ( $fileperms ) == '3' ){ $fileperms = '0' . $fileperms; }
			return $fileperms;	   
	   }
		
	  // заменяет руские урлы транстлитом в заданной параметром таблице
	  function _translit_table($table,$f1,$f2){
	  	global $DB;
	  	$sql = "SELECT `id`, `".$f1."`,`".$f2."` FROM `".$table."`";
	  	//_debug($sql,1);
	  	$res = $DB->GetAll($sql);
	  	for ($i = 0, $cn = count($res); $i < $cn; $i++) {
	  		$temp= _translit($res[$i][$f1]); 
	  		$sql = "UPDATE `".$table."` SET `".$f2."` = '".$temp."' WHERE `id` = '".$res[$i]['id']."'" ;
	  		$DB->Execute($sql);	
	  	}
	  }
	 // текуший урл + можно добавить доп параметр 
	 function _curent_url($add=''){
			$res = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			if($add) {
			    $pref = (strpos($res,'?') !== false) ? '&' : '?'; 
			    $res  = $res.$pref.$add;
			}
			return 'http://'.$res;
		}
	 
	function _cur_url($add){
		$res = str_replace($_SERVER['QUERY_STRING'],'',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$res = str_replace('?','',$res);
		if($add) {
			    $pref = (strpos($res,'?') !== false) ? '&' : '?'; 
			    $res  = $res.$pref.$add;
			}
			return 'http://'.$res;
	}	
		
	// обрезает строку на заданное кол-во символов с конца	
	function _cut_str(&$str, $clear = '1'){
		$str = substr($str, 0, strlen($str)-$clear);
		return;	
	}	
	function is_for($ar){ 
		return is_array($ar) && (count($ar) > 0 );
	}
	function _get_mysql_date($ar){
		return  implode('-',array_reverse(explode('.',$ar))).' 00:00:00';
	}
	
	function resize($file, $type, $height, $width){
			
		    $img = false;
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = @imagecreatefromjpeg($file);
					break;
				case 'image/x-png':
				case 'image/png':
					$img = @imagecreatefrompng($file);
					break;
				case 'image/gif':
					$img = @imagecreatefromgif($file);
					break;
		    }
		    if(!$img){
				return false;
		    }
		    
		    $curr = @getimagesize($file);
		    
		    $perc_w = $width / $curr[0];
		    $perc_h = $height / $curr[1];
		    
		    if(($width > $curr[0]))/* || ($height > $curr[1]))*/{
				return;
		    }
		    
		    if($perc_h > $perc_w){
				$width = $width;
				$height = round($curr[1] * $perc_w);
		    } else {
				$height = $height;
				$width = round($curr[0] * $perc_h);
		    }
		    
		  
		    $nwimg = @imagecreatetruecolor($width, $height);
		    @imagecopyresampled($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[1]);
		    
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					@imagejpeg($nwimg, $file);
					break;
				case 'image/x-png':
				case 'image/png':
					@imagepng($nwimg, $file);
					break;
				case 'image/gif':
					@imagegif($nwimg, $file);
					break;
		    }
		    
		    @imagedestroy($nwimg);
		    @imagedestroy($img);
			return true;
		}	
		
	function _utf8_sustr($str, $width, $break, $cut = false) {		
		if(mb_strlen($str,'UTF-8')>$width){
			$str = mb_substr($str, 0, $width)."...";
		}
		return $str;	
	}
	function _myutf8_substr($str,$from,$len){
		return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'.
		'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s',
		'$1',$str)."...";
	}
	
	// Генератор пароля. $number - кол-во символов в пароле
	function generate_password($number){
		$arr = array('a','b','c','d','e','f',
					 'g','h','i','j','k','l',
					 'm','n','o','p','r','s',
					 't','u','v','x','y','z',
					 'A','B','C','D','E','F',
					 'G','H','I','J','K','L',
					 'M','N','O','P','R','S',
					 'T','U','V','X','Y','Z',
					 '1','2','3','4','5','6',
					 '7','8','9','0',',',
					 '(',')','[',']','!','?',
					 '&','^','%','@','*','$',
					 '<','>','/','|','+','-',
					 '{','}','`','~');
		// Генерируем пароль
		$pass = "";
		for($i = 0; $i < $number; $i++)
		{
		  // Вычисляем случайный индекс массива
		  $index = rand(0, count($arr) - 1);
		  $pass .= $arr[$index];
		}
		return $pass;
	}
	
	/*
		Функция получает в качестве параметра путь к папке. Первое что она делает это проверяет, существует ли эта папка и папка это вообще если результат проверки положительный то открывает папку на чтение и запускается цикл чтения папки, получения всего ее содержимого (папки, файлы, ссылки) далее не забываем исключать папки '.'(точка) и '..'(две точки) первое - ссылка на этот каталог, второе - ссылка на каталог уровнем выше. После этого меняем права доступа к файлу или каталогу и если это папка то заходим в нее и удаляем все что в ней, иначе это файл или ссылка и тогда просто удаляем.
		
		Пример использования функции:
		// путь от корня сайта
		$DeletedFolder='/folder';
		RemoveDir($_SERVER['DOCUMENT_ROOT'].$DeletedFolder);
	*/
	function RemoveDir($path){
		if(file_exists($path) && is_dir($path)){
			$dirHandle = opendir($path);
			while (false !== ($file = readdir($dirHandle))){
				if ($file!='.' && $file!='..'){// исключаем папки с названием '.' и '..'
					$tmpPath=$path.'/'.$file;
					chmod($tmpPath, 0777);
					
					if (is_dir($tmpPath)){  // если папка
						RemoveDir($tmpPath);
					}else{
						if(file_exists($tmpPath)){
							// удаляем файл
							unlink($tmpPath);
						}
					}
				}
			}
			closedir($dirHandle);
			
			// удаляем текущую папку
			if(file_exists($path))
			{
				rmdir($path);
			}
		}else{
			echo "Удаляемой папки не существует или это файл!";
		}
	}
	
	// удаляет все файлы из папки
	function cleanDir($dir) {
		$files = glob($dir."/*");
		$c = count($files);
		if (count($files) > 0) {
			foreach ($files as $file) {      
				if (file_exists($file)) {
				unlink($file);
				}   
			}
		}
	}

	
	function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
            $object = get_object_vars( $object );
        }
        return array_map( 'objectToArray', $object );
    }
	
	// ф-я распарсивает в массив строку вида:
	// "элемент 1{опция 1;опция 2;опция 3}|элемент 2{опция 1;опция 2}|элемент 3"
	function explodeMultiString($string){
	
		$temp_arr = explode('|', $string);
		$result = '';
		$i = 0;
		foreach($temp_arr as $item){
			preg_match("/\{([^}]+)\}*/i", $item, $options);
			if($options){
				preg_match("/([^{]+)*\{/i", $item, $item);
				$result[$i]['item'] = $item[1];
				$result[$i]['options'] = explode(';', $options[1]);
			}else{
				$result[$i]['item'] = $item;
			}
			$i++;
		}
		return $result;
	}
	
	function myFileCopy($source, $dest, $filename){
	
		global $config;
		// _debug($config, 1);
		$absolute_path = $config['absolute-path'];
		// _debug($absolute_path, 1);
		// _debug($this->GetConfigParam('site_url'), 1);
		// _debug($absolute_path.$source);
		// _debug($absolute_path.$dest);
		// exit('show');
		// copy($absolute_path.$source, $absolute_path.$dest);
		// _debug($absolute_path.$dest, 0);
		mkdir($absolute_path.$dest, 0777, true);
		// mkdir('/home/stepshop/data/www/photo.stepshop.com.ua/public/files/images/orders/70/9x13g/x1/', 0777, true);
		// rename($absolute_path.$source.$filename, $absolute_path.$dest.$filename);
		copy($absolute_path.$source.$filename, $absolute_path.$dest.$filename);
		// exit('done');
	}
?>