<?
/*//////////////////////////////////////////////////////////////////////////////
        Azone v2.0
		AuthPage.php
        --------
        Started On:     July 16, 2009
        Copyright:      (C) 2008-2009 FlyUpStudio.
        E-mail:         support@flyupstudio.com.ua
        $Id: AuthPage.php,v 2.0 2009/07/16 13:21:00 Maxim Exp $
//////////////////////////////////////////////////////////////////////////////*/	

	AttachLib('Page');

	abstract class AuthPage extends Page {
		
		/*
		 * Private properties
		 */
		
		private $AuthData			= array();
		
		

		/*
		 * Events
		 */
		
		public function OnLogin() {
			
			global $DB, $IsAzone;
			if($IsAzone){
				$sql = 'SELECT * FROM %s WHERE %s=\'%s\' AND %s=\'%s\'';
				$sql = sprintf($sql, $this->AuthData['table'], 
									 $this->AuthData['f_username'], 
									 mysql_real_escape_string($_REQUEST[$this->AuthData['f_username']]), 
									 $this->AuthData['f_password'], 
									 $this->CryptPassword($_REQUEST[$this->AuthData['f_password']]));
			}elseif($_REQUEST['email']){
				$sql = 'SELECT *  
						FROM users u 
						WHERE u.email=\''.mysql_real_escape_string($_REQUEST['email']).'\' and
							  u.password=\''.$this->CryptPassword($_REQUEST['password']).'\' and
							  u.active = 1';
			}else{
				$sql = 'SELECT *  
						FROM users u 
						WHERE u.login=\''.mysql_real_escape_string($_REQUEST['login']).'\' and
							  u.password=\''.$this->CryptPassword($_REQUEST['password']).'\' and
							  u.active = 1';
			}
			
			$this->UserAccount = $DB->GetRow($sql);
			
			if($IsAzone){
				
			}
			$_SESSION[$this->AuthData['session']] = $this->UserAccount;
			
			if ($_REQUEST['remember_me']){
				if(!$IsAzone){
					setcookie(md5("asd"),base64_encode(serialize(array($this->UserAccount[$this->AuthData['f_username']],$this->UserAccount[$this->AuthData['f_password']]))),time()+60*60*24*31,"/");
				} else {
					setcookie(md5("asd1"),base64_encode(serialize(array($this->UserAccount[$this->AuthData['f_username']],$this->UserAccount[$this->AuthData['f_password']]))),time()+60*60*24*31,"/");
				}
			}
			
//			if($IsAzone){
//				$this->getPermissionList();
//			}

			if(!$this->_is_auth_user()) {
				$this->SetError($this->AuthData['error']);
			}
			elseif($_REQUEST[$this->AuthData['f_redirect']]) {	
				$this->GoToUrl($this->DecodeUrlParam($_REQUEST[$this->AuthData['f_redirect']]));
			}
		}
		
		public function OnAjaxLogin() {
			
			global $DB, $board_config;
			
			$this->SendAjaxHeaders();
			
			if($_REQUEST['email']){
				$sql = 'SELECT *  
						FROM users u 
						WHERE u.email=\''.mysql_real_escape_string($_REQUEST['email']).'\' and
							  u.password=\''.$this->CryptPassword($_REQUEST['password']).'\' and
							  u.active = 1';
			}else{
				$sql = 'SELECT *  
						FROM users u 
						WHERE u.login=\''.mysql_real_escape_string($_REQUEST['login']).'\' and
							  u.password=\''.$this->CryptPassword($_REQUEST['password']).'\' and
							  u.active = 1';
			}
			
			/*PHPBB LOGIN*/
			/*$result = $DB->GetAll("SELECT * FROM  phpbb_config");
			foreach ($result as $value)
				$board_config[$value['config_name']] = $value['config_value'];
			$this->log_in($_REQUEST['email_log'],$_REQUEST['pass_log']);*/
	
			$this->UserAccount = $DB->GetRow($sql);

			if($this->UserAccount){

				$_SESSION[$this->AuthData['session']] = $this->UserAccount;
				
				if ($_REQUEST['remember_me_log']){
					if(!$IsAzone){
						setcookie("asd",base64_encode(serialize(array($this->UserAccount))),time()+60*60*24*31,"/");
					} else {
						setcookie("asd1",base64_encode(serialize(array($this->UserAccount[$this->AuthData['f_username']],$this->UserAccount[$this->AuthData['f_password']]))),time()+60*60*24*31,"/");
					}
				}
			}else{
				
			}
			
			if(!$this->_is_auth_user()) {
				$text = $this->_get_lang_constant('LOGIN_FAIL');
				echo json_encode(array('login' => false,
										'log' => true,
										'text' => $text
									));
			}else{
				$text = $this->_get_lang_constant('LOGIN_SUCCESS');
				echo json_encode(array('success' => true,
										'text' => $text,
										'redirect' => $_REQUEST[$this->AuthData['f_redirect']],
										'user_info' => '<a href="/user-account.html" class="active">'.($this->UserAccount['login'] ? $this->UserAccount['login'] : $this->UserAccount['email']).'</a><a href="/user-account.html?Event=Logout&redirect=/">Выход</a>'
									));
			}
			
			exit;
		}
		
		public function OnLogout() {
			global $IsAzone;
			//_debug($this->AuthData['session'],1);
			unset($this->UserAccount);
			unset($_SESSION[$this->AuthData['session']]);
			unset($_SESSION['Trash']);
			unset($_SESSION['Cur_order']);
			
			if(!$IsAzone){
				setcookie(md5("asd"),"",time()-60*60,"/");
			} else {
				setcookie(md5("asd1"),"",time()-60*60,"/");
			}
			if($_REQUEST[$this->AuthData['f_redirect']]) {
				// $this->GoToUrl($this->DecodeUrlParam($_REQUEST[$this->AuthData['f_redirect']]));
				$this->GoToUrl($_REQUEST[$this->AuthData['f_redirect']]);
			}
		}
		

		/*
		 * Private methods
		 */
		
		private function _is_auth_user() {
			global $IsAzone;
			
			if(is_array($this->UserAccount) && count($this->UserAccount) > 0) {
				
				$this->IsAuthUser = true;
			}
			else {
				
				$this->IsAuthUser = false;
				if(!$IsAzone){
					$this->AuthData = $this->GetConfigParam('auth');
					
				}else{
					$this->AuthData = $this->GetConfigParam('azone_auth');
				}
				$this->TemplatesBaseDir = '';
			}
			
			
			return $this->IsAuthUser;
		}
		
		private function _load_auth_data() {
			
			global $IsAzone;
			
			if(!$IsAzone) {				
				$this->AuthData = $this->GetConfigParam('auth');
			}
			else {	
				$this->AuthData = $this->GetConfigParam('azone_auth');
				//print_r($this->AuthData);
				if(!$this->AuthData){
					if($_REQUEST['Event']){
					//	echo '<script type="text/javascript">alert("asdasd")</style>';
					//	exit();
					}
				}
			}
		}
		
		
		/*
		 * Protected methods
		 */
		
		protected function _process_events($event) {
			global $IsAzone;
			
			if($event == 'Login' || $event == 'Logout' || $event == 'AjaxLogin') {
				
				$this->_execute_method('On'.$event);
				$this->_execute_method('OnAfter'.$event);
				$this->_clear_event();
			}
			
			if(!$this->_is_auth_user()) {
			
				$this->ProcessEvents = false;
				if($IsAzone)
					$this->SetTemplate($this->AuthData['template']);
				else
					$this->GoToUrl('/');
					$this->SetTemplate($this->AuthData['template']);
			}
			
			Page::_process_events($event);
		}
		
		protected function _load_config() {
			
			Page::_load_config();
			$this->_load_auth_data();
		}
	}

?>