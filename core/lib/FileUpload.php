<?php
/**
 * CKFinder
 * ========
 * http://www.ckfinder.com
 * Copyright (C) 2007-2008 Frederico Caldeira Knabben (FredCK.com)
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
 */

/**
 * @package CKFinder
 * @subpackage CommandHandlers
 * @copyright Frederico Caldeira Knabben
 */

/**
 * Handle FileUpload command
 * 
 * @package CKFinder
 * @subpackage CommandHandlers
 * @copyright Frederico Caldeira Knabben
 */
class CKFinder_Connector_CommandHandler_FileUpload extends CKFinder_Connector_CommandHandler_CommandHandlerBase
{
    /**
     * Command name
     *
     * @access protected
     * @var string
     */
    protected $command = "FileUpload";

    /**
     * send response (save uploaded file, resize if required)
     * @access public
     *
     */
    public function sendResponse()
    {
        $iErrorNumber = CKFINDER_CONNECTOR_ERROR_NONE;

        $oRegistry =& CKFinder_Connector_Core_Factory::getInstance("Core_Registry");
        $oRegistry->set("FileUpload_fileName", "unknown file");

        $uploadedFile = array_shift($_FILES);

        if (!isset($uploadedFile['name'])) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_INVALID);
        }

        $sFileName = CKFinder_Connector_Utils_FileSystem::convertToFilesystemEncoding(basename($this->Ruslat($uploadedFile['name'])));
        
        $oRegistry->set("FileUpload_fileName", $sFileName);

        $this->checkConnector();
        $this->checkRequest();

        $_config =& CKFinder_Connector_Core_Factory::getInstance("Core_Config");
        if (!$this->_currentFolder->checkAcl(CKFINDER_CONNECTOR_ACL_FILE_UPLOAD)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UNAUTHORIZED);
        }

        $_resourceTypeConfig = $this->_currentFolder->getResourceTypeConfig();
        if (!CKFinder_Connector_Utils_FileSystem::checkFileName($sFileName) || $_resourceTypeConfig->checkIsHiddenFile($sFileName)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_INVALID_NAME);
        }

        $resourceTypeInfo = $this->_currentFolder->getResourceTypeConfig();
        if (!$resourceTypeInfo->checkExtension($sFileName)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_INVALID_EXTENSION);
        }

        $sFileNameOrginal = $sFileName;
        $oRegistry->set("FileUpload_fileName", $sFileName);

        $maxSize = $resourceTypeInfo->getMaxSize();
        if (!$_config->checkSizeAfterScaling() && $maxSize && $uploadedFile['size']>$maxSize) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_TOO_BIG);
        }

        $htmlExtensions = $_config->getHtmlExtensions();
        $sExtension = CKFinder_Connector_Utils_FileSystem::getExtension($sFileNameOrginal);

        if ($htmlExtensions
        && !CKFinder_Connector_Utils_Misc::inArrayCaseInsensitive($sExtension, $htmlExtensions)
        && ($detectHtml = CKFinder_Connector_Utils_FileSystem::detectHtml($uploadedFile['tmp_name'])) === true ) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_WRONG_HTML_FILE);
        }

        $sExtension = CKFinder_Connector_Utils_FileSystem::getExtension($sFileNameOrginal);
        $secureImageUploads = $_config->getSecureImageUploads();
        if ($secureImageUploads
        && ($isImageValid = CKFinder_Connector_Utils_FileSystem::isImageValid($uploadedFile['tmp_name'], $sExtension)) === false ) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_CORRUPT);
        }

        switch ($uploadedFile['error']) {
            case UPLOAD_ERR_OK:
                break;

            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_TOO_BIG);
                break;

            case UPLOAD_ERR_PARTIAL:
            case UPLOAD_ERR_NO_FILE:
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_CORRUPT);
                break;

            case UPLOAD_ERR_NO_TMP_DIR:
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_NO_TMP_DIR);
                break;

            case UPLOAD_ERR_CANT_WRITE:
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_ACCESS_DENIED);
                break;

            case UPLOAD_ERR_EXTENSION:
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_ACCESS_DENIED);
                break;
        }

        $sServerDir = $this->_currentFolder->getServerPath();
        $iCounter = 0;

        while (true)
        {
            $sFilePath = CKFinder_Connector_Utils_FileSystem::combinePaths($sServerDir, $sFileName);

            if (file_exists($sFilePath)) {
                $iCounter++;
                $sFileName =
                CKFinder_Connector_Utils_FileSystem::getFileNameWithoutExtension($sFileNameOrginal) .
                "(" . $iCounter . ")" . "." .
                CKFinder_Connector_Utils_FileSystem::getExtension($sFileNameOrginal);
                $oRegistry->set("FileUpload_fileName", $sFileName);

                $iErrorNumber = CKFINDER_CONNECTOR_ERROR_UPLOADED_FILE_RENAMED;
            } else {
                if (false === move_uploaded_file($uploadedFile['tmp_name'], $sFilePath)) {
                    $iErrorNumber = CKFINDER_CONNECTOR_ERROR_ACCESS_DENIED;
                }
                else {
                	
                    if (isset($detectHtml) && $detectHtml === -1 && CKFinder_Connector_Utils_FileSystem::detectHtml($sFilePath) === true) {
                        @unlink($sFilePath);
                        $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_WRONG_HTML_FILE);
                    }
                    else if (isset($isImageValid) && $isImageValid === -1 && CKFinder_Connector_Utils_FileSystem::isImageValid($sFilePath, $sExtension) === false) {
                        @unlink($sFilePath);
                        $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_CORRUPT);
                    }
                }
                if (is_file($sFilePath) && ($perms = $_config->getChmodFiles())) {
                    $oldumask = umask(0);
                    chmod($sFilePath, $perms);
                    umask($oldumask);
                }
                break;
            }
        }

        if (!$_config->checkSizeAfterScaling()) {
            $this->_errorHandler->throwError($iErrorNumber, $sFileName, false);
        }

        //resize image if required
        require_once CKFINDER_CONNECTOR_LIB_DIR . "/CommandHandler/Thumbnail.php";
        $_imagesConfig = $_config->getImagesConfig();

        if ($_imagesConfig->getMaxWidth()>0 && $_imagesConfig->getMaxHeight()>0 && $_imagesConfig->getQuality()>0) {
            CKFinder_Connector_CommandHandler_Thumbnail::createThumb($sFilePath, $sFilePath, $_imagesConfig->getMaxWidth(), $_imagesConfig->getMaxHeight(), $_imagesConfig->getQuality(), true) ;
        }

        if ($_config->checkSizeAfterScaling()) {
            //check file size after scaling, attempt to delete if too big
            clearstatcache();
            if ($maxSize && filesize($sFilePath)>$maxSize) {
                @unlink($sFilePath);
                $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UPLOADED_TOO_BIG);
            }
            else {
                $this->_errorHandler->throwError($iErrorNumber, $sFileName);
            }
        }
    }
    
    function Ruslat ($string){

			$string = ereg_replace("ж","zh",$string);
			$string = ereg_replace("ё","yo",$string);
			$string = ereg_replace("й","i",$string);
			$string = ereg_replace("ю","yu",$string);
			$string = ereg_replace("ь","",$string);
			$string = ereg_replace("ч","ch",$string);
			$string = ereg_replace("щ","sh",$string);
			$string = ereg_replace("ц","c",$string);
			$string = ereg_replace("у","u",$string);
			$string = ereg_replace("к","k",$string);
			$string = ereg_replace("е","e",$string);
			$string = ereg_replace("н","n",$string);
			$string = ereg_replace("г","g",$string);
			$string = ereg_replace("ш","sh",$string);
			$string = ereg_replace("з","z",$string);
			$string = ereg_replace("х","h",$string);
			$string = ereg_replace("ъ","",$string);
			$string = ereg_replace("ф","f",$string);
			$string = ereg_replace("ы","y",$string);
			$string = ereg_replace("в","v",$string);
			$string = ereg_replace("а","a",$string);
			$string = ereg_replace("п","p",$string);
			$string = ereg_replace("р","r",$string);
			$string = ereg_replace("о","o",$string);
			$string = ereg_replace("л","l",$string);
			$string = ereg_replace("д","d",$string);
			$string = ereg_replace("э","yе",$string);
			$string = ereg_replace("я","jа",$string);
			$string = ereg_replace("с","s",$string);
			$string = ereg_replace("м","m",$string);
			$string = ereg_replace("и","i",$string);
			$string = ereg_replace("т","t",$string);
			$string = ereg_replace("б","b",$string);
			$string = ereg_replace("Ё","yo",$string);
			$string = ereg_replace("Й","I",$string);
			$string = ereg_replace("Ю","YU",$string);
			$string = ereg_replace("Ч","CH",$string);
			$string = ereg_replace("Ь","",$string);
			$string = ereg_replace("Щ","SH'",$string);
			$string = ereg_replace("Ц","C",$string);
			$string = ereg_replace("У","U",$string);
			$string = ereg_replace("К","K",$string);
			$string = ereg_replace("Е","E",$string);
			$string = ereg_replace("Н","N",$string);
			$string = ereg_replace("Г","G",$string);
			$string = ereg_replace("Ш","SH",$string);
			$string = ereg_replace("З","Z",$string);
			$string = ereg_replace("Х","H",$string);
			$string = ereg_replace("Ъ","",$string);
			$string = ereg_replace("Ф","F",$string);
			$string = ereg_replace("Ы","Y",$string);
			$string = ereg_replace("В","V",$string);
			$string = ereg_replace("А","A",$string);
			$string = ereg_replace("П","P",$string);
			$string = ereg_replace("Р","R",$string);
			$string = ereg_replace("О","O",$string);
			$string = ereg_replace("Л","L",$string);
			$string = ereg_replace("Д","D",$string);
			$string = ereg_replace("Ж","Zh",$string);
			$string = ereg_replace("Э","Ye",$string);
			$string = ereg_replace("Я","Ja",$string);
			$string = ereg_replace("С","S",$string);
			$string = ereg_replace("М","M",$string);
			$string = ereg_replace("И","I",$string);
			$string = ereg_replace("Т","T",$string);
			$string = ereg_replace("Б","B",$string);
			$string = ereg_replace("&","AND",$string);
			//$string = ereg_replace(".","",$string);
			$string = ereg_replace(" ","_",$string);
		return $string;
	}
    
}
