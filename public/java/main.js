var humanMsg = {
	setup: function(appendTo, msgOpacity) {
		humanMsg.msgID = 'humanMsg';
		humanMsg.logID = 'humanMsgLog';
		// appendTo is the element the msg is appended to
		if (appendTo == undefined)
			appendTo = 'body';
		// Opacity of the message
		humanMsg.msgOpacity = .9;
		if (msgOpacity != undefined) 
			humanMsg.msgOpacity = parseFloat(msgOpacity);
		// Inject the message structure
		jQuery(appendTo).append('<div id="'+humanMsg.msgID+'" class="humanMsg"><p></p></div>')
		
	},

	displayMsg: function(msg) {
		
		if (msg == '')
			return;
		clearTimeout(humanMsg.t2);
		// Inject message
		jQuery('#'+humanMsg.msgID+' p').html(msg)
		// Show message
		jQuery('#'+humanMsg.msgID+'').show().animate({ opacity: humanMsg.msgOpacity}, 1000)
		// Watch for mouse & keyboard in .5s
		humanMsg.t1 = setTimeout("humanMsg.bindEvents()", 700)
		// Remove message after 5s
		humanMsg.t2 = setTimeout("humanMsg.removeMsg()", 5000)
	},

	bindEvents: function() {
	// Remove message if mouse is moved or key is pressed
		jQuery(window)
			.mousemove(humanMsg.removeMsg)
			.click(humanMsg.removeMsg)
			.keypress(humanMsg.removeMsg)
	},

	removeMsg: function() {
		// Unbind mouse & keyboard
		jQuery(window)
			.unbind('mousemove', humanMsg.removeMsg)
			.unbind('click', humanMsg.removeMsg)
			.unbind('keypress', humanMsg.removeMsg)
		// If message is fully transparent, fade it out
		//if (jQuery('#'+humanMsg.msgID).css('opacity') == humanMsg.msgOpacity)
			jQuery('#'+humanMsg.msgID).animate({ opacity: 0 }, 500, function() { jQuery(this).hide() })
	}
};

function OrderSuccess(){
	// alert('1');
	$('#order-form').remove();
	$('.in').append('<div class="text"><p>Ваша корзина пуста</p></div>');
	$('.basket').load('/order?Event=AddToCart');
	jQuery.scrollTo('0px',{duration:500});
	
}



$(document).ready(function() {
		$('a[rel="fb"]').fancybox({
			arrows: false
		}); 
	$('INPUT[type="text"]').autoClear();
	$('TEXTAREA').autoClear();
	
	$('.mess-slider ul').easyPaginate({
		auto:true,
		step:3
	});
	// Валидайия
	/* Валидация и отправка всех форм с классом validation-form */
	$('.validation-formm').submit(function(e){ // c всплывалкой
		
		e.preventDefault();
		
		var validation = $(".valid", this);
		var error = false;
		//console.log(validation);
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					validRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
					strEmail = el.value;
					if(strEmail.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					} 
				}else if($(this).hasClass('phone')){
					validRegExp = /^\d{10,13}$/i;
					strPhone = el.value;
					if (strPhone.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					}
				}else if($(this).hasClass('rules')){
					if(!el.checked){
						error = true;
						alert('Для регистрации необходимо принять условия использования сайта.');
					}
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');							
					}						
				}					
			});
		}
		if(!error){
			
			form = $(this);
			if(form.length){
				var action = form.attr('action');
				if(action == '') action = '/';
				var str = form.serialize();
				$.post(action, str+'&ajax=true', function(response){
					var params;
					//eval("params = " + response);
					params = response;
					
					if (params.success){
						
						if(params.url){
							//alert('asd');
							var host_name=window.location.hostname;
							//console.log(host_name);
							window.location= 'http://'+host_name+'/user-account.html'
						}
						if(params.text){
							// $('.thanks', form).html(params.text);
							// $('.thanks').html('<p>'+params.text+'</p>');
							humanMsg.displayMsg(params.text);
						}
						// $('.thanks', form).show();
						//$('.thanks').show();
						if(params.callback){
							if (eval("typeof " + params.callback + " == 'function'")) {
								eval(params.callback + '();');
							}
						}
						setTimeout(function(){
							$('.thanks', form).hide(500);
							
							$(form).find('input[type="text"], textarea').val('');						
						},2500);
					}else{
						
						humanMsg.displayMsg('Форма не отправлена. Проверьте все поля на правельность ввода.');
						$(params.errors).each(function(ind, el){$("input[name='"+el+"']", form).addClass('error-ico');});
						$(".validateTips", form).html(params.errortext);
						
						if(params.text){
							//$('.thanks', form).html(params.text);
						}
					}
				}, 'json');
			}
		}else{
			
			//$('#captcha').load('/registration.html?Event=ChangeCaptcha');
		}
		return false;
	});
//	humanMsg.displayMsg('Форма не отправлена. Проверьте все поля на правельность ввода. Возможно вы ввели не верный код.');
	$('.validation-form').submit(function(e){
		
		e.preventDefault();
		
		var validation = $(".valid", this);
		var error = false;
		
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					validRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
					strEmail = el.value;
					if(strEmail.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					} 
				}else if($(this).hasClass('phone')){
					validRegExp = /^\d{10,13}$/i;
					strPhone = el.value;
					if (strPhone.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					}
				}else if($(this).hasClass('rules')){
					if(!el.checked){
						error = true;
						alert('Для регистрации необходимо принять условия использования сайта.');
					}
				}else if($(this).hasClass('day') || $(this).hasClass('month') || $(this).hasClass('year')){
					if(isNaN(el.value)){
						error = true;
						$('#err').attr('src', '/public/images/error.gif');
					}
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');							
					}						
				}					
			});
		}
		if(!error){
			var form = $(this);
			if(form.length){
				var action = form.attr('action');
				if(action == '') action = '/';
				var str = form.serialize();
				$.post(action, str+'&ajax=true', function(response){
					var params;
					//eval("params = " + response);
					params = response;
					if (params.success){
						$(".validateTipsTitle", form).hide();
						$(".validateTips",form).html('');
						// $('.form_content',form).hide();
						/*offset = form.offset();
						$(document).scrollTop(offset.top);*/
						if(params.text){
							$('.thanks', form).html(params.text);
						}
						if (form.hasClass('contacts')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Ваше сообщение отправлено</div><div class="message_thanks">Ожидайте ответ в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('comment')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за комментарий</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('report')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за отзыв</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('coupon')){
							$.fancybox(params.coupon, {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('ajax-login')){
							
							$('.enter').html(params.user_info);
							$.fancybox.close();	
						}else if(form.hasClass('ajax-registration')){
							
							$('.registeren').attr('title',$('.registeren').html());	
							$('.registeren').html(params.text);
							setTimeout(function() {								
								$('.registeren').html($('.registeren').attr('title'));
								$.fancybox.close();	
								window.location = '/';
							}, 3000);
						}else if(form.hasClass('ajax-forgot-pass')){
							
							$('.forgotten').attr('title',$('.forgotten').html());	
							$('.forgotten').removeClass('error_msg');								
							$('.forgotten').html(params.text);
							setTimeout(function() {								
								$('.forgotten').html($('.forgotten').attr('title'));			
								$('.forgotten').addClass('error_msg');
								$.fancybox.close();								
							}, 3000);
						}
		
						setTimeout(function() {
							// parent.$.fancybox.close();
						}, 5000);
						
						// $('.thanks',form).show();
						if(params.callback){
							if (eval("typeof " + params.callback + " == 'function'")) {
								eval(params.callback + '();');
							}
						}
						if(params.redirect){
							location = params.redirect;
						}
					}else{
						$(params.errors).each(function(ind, el){$("input[name='"+el+"']", form).addClass('error-ico');});
						// $(".validateTipsTitle", form).show();
						// $(".validateTips", form).html(params.errortext);
						if(form.hasClass('ajax-registration')){							
							$('.registeren').attr('title',$('.registeren').html());	
							$('.registeren').html(params.text);
							
						}
						if(params.log){
								humanMsg.displayMsg('Авторизация не удалась. Проверьте логин и пароль');
						}
						if(params.text){
							$(form).parent().addClass('error');
							$('.error_msg', form).html(params.text);
							$('.error_msg', form).css('visibility', 'visible');
						}
					}
				}, 'json');
			}
		}else{
			//$('#captcha').load('/registration.html?Event=ChangeCaptcha');
		}
		return false;
	});
	// Конец валидации
	

	humanMsg.setup();
});
