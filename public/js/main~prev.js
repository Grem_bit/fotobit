$(document).ready(function(){

	/*$('#ContactForm').submit(function() {
		
		var validation = $("#ContactForm .valid");
		var error = false;
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					 validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
					 strEmail = el.value;
					 if (strEmail.search(validRegExp) == -1){
					 	error = true;
					 	$(this).addClass('error-ico');
					 } 
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');
					}
				}
			});
		}
		if(!error){
			if(validation){validation.removeClass('error-ico');}
			var str = $('#ContactForm').serialize();
			$.post(	'/', str,
				function(response){
					var params;
					eval("params = "+response);
					if(params.success){
						$('#ContactForm .ppp').css('display','block');
						
						setTimeout(function() {
								$('#ContactForm .thanks').css('display','none');
							},
							3000
						);
						var validation = $("#ContactForm .valid");
						validation.each(function(index, el){
							if($(this).hasClass('textarea')){
								el.innerHTML = el.title;
								el.value = el.title;							
							}else{
								el.value = el.alt;						
							}
						});	
						$("#message").html('');
					}else{
						$('#ajax_container_contact .ppp td').html('<p>К сожалению произошла ошибка!</p><p>Попробуйте отправить запрос ещё раз!</p>');
						$('#ajax_container_contact .ppp').css('display','block');
						
						setTimeout(function() {
								$('#ajax_container_contact .ppp').css('display','none');
								$('#ajax_container_contact .ppp td').html('<p>Спасибо за запрос.</p> <p>В ближайшее время с вами свяжется менеджер.</p>');
							}, 
							3000
						);
					}
				}
			);
		}
		return false;
	});*/
	
	$('.validation-form').submit(function(e){
		
		e.preventDefault();
		
		var validation = $(".valid", this);
		var error = false;
		
		if(validation){
			validation.removeClass('error-ico');
			validation.each(function(index, el){
				if($(this).hasClass('email')){
					validRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
					strEmail = el.value;
					if(strEmail.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					} 
				}else if($(this).hasClass('phone')){
					validRegExp = /^\d{10,13}$/i;
					strPhone = el.value;
					if (strPhone.search(validRegExp) == -1){
						error = true;
						$(this).addClass('error-ico');
					}
				}else if($(this).hasClass('rules')){
					if(!el.checked){
						error = true;
						alert('Для регистрации необходимо принять условия использования сайта.');
					}
				}else if($(this).hasClass('day') || $(this).hasClass('month') || $(this).hasClass('year')){
					if(isNaN(el.value)){
						error = true;
						$('#err').attr('src', '/public/images/error.gif');
					}
				}else{
					if(!el.value || el.value == el.alt || el.value == el.title){
						error = true;
						$(this).addClass('error-ico');							
					}						
				}					
			});
		}
		if(!error){
			var form = $(this);
			if(form.length){
				var action = form.attr('action');
				if(action == '') action = '/';
				var str = form.serialize();
				$.post(action, str+'&ajax=true', function(response){
					var params;
					//eval("params = " + response);
					params = response;
					if (params.success){
						$(".validateTipsTitle", form).hide();
						$(".validateTips",form).html('');
						// $('.form_content',form).hide();
						/*offset = form.offset();
						$(document).scrollTop(offset.top);*/
						if(params.text){
							$('.thanks', form).html(params.text);
						}
						if (form.hasClass('contacts')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Ваше сообщение отправлено</div><div class="message_thanks">Ожидайте ответ в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('comment')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за комментарий</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('report')){
							$.fancybox('<div class="thanks"><div class="OrderOuter pie"><div class="OrderInner pie"><div class="title_thanks">Спасибо за отзыв</div><div class="message_thanks">Он будет промодерирован в ближайшее время</div><div style="text-align:right"><a onclick="parent.$.fancybox.close(); return false;" href="#"><img class="close_thanks" alt="" src="/public/images/close.jpg"></a></div></div></div></div>', {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}else if(form.hasClass('coupon')){
							$.fancybox(params.coupon, {'scrolling' : 'no', 'padding' : '0','margin' : '0',});
						}
		
						setTimeout(function() {
							// parent.$.fancybox.close();
						}, 5000);
						
						// $('.thanks',form).show();
						if(params.callback){
							if (eval("typeof " + params.callback + " == 'function'")) {
								eval(params.callback + '();');
							}
						}
					}else{
						$(params.errors).each(function(ind, el){$("input[name='"+el+"']", form).addClass('error-ico');});
						$(".validateTipsTitle", form).show();
						$(".validateTips", form).html(params.errortext);
						
						if(params.text){
							$('.thanks', form).html(params.text);
						}
					}
				}, 'json');
			}
		}else{
			//$('#captcha').load('/registration.html?Event=ChangeCaptcha');
		}
		return false;
	});
	
	/*$("#subscribe").fancybox({
		'scrolling'			: 'no',
		'padding'			: '0',
		'margin'			: '0',
		'titleShow'			: false //,
		// 'showCloseButton' 	: false,
		// 'onClosed'			: function() {
			// $("#login_error").hide();
		// }
	});*/
	
	$('#shop_id').change(function(){
		// val = $(this).value();
		// val = $("#myselect option:selected").text();
		id = $(this).attr('id');
		val = $("#"+id+" option:selected").val();
		// alert(val);
		if(val == '0'){
			$('#other').show();
		}else{
			$('#other').hide();
		}
	});
		
});