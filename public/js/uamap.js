$(document).ready(function() {
(function(){
    // remove layerX and layerY
    var all = $.event.props,
        len = all.length,
        res = [];
    while (len--) {
      var el = all[len];
      if (el != 'layerX' && el != 'layerY') res.push(el);
    }
    $.event.props = res;
}());

	$(function(){
		$('.map').maphilight({
			fill: true,
			fillColor: 'ed1b26',
			fillOpacity: 1,
			stroke: true,
			strokeColor: '686868',
			strokeOpacity: 1,
			strokeWidth: 1,
			fade: true,
			alwaysOn: false,
			neverOn: false,
			groupBy: false,
			wrapClass: true,
			shadow: true,
			shadowX: 0,
			shadowY: 0,
			shadowRadius: 2,
			shadowColor: '686868',
			shadowOpacity: 1,
			shadowPosition: 'outside',
			shadowFrom: false
		});
	});

	/*$.fn.maphilight.defaults={
		fill: true,
		fillColor: 'ed1b26',
		fillOpacity: 1,
		stroke: true,
		strokeColor: '686868',
		strokeOpacity: 1,
		strokeWidth: 1,
		fade: true,
		alwaysOn: false,
		neverOn: false,
		groupBy: false,
		wrapClass: true,
		shadow: true,
		shadowX: 0,
		shadowY: 2,
		shadowRadius: 2,
		shadowColor: '686868',
		shadowOpacity: 1,
		shadowPosition: 'outside',
		shadowFrom: false
	}*/

/* ids #kr, #vn, #vl, #dp, #dn, #zt, #zk, #zp, #if, #ki, #kg, #lg, #lv, #nk, #od, #pl, #rv, #sm, #tp, #kh, #hr, #hm, #ck, #cn, #cv */

	$(function() {
	        $('#uacity area').mouseover(function(event) {
	            var tid = event.target.id+'d';
		    $('#' + tid).trigger('mouseover');
	       	});
	});

	$(function() {
	        $('#uacity area').mouseout(function(event) {
	            var tid = event.target.id+'d';
		    $('#' + tid).trigger('mouseout');
	       	});
	});

	$(function() {
	        $('#uacity area').click(function(event) {
	            var tid = event.target.id+'d';
	            var data = $("#" + tid).mouseout().data('maphilight') || {};
			data.alwaysOn = true;
		    $('#' + tid).parent().children().removeData('maphilight', data);
	       	});
	});

	$(function() {
	        $('#uacity area').click(function(event) {
		    event.preventDefault();
	            var tid = event.target.id+'d';
	            var data = $("#" + tid).mouseout().data('maphilight') || {};
			data.alwaysOn = true;
	            $('#' + tid).data('maphilight', data).trigger('alwaysOn.maphilight');
		    $('#' + tid).parent().children().removeData('maphilight', data);
	       	});
			
	        /*var data = $('#id_13d').data('maphilight') || {};
	        data.alwaysOn = true;
			$('#id_13d').data('maphilight', data).trigger('alwaysOn.maphilight');*/
	});

});                   	