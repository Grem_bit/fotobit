<form method="post" action="/order.html" id="OrderForm">
		{[_LNG_ORDER_PRODUCT]}
		<div class="" id="ajax_container_reserve">
		<div class="reg-fields" >
			<table cellspacing="0" cellpadding="0" class="input-form" width="100%">
				<tr>
					<td>Фамилия </td>
					<td>
						<input type="text" value="<?= $this->UserAccount['surname']?>" name="surname" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Имя* </td>
					<td>
						<input type="text" value="<?= $this->UserAccount['name']?>" name="name" class="text-field valid text ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Отчество </td>
					<td>
						<input type="text" value="<?= $this->UserAccount['patronymic']?>" name="patronymic" class="text-field  ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Телефон* </td>
					<td>
						<input type="text" value="<?= $this->UserAccount['phone']?>" name="phone" class="text-field valid text ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Email* </td>
					<td>
						<input type="text" value="<?= $this->UserAccount['email']?>" name="email" class="text-field valid email ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Адрес </td>
					<td>
						<textarea name="message" class="text-field textarea ui-widget-content ui-corner-all"><?= $this->UserAccount['adres']?></textarea>
					</td>
				</tr>
				<!--<tr>					
					<td>
						<table cellspacing="0" cellpadding="0" class="captcha"> 
							<tbody><tr>
								<td colspan="2">{[_ENTER_CODE]}</td>
							</tr>
							<tr>
								<td class="img"><img src="<?=$this->RootUrl?>3dcaptcha/index.php?<?= time()?>" alt=""></td>
								<td class="img"><input type="text" name="code" size="4" class="text-field valid " value="{[_CODE]}" onblur="set(this,'{[_CODE]}')" alt="{[_CODE]}" onclick="unset(this,'{[_CODE]}');"></td>								
							</tr>
						</tbody></table>
					</td>
				</tr>
			--></table>
		</div>
		
		</div>
		<input type="hidden" name="Event" value="MakeOrder"/>
	</form>