<?if($this->Products[items]){?>
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<th>Фото</th>
			<th>Артикул</th>
			<th>Наименование</th>
			<th>Цена</th>
			<th>Кол-во</th>
			<th>Добавить</th>
		</tr>
		<?$i=0;$cat=''; $count = sizeof((array)$this->Products[items]);
			foreach((array)$this->Products[items] as $prod){?>
		<? 
			if($cat!=$prod['category_id']){
				$this->ProductUrl = array();
				$this->getProdCategoryUrl($prod['category_id']);
				$this->Href = '/catalog/'.implode('/',$this->ProductUrl).'/';
				$cat = $prod['category_id'];
			}
			$prodHref = htmlentities($this->Href.$prod['url'], ENT_QUOTES, "UTF-8");
		?>
			<tr class="<?= $i%2 ? 'white':'grey'?>">
				<td class="<?= !$i ? 'topleft' : ($count-1 == $i && $count > 1? 'bottomleft' : 'left')?>">
					<?if($prod['filename']){?>
						<!--<a href="<?= $prodHref?>.html"><img class="pic" src="/thumb/small/products/<?= $prod['filename']?>" alt="" /></a>
						-->
						<a href="<?= $prodHref?>.html"><img class="pic" width="36px" src="<?= PATH_PUBLIC_FILES_IMAGES.'products/'.$prod['filename']?>" alt="" /></a>
					<?}else{?>
						<img class="pic" src="/thumb/small/no_img.jpg" alt="" />
					<?}?>
				</td>
				<td <?= $count-1 == $i && $count > 1 ? 'class="bottom"' : ''?>>
					<?= $prod['code']?>
				</td>
				<td <?= $count-1 == $i && $count > 1 ? 'class="bottom"' : ''?>>
					<a href="<?= $prodHref?>.html"><?= $prod['title']?></a>
				</td>
				<td <?= $count-1 == $i && $count > 1 ? 'class="bottom"' : ''?>>
					<p><?=$this->_show_price($prod['price'],$prod['currency_id'])?></p>
				</td>
				<td <?= $count-1 == $i && $count > 1 ? 'class="bottom"' : ''?>>
					<input type="text" name="<?= $prod[id]?>" value="1" class="cartamount">
				</td>
				<td class="<?= !$i ? 'topright' : ($count-1 == $i && $count > 1? 'bottomright' : 'right')?>">
					<a href="#" title="" class="totheorder" rel="<?= $prod['id']?>">в заказ</a>
				</td>
			</tr>
		<?$i++;}?>
	</table>
	<span class="links"><?$this->Products[pn]->Run()?></span>
<?}else{?>
	<p>Товаров не найдено</p>
<?}?>