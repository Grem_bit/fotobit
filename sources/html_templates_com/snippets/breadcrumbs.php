<?if($this->Breadcrumbs){?>
	<?
		// $this->Breadcrumbs = array_merge($this->Breadcrumbs, $this->Breadcrumbs, $this->Breadcrumbs);
		$last_crumb = array_pop($this->Breadcrumbs);
	?>
	<div class="breadcrumb">
		<a href="<?= $url = $this->LN_url?>" title="Главная">Главная</a> > 
		<?foreach($this->Breadcrumbs as $crumb){?>
			<a href="<?= $url = str_replace('.html', '/', $url).$crumb['url'].'.html'?>" title="<?= $crumb['title']?>"><?= $crumb['title']?></a> > 
		<?}?>
		<span><?= $last_crumb['title']?></span>
	</div>
<?}?>