	<div class="PageTitle">
		<ul class="breadcrumbs">
			<li><a href="/" title="" class="arrow">Главная</a></li>
			<li><p>Регистрация</p></li>
		</ul>
	</div>
	<div class="SingleText">
 		<form id="regform" action="/registration.html" method="post" class="validation-form">
			
			<div class="form_content" >
				<div class="validateTips" style="color:red"></div>
				<table class="input-form">
				<?/* <tr>
					<td>Логин*:</td>
					<td>
						<input type="text" value="<?= $_POST['login:str']?>" name="login:str" class="text-field valid text <?= $this->ErrorsUser['login'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all">
					</td>
				</tr>
				
				<tr>
					<td>Пароль*</td>
					<td>
					<input type="password" name="password:pass" value="<?= $_POST['password:pass'] ?>" id="password" class="text-field input valid <?= $this->ErrorsUser['password'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all"/><br/>
					</td>
				</tr>
				<tr>
					<td>Подтверждение пароля*:</td>
					<td>
						<input type="password" name="confirm" value="<?= $_POST['confirm'] ?>" id="confirm" class="text-field input valid <?= $this->ErrorsUser['confirm'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all"/><br/>
					</td>
				</tr> */?>
				<tr>
					<td>ФИО участника акции*</td>
					<td>
						<input type="text" value="<?= $_POST['fio:str']?>" name="fio:str" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr>
				<?/* <tr>
					<td>Фамилия</td>
					<td>
						<input type="text" value="<?= $_POST['surname:str']?>" name="surname:str" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Имя*</td>
					<td>
						<input type="text" value="<?= $_POST['name:str'] ?>" name="name:str" class="text-field valid text <?= $this->ErrorsUser['name'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Отчество</td>
					<td>
						<input type="text" value="<?= $_POST['patronymic:str']?>" name="patronymic:str" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr> */?>
				<tr>
					<td>Год рождения участника</td>
					<td>
						<input type="text" style="width: 100px; float: left;" id="datepicker" name="adr[<?=$pref?>][Дата доставки][]">
						<div style="clear: left;"></div>
					</td>
				</tr>
				<tr>
					<td>Дата приобретения товара*</td>
					<td>
						<input type="text" style="width: 100px; float: left;" id="datepicker" name="adr[<?=$pref?>][Дата доставки][]">
						<div style="clear: left;"></div>
					</td>
				</tr>
				<tr>
					<td>Наименования приобретенных товаров*</td>
					<td>
						<textarea style="width: 150px;" rows="" name="adr[<?=$pref?>][комментарий][]" cols=""></textarea>
					</td>
				</tr>
				<tr>
					<td>Серийный номер/-а приобретенного товара*</td>
					<td>
						<textarea style="width: 150px;" rows="" name="adr[<?=$pref?>][комментарий][]" cols=""></textarea>
					</td>
				</tr>
				<tr>
					<td>Место покупки (название торговой точки, указанное на чеке)*</td>
					<td>
						<select id="order_region<?= $pref?>" name="adr[<?=$pref?>][Регион][]">
							<option value="1">Торговая точка 1</option>
							<option value="2">Торговая точка 2</option>
							<option value="0">Другое</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>другое (hidden)</td>
					<td>
						<input type="text" value="<?= $_POST['fio:str']?>" name="fio:str" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Город*</td>
					<td>
						<input type="text" value="<?= $_POST['fio:str']?>" name="fio:str" class="text-field ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Мобильный телефон с указанием кода оператора</td>
					<td>
						<input type="text" value="<?= $_POST['phone:str'] ?>" name="phone:str" class="text-field valid text <?= $this->ErrorsUser['phone'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all">
					</td>
				</tr>
				<tr>
					<td>Адрес электронной почты (E-mail)*</td>
					<td>
						<input type="text" value="<?= $_POST['email:str'] ?>" name="email:str" class="text-field valid email <?= $this->ErrorsUser['email'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all">
					</td>
				</tr>
				
				<?/* <tr class="captcha">
					<td>Код*</td>
					<td>
						<table cellpadding="3" cellspacing="3">
							<tr>
								<td><span id="captcha" >
										<img alt="" class="captcha" src="<?=$this->RootUrl?>captcha/index.php" /><br /><br />
									</span>
								</td>
								<td>
									<input class="code valid <?= $this->ErrorsUser['captcha'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all" value="" style="width: 114px;" name="captcha" id="captcha" />
								</td>
							</tr>
						</table>					
					</td>
				</tr> */?>
				<tr>
					<td>Информационная рассылка</td>
					<td>
						<input type="checkbox" name="subscribe:int" style="width: auto; height: auto;" id="subscribe" value="1" <?= $_POST['subscribe:int'] ? 'checked="checked"' : '' ?> href="http://yandex.ru"/>
					</td>
				</tr>
				<?/* <tr>
					<td></td>
					<td><input type="image" name="reg" id="reg" style="padding: 4px; height: auto;" value="Регистрация" src="<?=$this->RootUrl?>public/images/registration.jpg" /></td>
				</tr> */?>
				<tr>
					<td></td>
					<td><input type="submit" name="reg" id="reg" style="padding: 4px; height: auto;" value="Регистрация" src="<?=$this->RootUrl?>public/images/registration.jpg" /></td>
				</tr>
				</table>
				
				<input type="hidden" name="Event" value="UserRegister" />
			</div>
			<p class="thanks" style="display:none"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>{[REGISTER_SUCCESS_MESSAGE]}</p>
		</form>
	</div>