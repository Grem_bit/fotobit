<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<title><?= $this->Title?></title>
	<meta name="description" content="<?= $this->MetaDescription ?>" />
	<meta name="keywords" content="<?= $this->MetaKeywords ?>" />
	
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400' rel='stylesheet' type='text/css'/>

	<link href="<?= $this->RootUrl?>public_com/style/jquery.fancybox.css" type="text/css" rel="stylesheet"/>
	<link href="<?= $this->RootUrl?>public_com/style/main.css" type="text/css" rel="stylesheet"/>
	<!--[if gte IE 9]>
		<style type="text/css">
			*{filter: none !important;}
		</style>
	<![endif]-->

	<script type="text/javascript" src="<?= $this->RootUrl?>public_com/java/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?= $this->RootUrl?>public_com/java/jquery.fancybox.pack.js"></script>
 	<script type="text/javascript" src="<?= $this->RootUrl?>public_com/java/main.js"></script>
	<?foreach($this->CSS as $path){?>
		<link rel="stylesheet" type="text/css" href="<?= $path ?>" media="screen" />
	<?}?>
		
	<?foreach($this->JS as $path){?>
		<script type="text/javascript" src="<?= $path ?>"></script>
	<?}?>
	<? /*<link rel="icon" type="image/vnd.microsoft.icon" href="<?= $this->RootUrl?>favicon.ico"> */?>
	<link href="/favicon.png" type="image/png" rel="icon"/>
</head>
<!-- body -->
<body>
<?/*<div style="background: #333;"><h2 style="font-size: 18px;text-align: center;margin: 0px; padding: 5px;color:#FFF;">Внимание! Заказы принятые с 30.09 по 07.10 будут обработаны 06-07.10, доставлены 08.10.</h2></div>
*/?><div class="wrap">
<!-- header -->
	<div class="header">
		<div id="g_plus">
		<!-- Поместите этот тег туда, где должна отображаться кнопка +1. -->
		<div class="g-plusone" data-annotation="bubble" data-href="http://www.foto-servis.com.ua"></div>
		
		<!-- Поместите этот тег за последним тегом виджета кнопка +1. -->
		<script type="text/javascript">
		  window.___gcfg = {lang: 'ru'};
		
		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/platform.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
		</div>
		<div class="logo left">
			
			<div class="name"><?if($this->FrontPage){?>
				{[LOGO]}
			<?}else{?>
				<a href="<?=$this->RootUrl?>" title="">{[LOGO]}</a>
			<?}?></div>
			<div class="desc">{[LOGO_SUB]}</div>
		</div>
		<div class="phone left">
			<div class="regular"><img src="<?= $this->RootUrl?>public_com/img/phone.png" alt=""/>{[PHONE]}<span class="small">{[WORK]}</span></div>
			<ul class="mobile">
				<li>{[PHONE1]}</li>
				<li>{[PHONE2]}</li>
				<li>{[PHONE3]}</li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="signin link right">
			<div class="clear"></div>
			<?if($_SESSION['user_account']){?>
				<?if(!$this->FrontPage){?>			
					<?if($this->UrlParts[1] == 'orders') {?>Мои заказы<?}else{?><a href="/user-account/orders.html">Мои заказы</a><?}?><span>|</span><?if($this->UrlParts[1] == 'data') {?>Личные данные<?}else{?><a href="/user-account/data.html">Личные данные</a><?}?><span>|</span><a href="/user-account.html?Event=Logout&redirect=/">Выход</a>					
				<?}else{?>
					<a href="/user-account/orders.html">Мои заказы</a><span>|</span><a class="active" href="/user-account/data.html">Личные данные</a><span>|</span><a href="/user-account.html?Event=Logout&redirect=/">Выход</a>
				<?}?>
			<?}else{?>
			<img src="<?= $this->RootUrl?>public_com/img/login.png" alt=""/><a href="#login-form" title="" rel="fb">вход</a>
			<span>|</span>
			<img src="<?= $this->RootUrl?>public_com/img/register.png" alt=""/><a href="#reg-form" title="" rel="fb">регистрация</a>
			<?}?>
			
		</div>
		<div class="clear"></div>
	</div>                 
	
<!-- /header -->



<!-- content -->
	<div class="content <?=!$this->FrontPage ? 'text':''?>">
		<div class="sidebar left">
			<?
				$list = $this->_load_content_translation('`pages`', 't.`footer_menu` = \'1\' AND t.`parent_id` = 0', 't.`order_by`', ', tr.`title`, t.`url`', '', '', 0);
				
			?>
			<?if($list){?>
				
			<ul class="nav link">
				<?foreach($list as $item){?>
					<li <?=$this->UrlParts[0] == $item['url'] ? 'class="active"':''?>><a href="/<?= $item['url']?>.html" title="<?//= $item['title']?>"><?= $item['title']?></a>
					<?
						$listSub = $this->_load_content_translation('`pages`', 't.`footer_menu` = \'1\' AND t.`parent_id` = \''.$item['id'].'\'', 't.`order_by`', ', tr.`title`, t.`url`', '', '', 0);
						
					?>
					<?if($listSub){?>
					<ul class="sub">
						<?foreach($listSub as $item1){?>
						<li <?=$this->UrlParts[0] == $item['url'] && $this->UrlParts[1] == $item1['url'] ? 'class="active"':''?>>
							<a href="/<?= $item['url']?>/<?=$item1['url']?>.html" title="<?//= $item1['title']?>"><?= $item1['title']?></a>
						</li>
						<?}?>
					</ul>
					<?}?>
					</li>
				<?}?>
				
				
			</ul>
			
			<?}?>
			<?if(!$this->FrontPage){?>
				<?include PATH_SNIPPETS.'button-block.php'?>
				
			<?}?>
		</div>
		
	