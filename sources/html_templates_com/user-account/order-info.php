<?
	// _debug($_SESSION, 0);
	// _debug($this->Order, 1);
	// _debug($this->Products, 1);
	// _debug($this->Chars, 1);
?>
<?/* 
<h1>Оформление заказа</h1>
<?foreach($this->Products as $foto){?>
	<li><?= $foto['img']?></li>
<?}?>   
 */?>

<h2><strong>Шаг 3 из 3:</strong> Данные о заказе печати фото</h2><a class="back" href="/user-account/orders.html" title="Вернуться к заказам">Вернуться к заказам</a>
<p>Если какой-либо параметр вы хотите поменять - вы можете нажать "Вернуться к заказам" и отредактировать таблицу со списком фотографий.
</p>
<!--<div class="clear"></div> -->

<div class="ProducList" id="my_order">
	<? 
		
	/* include PATH_SNIPPETS.'order-cart-list.php' */ ?>
	
	<?//if($this->Order['state'] == 'not_order'){ ?>
		<?
			$string = $this->_get_system_variable('delivery_methods');
			$delivery_methods = explodeMultiString($string);
			$string = $this->_get_system_variable('payment_methods');
			$payment_methods = explodeMultiString($string);
			$payment_method = $payment_methods[$this->Order['payment_way']]['item'];
			$payment_method_low = mb_convert_case($payment_method, MB_CASE_LOWER, "UTF-8");
			
		?>
		<?
			$ik_shop_id = $this->_get_system_variable('ik_shop_id');
			// $ik_form_action = '/interkassa/'.$this->_get_system_variable('ik_status_url').'?debug_pay=1&ik_sign_hash=1';
			// $ik_form_action = $this->_get_system_variable('ik_form_action');
			// $ik_form_action = '/?Event=OrderPhoto';
			$ik_form_action = '/order.html?Event=Order';
			if (preg_match('/интеркасса/', $payment_method_low) || preg_match('/interkassa/', $payment_method_low)){
				$ik_form_action = $this->_get_system_variable('ik_form_action');
			}else{
				$ik_form_action = '/order.html?Event=Order';
			}
			$ik_payment_amount = $_SESSION['Trash']['Sum'];
			$ik_payment_id = $_SESSION['Cur_order'];
			$site_name = $this->_get_lang_constant('SITE_NAME');
			$ik_payment_desc = 'Оплата заказа № '.$_SESSION['Cur_order'].' на сайте '.$site_name;
			
		?>
		<a name="payment"></a>
		<form id="ik_payment" name="payment" action="<?= $ik_form_action?>" method="post" enctype="application/x-www-form-urlencoded" accept-charset="cp1251">
			<input type="hidden" name="ik_shop_id" value="<?= $ik_shop_id?>" />
			<input type="hidden" name="ik_payment_amount" value="<?= $ik_payment_amount?>" />
			<input type="hidden" name="ik_payment_id" value="<?= $ik_payment_id?>" />
			<input type="hidden" name="ik_payment_desc" value="<?= $ik_payment_desc?>" />
			<input type="hidden" name="ik_paysystem_alias" value="" />
			<?//_debug($this->Order);?>
			<?//_debug($delivery_methods);?>
			<? $newpost_kount=false;
				for($i=0;$i<count($delivery_methods);$i++){
					if($delivery_methods[$i]['options'][0]=='newpost'){
						$newpost_kount=$i;
					}
				}?>
			<table class="order-info">
				<tr>
					<td>Общая стоимость:</td>
					<td><?= $this->Order['total']?> грн. <?
					//var_dump($this->Order);
					if (isset($this->Order['courieradd']) && $this->Order['courieradd']) {echo '+'.$this->_get_system_variable('sum_courier').'грн'; $_SESSION['Trash']['dopol']=$this->_get_system_variable('sum_courier');} elseif ($this->Order['delivery_way']==1 && $this->Order['total'] < $this->_get_system_variable('min_sum_courier')) { echo '+ стоимоcть доставки курьерской cлужбой " Новая Почта" (цену смотрите на сайте "Новая Почта")'; $_SESSION['Trash']['dopol']=' стоимоcть доставки курьерской cлужбой " Новая Почта" '; } ?></td>
				</tr>
				<tr>
					<td>Кол-во фотографий:</td>
					<td><?= $this->Order['qty']?> шт.</td>
				</tr>
				<?if($this->Order['delivery_way']==$newpost_kount){?>
				<tr>
					<td>Способ доставки:</td>
					<td><?= $delivery_methods[$this->Order['delivery_way']]['item'].' ( '.( $this->Order['delivery_option'] ).' ) '?></td>
				</tr>
				<?}else{?>
				<tr>
					<td>Способ доставки:</td>
					<td><?= $delivery_methods[$this->Order['delivery_way']]['item'].($this->Order['delivery_option'] ? ' ('.$delivery_methods[$this->Order['delivery_way']]['options'][$this->Order['delivery_option']].')':'')?><?= $this->Order['address2'] ? ' ('.$this->Order['address2'].')' : ''?></td>
				</tr>
				<?}?>
				<tr>
					<td>Способ оплаты:</td>
					<td><?= $payment_methods[$this->Order['payment_way']]['item']?></td>
				</tr>
				<?if($this->Order['comment']){?>
					<tr>
						<td>Комментарий к заказу:</td>
						<td><?= $this->Order['comment']?></td>
					</tr>
				<?}?>
			</table>
			<?if($this->Order['state'] == 'not_order' && ($this->Order['total'] >= $this->_get_system_variable('order_min_amount'))){?>
				<input type="submit" name="process" value="Подтвердить заказ" class="qq-order-button" />  <strong>Шаг 3 из 3</strong> После подтверждения заказа - менеджер по печати фото получит уведомление о заказе. После чего он свяжется с вами!
			<?}?>
			<input type="hidden" name="DeliveryMethod" value="<?= $this->Order['delivery_way']?>" />
			<input type="hidden" name="DeliveryOption" value="<?= $this->Order['delivery_option']?>" />
			<input type="hidden" name="PaymentMethod" value="<?= $this->Order['payment_way']?>" />
		</form>
		<?if($this->Order['state'] == 'not_order'){?>
			<a class="qq-order-button" href="/user-account/orders/<?= $this->Order['id']?>.html">Вернуться к оформлению</a>
		<?}?>
	<?//}?>
</div>

<div style="margin-top: 20px;">
	<a class="back" href="/user-account/orders.html" title="Вернуться к заказам">Вернуться к заказам</a>
</div>
<div class="clear"></div>
<?/* 
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		$('.ph-count').keyup(function() {
			var pid = $(this).attr('rel');
			var count = $(this).val();

			$.ajax({
				url: "?Event=ChangeCount",
				data: { pid: pid, count: count },
				dataType:  'json'
			}).done(function(data) {
				if(data) {
					$('#ph-price-'+pid).html(data.photo_price);
					$('#sum-prices').html(data.sum_photo_price);
					// paymentFormRefresh(data.sum_photo_price);
					$("input[name='ik_payment_amount']").val(data.sum_photo_price);
				}
			});
		});
		
		$('#ik_payment input[name="DeliveryMethod"]').change(function(){
			$('#ik_payment select').attr('disabled', 'disabled');
			$('#ik_payment input[type=text]').attr('disabled', 'disabled');
			
			$(this).parent('label').next('select').removeAttr('disabled');
			$(this).parent('label').next('div').children('input').removeAttr('disabled');
		});
		
		$('#ik_payment input[name="PaymentMethod"]').change(function(){
			text = $(this).parent('label').text();
			textLow = text.toLowerCase();
			if((/interkassa/.test(textLow)) || (/интеркасса/.test(textLow))){
				$('form#ik_payment').attr('action', '<?= $this->_get_system_variable('ik_form_action')?>');
			}else{
				$('form#ik_payment').attr('action', '<?= $ik_form_action?>');
			}
		});
		
		$('#ik_payment input, #ik_payment select, #ik_payment textarea').change(function() {
			
			var pid = $('input[name="ik_payment_id"]').val();
			$.ajax({
				url: "?Event=SaveDelivPay&"+$('#ik_payment').serialize(),
				dataType:  'json'
			});
		});
		
		$('#ik_payment').submit(function(event){
			// event.preventDefault();
			var errors = new Array();
			if (
					$(this).find("input[name='DeliveryMethod']:radio:checked").length == 0
					||
					$(this).find("select:enabled").val() == 0
				) {
				// errors['DeliveryMethod'] = 'Выберите способ доставки';
				errors[0] = 'Выберите способ доставки';
			}
			if ($(this).find("input[name='PaymentMethod']:radio:checked").length == 0) {
				// errors['PaymentMethod'] = 'Выберите способ оплаты';
				errors[1] = 'Выберите способ оплаты';
			}
			// console.log(errors);
			// console.log(errors.length);
			var message = '';
			if(errors.length > 0){
				// console.log(errors.length);
				errors.forEach(function(val) {
					// message += '<p>'+val+'</p>';
					message += val+'\n';
				});
				
				alert(message);
				return false;
			}
			// return false;
		});
		
	});
</script>
*/?>