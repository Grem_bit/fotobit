<?if($this->Products[items]){?>
	<table cellpadding="0" cellspacing="0">
		<colgroup>
			<col width="8%" />
			<col width="10%" />
			<col width="50%" />
			<col width="15%" />
			<col width="1%" />
			<col width="15%" />
			<col width="1%" />
		</colgroup>
		<?$i=0;$cat=''; foreach((array)$this->Products[items] as $prod){?>
		<? 
			if($cat!=$prod['category_id']){
				$this->ProductUrl = array();
				$this->getProdCategoryUrl($prod['category_id']);
				$this->Href = '/catalog/'.implode('/',$this->ProductUrl).'/';
				$cat = $prod['category_id'];
			}
		?>
			<tr class="<?= $i%2 ? 'white':'grey'?>">
				<td class="<?= !$i ? 'topleft' : ($this->Products[cnt]-1 == $i && $this->Products[cnt] > 1? 'bottomleft' : 'left')?>">
					<?if($prod['filename']){?>
						<a href="<?= $this->Href.$prod['url']?>.html">
							<!--<img class="pic" src="<?=$this->RootUrl?>thumb/small/products/<?= $prod['filename']?>" alt="" />-->
							<img class="pic" width="36" src="<?=$this->RootUrl?><?= PATH_PUBLIC_FILES_IMAGES.'products/'.$prod['filename']?>" alt="" />
						</a>
					<?}else{?>
						<img class="pic" src="<?=$this->RootUrl?>thumb/small/no_img.jpg" alt="" />
					<?}?>
				</td>
				<td <?= $this->Products[cnt]-1 == $i && $this->Products[cnt] > 1 ? 'class="bottom"' : ''?>>
					<?= $prod['code']?>
				</td>
				<td <?= $this->Products[cnt]-1 == $i && $this->Products[cnt] > 1 ? 'class="bottom"' : ''?>>
					<a href="<?= $this->Href.$prod['url']?>.html"><?= $prod['title']?></a>
				</td>
				<td <?= $this->Products[cnt]-1 == $i && $this->Products[cnt] > 1 ? 'class="bottom"' : ''?>>
					<div class="OrangePriceLeft"></div>
					<div class="OrangePriceMiddle">
					<p><?=$this->_show_price(money(floatval($this->Currencies[$this->Currency][course])*$prod[price]))?></p>
					</div>
					<div class="OrangePriceRight"></div>
				</td>
				<td <?= $this->Products[cnt]-1 == $i && $this->Products[cnt] > 1 ? 'class="bottom"' : ''?>>
					<input type="text" name="<?= $prod[id]?>" value="<?= $prod[count]?>" class="cartamount">
				</td>
				<td <?= $this->Products[cnt]-1 == $i && $this->Products[cnt] > 1 ? 'class="bottom"' : ''?>>
					<p class="cartprice"><?=$this->_show_price(money(floatval($this->Currencies[$this->Currency][course])*$prod[price]*$prod[count]))?></p>
				</td>
				<td class="<?= !$i ? 'topright' : ($this->Products[cnt]-1 == $i && $this->Products[cnt] > 1? 'bottomright' : 'right')?>">
					<a rel="<?= $prod[id]?>" class="del_cart" href="#"><img alt="" src="<?=$this->RootUrl?>public/images/cart_delete.gif"></a>
				</td>
			</tr>
		<?$i++;}?>
	</table>
	<div class="cartline"></div>
	<a href="javascript:PopupForm.show('GetForm','Оформить заказ',0);" title=""><img src="<?=$this->RootUrl?>public/images/oformit.jpg" alt="" /></a>
	<div class="carttotal">
		<p>всего: <span><?= $this->_show_price(money(floatval($this->Currencies[$this->Currency][course])*$_SESSION['Trash']['sum']))?></span></p>
	</div>
	<?if($_SESSION['Trash']['discount']){?>
		<div class="carttotal">
			<p>скидка: <span><?= $this->_show_price(money(floatval($this->Currencies[$this->Currency][course])*$_SESSION['Trash']['discount']))?></span></p>
		</div>
	<?}?>
<?}else{?>
	<p>Корзина пуста</p>
<?}?>