<?

	class ForgotPassword extends ContentPage {
		
		/*
		 * Protected Properties
		 */
		
		protected $TemplatesBaseDir		= 'forgot-password/';
		protected $Success				= '';
		protected $RecSubject			= 'Восстановление пароля';
		protected $RecMail				= 'noreply@localhost';
		protected $RecPeriod			= 86400;	//24 hours
		
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			global $config;
			$this->AddTitle('Восстановление пароля');
			$this->Breadcrumbs[] = array('url'=>'forgot-password','title' => 'Восстановление пароля');
			$this->SetTemplate('main.html');
		}
		
		public function OnDefault() {
			
			if($this->UserAccount) {
				$this->GoTo($this->RootUrl.'account.html');
			}
		}
		
		public function OnRecover() {
			if($this->_check_email($_POST['email'])) {
				$this->_send_recover_link($_POST['email']);
			}
		}
		
		public function OnPasswordRecovery(){
			
			if($this->_check_link($_GET['data'])) {
				$this->SetTemplate('recovery.html');
			}
			else {
				$this->Error = 'Некорректная ссылка для восстановления';
				$this->SetTemplate('msg.html');
			}
		}
		
		public function OnChangePassword() {
			
			if($this->_check_link($_GET['data'])) {
				
				if($this->_check_password($_POST)) {
					
					$data = $this->DecodeUrlParam($_GET['data']);
					$this->_update_password($_POST['password'], $data);
				}
				else {
					
					$this->SetTemplate('recovery.html');
					return;
				}
				
				$this->Success = 'Пароль изменен успешно';
				$this->SetTemplate('msg.html');
			}
			else {
				
				$this->Error = 'Некорректная ссылка для восстановления';
				$this->SetTemplate('msg.html');
			}
		}
		
		
		/*
		 * Protected methods
		 */
		
		protected function _update_password($password, $data) {
			
			global $DB;
			
			$sql = 'UPDATE users 
					SET password=\''.$this->CryptPassword($password).'\' 
					WHERE id=\''.$data['id'].'\'';
			$DB->Execute($sql);
			
			//Execute('update phpbb_users set user_password = \''.md5($password).'\' where username = \''.s($data['email']).'\'');
			
			$sql = 'SELECT name 
					FROM users 
					WHERE id=\''.$data['id'].'\'';
			
			$user = $DB->GetRow($sql);
			
			AttachLib('Mailer');
			
			$m = new Mailer();
			$m->AddRecepient($data['email']);
			
			$variables = array('password' => $password, 
							   'name' => $user['name'], 
							   'title' => 'Пароль изменён');
			$m->AddVars($variables);
			
			$m->Send('change-password.html', 'Пароль изменён', $this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name'));
			
		}
		
		protected function _check_password($data) {
			
			if(!$data['password']) {
				
				$this->Error = 'Введите новый пароль';
				return false;
			}
			if(strlen($data['password'])<6) {
				
				$this->Error = 'пароль должен быть не короче 6 символов';
				return false;
			}
			
			if($data['password'] != $data['cpassword']) {
				
				$this->Error = 'Пароли не совпадают';
				return false;
			}
			
			return true;
		}
		
		protected function _check_link($data) {
			
			global $DB;
			
			$data = $this->DecodeUrlParam($_GET['data']);
			$sql = 'SELECT COUNT(*) AS cnt 
					FROM users 
					WHERE id			= \''.$data['id'].'\' AND 
						  email			= \''.$data['email'].'\' AND 
						  pass_rec_code	= \''.$data['secret_code'].'\' AND 
						  pass_rec_ts	> \''.(time() - $this->RecPeriod).'\'';
			$res = $DB->GetRow($sql);
		
			if($res['cnt'] > 0) {
				
				return true;
			}
			
			return false;
		}
		
		protected function _send_recover_link($email) {
			
			global $DB;
			

			$sql = 'SELECT id, 
						   name
					FROM users 
					WHERE email =\''.$email.'\'';
			
			$buyer = $DB->GetRow($sql);
			
			$data['email'] = $email;
			$data['id'] = $buyer['id'];
			$data['secret_code'] = $this->GenRandomString(64);
			
			$sql = 'UPDATE users 
					SET pass_rec_code=\''.$data['secret_code'].'\', 
						pass_rec_ts=UNIX_TIMESTAMP() 
					WHERE id = \''.$buyer['id'].'\'';
			$DB->Execute($sql);
			
			$link = $this->RootUrl.'forgot-password.html?Event=PasswordRecovery&data='.$this->EncodeUrlParam($data);
			AttachLib('Mailer');
			
			$m = new Mailer();
			$m->AddRecepient($email);
			/*echo mb_detect_encoding($buyer['firstname']);
			exit;*/
			$variables = array('link' => $link, 
							   'name' => $buyer['name'], 
							   'title' => $this->RecSubject);
			$m->AddVars($variables);
			$m->Send('password-recovery.html', $this->RecSubject, $this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name'));
			
			if($_REQUEST['ajax']){
				echo json_encode(array('success' => true,
										'text' => '<strong style="color:#999">Вам на почту было выслано письмо со ссылкой.<br />
										Перейдя по ней, Вы сможете задать новый пароль.</strong>'
									));
				exit;
			}
		}
		
		protected function _check_email($email) {
			if(!$email || $email == 'Email'){
				$this->Error = 'Введите email!';
				return false;
			}
			
			$sql = 'SELECT COUNT(*) AS cnt FROM users WHERE email=\''.$email.'\'';
			$res = GetRow($sql);
			
			if($res['cnt'] > 0) {
				
				$this->Success = '<strong style="color:#999">Вам на почту была выслана<br /> инструкция по дальнейшему восстановлению</strong>';
				
				return true;
			}
			
			
			echo json_encode(array('success' => false,
						'text' => 'Такой Email не зарегистрирован'
					));
			exit;
			
		}
	}

?>