<?
	//ini_set('display_errors',1);
	 //error_reporting(E_ALL);

	class Catalog extends ContentPage {

		protected	$TemplatesBaseDir		= 'catalog/';
		protected	$Catalog				= array();
		protected	$Category				= array();
		protected	$Product				= array();
		private		$Ids					= array();
		protected	$Href					= 'catalog/';
		public 		$Maker					= '';
		public 		$TableFields			= array();
		
		
		public function OnCreate() {
			/*$title = 'Каталог';			
			if($this->LN) $title = 'Catalog';
			$this->Breadcrumbs[] = array('url'=>'catalog','title' => $title);*/
			$UrlParts_sz = sizeof($this->UrlParts); 
			$this->IsCat = true;
			switch($UrlParts_sz){
				case 1:

						$this->Catalog = $this->_load_catalog(0);
						$this->SetTemplate('catalog-front.php');

				break;
				
				default:
					
					if($this->UrlParts[1] == 'special'){
						$this->_get_products(' AND t.specials = 1');												
						$this->Breadcrumbs[] = array('title' => 'Специальное предложение');
						$this->AddTitle('Распродажа');
						$this->SetTemplate('products.php');
					}else{
						
						$this->Category = $this->_load_content_info('categories','','publish = 1 AND url=\''.s(end($this->UrlParts)).'\'');
						
						if($this->Category){ // категория
							$category = $this->_load_content('categories','publish = 1 AND parent_id = '.i($this->Category['id']).' ');
							
							if(is_for($category)){ // есть подкатегории
								
								$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->_breadcrumbs($this->Category['id'],'categories','catalog'));
								$this->Catalog = $this->_load_catalog($this->Category['id']);
								$this->SetTemplate('catalog.php');
							}else{ 			// конечная, выводим продукты
								
								$this->SetTemplate('products.php');
								//$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->_breadcrumbs($this->Category['id'],'categories','catalog'));
								$this->NEWPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.product_top = 1 AND
																			 t.category_id =  \''.$this->Category['id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
								$this->MONTHPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.month = 1 AND
																			 t.category_id =  \''.$this->Category['id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
								
								$this->ShowCategory();			
											
							}
						}else{ // продукт
								$this->Product = $this->_load_content_info('products t',
																			'LEFT JOIN categories c ON c.id = t.category_id AND c.publish = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																			 LEFT JOIN product_status ps ON ps.id = t.status_id
																			 LEFT JOIN video p2 ON p2.product_id = t.id AND p2.default = 1'
																			, 
																			't.publish = 1 AND t.url =\''.s(end($this->UrlParts)).'\' ',
																			't.*, IF(t.url != \'\',t.url,t.id) as url, ps.img as status, c.title as category, p.filename, pt.* , p2.title as videos
																			 '
																			);
								// _debug($this->Product, 1);
								if($this->Product){
									
									$this->NEWPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.product_top = 1 AND
																			 t.id <> \''.$this->Product['id'].'\' AND 
																			 t.category_id =  \''.$this->Product['category_id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
									$this->RECPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.id = \''.$this->Product['rec'].'\' ',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
																			
									$this->RANDOMPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			',  
																			't.publish = 1  AND t.id <> \''.$this->Product['id'].'\' AND 
																			 t.category_id =  \''.$this->Product['category_id'].'\' ORDER BY RAND()',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title 
																			 '
																			);
									
									//_debug($this->RECPRODUCT,1);
									$this->AddJS('/public/js/fancybox/jquery.mousewheel-3.0.4.pack.js');
									$this->AddJS('/public/js/fancybox/jquery.fancybox-1.3.4.pack.js');
									$this->AddCSS('/public/js/fancybox/jquery.fancybox-1.3.4.css');
									$this->AddJS('/public/js/jquery.jcarousel.min.js');
									$this->AddJS('/public/js/jquery.slider-1.5.js');
									$this->AddJS('/public/js/FlyUpSlider.js');
									$this->ProductUrl = array();
									$this->getProdCategoryUrl($this->Product['category_id'],1);
									// _debug($this->ProductUrl, 1);
									// $this->ProductUrl = array_merge($this->ProductUrl, $this->ProductUrl);
									// _debug($this->ProductUrl, 0);
									foreach($this->ProductUrl as $url){
										$this->Href .= $url['url'].'/';
										// $addedCatalog[] = array('url'=>'catalog/'.$url['url'], 'title'=>$url['title']);
									}
									$addedCatalog = $this->ProductUrl;
									$addedCatalog[0]['url'] = 'catalog/'.$addedCatalog[0]['url'];
									// _debug($addedCatalog, 1);
									
									// $this->Breadcrumbs = array_merge($this->Breadcrumbs, $this->ProductUrl);
									$this->Breadcrumbs = array_merge($this->Breadcrumbs, $addedCatalog);
									$this->Breadcrumbs[] = array('title'=>$this->Product['title']);
									
									$this->AddTitle($this->Product['title']);
									$this->SetMetaKeywords($this->Product['meta_keywords']);
									$this->SetMetaDescription($this->Product['meta_description']);
									$this->Product['photos'] = $this->_load_content('photos','product_id = '.$this->Product['id'].' AND plan = 0','`default` DESC');
									$this->Product['gallery'] = $this->_load_content('photos','product_id = '.$this->Product['id'].' AND plan = 1','`default` DESC');
									$this->Product['slider'] = $this->_load_content('photos','product_id = '.$this->Product['obj_id'].' AND slider = 1','`order_by');
									$this->Product['features'] = $this->_load_content('photos','product_id = '.$this->Product['obj_id'].' AND features = 1','`default` DESC');
									$this->Product['videos'] = $this->_load_content('video','product_id = '.$this->Product['id'],'`default` DESC');
									// _debug($this->Product, 1);
									$this->get_prevnext_products($this->Product['obj_id'], $this->Product['category_id']);
									$this->SetTemplate('product.php');

									
								}else{
									$this->GoTo404();
								}
						}
					}
			}
		}
		
		private function _get_category_ids($id){
			$sql = 'SELECT id FROM categories WHERE publish = 1 AND parent_id = '.$id;
			$res = GetCol($sql);
			if($res){
				$this->Ids = array_merge($this->Ids,$res);
				foreach($res as $val){
					$this->_get_category_ids($val);
				}
			}
		}
		
		public function _load_catalog($parent_id){
			 $sql = 'SELECT *, `'.$this->LN.'title` as title,  `'.$this->LN.'content` as content FROM categories WHERE publish = 1 AND parent_id = '.i($parent_id).' ORDER BY order_by,  `'.$this->LN.'title` ';
			return GetAll($sql);
		}
		
	
		protected function ShowCategory(){

			$this->ProductUrl = array();
			$this->getProdCategoryUrl($this->Category['id'], 1);
			foreach($this->ProductUrl as $url){
				$this->Href .= $url['url'].'/';
			}
			$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->ProductUrl);
			
			$this->AddTitle($this->Category['title']);
			$this->Catalog = $this->_load_catalog($this->Category['id']);
			if($this->Catalog){
				$this->SetTemplate('catalog.php'); 
			}else{
				$this->_get_category_ids($this->Category['id']);
				$this->Ids[] = $this->Category['id'];
				$this->_load_page_info();
				
				$this->_get_products('AND t.category_id IN ('.implode(',',$this->Ids).')');
				$this->SetTemplate('products.php');
			}
		}
		public function _load_page_info() {

			global $DB;
			
			if($this->UrlParts[1] == 'page'){
				
				$this->Page = intval($this->UrlParts[2]);
				$sql = 'SELECT pt.*, p.* 
						FROM pages p 
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
						WHERE 
							  p.publish=\'1\' AND 
							  p.url=\''.$this->UrlParts[0].'\'';
				
				$this->PageInfo = $DB->GetRow($sql);
			
			}else{
			
				$is_front_page = '';
				global $PARENT_ID,$server;
				
				if($PARENT_ID && $this->UrlParts[0] == 'index' ){
					$this->UrlParts[0] = 'chejazz';
				}else{				
					if(!end($this->UrlParts)) $this->UrlParts[0] = 'index';
				}
				
				$sql = 'SELECT pt.*, p.* 
						FROM pages p 
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
						WHERE 
							  p.publish=\'1\' AND 
							  p.url=\'catalog/'.end($this->UrlParts).'\'';
				
				$this->PageInfo = $DB->GetRow($sql);
				//print_r($this->PageInfo);
				/*if(!$this->PageInfo['title']){
					$this->PageInfo = $this->_load_custom_translation('pages', 'pages_translation', $this->PageInfo['id']);	
				}*/
			}
		}
		public function _get_other_products($where ='', $limit = '', $order_by=' RAND() '){
			$lim = ($limit) ? (' LIMIT '.$limit) : '';	
		
			$sql ="SELECT  t.* , IF(t.url != '',t.url,t.id) as url, c.title as category, p.filename,
															   IF(t.price_old !='', t.price_old, t.price) as real_price,
															   pg.title as mtitle 	
					FROM products t 
						INNER JOIN categories c ON c.id = t.category_id 
															 LEFT JOIN photos p ON p.product_id = t.id 
															 LEFT JOIN course cr ON t.currency_id = cr.id
															 LEFT JOIN pages pg ON t.maker_id = pg.id
															 
					 WHERE t.publish = 1 ".$where."
					
					 ORDER BY ".$order_by.$lim;
			//_debug($sql);
			return GetAll($sql);
		}
		
		public function _get_more_info($id){
			$product = GetRow("SELECT * FROM `products` WHERE `id` = '".$id."' ");
			//_debug($this->TableFields);
			if($product){
				$info = '';
			foreach ($this->TableFields as $key => $fl){
												if($product[$fl] && $key != 'описание' && $key != 'артикул' && $key != 'цена'){
														$info .= $key .' - '. $product[$fl].' ';
													 }
												}			
			}
			return $info;
		}
			
		public function get_prevnext_products ($id, $category_id){
			$sql = "SELECT pr.url AS 'product_url', cat.url AS 'category_url' 
					FROM products pr 
					LEFT JOIN `categories` cat ON (cat.`id` = pr.`category_id`) 
					WHERE pr.publish = '1' AND pr.category_id = '$category_id' AND pr.id < '$id' 
					ORDER BY pr.id DESC LIMIT 1
					";
			$this->prev_product = GetRow($sql);
			
			$sql = "SELECT pr.url AS 'product_url', cat.url AS 'category_url' 
					FROM products pr 
					LEFT JOIN `categories` cat ON (cat.`id` = pr.`category_id`) 
					WHERE pr.publish = '1' AND pr.category_id = '$category_id' AND pr.id > '$id' 
					ORDER BY pr.id ASC LIMIT 1
					";
			$this->next_product = GetRow($sql);
		}
	}

?>