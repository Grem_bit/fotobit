<?

	class Search extends ContentPage {
		
		public $Products = array();
		
		public function OnCreate() {
					
			$this->SetTitle('Поиск');
			//$this->_get_simple_result();
			// $this->AddJS('/public/js/jquery.jcarousel.min.js');
			if($keyword = s(trim($_REQUEST['find']))){
				$this->_product_search($keyword);
			}else{
				$this->_product_search();
			}
			
			$this->SetTemplate('search.php');
			
		}
		
		public function OnSearchProducts(){
			include PATH_HTML_TEMPLATES.'search.php';
			exit;
		}
		
		public function OnSearchProductsOrder(){
			include PATH_SNIPPETS.'order-add-list.php';
			exit;
		}
		
		private function _product_search($keyword){
			$items_per_page = $this->_get_system_variable('search_items_per_page');
			$this->_load_lang_constants();
			$query = $_GET;
			unset($query['page'],$query['Event']);
			$query = http_build_query($query);
			$order_by = ' rel DESC';
			$list = explode(" ",$keyword);
			$str = array();			
			foreach($list as $el){
				$str[] = '(b.title LIKE \''.$el.'%\' OR t.code LIKE \''.$el.'%\' OR t.title LIKE \''.$el.'%\')';
			}
			//$search_str = implode(" OR ",$str);
			if ($keyword){
				//$search_str = 'AND (MATCH (t.title) AGAINST (\''.$keyword.'\'))';
			}
			if (i($_GET['category'])){
				$search_str = ' AND t.category_id = '.$_GET['category'].'';
			}
			
			if (i($_GET['price_to'])){
				$search_str = ' AND price > '.i($_GET['price_to']).'';
			}
			if (i($_GET['price_from'])){
				$search_str = ' AND price < '.i($_GET['price_from']).'';
			}
			$page = $_GET['page'];
			if($page < 1){				
				$page = 1;
			}
			
			/*$prod = $this->_load_content_with_pn('products', 
							't.publish = 1 ',
							mysql_real_escape_string($order_by),
							',MATCH (t.title) AGAINST (\''.$keyword.'\') as rel, IF(t.url != \'\',t.url,t.id) as url, c.title as category, p.filename,ROUND(t.price*'.floatval($this->Currencies[$this->Currency][course]).',0) as price',
							'INNER JOIN categories c ON c.id = t.category_id 
							 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1',
							 $items_per_page,9,$query, 'page', 't.id', 1);		*/
			$union = ' UNION ' ;
			$sql .= '(SELECT t.id, t.url, pr_t.title, t.category_id , t.img_preview, \'product\' as type, \'product\' AS sub_type, pr_t.`catalog_description` AS catalog_description 
					FROM products t 
					LEFT JOIN `products_translation` pr_t ON (pr_t.`obj_id` = t.`id` AND pr_t.lang = \''.$this->SiteLang.'\') 
					LEFT JOIN categories c ON c.id = t.category_id 
					LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
					WHERE t.publish = 1 AND 
							(
								pr_t.title LIKE \'%'.$keyword.'%\' 
							OR	pr_t.fron_text LIKE \'%'.$keyword.'%\' 
							OR	pr_t.content LIKE \'%'.$keyword.'%\' 
							OR	pr_t.right_column LIKE \'%'.$keyword.'%\' 
							OR	pr_t.catalog_description LIKE \'%'.$keyword.'%\' 
							) '.')';
				
			$sql .= $union.'(SELECT	n.id, n.url, n_t.title, n.id as category_id, n.filename, \'news\' as type, `module` AS sub_type, n_t.introtext as content 
					FROM news n 
					LEFT JOIN `news_translation` n_t ON (n_t.`obj_id` = n.`id` AND n_t.lang = \''.$this->SiteLang.'\') 
					WHERE 	n.published  = 1 AND 
							(n_t.title LIKE \'%'.$keyword.'%\' OR n_t.introtext LIKE \'%'.$keyword.'%\' OR n_t.fulltext LIKE \'%'.$keyword.'%\' ) '.')';
			
			$sql .= $union.'(SELECT	bl.id, bl.url, bl_t.title, bl.category_id as category_id, bl.filename2, \'blog\' as type, \'blog\' AS sub_type, bl_t.fron_text as content 
					FROM blogs bl 
					LEFT JOIN `blogs_translation` bl_t ON (bl_t.`obj_id` = bl.`id` AND bl_t.lang = \''.$this->SiteLang.'\') 
					WHERE 	bl.publish  = 1 AND 
							(bl_t.title LIKE \'%'.$keyword.'%\' OR bl_t.fron_text LIKE \'%'.$keyword.'%\' OR bl_t.content LIKE \'%'.$keyword.'%\' ) '.')';
			
			$sql .= $union.'(SELECT p.id, p.url, p_t.title, p.id as category_id, p.filename, \'page\' as type, \'page\' AS sub_type, p_t.fron_text 
					FROM 	pages p  
					LEFT JOIN `pages_translation` p_t ON (p_t.`obj_id` = p.`id` AND p_t.lang = \''.$this->SiteLang.'\') 
					WHERE 	p.publish = 1 AND 
							(p_t.title LIKE \'%'.$keyword.'%\' OR p_t.content LIKE \'%'.$keyword.'%\' ) '.')';
			
			$sql .= ' ORDER BY type, sub_type DESC LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
			// if($_GET['alex']) _debug($sql, 1);
			$prod = GetAll($sql);
			$this->Products['items'] = $prod;
			//_debug($this->Products['items'],1);
			$sql = 'SELECT FOUND_ROWS()';
			$cnt = GetOne($sql);

			AttachLib('PageNavigator');
			$this->Products['pn'] = new PageNavigator($cnt, $items_per_page, 10, $this->RootUrl . 'search.html', $page, '?find='.$keyword.'&page=%s');
			// if($_GET['alex']) _debug($this->Products, 1);
			/*Фото галлерея*/
			$prodgallery = $this->_load_content('products', 
							't.publish = 1 ',
							 mysql_real_escape_string($order_by),
							',MATCH (t.title) AGAINST (\''.$keyword.'\') as rel, IF(t.url != \'\',t.url,t.id) as url, c.title as category, p.filename,ROUND(t.price*'.floatval($this->Currencies[$this->Currency][course]).',0) as price',
							'INNER JOIN categories c ON c.id = t.category_id 
							 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1');	
						
			
			$this->ProductsPhotos = $prodgallery;
			
						
		}

	}		
?>
