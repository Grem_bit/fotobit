<?
	//ini_set('display_errors',1);
	 //error_reporting(E_ALL);

	class Experts extends ContentPage {

		protected	$TemplatesBaseDir		= 'experts/';
		protected	$Catalog				= array();
		protected	$Category				= array();
		protected	$Post				= array();
		private		$Ids					= array();
		protected	$Href					= 'experts/';
		public 		$Maker					= '';
		public 		$TableFields			= array();
		
		
		public function OnCreate() {
			
			$title = $this->_get_lang_constant('EXPERTS_TITLE');
			$this->AddTitle($title);
			$this->Breadcrumbs[] = array('url'=>'experts','title' => $title);			

			$UrlParts_sz = sizeof($this->UrlParts); 
			$this->IsCat = true;
			switch($UrlParts_sz){
				case 1:

						$this->Experts = $this->_load_catalog(0);
						// $this->SetTemplate('experts-front.php');
						$items_per_page = $this->_get_system_variable('all_posts_per_page');
						$max_pages_cnt = $this->_get_system_variable('max_all_posts_pages_cnt');
						$this->_load_posts('', $items_per_page, $max_pages_cnt);
						$this->SetTemplate('experts.php');

				break;
				
				default:
					
					if($this->UrlParts[1] == 'special'){
						$this->_get_products(' AND t.specials = 1');												
						$this->Breadcrumbs[] = array('title' => 'Специальное предложение');
						$this->AddTitle('Распродажа');
						$this->SetTemplate('expert.php');
					}else{
						
						// $this->Expert = $this->_load_content_info('experts','','publish = 1 AND url=\''.s(end($this->UrlParts)).'\'');
						$sql = 'SELECT *, `'.$this->LN.'title` as title, `'.$this->LN.'content` as content, `'.$this->LN.'profession` as profession, `'.$this->LN.'blog_title` as blog_title, `'.$this->LN.'experience` as experience FROM experts WHERE publish = 1 AND url=\''.s(end($this->UrlParts)).'\'';
						$this->Expert = GetRow($sql);
						
						if($this->Expert){ // эксперт
							$expert = $this->_load_content('experts','publish = 1 AND parent_id = '.i($this->Expert['id']).' ');
							
							if(is_for($expert)){ // есть подкатегории
								
								$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->_breadcrumbs($this->Expert['id'],'categories','catalog'));
								$this->Experts = $this->_load_catalog($this->Expert['id']);
								$this->SetTemplate('experts.php');
							}else{ 			// конечная, выводим продукты
								
								$this->SetTemplate('expert.php');
								//$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->_breadcrumbs($this->Expert['id'],'categories','catalog'));
								$this->NEWPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.product_top = 1 AND
																			 t.category_id =  \''.$this->Expert['id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
								$this->MONTHPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.month = 1 AND
																			 t.category_id =  \''.$this->Expert['id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
								
								$this->ShowCategory();
								// if($_GET['alex']) _debug($this->Expert, 1);
								$this->Breadcrumbs[] = array('url' => $this->Expert['url'], 'title' => $this->Expert['title']);
											
							}
						}else{ // продукт
								if($this->SiteLang == 'ru'){
									$site_lang = '';
								}else{
									$site_lang = $this->SiteLang.'_';
								}
								
								$this->Post = $this->_load_content_info('blogs t',
																			'LEFT JOIN experts c ON c.id = t.category_id AND c.publish = 1 
																			 LEFT JOIN blogs_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																			 LEFT JOIN product_status ps ON ps.id = t.status_id
																			 LEFT JOIN video p2 ON p2.product_id = t.id AND p2.default = 1'
																			, 
																			't.publish = 1 AND t.url =\''.s(end($this->UrlParts)).'\' ',
																			't.*, IF(t.url != \'\',t.url,t.id) as url, ps.img as status, c.'.$this->LN.'title as author, c.'.$this->LN.'blog_index as blog_index, p.filename AS photo, pt.* , p2.title as video
																			 '
																			);
								if($this->Post){
									
									$this->NEWPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.product_top = 1 AND
																			 t.id <> \''.$this->Post['id'].'\' AND 
																			 t.category_id =  \''.$this->Post['category_id'].'\'',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
									$this->RECPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			', 
																			't.publish = 1 AND t.id = \''.$this->Post['rec'].'\' ',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title
																			 '
																			);
																			
									$this->RANDOMPRODUCT = $this->_load_content_info('products t',
																			'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
																			LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																			',  
																			't.publish = 1  AND t.id <> \''.$this->Post['id'].'\' AND 
																			 t.category_id =  \''.$this->Post['category_id'].'\' ORDER BY RAND()',
																			't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename, pt.title 
																			 '
																			);
									
									//_debug($this->RECPRODUCT,1);
									$this->AddJS('/public/js/fancybox/jquery.mousewheel-3.0.4.pack.js');
									$this->AddJS('/public/js/fancybox/jquery.fancybox-1.3.4.pack.js');
									$this->AddCSS('/public/js/fancybox/jquery.fancybox-1.3.4.css');
									$this->AddJS('/public/js/jquery.jcarousel.min.js');
									$this->AddJS('/public/js/jquery.slider-1.5.js');
									$this->AddJS('/public/js/FlyUpSlider.js');
									
									$this->Experts = $this->_load_catalog(0, $this->Post['category_id']);
									$this->_load_posts($this->Post['category_id']);
									
									$this->PostUrl = array();
									$this->getPostAuthorUrl($this->Post['category_id'],1);
									// if($_GET['alex']) _debug($this->PostUrl, 1);
									foreach($this->PostUrl as $url){
										$this->Href .= $url['url'].'/';
									}
									$this->Breadcrumbs = array_merge($this->Breadcrumbs, $this->PostUrl);
									$this->Breadcrumbs[] = array('url' => $this->Post['url'], 'title' => $this->Post['title']);
									
									$this->AddTitle($this->Post['title']);
									$this->SetMetaKeywords($this->Post['meta_keywords']);
									$this->SetMetaDescription($this->Post['meta_description']);
									/*$this->Post['photos'] = $this->_load_content('photos','product_id = '.$this->Post['id'].' AND plan = 0','`default` DESC');
									$this->Post['gallery'] = $this->_load_content('photos','product_id = '.$this->Post['id'].' AND plan = 1','`default` DESC');
									$this->Post['videos'] = $this->_load_content('video','product_id = '.$this->Post['id'],'`default` DESC');*/
									$this->SetTemplate('post.php');

									
								}else{
									$this->GoTo404();
								}
						}
					}
			}
		}
		
		private function _get_category_ids($id){
			$sql = 'SELECT id FROM categories WHERE publish = 1 AND parent_id = '.$id;
			$res = GetCol($sql);
			if($res){
				$this->Ids = array_merge($this->Ids,$res);
				foreach($res as $val){
					$this->_get_category_ids($val);
				}
			}
		}
		
		public function _load_catalog($parent_id, $forbidden_id){
		
			$where = '';
			if($forbidden_id){
				if(is_array($forbidden_id)) $forbidden_id = implode(',', $forbidden_id);
				$where = "AND `id` NOT IN ($forbidden_id)";
			}
			
			$sql = 'SELECT *, `'.$this->LN.'title` as title, `'.$this->LN.'content` as content, `'.$this->LN.'profession` as profession, `'.$this->LN.'blog_title` as blog_title, `'.$this->LN.'experience` as experience FROM experts WHERE publish = 1 AND parent_id = '.i($parent_id).' '.$where.' ORDER BY order_by,  `'.$this->LN.'title` ';
			
			return GetAll($sql);
		}
		
	
		protected function ShowCategory(){

			$this->PostUrl = array();
			$this->getProdCategoryUrl($this->Expert['id'], 1);
			foreach($this->PostUrl as $url){
				$this->Href .= $url['url'].'/';
			}
			$this->Breadcrumbs = array_merge($this->Breadcrumbs,$this->PostUrl);
			
			$this->AddTitle($this->Expert['title']);
			$this->Experts = $this->_load_catalog($this->Expert['id']);
			if($this->Experts){
				$this->SetTemplate('experts.php'); 
			}else{
				$this->_get_category_ids($this->Expert['id']);
				$this->Ids[] = $this->Expert['id'];
				
				// $this->_get_products('AND t.category_id IN ('.implode(',',$this->Ids).')');
				$this->_load_posts($this->Expert['id']);
				// $this->_get_posts('AND t.category_id IN ('.implode(',',$this->Ids).')');
				$this->Experts = $this->_load_catalog(0, $this->Expert['id']);
				$this->SetTemplate('expert.php');
			}
		}
		
		public function _get_other_products($where ='', $limit = '', $order_by=' RAND() '){
			$lim = ($limit) ? (' LIMIT '.$limit) : '';	
		
			$sql ="SELECT  t.* , IF(t.url != '',t.url,t.id) as url, c.title as category, p.filename,
															   IF(t.price_old !='', t.price_old, t.price) as real_price,
															   pg.title as mtitle 	
					FROM products t 
						INNER JOIN categories c ON c.id = t.category_id 
															 LEFT JOIN photos p ON p.product_id = t.id 
															 LEFT JOIN course cr ON t.currency_id = cr.id
															 LEFT JOIN pages pg ON t.maker_id = pg.id
															 
					 WHERE t.publish = 1 ".$where."
					
					 ORDER BY ".$order_by.$lim;
			//_debug($sql);
			return GetAll($sql);
		}
		
		public function _get_more_info($id){
			$product = GetRow("SELECT * FROM `products` WHERE `id` = '".$id."' ");
			//_debug($this->TableFields);
			if($product){
				$info = '';
			foreach ($this->TableFields as $key => $fl){
												if($product[$fl] && $key != 'описание' && $key != 'артикул' && $key != 'цена'){
														$info .= $key .' - '. $product[$fl].' ';
													 }
												}			
			}
			return $info;
		}
			
		public function get_prevnext_products ($id, $category_id){
			$sql = 'SELECT url 
					FROM products 
					WHERE publish = \'1\' AND category_id = \''.$category_id.'\' AND id < \''.$id.'\' 
					ORDER BY id DESC LIMIT 1
					';
			$this->prev_product = GetRow($sql);
			$sql = 'SELECT url 
					FROM products 
					WHERE publish = \'1\' AND category_id = \''.$category_id.'\' AND id > \''.$id.'\' 
					ORDER BY id ASC LIMIT 1
					';
			$this->next_product = GetRow($sql);
		}
		
		private function _load_posts($author_id, $items_per_page, $max_pages_cnt){
			
			global $DB;
			
			if(!$items_per_page) $items_per_page = $this->_get_system_variable('posts_per_page');
			// $items_per_page = 1;
			if(!$max_pages_cnt) $max_pages_cnt = $this->_get_system_variable('max_posts_pages_cnt');
			// $max_pages_cnt = 3;
			
			// $page = $this->Page;
			$page = intval($_GET['page']);
			// _debug($page, 1);
			$where = '';
			if ($_GET['now']) {
				$firstday = mktime(0, 0, 0, date('m'), 1, date('Y')); 
				$where = ' AND (bl.publish_ts > '.$firstday.' AND bl.publish_ts < '.time().')';
			}
			if($author_id){
				$where = " AND bl.`category_id` = $author_id";
			}
			if($page < 1){
				$page = 1;
			}

			// $url = 'experts/'.end($this->UrlParts).'.html';
			if($author_id) $pref = 'experts/';
			$url = $pref.end($this->UrlParts).'.html';
			
				$sql = ' 
						SELECT SQL_CALC_FOUND_ROWS blt.* , bl.url, bl.publish_ts, bl.filename2, exp.`url` AS \'author_url\', exp.`'.$this->LN.'blog_title` AS \'blog_title\' 
					FROM blogs bl
					LEFT JOIN blogs_translation blt ON (blt.obj_id = bl.id AND blt.lang = \''.$this->SiteLang.'\') 
					LEFT JOIN experts exp ON(exp.`id` = bl.`category_id`) 
					WHERE 
						bl.publish = \'1\' AND blt.title <> "" '.$where.'
					ORDER BY	bl.publish_ts  DESC
					
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page.'
					';
		
			// exit($sql);
			$this->Posts = GetAll($sql);
			// _debug($this->News, 1);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			AttachLib('PageNavigator');
			// echo $this->RootUrl, $url, $page; exit();
			$this->PageNavigator = new PageNavigator($res['total'], $items_per_page, $max_pages_cnt, $this->RootUrl .($this->SiteLang == 'en'?$this->SiteLang.'/':''). $url, $page, '?page=%s',true, true);
			$this->PageNavigator->Template = 'page-navigator-news.html';
				
			$sql = 'SELECT pt.*, p.* 
					FROM pages p 
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.$this->UrlParts[0].'\'';
				
				
			// _debug($sql, 1);
			$this->PageInfo = $DB->GetRow($sql);
			
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);

			$this->SetMetaDescription($this->PageInfo['meta_description']);
			$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
		//	print_r($this->News);
		}
		
		public function getPostUrl($id){
		
			$sql = "SELECT bl.`url` AS 'post_url', exp.`url` AS 'author_url' 
					FROM `blogs` bl 
					LEFT JOIN `experts` exp ON (exp.`id` = bl.`category_id`) 
					WHERE bl.`id` = '$id' 
					";
			$urls = GetRow($sql);
			$url = 'experts/'.$urls['author_url'].'/'.$urls['post_url'].'.html';
			
			return $url;
		}
	}

?>