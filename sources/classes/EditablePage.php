<?

	class EditablePage extends ContentPage {

		protected $PageInfo			= array();
		protected $Category			= array();
		protected $View				= '';
		protected $TitleP			= '';
		
		
		protected $LastProfiles		= array();
		
		public function OnCreate() {
			

	
			if(end($this->UrlParts) != 'index' && end($this->UrlParts)){
				if($this->Serv=='com'){
					$this->SetTemplate('editable-page.php');
					$this->_load_page_info();
					$this->_set_meta_data();
					$this->Breadcrumbs[] = array('url'=>$this->PageInfo['url'],'title' => $this->PageInfo['title']);
					// _debug($this->PageInfo);
					
					if(!$this->PageInfo['content'] && $_REQUEST['Event'] != 'AjaxRequest'){
						// header("HTTP/1.0 404 Not Found");
						// $this->SetTemplate('404.html');
						$this->GoTo404();
					}
					
				}elseif($this->Serv=='net'){
					if($this->UrlParts[0]=='delivery'){
						
						$this->SetTemplate('editable-delivery.php');
					}elseif($this->UrlParts[0]=='price'){
						$this->SetTemplate('editable-price.php');
					}else{
						$this->SetTemplate('editable-page.php');
					}
					$this->_load_page_info();
					$this->_set_meta_data();
					$this->Breadcrumbs[] = array('url'=>$this->PageInfo['url'],'title' => $this->PageInfo['title']);
					// _debug($this->PageInfo);
					
					if(!$this->PageInfo['content'] && $_REQUEST['Event'] != 'AjaxRequest'){
						// header("HTTP/1.0 404 Not Found");
						// $this->SetTemplate('404.html');
						$this->GoTo404();
					}
				}
			}else{
				$this->FrontPage = true;
				
				$this->SetTemplate('front-page.php');

				$this->Gallery = $this->_load_content('gallery', 'front = 1');
				$this->RecProd = $this->_load_content_translation('products', 'new = 1 AND publish = 1', 't.create_ts', ', ph.filename', ' LEFT JOIN photos ph ON ph.default=1 AND ph.product_id = t.id', 2);
				
				$this->SetTemplate('front-page.php');

				$this->_load_page_info();
				$this->_set_meta_data();
				//_debug($this);
			}
		}
		
		
				
		public function OnBeforeDisplay() {
			
			$this->_proccess_content();
		}
		
		protected function OnAjaxRequest(){
			$this->_load_page_info();
			echo $this->PageInfo['fron_text'];
			exit();
		}
		
		/*
		 * Private methods
		 */
		
		private function _proccess_content() {
			
			if(strpos($this->PageInfo['content'], '{[ContactForm]}')) {
				
				$this->PageInfo['content'] = str_replace('{[ContactForm]}', $this->_get_snippet_content('contact-form'), $this->PageInfo['content']);
			}
			if(strpos($this->PageInfo['content'], '{[BUYBUTTON]}')) {
				
				$this->PageInfo['content'] = str_replace('{[BUYBUTTON]}', $this->_get_snippet_content('buybuttton'), $this->PageInfo['content']);
			}
			
			if(strpos($this->PageInfo['content'], '{[AskQuestionForm]}')) {
				
				$this->PageInfo['content'] = str_replace('{[AskQuestionForm]}', $this->_get_snippet_content('ask-question-form'), $this->PageInfo['content']);
			}
			
			if(strpos($this->PageInfo['content'], '{[MAP]}')) {
				
				$this->PageInfo['content'] = str_replace('{[MAP]}', $this->_get_snippet_content('map'), $this->PageInfo['content']);
			}
			
			if(strpos($this->PageInfo['content'], '{[Events]}')) {
				
				$this->PageInfo['content'] = str_replace('{[Events]}', $this->_get_snippet_content('events'), $this->PageInfo['content']);
			}
			
			if(strpos($this->PageInfo['content'], '{[News]}')) {
				
				$this->PageInfo['content'] = str_replace('{[News]}', $this->_get_snippet_content('news'), $this->PageInfo['content']);
			}
		}
		
		public function _set_meta_data() {
			
			// Title
			//if($this->PageInfo['url'] != 'index'){
				if($this->PageInfo['page_title']) {
					$this->SetTitle($this->PageInfo['page_title']);
				}
			//}	
			
			// Keywords 
			if($this->PageInfo['meta_keywords']) {
				
				$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
			}
			else {
				
				$this->SetMetaKeywords($this->GetConfigParam('meta-keywords'));
			}
			
			// Description
			if($this->PageInfo['meta_description']) {
				
				$this->SetMetaDescription($this->PageInfo['meta_description']);
			}
			else {
				
				$this->SetMetaDescription($this->GetConfigParam('meta-description'));
			}
		}
		
		public function _load_page_info() {

			global $DB;
			
			if($this->UrlParts[1] == 'page'){
				
				$this->Page = intval($this->UrlParts[2]);
				$sql = 'SELECT pt.*, p.* 
						FROM pages p 
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
						WHERE 
							  p.publish=\'1\' AND 
							  p.url=\''.$this->UrlParts[0].'\'';
				
				$this->PageInfo = $DB->GetRow($sql);
			
			}else{
			
				$is_front_page = '';
				global $PARENT_ID,$server;
				
				if($PARENT_ID && $this->UrlParts[0] == 'index' ){
					$this->UrlParts[0] = 'chejazz';
				}else{				
					if(!end($this->UrlParts)) $this->UrlParts[0] = 'index';
				}
				
				$sql = 'SELECT pt.*, p.* 
						FROM pages p 
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
						WHERE 
							  p.publish=\'1\' AND 
							  p.url=\''.end($this->UrlParts).'\'';
				
				$this->PageInfo = $DB->GetRow($sql);
				//print_r($this->PageInfo);
				/*if(!$this->PageInfo['title']){
					$this->PageInfo = $this->_load_custom_translation('pages', 'pages_translation', $this->PageInfo['id']);	
				}*/
			}
		}
	
		
	}

?>