<?

	class Interkassa extends ContentPage {
		
		/*
		 * Protected Properties
		 */
		
		protected $TemplatesBaseDir		= 'interkassa/';		
		
		
		/*
		 * Пример HTML формы для оплаты через платежный шлюз Interkassa.com:

		<form name="payment" action="https://www.interkassa.com/lib/payment.php" method="post" enctype="application/x-www-form-urlencoded" accept-charset="cp1251">
			<input type="hidden" name="ik_shop_id" value="452CA966-A958-173C-5387-DF40C6C9927C">
			<input type="hidden" name="ik_payment_amount" value="1.00">
			<input type="hidden" name="ik_payment_id" value="PAYMENT_ID">
			<input type="hidden" name="ik_payment_desc" value="PAYMENT_DESCRIPTION">
			<input type="hidden" name="ik_paysystem_alias" value="">
			<input type="submit" name="process" value="Оплатить">
		</form>
		
			Системные переменные
		Interkassa ID магазина				ik_shop_id
		Interkassa Status URL 				ik_status_url
		Interkassa Success URL 				ik_success_url
		Interkassa Fail URL 				ik_fail_url
		Interkassa Secret Key 				ik_secret_key
		Interkassa URL для отправки формы	ik_form_action
		
		INSERT INTO `BD_name`.`sys_vars` (`id`, `title`, `var_name`, `var_value`) VALUES (NULL, 'Interkassa ID магазина', 'ik_shop_id', '452CA966-A958-173C-5387-DF40C6C9927C'), (NULL, 'Interkassa Status URL', 'ik_status_url', 'ik_status_bw8ng4h2'), (NULL, 'Interkassa Success URL', 'ik_success_url', 'success'), (NULL, 'Interkassa Fail URL', 'ik_fail_url', 'fail'), (NULL, 'Interkassa Secret Key', 'ik_secret_key', 'rnUsMbTy1gWAJkBx'), (NULL, 'Interkassa URL для отправки формы', 'ik_form_action', 'https://www.interkassa.com/lib/payment.php');
		
		 *
		 */
		 
		/*
		 * Public methods
		 */
		
		public function OnCreate(){//_debug($_GET, 1);
			global $config;
			$this->AddTitle('Интеркасса');
			switch($this->UrlParts[1]){
				case 'pay':
					$this->SetTemplate('pay.php');
				break;
				case $this->_get_system_variable('ik_status_url'):
					// имитация оплаты url: photo.stepshop.com.ua/interkassa/ik_status_bw8ng4h2?debug_pay=1&ik_payment_id=164
					// для имитации необходимо в проверках включить $_GET['debug_pay']
					$this->Status();
				break;
				case $this->_get_system_variable('ik_success_url'):
					// $this->SetTemplate('success.php');
					// $this->OnOrderPhoto();
					unset($_SESSION['Trash']);
					unset($_SESSION['Cur_order']);
					$this->GoToUrl('/user-account/orders.html');
				break;
				case $this->_get_system_variable('ik_fail_url'):
					// $this->SetTemplate('fail.php');
					$this->GoToUrl('/user-account/upload.html');
				break;
			}
		}
		
		private function Status(){
		
			/*Извлекаем данные из массива POST */
			// extract($_POST);
			// extract($_POST, EXTR_SKIP);
			extract($_REQUEST, EXTR_SKIP);
			
			if($ik_payment_id){
				$sql = "SELECT * FROM `orders` WHERE `id` = '$ik_payment_id'";
				$order = GetRow($sql);
			}
			
			$ik_secret_key = $this->_get_system_variable('ik_secret_key'); //Сюда  необходимо вписать Ваш Secret Key
			$amount = $order['total']; //Сюда  необходимо вписать сумму платежа

			/* Если контрольная подпись не передана, выводим ошибку */
			// if (empty($ik_sign_hash) && !$_GET['debug_pay']){ // для тестовой оплаты
			if (empty($ik_sign_hash)){
			   die ('Error: Не переданы  параметры');
			}

			/* Формирование контрольной подписи */
			$sing_hash_str =  $ik_shop_id.':'.$amount.':'.$ik_payment_id.':'.
							  $ik_paysystem_alias.':'.$ik_baggage_fields.':'.
							  $ik_payment_state.':'.$ik_trans_id.':'.
							  $ik_currency_exch.':'.$ik_fees_payer.':'.
							  $ik_secret_key;
// _debug($sing_hash_str, 1);
// _debug(strtoupper(md5($sing_hash_str)), 1);
/*
http://stepshop_photo.misha/interkassa/ik_status_bw8ng4h2?ik_shop_id=1&ik_payment_amount=900.00&ik_payment_id=1&ik_paysystem_alias=1&ik_baggage_fields=1&ik_payment_state=success&ik_trans_id=1&ik_currency_exch=1&ik_fees_payer=1&secret_key=ibFuq1866jvWnvrM&ik_sign_hash=93E3F72E298EE66130FCBE0ACCCA4264
1:270:56:1:1:success:1:1:1:ibFuq1866jvWnvrM = d8e90df8a8e942beecd850c450f6e128
1:270:56:1:1:success:1:1:1:ibFuq1866jvWnvrM = D8E90DF8A8E942BEECD850C450F6E128
1:900.00:1:1:1:success:1:1:1:rnUsMbTy1gWAJkBx = 93e3f72e298ee66130fcbe0accca4264
1:900.00:1:1:1:success:1:1:1:rnUsMbTy1gWAJkBx = 93E3F72E298EE66130FCBE0ACCCA4264
*/

			/* Применяем алгоритм MD5 и переводим буквы в верхний  регистр */
			$sign_hash  = strtoupper(md5($sing_hash_str));

			/* Проверка контрольной суммы */
			// if ($_REQUEST['ik_sign_hash'] === $sign_hash || $_GET['debug_pay']){ // для тестовой оплаты
			if ($_POST['ik_sign_hash'] === $sign_hash){
			   /* Отправляем товар покупателю*/
			   // $this->OnOrderPhoto();
				$sql = "UPDATE `orders` SET `state` = 'paid' WHERE `id` = '".$order['id']."'";
				// _debug($sql, 1);
				if(Execute($sql)){
					$DeletedFolder = '/public/files/images/orders/'.$order['id'].'/thumb';
					RemoveDir($_SERVER['DOCUMENT_ROOT'].$DeletedFolder);
					cleanDir($_SERVER['DOCUMENT_ROOT'].'/public/files/images/orders/'.$order['id']);
					$this->SendMessages($order);
				}
				// if($_GET['debug_pay']){$this->GoToUrl('/interkassa/'.$this->_get_system_variable('ik_success_url').'.html');} // для тестовой оплаты
			}else{
			   die  ('Error: Неверная контрольная сумма');
			}

		}
		
		/*private function SendMessages_($order){//_debug($order, 0);

			AttachLib('Mailer');
			$mail = new Mailer();

			$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
			$row = GetAll($sql);
			
			for($i = 0, $count = sizeof($row); $i < $count; $i++){
				$mail->AddRecepient($row[$i]['email']);                    
			}
			
			$string = $this->_get_system_variable('delivery_methods');
			$delivery_methods = explodeMultiString($string);
			// _debug($delivery_methods, 0);
			$string = $this->_get_system_variable('payment_methods');
			$payment_methods = explodeMultiString($string);
			
			$user_data = '<p>'.
			'Имя пользователя: '.$order['name'].'<br />'.
			'Email: '.$order['email'].'<br />'.
			'Телефон: '.$order['phone'].'<br />'.
			// 'Транспортировка: '.($_POST['dostavka'] == 1 ? 'Доставка' : 'Самовывоз товара').'<br />'.
			'Адрес: '.$order['address'].'<br />'.
			'Доставка: '.$delivery_methods[$order['delivery_way']]['item'].($order['delivery_option'] ? ' ('.$delivery_methods[$order['delivery_way']]['options'][$order['delivery_option']].')':'').'<br />'.
			// '(заполняется, если необходима доставка): '.$_POST['address2'].'<br />'.
			// 'Коментарий: '.$_POST['comment'].'<br />'.
			// 'Форма оплаты: '.$_POST['pay'].'<br />'.
			'Оплата: '.$payment_methods[$order['payment_way']]['item'].'<br />'.
			'</p>';
			// _debug($user_data, 1);
			// $cart_table = $this->generate_order_table($_SESSION['Trash']['cart']);
			$cart_table = $this->generate_order_table(unserialize($order['cart']));
			// _debug($_SESSION);
			// _debug($cart_table, 1);
			$mail->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' => $cart_table,
								'message'	=> $order['comment'],
								'date'		=> date('d:m:Y H:i',time())								
			));
			$mail->Send(	'order.html',
							'Заказ № '.$order['id'].' на сайте '.$this->_get_lang_constant('SITE_NAME'),//$this->_get_system_variable('order_email_subject')
							$this->_get_system_variable('noreply_email'), 
							$this->_get_system_variable('sender_name')
						);
			if($this->ValidEmail($order['email'])) {
				$mail2 = new Mailer();
				$mail2->AddRecepient($order['email']);
				$mail2->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' 	=> $cart_table,
								'message'	=> $order['comment'],
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail2->Send(	'order_consumer.html',
								$this->_get_system_variable('email_subject_consumer'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
			}
		}*/
		private function SendMessages($order){//_debug($order, 0);
		
			AttachLib('Mailer');
			$mail = new Mailer();

			$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
			$row = GetAll($sql);
			
			for($i = 0, $count = sizeof($row); $i < $count; $i++){
				$mail->AddRecepient($row[$i]['email']);                    
			}
			
			$string = $this->_get_system_variable('delivery_methods');
			$delivery_methods = explodeMultiString($string);
			$string = $this->_get_system_variable('payment_methods');
			$payment_methods = explodeMultiString($string);
			
			$message = iconv("cp1251", "utf-8", $_POST['message']);
			if($message == $this->_get_lang_constant('FORM_COMMENT_TEXT')){
				$message = '';
			}
			
			$user_data = '<p>'.
			'Имя пользователя: '.$order['name'].'<br />'.
			'Email: '.$order['email'].'<br />'.
			'Телефон: '.$order['phone'].'<br />'.
			// 'Транспортировка: '.($_POST['dostavka'] == 1 ? 'Доставка' : 'Самовывоз товара').'<br />'.
			'Адрес: '.$order['address'].'<br />'.
			'Доставка: '.$delivery_methods[$order['delivery_way']]['item'].($order['delivery_option'] ? ' ('.$delivery_methods[$order['delivery_way']]['options'][$order['delivery_option']].')':'').'<br />'.
			// 'Адрес доставки (заполняется, если необходима доставка): '.$_POST['address2'].'<br />'.
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$_POST['address2'].'<br />'.
			// 'Коментарий: '.$_POST['comment'].'<br />'.
			// 'Форма оплаты: '.$_POST['pay'].'<br />'.
			'Оплата: '.$payment_methods[$order['payment_way']]['item'].'<br />'.
			'</p>';
			// _debug($user_data, 1);
			// $cart_table = $this->generate_order_table($_SESSION['Trash']['cart']);
			$cart_table = $this->generate_order_table(unserialize($order['cart']));
			// _debug($_SESSION);
			// _debug($cart_table, 1);
			$mail->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' => $cart_table,
								'message'	=> $message,
								'date'		=> date('d:m:Y H:i',time())								
			));
			$mail->Send(	'order.html',
							'Заказ № '.$order['id'].' на сайте '.$this->_get_lang_constant('SITE_NAME'),//$this->_get_system_variable('order_email_subject')
							$this->_get_system_variable('noreply_email'), 
							$this->_get_system_variable('sender_name')
						);
			if($this->ValidEmail($order['email'])) {
				$mail2 = new Mailer();
				$mail2->AddRecepient($order['email']);
				$mail2->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' 	=> $cart_table,
								'message'	=> $message,
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail2->Send(	'order_consumer.html',
								$this->_get_system_variable('email_subject_consumer'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
			}
		}
		
		private function generate_order_table($cart){//_debug($cart, 1);
			
			$res ='
			<style type="text/css">
			.trash-list TD,
			.trash-list TH
			{
				padding:5px;
			}
			</style>

			<table  cellpadding="2" cellspacing="2" class="trash-list" width="100%">
					
					<tr class="table-title">
						<th style="background:#D0D0D0;width:70px;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Код товара</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Название</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Кол-во/шт.</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Цена</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC" class="last">Стоимость</th>
					</tr> ';
			$count = $total = $price = $discount = 0;
			foreach($cart as $prod){
				$price = $prod['price'];
				$prod['price']*=$prod['count'];
				$prod['discount']*=$prod['count'];
				$res.='	<tr>
						<td class="image" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">'.$prod['id'].'</td>
						<td class="name" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$prod['catalog'].' <strong>'.$prod['img'].'</strong></p>
						</td>
						<td class="count" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$prod['count'].'</p>
						</td>
						<td class="price" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$this->_show_price($price,$prod['currency_id']).'</p>							
						</td>
						<td class="price" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC">
							<p>'.$this->_show_price($prod['price'],$prod['currency_id']).'</p>							
						</td>
					</tr>';
				$count += $prod['count'];
				$total += $prod['price']-$prod['discount'];
			}
			
			$res.='	<tr class="summary">
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Итого:</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">'.$count.' шт.</th>
						<th colspan="2" style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC">'.$this->_show_price($total).'</th>
					</tr>';
			return $res;
		}
	}
?>