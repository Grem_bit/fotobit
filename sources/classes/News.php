<?	class News extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		public $News				= array();
		public $News_sz				= 0;
		public $PageNavigator		= null;
		
		public $Info				= array();
		public $Info_sz				= 0;
		public $PageNavigatorInfo		= null;
		
		
		public $Url 				= '';
		public $Page 				= '';
		public $PageI 				= '';
		
		public $Comments			= array();
		public $Comments_sz			= 0;
		public $OneNews 			= array();
		
		public $ViewEvent 			= '';
		public $Lang				= '';
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			global $config, $url_parts;
			
			$title = $this->_get_lang_constant('NEWS');
			$this->Breadcrumbs[] = array('url'=>'news','title' => $title);
			
			if($this->UrlParts[1] == 'calendar'){
				$this->AddTitle('{[CALENDAR_ALP]}');
				$title = $this->_get_lang_constant('CALENDAR');
				$this->Breadcrumbs[] = array('url'=>'calendar','title' => $title);
				if(isset($_GET["date"])){
					$month = (int)substr($_GET["date"], 4, 2);
					// $this->Breadcrumbs[] = array('url'=>'calendar','title' => $title);
				}else{
					$month = date("n", mktime(0,0,0,date('m'),1,date('Y')));
					// $this->Breadcrumbs[] = array('url'=>'calendar','title' => $title);
				}
				// $this->Breadcrumbs[] = array('url'=>'','title' => $month);
				$this->Breadcrumbs[] = array('url'=>'','title' => $this->Monthnow[$month - 1]);
				$this->ViewEvent = 'Calendar';
			}else{
				$this->AddTitle('{[NEWS_TITLE]}');
				$count = count($this->UrlParts);
				
				switch($this->UrlParts[$count - 2]){
					case '':
					case NULL:
						// load all news
						$this->_load_all_news();
						$this->_load_all_info();
						$this->ViewEvent = 'AllNews';
						break;
					case 'view':
						//load news by ID
						$this->_load_news($this->UrlParts[$count-1],true);
						$this->ViewEvent = 'View';
						break;
					case 'page-news':
						//load news by ID
						$this->Page = $this->UrlParts[$count-1];
						$this->_load_all_news();
						$this->_load_all_info();
						$this->ViewEvent = 'AllNews';
						break;
					case 'page-info':
						//load news by ID
						$this->PageI = $this->UrlParts[$count-1];
						$this->_load_all_news();
						$this->_load_all_info();
						$this->ViewEvent = 'AllNews';
						break;
					default:
						//load news by SEO url
						$this->_load_news($this->UrlParts[$count],true);
						$this->ViewEvent = 'View';
						// $this->SetTitle($this->OneNews['title']);
						break;
				}
			}

			$this->SetTemplate('news.html');
			
			return;
		}
		
		/*
		 * Private methods
		 */

		private function _load_news($url, $seo = true){
			$this->ViewEvent == 'ViewNews';
			global $DB;
			switch($seo){
				case true;				
					$sql = "SELECT * FROM news WHERE published = '1' AND url = '$url'";
					$this->OneNews = $DB->GetRow($sql);
					break; 
				case false;
					$sql = "SELECT * FROM news WHERE published = '1' AND  id = '$url'";
					$this->OneNews = $DB->GetRow($sql);
					break;
			}
			
			$sql = "SELECT 
							* 
						FROM 
							news_translation 
						WHERE 
							obj_id = '{$this->OneNews['id']}' AND lang = '".$this->SiteLang."' 
						";
			
			$_news = $DB->GetRow($sql);
			$_news['date'] = date($this->_get_system_variable('date_format'),$this->OneNews['publish_ts']);
			$this->OneNews = array('date'=> $_news['date'], 
								'title' => $_news['title'], 
								'publish_ts'=> $_news['publish_ts'],
								'filename'=> $this->OneNews['filename'],
								'introtext' => $_news['introtext'],
								'fulltext' => $_news['fulltext'],
								'extratext1' => $_news['extratext1'],
								'extratext2' => $_news['extratext2'],
								'url'=>$this->OneNews['url'], 
								'id'=>$this->OneNews['id']
			
			);
			
			$sql = 'SELECT pt.*, p.*  
					FROM pages p
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						 	p.publish = 1 AND
						  p.url=\'news\'';
			$this->PageInfo = $DB->GetRow($sql);
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);
			$this->SetTitle($_news['page_title']);
			//print_r($this->PageInfo);
			$this->SetMetaDescription($_news['description']);
			$this->SetMetaKeywords($_news['keywords']);
			$this->Breadcrumbs[] = array('url'=>'news','title' => $this->OneNews['title']);
		}
		
		private function _load_news_id($url, $seo = true){
			global $DB;
			switch($seo){
				case true;				
					$sql = "SELECT id FROM news WHERE published = '1' AND url = '$url'";
					$this->NewsId = $DB->GetOne($sql);
					break; 
				case false;
					$sql = "SELECT id FROM news WHERE published = '1' AND  id = '$url'";
					$this->NewsId = $DB->GetOne($sql);
					break;
			}
		}
		
		private function _load_all_news(){
			
			global $DB;
			
			$items_per_page = $this->_get_system_variable('news_per_page');
			$max_pages_cnt = $this->_get_system_variable('max_news_pages_cnt');
			$page = $this->Page;
			if ($_GET['now']) {
				$firstday = mktime(0, 0, 0, date('m'), 1, date('Y')); 
				$where = ' AND (n.publish_ts > '.$firstday.' AND n.publish_ts < '.time().')';
			}
			if($page < 1){				
				$page = 1;
			}

			$url = 'news/';
			
				$sql = ' 
						SELECT SQL_CALC_FOUND_ROWS nt.* , n.url, n.publish_ts, n.filename2
					FROM news n
					LEFT JOIN news_translation  nt ON (nt.obj_id = n.id AND nt.lang = \''.$this->SiteLang.'\')
					WHERE 
						n.module = \'news\' AND 
						n.published = \'1\' AND nt.title <> "" '.$where.'
					ORDER BY	n.publish_ts  DESC
					
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page.'
					';
		
			// exit($sql);
			$this->News = GetAll($sql);
			// _debug($this->News, 1);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			AttachLib('PageNavigator');
			// echo $this->RootUrl, $url, $page; exit();
			$this->PageNavigator = new PageNavigator($res['total'], $items_per_page, $max_pages_cnt, $this->RootUrl .($this->SiteLang == 'en'?$this->SiteLang.'/':''). $url, $page, 'page-news/%s',true, true);
			$this->PageNavigator->Template = 'page-navigator-news.html';
			
				
				
			$sql = 'SELECT pt.*, p.* 
					FROM pages p 
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.$this->UrlParts[0].'\'';
				
				
						
			$this->PageInfo = $DB->GetRow($sql);
			
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);

			$this->SetMetaDescription($this->PageInfo['meta_description']);
			$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
		//	print_r($this->News);
			
			
		}
		
		private function _load_all_info(){
			
			global $DB;
			
			$items_per_page = $this->_get_system_variable('info_per_page');
			$max_pages_cnt = $this->_get_system_variable('max_info_pages_cnt');
			$page = $this->PageI;
			if ($_GET['now']) {
				$firstday =  mktime(0, 0, 0, date("m"));
				$where = ' AND (n.publish_ts > '.$firstday.' AND n.publish_ts < '.time().')';
			}
			if($page < 1){				
				$page = 1;
			}

			$url = 'news/';
			
				$sql = ' 
						SELECT SQL_CALC_FOUND_ROWS nt.* , n.url, n.publish_ts,n.filename2
					FROM news n
					LEFT JOIN news_translation  nt ON (nt.obj_id = n.id AND nt.lang = \''.$this->SiteLang.'\')
					WHERE 
						n.news = 1 AND 
						n.published = \'1\' AND nt.title <> "" '.$where.'
					ORDER BY	n.publish_ts  DESC
					
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page.'
					';
		
			//echo $sqlecho $sql;
			$this->Info = GetAll($sql);
			// _debug($this->News, 1);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			AttachLib('PageNavigator');
			// echo $this->RootUrl, $url, $page; exit();
			$this->PageNavigatorInfo = new PageNavigator($res['total'], $items_per_page, $max_pages_cnt, $this->RootUrl .($this->SiteLang == 'en'?$this->SiteLang.'/':''). $url, $page, 'page-info/%s',true, true);
			$this->PageNavigatorInfo->Template = 'page-navigator-news.html';
				
			$sql = 'SELECT pt.*, p.* 
					FROM pages p 
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.$this->UrlParts[0].'\'';
				
			$this->PageInfo = $DB->GetRow($sql);
			
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);

			$this->SetMetaDescription($this->PageInfo['meta_description']);
			$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
		}
		
		
		private function _load_comments($art_id, $parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT ac.*, u.login 
					FROM news_comments ac,
						 users u
					WHERE 	ac.news_id	= \''.$this->NewsId.'\' and 
							ac.published 	= 1 and
						  	ac.user_id 		= u.id and
						  	ac.parent_id	= \''.$parent_id.'\'
					ORDER BY ac.create_ts DESC';
		
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['level'] = $level;
				array_push($this->Comments, $cat[$i]);
				$level++;
				$this->_load_comments($art_id, $cat[$i]['id']);
				
				$level--;
			}
			
			if($parent_id == 0) {
				
				$this->Comments_sz = count($this->Comments);
			}
			
		}
		
		private function _load_top_level_categories() {
			
			global $DB;
			
			$sql = 'SELECT title, 
						   url 
					FROM categories 
					WHERE parent_id=\'0\'';
			$this->Categories = $DB->GetAll($sql);
			$this->Categories_sz = count($this->Categories);
			
			for($i = 0; $i < $this->Categories_sz; $i++) {
				
				$this->Categories[$i]['href'] = $this->RootUrl.$this->Categories[$i]['url'].'/';
			}
		}
		
		public function getCalendar($date, $current_month){
		
			// проверяем передали ли нам месяц и год
			if(isset($date)){
					$month = (int)substr($date, 4, 2);
					$year  = (int)substr($date, 0, 4);
			}
			else{ // иначе выводить текущие месяц и год
					$month = date("m", mktime(0,0,0,date('m'),1,date('Y')));
					$year  = date("Y", mktime(0,0,0,date('m'),1,date('Y')));
			}
			
			// получаем новости
			$date1_sec = mktime(0, 0, 0, $month, 1, $year);
			$date2_sec = strtotime("next Month", $date1_sec) - 1;
			
			$sql = "SELECT * 
					FROM news n 
					LEFT JOIN news_translation  nt ON (nt.obj_id = n.id AND nt.lang = '".$this->SiteLang."') 
					WHERE 
						n.module = 'events' AND 
						n.published = '1' AND nt.title <> '' AND
						n.`publish_ts` BETWEEN '$date1_sec' AND '$date2_sec' 
					ORDER BY	n.publish_ts  ASC
					";
			// _debug($sql, 1);
			$news = GetAll($sql);
			if($current_month) $this->NewsCurrMonth = $news;
			// _debug($news, 1);
			foreach($news as &$oneNews){
				$oneNews['publish_ts_day'] = date(j, $oneNews['publish_ts']);
			}
			unset($oneNews); 
			// _debug($news, 1);
			
			$skip = date("w", mktime(0,0,0,$month,1,$year)) - 1; // узнаем номер дня недели
			if($skip < 0){
				$skip = 6;
			}
			$daysInMonth = date("t", mktime(0,0,0,$month,1,$year)); // узнаем число дней в месяце
			$calendar_head = '';    // обнуляем calendar head
			$calendar_body = '';    // обнуляем calendar boday
			$day = 1;       // для цикла, далее будем увеличивать значение

			for($i = 0; $i < 6; $i++){ // Внешний цикл для недель 6 с неполыми
				$calendar_body .= '<ul>';       // открываем тэг строки
				for($j = 0; $j < 7; $j++){      // Внутренний цикл для дней недели
					if(($skip > 0)or($day > $daysInMonth)){ // выводим пустые ячейки до 1-го дня и после полного кол-ва дней
						$calendar_body .= '<li></li>';
						$skip--;
					}else{
						$info_div = '<div class="spl">
										<div class="spl-box" style="overflow: auto;">
									';
						foreach($news as $oneNews){
							if($oneNews['publish_ts_day'] == $day){
								$news_exist = true;
								$id = $oneNews['obj_id'];
								$info_div .= '
											<div class="spl-text font-cond"><a class="get-news" rel="'.$id.'" href="#">'.$oneNews['title'].'</a></div>
											<a class="spl-link get-news" rel="'.$id.'" href="#" title=""></a>
											<div class="spl-valign"></div>
											';
							}
						}
						$info_div .= '
										</div>
									</div>
									';
						if($j == 0){     // если воскресенье, то отмечаем выходной
							if($news_exist){
								$calendar_body .= '	<li><div>
													<a class="red-event get-news" title="" href="" rel="'.$id.'">'.$day.'</a>';
								$calendar_body .= $info_div;
								$calendar_body .= '</div></li>';
							}else{
								$calendar_body .= '<li><span>'.$day.'</span></li>';
							}
						}else{   // в противном случае просто выводим день в ячейке
								if ((date(j)==$day)&&(date(m)==$month)&&(date(Y)==$year)){//проверяем на текущий день
									// $calendar_body .= '<li><a class="red-event" title="">'.$day.'</a></li>';
									if($news_exist){
										$calendar_body .= '	<li><div>
															<a class="red-event get-news" title="" href="" rel="'.$id.'">'.$day.'</a>';
										$calendar_body .= $info_div;
										$calendar_body .= '</div></li>';
									}else{
										$calendar_body .= '<li><span>'.$day.'</span></li>';
									}
								}else{
									if($news_exist){
										$calendar_body .= '	<li><div>
															<a class="red-event get-news" title="" href="" rel="'.$id.'">'.$day.'</a>';
										$calendar_body .= $info_div;
										$calendar_body .= '</div></li>';
									}else{
										$calendar_body .= '<li><span>'.$day.'</span></li>';
									}
								}
						}
						$day++; // увеличиваем $day
						$news_exist = false;
					}
				}
				$calendar_body .= '</ul>'; // закрываем тэг строки
			}

			// заголовок календаря
			if($current_month){
				$pref = '';
			}else{
				$pref = 'small-';
			}
			$calendar_head = '
							<a title="" href="?date='.date("Ym", mktime(0,0,0,$month-1,1,$year)).'" class="arr-prev"></a>
							<div class="gallery-box">
								<div class="gallery-thumbs">
									<ul>
										<li>
											<a title="">
												<img alt="" src="/public/images/'.$this->LN.$pref.date("F", mktime(0,0,0,$month,1,$year)).'.png">
												<span class="font-cond">'.date("Y", mktime(0,0,0,$month,1,$year)).'</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<a title="" href="?date='.date("Ym", mktime(0,0,0,$month+1,1,$year)).'" class="arr-next"></a>
							';
			
			return array('head' => $calendar_head, 'body' => $calendar_body);
		}
		
		public function OnGetOneNews(){
		
			$id = i($_GET['id']);
			$this->_load_news($id, false);
			include PATH_SNIPPETS.'event.php';
			exit;
		}
		
		protected function _get_all_news($module = 'news', $forbidden_id){
		
			$where = "AND t.`module` = '$module'";
			if($forbidden_id) $where .= " AND t.`id` <> '$forbidden_id'";
			$sql = "SELECT t.`id`, t.`url`, t.`publish_ts`, n_t.`title` , t.`filename2` 
					FROM `news` t 
					LEFT JOIN `news_translation` n_t ON (n_t.`obj_id` = t.`id` AND n_t.lang = '".$this->SiteLang."') 
					WHERE t.`published` = '1' $where 
					ORDER BY t.`publish_ts`
				";
			
			$other_news = GetAll($sql);
			// _debug($sql, 1);
			// if($_GET['alex']) _debug($other_news, 1);
			return $other_news;
		}
		
	}

?>