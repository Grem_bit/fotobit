<?
 
	class Order extends ContentPage { 

		
		
		public function OnCreate() {	
			//$this->AddJS('/public/js/order.js');		
			//unset($_SESSION['Trash']);
			//_debug($_SESSION['Trash']);
			$this->AddTitle('Корзина товаров');
			$this->SetTemplate('order.php');
		}
		
		public function OnAddToCart(){
			
			if($id = intval($_REQUEST['id'])){
				$this->_add_cart($id);
			}
			ob_start();

			include PATH_SNIPPETS.'small_basket.php'; 

			$pn = ob_get_contents();

			$this->_load_lang_constants();
			$pn = $this->_process_constants($pn);

			ob_end_clean();
			echo $pn;
			exit;
		}
		
		public function OnAddToOrder(){
			
			if($id = intval($_REQUEST['id'])){
				$count = intval($_REQUEST['count']);
				$order = intval($_REQUEST['order']);
				$this->_add_to_order($id, $count, $order);
			}
			exit;
		}
		public function OnDelCart(){
			unset($_SESSION['Trash']);
		}
		public function OnDelFromCart(){
			
			$cart = array();
			
			foreach($_SESSION['Trash']['cart'] as $key=>$prod){
				if( intval($_REQUEST['id']) != $key ){
					$cart[$key] = $prod;
				}else{
					$_SESSION['Trash']['count'] -= $prod['count'];
					$_SESSION['Trash']['sum'] -= $prod['price']*$prod['count'];
				}
			}
			$_SESSION['Trash']['cart'] = $cart;
			print_r($this->UrlParts);
			exit();
		}
		
		public function OnDelFromOrder(){
			
			$discount = 0;
			$data = explode('_',$_POST['id']);
			$prod_id = i($data[1]);
			$order = $this->_load_content_info('orders o','',
							'o.id = '.i($data[0]).' AND o.buyer_id = '.$this->UserAccount['id']);
			if($order && $order['state'] == 'new'){
				$cart = unserialize($order['cart']);
				$qty = $total = 0;
				foreach($cart as $id=>$product){
					if($id == $prod_id){
						unset($cart[$id]);
						continue;
					}else{
						$qty += $product['count'];
					}
				}
				$sql = "UPDATE orders 
						SET 
							ts = '".time()."',
							qty = ".$qty.",
							currency_id = '".$this->Currency."', 
							cart = '".serialize($cart)."',
							changed = 1 
						WHERE id = ".$order['id']."
				";
				Execute($sql);
			}
			$this->Order = $this->_load_content_info('orders o',
							'',
							'o.id = '.i($data[0]).' AND o.buyer_id = '.$this->UserAccount['id']);
			$this->Products = unserialize($this->Order['cart']);
			$this->Products_sz = sizeof($this->Products);
			include PATH_SNIPPETS.'order-cart-list.php';
			exit;
		}
		
		public function OnRefresh(){
			include PATH_SNIPPETS.'small_basket.php';
			exit;
		}
		
		public function OnRecalculate(){
			$this->recalculate();
			//$this->GoToUrl('/order.html');
		}
		
		public function OnRecalculateOrder(){
			$discount = 0;
			$data = explode('_',$_POST['id']);
			$id = i($data[1]);
			$count = intval($_POST['count']);
			$order = $this->_load_content_info('orders o',
							'',
							'o.id = '.i($data[0]).' AND o.buyer_id = '.$this->UserAccount['id']);
			if($order && $order['state'] == 'new'){
				$cart = unserialize($order['cart']);
				if($count && $cart[$id]){
					$cart[$id]['count'] = $count;
					foreach($cart as &$product){
						$qty += $product['count'];
					}
					$sql = "UPDATE orders 
							SET 
								ts = '".time()."',
								qty = ".$qty.",
								cart = '".serialize($cart)."',
								currency_id = '".$this->Currency."',
								changed = 1 
							WHERE id = ".$order['id']."
					";
					Execute($sql);
				}
			}
			$this->Order = $this->_load_content_info('orders o','',
							'o.id = '.i($data[0]).' AND o.buyer_id = '.$this->UserAccount['id']);
			$this->Products = unserialize($this->Order['cart']);
			$this->Products_sz = sizeof($this->Products);
			include PATH_SNIPPETS.'order-cart-list.php';
			exit;
		}
		
		public final function OnMakeOrder(){ 
			
			global $config;
			
			$this->Error = array();
			if(!$_POST['name']) $this->Error['name'] = true;
			if(!$_POST['phone']) $this->Error['phone'] = true;
			//if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			if(!$_SESSION['Trash']['cart']) $this->Error['cart'] = true;
			
			if($this->Error){
				$errors = array();
				foreach($this->Error as $el => $item){
					$errors[] = $el;
				}		
				if($_REQUEST['ajax']){
					echo json_encode( array(
							'success'	=> false,
							'errors'	=> $errors
						));							
					exit();	
				}	
			}else{
				
				$username = trim(implode(' ', array($_POST['surname'],$_POST['name'],$_POST['patronymic'])));
				
				/*if($this->UserAccount && ($this->UserAccount['phone'] != $_POST['phone'] || $this->UserAccount['adres'] != $_POST['message'])){
					$sql = 'UPDATE users
							SET phone = \''.s($_POST['phone']).'\',
								address = \''.s($_POST['message']).'\'';
				}*/
				
				$sql = "INSERT INTO orders 
						SET 
							buyer_id = ".i($this->UserAccount[id]).",
							ts = '".time()."',
							qty = '".i($_SESSION['Trash']['count'])."',
							discount = '".f($_SESSION['Trash']['discount'])."',
							total = '".f($_SESSION['Trash']['sum'])."',
							address = '".s($_POST['address'])."',
							address2 = '".s($_POST['address2'])."',
							name = '".s($username)."',
							phone = '".s($_POST['phone'])."',
							email = '".s($_POST['email'])."',
							`comment` = '".s($_POST['comment'])."',
							type='".($_POST['dostavka'] == 1 ? 'Доставка' : 'Самовывоз товара')."',
							pay = '".s($_POST['pay'])."',
							currency_id = '".$this->Currency."', 
							cart = '".serialize($_SESSION['Trash']['cart'])."'
				";
				
				Execute($sql);
				
				$order_id = GetOne('SELECT LAST_INSERT_ID()');
				
				AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				$user_data = '<p>'.
				'Имя пользователя: '.$username.'<br />'.
				'Email: '.$_POST['email'].'<br />'.
				'Телефон: '.$_POST['phone'].'<br />'.
				'Транспортировка: '.($_POST['dostavka'] == 1 ? 'Cпособ получения' : 'Самовывоз товара').'<br />'.
				'Адрес: '.$_POST['address'].'<br />'.
				'(заполняется, если необходима доставка): '.$_POST['address2'].'<br />'.
				'Коментарий: '.$_POST['comment'].'<br />'.
				'Форма оплаты: '.$_POST['pay'].'<br />'.
				'</p>';
				$cart_table = $this->generate_order_table($_SESSION['Trash']['cart']);
				$mail->AddVars(array(
									'order_id'		=> $order_id,
									'user_data'		=> $user_data,
									'price_table' => $cart_table,
									'message'	=> $_POST['message'],
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'order.html',
								'№'.$order_id.' '.$username,//$this->_get_system_variables('order_email_subject')
								$this->_get_system_variables('noreply_email'), 
								$this->_get_system_variables('sender_name')
							);
				if($this->ValidEmail($_POST['email'])) {
					$mail2 = new Mailer();
					$mail2->AddRecepient($_POST['email']);
					$mail2->AddVars(array(
									'order_id'		=> $order_id,
									'user_data'		=> $user_data,
									'price_table' => $cart_table,
									'message'	=> $_POST['message'],
									'date'		=> date('d:m:Y H:i',time())								
					));
					$mail2->Send(	'order_consumer.html',
									$this->_get_system_variables('email_subject_consumer'),
									$this->_get_system_variables('noreply_email'), 
									$this->_get_system_variables('sender_name')
								);
				}
				unset($_SESSION['Trash']);
				
				if($_REQUEST['ajax']){
					echo json_encode( array(
						'success'	=> true,
						'callback'	=> 'OrderSuccess',
						'text'	=> 'Спасибо, '.s(trim(implode(' ',array($_POST['surname'],$_POST['name'],$_POST['patronymic'])))).', за Ваш заказ. № заказа - '.$order_id
					));
					exit;
				}else{
					setcookie("cart","",time()-60*60,"/");
					$this->GoToUrl('/order.html?Success=true');	
				}	
			}
		}
		
		public function OnSaveOrder(){
			
			$order = GetRow('SELECT * FROM orders WHERE id = '.i($_GET['id']).' AND buyer_id = '.$this->UserAccount['id']);
			
			if($order){
				Execute('UPDATE orders SET changed = 0 WHERE id='.i($order['id']));
				AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				$user_data = '<p>'.
				'Имя пользователя:'.$order['name'].'<br />'.
				'Email: '.$order['email'].'<br />'.
				'Телефон: '.$order['phone'].'<br />'.
				'Адрес: '.$order['address'].'<br />'.
				'</p>';
				$cart_table = $this->generate_order_table(unserialize($order['cart']));
				$mail->AddVars(array(
									'order_id'		=> $order['id'],
									'user_data'		=> $user_data,
									'price_table' => $cart_table,
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'order.html',
								'Заказ отредактирован №'.$order['id'].' '.$username,//$this->_get_system_variables('order_email_subject')
								$this->_get_system_variables('noreply_email'), 
								$this->_get_system_variables('sender_name')
							);
						
				$mail2 = new Mailer();
				$mail2->AddRecepient($_POST['email']);
				$mail2->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' => $cart_table,
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail2->Send(	'order_consumer.html',
								'Ваш заказ отредактирован №'.$order['id'],
								$this->_get_system_variables('noreply_email'), 
								$this->_get_system_variables('sender_name')
							);
			}
			$this->GoToUrl('/user-account/orders.html');
		}
		
		public function OnDeleteOrder(){
		// _debug($_SESSION, 1);
			global $config;
		
			$order_id = i($_GET['order_id']);
			$order = GetRow("SELECT * FROM `orders` WHERE `id` = '$order_id' AND `buyer_id` = ".$this->UserAccount['id']);
			$us= GetRow("SELECT * FROM `users` WHERE `id` = ".$this->UserAccount['id']);
			if($order){
				
				$steyt_del=$us['product_name'];
				$steyt_del_arr=explode(';',$steyt_del);
				if(is_array($steyt_del_arr) && $steyt_del_arr[0] ){
					foreach($steyt_del_arr  as $key=>$val){
						$v=array();
						$v=explode('/',$val);
						$steyt_del_arr_sort[$v[0]]=$v[1];
						
					}
					if($steyt_del_arr_sort[$order['state']]){
						$v=array();
						$v=explode(':',$steyt_del_arr_sort[$order['state']]);
						$v[0]=$v[0]+1;
						$v[1]=$v[1]+$order['total'];
						$steyt_del_arr_sort[$order['state']]=$v[0].':'.$v[1];
					}else{
						$steyt_del_arr_sort[$order['state']]='1:'.$order['total'];
					}
					$ss='';
					foreach($steyt_del_arr_sort as $key=>$val){
						$ss.=$key.'/'.$val.';';
						
					}
					$ss=substr($ss,0,-1);
				}else{
					$ss=$order['state'].'/1:'.$order['total'];
				}
				
				
				
				$sql="UPDATE users 
							SET 
								product_name = '".$ss."'
				WHERE id=".$this->UserAccount['id']." ";
				Execute($sql);
			
				$DeletedFolder = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order['id'];
				RemoveDir($DeletedFolder);
				
				$sql = "DELETE FROM `orders` WHERE `id` = '$order_id' AND `buyer_id` = ".$this->UserAccount['id'];
				Execute($sql);
				
				if($_SESSION['Cur_order'] == $order_id){
					foreach($_SESSION['Trash']['cart'] as $product){
						$products_id_arr[] = $product['id'];
					}
					$products_id = implode(',', $products_id_arr);
					$sql = "DELETE FROM `products` WHERE `id` IN ($products_id)";
					Execute($sql);
					unset($_SESSION['Trash']);
					unset($_SESSION['Cur_order']);
				}
				
				// Запись в табилце order_info
				$sql="INSERT INTO  order_info 
							SET 
								user_id = '".$order['buyer_id']."'
								,order_id = '".$order['id']."'
								,state = '".$order['state']."'
								,qty = '".$order['qty']."'
								,total = '".$order['total']."'
								,ts = '".$order['ts']."'
				";
				//_debug($sql,1);
				Execute($sql);
				//
			}
			
			$this->GoToUrl($_SERVER['HTTP_REFERER']);
		}
		
		protected function _add_cart($id, $count){
			$product = $this->_load_content_info('products t',
												'',
												't.id = '.$id,
												't.*');
		
			if($product){
				if($_SESSION['Trash']['cart'][$product['id']]){
					$_SESSION['Trash']['cart'][$product['id']]['count']++;
				}else{
					$_SESSION['Trash']['cart'][$product['id']] = $product;
					$_SESSION['Trash']['cart'][$product['id']]['count'] = 1;
				}
				$_SESSION['Trash']['count']++;
				$_SESSION['Trash']['sum'] = $_SESSION['Trash']['sum']+$product['price']; 
			}
		}
		
		protected function _add_to_order($id, $count, $order_id){
			
			if(!$count) $count = 1;
			$product = $this->_load_content_info('products t',
												'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1',
												't.id = '.$id,
												't.id, t.category_id, t.title, t.price, t.discount, t.currency_id, t.code, t.`new`, IF(t.url != \'\',t.url,t.id) as url, p.filename');
			if($product){
				$order = $this->_load_content_info('orders o',
													'',
													'o.id = '.$order_id.' AND o.buyer_id = '.$this->UserAccount['id']);
				if($order && $order['state'] == 'new'){
					$cart = unserialize($order['cart']);
					if($cart[$product['id']]){
						$cart[$product['id']]['count'] += $count;
					}else{
						$product['discount'] = $this->calculate_discount($product);
						$cart[$product['id']] = $product;
						$cart[$product['id']]['count'] = $count;
					}
					
					$sql = "UPDATE orders 
							SET 
								ts = '".time()."',
								qty = qty + ".i($count).",
								currency_id = '".$this->Currency."', 
								cart = '".serialize($cart)."',
								changed = 1 
							WHERE id = ".$order['id']."
					";
					Execute($sql);
				}
			}
		}
		
		public function OnPreOrder(){
			//_debug($_POST,1);
			$ik_payment_id = i($_POST['ik_payment_id']);
			if($ik_payment_id){
				$sql = "SELECT * FROM `orders` WHERE `id` = '$ik_payment_id' AND `buyer_id` = '".$this->UserAccount['id']."'";
				$order = GetRow($sql);
			}
			
			if($order && $order['total'] >= $this->_get_system_variable('order_min_amount')){
				// if($_COOKIE['PHPSESSID'] == '4ksgpl8u8hl2kkkjotaaoj0an7'){ // убрать этот if
				if(isset($_POST['dobavljt']) &&  $_POST['dobavljt'] ){$courier= 1;}else{ $courier= 0;}
				
				$cart = $this->generate_order_path($order);
				
				if($cart){
					$sql = "UPDATE `orders` SET `cart` = '$cart' , `courieradd`='$courier' 	 WHERE `id` = '".$order['id']."'"; 
					Execute($sql);
				}
				// myFileRename('public/files/images/temp/1/for_copy/', 'public/files/images/temp/2/for_copy/');
				// }
				$this->GoToUrl('/user-account/orders/'.$order['id'].'/info.html');
			}
		}
		
		protected function OnOrder(){
			//_debug($_POST,1);
			//_debug($_POST['PaymentMethod'],1);

			$ik_payment_id = i($_POST['ik_payment_id']);
			$delivery_way = i($_POST['DeliveryMethod']);
			$delivery_option =  iconv('CP1251','UTF-8',($_POST['DeliveryOption']));
			
			if($delivery_way==1){
				$state_issuing='courier';
			}elseif($delivery_way==2){
				$state_issuing='n_post';
			}elseif($delivery_way==0){
				$state_issuing='lab';
			}else{
				$state_issuing='-';
			}
			/* if($_POST['DeliveryOption']){
				
			}else{
				$delivery_option = NULL;
			} */
			$payment_way = i($_POST['PaymentMethod']);
			if($payment_way ==5){
				$state_ord='wait_pay';
				//Реквизиты для оплаты : Карта 1123456789, получатель ФОП Косминский А.Ю. Заказ будет отправлен в работу после оплаты.
				$privatcartnumber=' Реквизиты для оплаты : Карта '.$this->_get_system_variable('privatcartnumber').', получатель ФОП Косминский А.Ю. Заказ будет отправлен в печать после оплаты.';
			}else{
				$state_ord='new';
				$privatcartnumber='';
			}
			
			if($ik_payment_id){
				$sql = "SELECT * FROM `orders` WHERE `id` = '$ik_payment_id' AND `buyer_id` = '".$this->UserAccount['id']."'";
				$order = GetRow($sql);
			}
			//_debug($order,1);
			
			//exit('fff');
			if($order && ($order['total'] >= $this->_get_system_variable('order_min_amount'))){
				$adres_user= $this->UserAccount['city'].', '.$this->UserAccount['street'].', '.$this->UserAccount['house'].', '.$this->UserAccount['apartment'];
				$sql = "UPDATE `orders` 
						SET 
							`state` = '$state_ord', 
							`address` = '$adres_user', 
							`state_issuing` = '$state_issuing', 
							`delivery_way` = '$delivery_way', 
							`delivery_option` = '$delivery_option', 
							`payment_way` = '$payment_way' 
						WHERE `id` = '".$order['id']."'";
				
				if(Execute($sql)){
					//$photo_cnt_old=GetOne("SELECT photo_cnt FROM `users` WHERE `id` = '".$this->UserAccount['id']."'");
					//$ph_cnt=$photo_cnt_old+$order['qty'];
					/* if($ph_cnt>=300){
						$sql = "UPDATE `users` 
						SET 
							`photo_cnt` = $ph_cnt, 
							`pricenamber` = '2'
						WHERE `id` = '".$this->UserAccount['id']."'";
					}else{
						$sql = "UPDATE `users` 
						SET 
							`photo_cnt` = $ph_cnt 
						WHERE `id` = '".$this->UserAccount['id']."'";
					}
					Execute($sql); */
				
					
					//$DeletedFolder = '/public/files/images/orders/'.$order['id'].'/thumb';
					//RemoveDir($_SERVER['DOCUMENT_ROOT'].$DeletedFolder);
					cleanDir($_SERVER['DOCUMENT_ROOT'].'/public/files/images/orders/'.$order['id']);
					$order['delivery_way'] = $delivery_way;
					$order['address'] = $adres_user;
					$order['delivery_option'] = $delivery_option;
					$order['payment_way'] = $payment_way;
					$this->SendMessages($order);
					
					// Отправка sms 
					if(i($order['total'])>i($this->_get_system_variable('min_sms')) AND $order['phone'] ){
						
						require_once PATH_LIB."/SMSSender/smssender.php";
						$sms2 = new SMSSender();
							$string = $this->_get_system_variable('delivery_methods');
							$delivery_methods = explodeMultiString($string);
							$string = $this->_get_system_variable('payment_methods');
							$payment_methods = explodeMultiString($string);
							
							
							 if($order['delivery_option'] ){
								if($delivery_methods[$order['delivery_way']]['options'][0]== 'newpost'){
									$deliv_option=$order['delivery_option'];
								}else{
									$deliv_option=$delivery_methods[$order['delivery_way']]['options'][$order['delivery_option']];
								}
							}else{
								$deliv_option=$order['address2'];
							} 
							
							if($delivery_way==2){
								$dw='со склада у курьерской компании Новая почта';
							}else{
								$dw=$delivery_methods[$order['delivery_way']]['item'];
							}
							
							
							$text = 'Вы сделали заказ №'.$order['id'].'.
							Сумма заказа: '.$order['total'].'грн.
							Cпособ получения: '.$dw.'.
							Оплата: '.$payment_methods[$order['payment_way']]['item'].'.'.$privatcartnumber;
							//_debug($text,1);
							$sms2->registerSender('Foto-Servis','ua');
							$ph=$order['phone'];

							$phd= str_replace('+38','',$ph);
						$phd=substr($phd,0,10);
						
						$phone =  "38".$phd;
						
						if ($order['buyer_id']!=7)$sms2->sendSMS($phone,$text);
					} 
					
					// end Отправка sms 
					
					unset($_SESSION['Trash']);
					unset($_SESSION['Cur_order']);
					// для оплаты через банк
					if($payment_way==3){
						//ini_set('display_errors',1); error_reporting(E_ALL);
						$this->BankOrder['id_order_bank']=$order['id'];
						$this->BankOrder['total_order_bank']=$order['total'];
						//$this->BankOrder['total_order_bank']=0.3;
						$this->BankOrder['desc_order_bank']='photo';
						$this->BankOrder['phone_order_bank']=$this->_get_system_variable('phone_order_bank');
						$this->BankOrder['url_order_req']=$this->RootUrl.'otvet/fff';
						$this->SetHeader('');
						$this->SetTemplate('bank_form.php');
						$this->SetFooter('');
					}elseif($payment_way==4){
						//ini_set('display_errors',1); error_reporting(E_ALL);
						$this->BankOrder['id_order_bank']=$order['id'];
						$this->BankOrder['total_order_bank']=$order['total'];
						//$this->BankOrder['total_order_bank']=0.3;
						$this->BankOrder['desc_order_bank']='photo';
						$this->BankOrder['phone_order_bank']=$this->_get_system_variable('phone_order_bank');
						$this->BankOrder['url_order_req']=$this->RootUrl.'otvet/fff';
						$this->SetHeader('');
						$this->SetTemplate('privat24_form.php');
						$this->SetFooter('');
								
					}else{
						$this->GoToUrl('/user-account/orders.html');		
					}				
					//			
				}else{
					die  ('Error: SQL');
				}
			}else{
			   die  ('Error: ');
			}
		}
		
		private function SendMessages($order){//_debug($order, 0);
		
			AttachLib('Mailer');
			$mail = new Mailer();

			$sql = "SELECT * FROM manager_emails WHERE online_order = '1'";
			$row = GetAll($sql);
			
			for($i = 0, $count = sizeof($row); $i < $count; $i++){
				$mail->AddRecepient($row[$i]['email']);
				
				
			}
			
			$string = $this->_get_system_variable('delivery_methods');
			$delivery_methods = explodeMultiString($string);
			$string = $this->_get_system_variable('payment_methods');
			$payment_methods = explodeMultiString($string);
			
			// $message = iconv("cp1251", "utf-8", $order['comment']);
			$message = $order['comment'];
			if($message == $this->_get_lang_constant('FORM_COMMENT_TEXT')){
				$message = '';
			}
			$plus_delivery=' ';
			$tf=false;
			 if($order['delivery_option'] ){
				if($delivery_methods[$order['delivery_way']]['options'][0]== 'newpost'){
					$deliv_option=$order['delivery_option'];
					if($order['total']<200){ 
						$plus_delivery = ' + стоимость доставки курьерской службой "Новая Почтя" ';
						$tf=true;
					}
				}else{
					$deliv_option=$delivery_methods[$order['delivery_way']]['options'][$order['delivery_option']];
				}
			}else{
				$deliv_option=$order['address2'];
			} 
			/* _debug($order);
			_debug($delivery_methods,1); */
			if($order['payment_way'] ==5){
				$privatcartnumber='<br/> Номер нашей карты ПриватБанк : '.$this->_get_system_variable('privatcartnumber').', получатель ФОП Косминский А. Ю.<br> После поступления денег на наш счет, заказ будет отправлен в работу.';
			}else{
				$privatcartnumber='';
			}
		
			if($order['payment_way'] ==6){
				$nal_rasch=' наличный расчет в отделении ';
			}else{
				$nal_rasch='';
			}
			$user_data = '<p>'.
			'Имя пользователя: '.$order['name'].'<br />'.
			'Email: '.$order['email'].'<br />'.
			'Телефон: '.$order['phone'].'<br />'.
			// 'Транспортировка: '.($_POST['dostavka'] == 1 ? 'Доставка' : 'Самовывоз товара').'<br />'.
			'Адрес(пользователя): '.$order['address'].'<br />'.
			//'Адрес(доставки ): '.$order['address2'].'<br />'.
			//'Доставка: '.$delivery_methods[$order['delivery_way']]['item'].($order['delivery_option'] ? ' ('.$delivery_methods[$order['delivery_way']]['options'][$order['delivery_option']].')':'').'<br />'.
			//'Cпособ получения: '.$delivery_methods[$order['delivery_way']]['item'].' <br /> ( '.($deliv_option).' ) <br />'.
			'Cпособ получения: '.$delivery_methods[$order['delivery_way']]['item'].' <br /> '.
			// 'Адрес доставки (заполняется, если необходима доставка): '.$_POST['address2'].'<br />'.
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$_POST['address2'].'<br />'.
			// 'Коментарий: '.$_POST['comment'].'<br />'.
			// 'Форма оплаты: '.$_POST['pay'].'<br />'.
			'Оплата: '.($nal_rasch? $nal_rasch : $payment_methods[$order['payment_way']]['item'].' '.$plus_delivery.' '.$privatcartnumber).'<br />'.
			'</p>';
			// _debug($user_data, 1);
			// $cart_table = $this->generate_order_table($_SESSION['Trash']['cart']); 
			
			$cart_table = $this->generate_order_table(unserialize($order['cart']),$tf);
			// _debug($_SESSION);
			// _debug($cart_table, 1);
			$mail->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' => $cart_table,
								'message'	=> $message,
								'date'		=> date('d:m:Y H:i',time())								
			));
			$mail->Send(	'order.html',
							'Заказ № '.$order['id'].' на сайте '.$this->_get_lang_constant('SITE_NAME'),//$this->_get_system_variable('order_email_subject')
							$this->_get_system_variable('noreply_email'), 
							$this->_get_system_variable('sender_name')
						);
			if($this->ValidEmail($order['email'])) {
				
				
				$mail2 = new Mailer();
				$mail2->AddRecepient($order['email']);
				$mail2->AddVars(array(
								'order_id'		=> $order['id'],
								'user_data'		=> $user_data,
								'price_table' 	=> $cart_table,
								'message'	=> $message,
								'date'		=> date('d:m:Y H:i',time())								
				));
				$mail2->Send(	'order_consumer.html',
								$this->_get_system_variable('email_subject_consumer'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
			}
		}
		
		private function generate_order_table($cart,$add_np=false){//_debug($cart, 0);
			
			$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
			// _debug($this->Chars, 0);
			
			$res ='
			<style type="text/css">
			.trash-list TD,
			.trash-list TH
			{
				padding:5px;
			}
			</style>

			<table  cellpadding="2" cellspacing="2" class="trash-list" width="100%">
					
					<tr class="table-title">
						<th style="background:#D0D0D0;width:190px;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Формат</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Название</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Кол-во/шт.</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Цена</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC" class="last">Стоимость</th>
					</tr> ';
			$count = $total = $price = $discount = 0;
			foreach($cart as $prod){
				$photo_print_size='';
				foreach($this->Chars['all'][0]['items'] as $option){
						//_debug($option);
					if($option['sizes'] ){
						foreach($option['variants'] as $variant){
							//_debug($variant);
							//_debug($prod['options'],1);
							if($variant['id'] == $prod['options'][$option['id']]){
								$photo_print_size .=' '.$variant['title'];
							}
						}
						// $photo_print_size = $option['variants'][$prod['options'][$option['id']]]['title'];
					}
				}
				// _debug($photo_print_size, 1);
			
				$price = $prod['price'];
				$prod['price']*=$prod['count'];
				$prod['discount']*=$prod['count'];
				$res.='	<tr>
						<td class="image" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">'.$photo_print_size.'</td>
						<td class="name" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$prod['catalog'].' <strong>'.$prod['img'].'</strong></p>
						</td>
						<td class="count" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$prod['count'].'</p>
						</td>
						<td class="price" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">
							<p>'.$this->_show_price($price,$prod['currency_id']).'</p>							
						</td>
						<td class="price" style="border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC">
							<p>'.$this->_show_price($prod['price'],$prod['currency_id']).'</p>							
						</td>
					</tr>';
				$count += $prod['count'];
				$total += $prod['price']-$prod['discount'];
			}
			
			$res.='	<tr class="summary">
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">Итого:</th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;"></th>
						<th style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;">'.$count.' шт.</th>
						<th colspan="2" style="background:#D0D0D0;border-left:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC">'.$this->_show_price($total).($add_np?' + стоимость доставки курьерской службой "Новая Почтя" ':'').''.($_SESSION['Trash']['dopol']?' + '.$_SESSION['Trash']['dopol']:'').'  грн</th>
					</tr>';
			//exit($res);
			return $res;
		}
		
		private function generate_order_path($order){
		
			// _debug($order, 0, '$order');
			
			$cart = unserialize($order['cart']);
			// _debug($cart, 0, '$cart');
			
			$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
			// _debug($this->Chars,1);
			$pdir=PATH_PUBLIC_FILES_IMAGES.'orders/'.$order['id'];
			$files = array_diff(scandir($pdir), array('.','..')); 
    			foreach ($files as $file) { 
			
					if(is_dir("$pdir/$file") && $file!='thumb') {
							
						$nfiles = array_diff(scandir("$pdir/$file"), array('.','..')); 
							foreach ($nfiles as $nfile) {
							unlink("$pdir/$file/$nfile");	 
						}
						 rmdir("$pdir/$file"); 
					} 
    			} 
    			   
			
			foreach($cart as &$photo){
				$prefix = 'public/files/images/orders/';
				// $prefix = '/';
				//$size_title = $this->get_prod_option_field($photo, $this->Chars, 'sizes', 'title'); //OLD
				//$size_title = $this->get_prod_option_field($photo, $this->Chars,1, $photo['options'][1],'title'); 
				$size_opt_id=$this->Optionsize[$this->UserAccount['pricenamber']];
				$size_title = $this->get_prod_option_field($photo, $this->Chars,$size_opt_id, $photo['options'][1],'title'); 
				//$size_title = $this->get_prod_option_field($photo, $this->Chars,1, $photo['options'][1],'title'); 
				//$this->Optionsize[$this->UserAccount['pricenamber']]
				/* if( empty($size_title)){
					$size_title = $this->get_prod_option_field($photo, $this->Chars,6, $photo['options'][6],'title'); 
				} */
				$korekcij = $this->get_prod_option_field($photo, $this->Chars,5, $photo['options'][5],'title'); 
				$kadrirov = $this->get_prod_option_field($photo, $this->Chars,2, $photo['options'][2],'title'); 
				$size_separator = $this->_get_system_variable('photo_size_separator');
				$size_title_x = str_replace($size_separator, 'x', $size_title);
				
				$size_type_array = explode(" ", $size_title_x); 
			/* 	_debug($size_title);
				_debug($kadrirov);
				_debug($korekcij);
				_debug($size_type_array); */
				switch($size_type_array[1]){
					case 'глянец':
					case 'глянцевая':
						$paper_symbol = 'gl';
						break;
					case 'мат':
					case 'матовая':
						$paper_symbol = 'mat';
						break;
					case 'метталлик':
					case 'металлик':
						$paper_symbol = 'met';
						break;
					case 'шёлк':
						$paper_symbol = 'sholk';
						break;
					default:
						$paper_symbol = '_type_not_defined';
						break;
				}
				$size_type=$size_type_array[0].$paper_symbol;
				
				$korek_array = explode(" ", $korekcij); 
				switch($korek_array[0]){
					case 'с':
						$kor = 'skor';
						break;
					case 'без':
						$kor = 'bezkor';
						break; 
					default:
						$kor = '_type_not_defined';
						break;
				}
				$kadrirov_array=explode(" ", $kadrirov); 
				//_debug($kadrirov_array,1);
				switch($kadrirov_array[1]){
					case 'формат':
						$kadr = 'polform';
						break;
					case 'кадр':
						$kadr = 'polkadr';
						break;
					case 'масштабирования':
						$kadr = 'bez_mashtabir';
						break; 
					case 'размер':
						$kadr = 'realraz';
						break; 
					default:
						$kadr = '_type_not_defined';
						break;
				}
				
				
				//$size_separator = $this->_get_system_variable('photo_size_separator');
				//$size_title_x = str_replace($size_separator, 'x', $size_title);
				// _debug($size_title_x);
				//$paper_title = $this->get_prod_option_field($photo, $this->Chars, 'paper', 'title');
				//$paper_title_low = mb_convert_case($paper_title, MB_CASE_LOWER, "UTF-8");
				// _debug($paper_title);
				/* switch($paper_title_low){
					case 'глянцевая':
						$paper_symbol = 'g';
						break;
					case 'матовая':
						$paper_symbol = 'm';
						break;
					default:
						$paper_symbol = '_type_not_defined';
						break;
				} */
				// $photo['img'] = ''; /*исключаем название фото из пути*/
				// $photo['path'] = $prefix.$order['id'].'/'.$size_title_x.$paper_symbol.'/'.'x'.$photo['count'].'/'.$photo['img'];
				//$photo['path'] = $prefix.$order['id'].'/'.$size_title_x.$paper_symbol.'/'.'x'.$photo['count'].'/';  //old
				$photo['path'] = $prefix.$order['id'].'/'.$photo['count'].'_'.$size_type.'_'.$kor.'_'.$kadr.'/';  
				//_debug($photo['path'] ,1);
				// myFileRename('public/files/images/temp/1/for_copy/', 'public/files/images/temp/2/for_copy/');
				myFileCopy('public/files/images/orders/'.$order['id'].'/', $photo['path'], $photo['img']);
			}
			// _debug($cart, 1, '$cart');
			return serialize($cart);
		}
		
	}

?>
