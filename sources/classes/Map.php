<?

	class Map extends ContentPage {
		
		public $Map				= array();
		public $News			= array();
		public $Makers			= array();
		public $Collections		= array();
		/*
		 * Protected properties
		 */
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			
			$this->SetTemplate('map.php');
			//$this->Title('Карта сайта');
			$this->_load_content();
			$this->Map = $this->_get_map();
			$this->News = $this->_get_news($table = 'news', $href = '/news-view-');
			$parent_id_maker = GetOne("SELECT `id` FROM `pages` WHERE TRIM(`title`) = 'Производители'");
			$this->Makers = $this->_get_makers($table = 'pages', $href = '/catalog.html?maker=', $where =' AND `parent_id` = \''.$parent_id_maker.'\' ');
			$parent_id_coll = GetOne("SELECT `id` FROM `pages` WHERE TRIM(`title`) = 'Коллекции'");
			$this->Collections = $this->_get_makers($table = 'pages', $href = '/catalog.html?сollection=', $where =' AND `parent_id` = \''.$parent_id_coll.'\' ');
			
			$this->AddTitle('Карта сайта');
		}
		
		private function _get_makers($table, $href, $where=''){
			global $DB;
			$sql = 'SELECT id, 
						   title, url 
					FROM `'.$table.'` 
					WHERE  
							 publish=\'1\' 
							 '.$where;
			//echo $sql; exit;
			$new = $DB->GetAll($sql);
			//_debug($new);
			$res_sz = count($new);
			
			for($i = 0; $i < $res_sz; $i++) {
					$new[$i]['href'] =  $href . $new[$i]['id'] ;
			}
			return  $new;
		}
 	
		
		public function OutputTree($tree) {
			
			$sz = count($tree);
			
			if($sz) {
				
				echo '<ul>';
				
				for($i = 0; $i < $sz; $i++) {
					
					echo '<li><a href="'.$tree[$i]['href'].'.html">'.$tree[$i]['title'].'</a>';
					
					if($tree[$i]['child_nodes_sz']) {
						
						$this->OutputTree($tree[$i]['child_nodes']);
					}
					
					echo '</li>';
				}
				
				echo '</ul>';
			}
		}
		
		/*
		 * Private methods
		 */
		
		public function _get_map($parent_id = 0, $href = '') {

			global $DB;
			
			$sql = 'SELECT id, 
						   title, 
						   parent_id, 
						   url 
					FROM pages 
					WHERE parent_id=\''.$parent_id.'\' AND							
						  publish=\'1\' AND (
						  title <> \'Производители\' AND
						  title <> \'Коллекции\' )
						  ';
			$res = $DB->GetAll($sql);
			
			$res_sz = count($res);
			
			for($i = 0; $i < $res_sz; $i++) {

				
				 	if (strpos($res[$i]['url'], 'http://')!==false) 
				 	  	$res[$i]['href'] = $res[$i]['url'];
				 	else 
				 	  	$res[$i]['href'] = $href.'/'.$res[$i]['url'];
				
				
				
				$res[$i]['child_nodes'] = $this->_get_map($res[$i]['id'], $res[$i]['href']);
				$res[$i]['child_nodes_sz'] = count($res[$i]['child_nodes']);
			}
		
			return $res;
		}	
		
		
			
		protected function _load_content() {
			
			global $DB;
			
			$sql = 'SELECT p.* 
					FROM pages p
					WHERE p.publish=\'1\' AND 
						  
						  p.url=\'sitemap\' ';
			$this->PageInfo = $DB->GetRow($sql);
			$this->PageInfo['content_parts'] = explode('{[Sitemap]}', $this->PageInfo['content']);
			$this->SetTitle($this->PageInfo['page_title']);
			$this->SetMetaDescription($this->PageInfo['meta_description']);
			$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
		}
		
		protected function _get_news($table = '', $href = ''){
		global $DB;
		$sql = 'SELECT id, 
						   title 
			 
					FROM `'.$table.'` 
					WHERE  
							 publish=\'1\' 
							 ORDER BY publish_ts DESC
					LIMIT 10		 ';
			//echo $sql; exit;
			$new = $DB->GetAll($sql);
			//_debug($new);
			$res_sz = count($new);
			
			for($i = 0; $i < $res_sz; $i++) {
				
				if ($new[$i]['url']) {
				 	if (strpos($new[$i]['url'], 'http://')!==false) 
				 	  	$new[$i]['href'] = $new[$i]['url'];
				 	else 
				 	  	$new[$i]['href'] = $href . $new[$i]['url'] ;
				
				}else{
					$new[$i]['href'] =  $href . $new[$i]['id'] ;
				}
				
			}
			return  $new;
		}
		
	
		
	}

?>