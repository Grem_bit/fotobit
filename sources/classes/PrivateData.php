<?

	class PrivateData extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		protected $Message				= '';
		protected $TemplatesBaseDir		= 'user-account/';
		protected $CatIdList			= array();
		protected $Orders				= array();
		protected $ErrorsUser 			= array();
		protected $OrderStatus			= array('wait_pay'=>'Ожидает оплаты','inprint' => 'В печати','printed' => 'Напечатан','made' => 'Выполнен','rejected' => 'Отклонен','rejected_user' => 'Отклонен пользователем','not_order' => 'Не оформлен','sent'=> 'Отправлен','took'=> 'Забрали','not_taken'=> 'Не Забрали', 'new' => 'Новый', 'paid' => 'Оплачен', 'delivered' => 'Доставлен', 'cancelled' => 'Отменен', 'wait' => 'Ожидание');
		
		/*
		 * Public methods
		 */
		public function OnDelFromCart() {
			global $config;
			$pid = i($_REQUEST['id']);
			$order_id = $_SESSION['Cur_order'];
			$path = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR;
			if($_SESSION['Trash']['cart'][$pid]['img']){unlink($path.$_SESSION['Trash']['cart'][$pid]['img']);}
			if($_SESSION['Trash']['cart'][$pid]['img']){unlink($path."thumb".DIRECTORY_SEPARATOR.$_SESSION['Trash']['cart'][$pid]['img']);}
			$_SESSION['Trash']['Count'] -= $_SESSION['Trash']['cart'][$pid]['count'];
			$_SESSION['Trash']['Sum'] -= $_SESSION['Trash']['cart'][$pid]['price']*$_SESSION['Trash']['cart'][$pid]['count'];
			unset($_SESSION['Trash']['cart'][$pid]);
			if(!$_SESSION['Trash']['Count']) {
				RemoveDir($path);
				Execute("DELETE FROM orders WHERE id = '".$_SESSION['Cur_order']."'");
				unset($_SESSION['Trash']);
				unset($_SESSION['Cur_order']);
				$result['hide'] = 1;
			}
			Execute("DELETE FROM products WHERE id = '".$pid."'");
			Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");				
			$result['sum'] = number_format($_SESSION['Trash']['Sum'],2);
			$this->GoToUrl("/".implode("/",$this->UrlParts));
			exit();
		}
		
		public function OnBeforeDisplay() {
			global $config, $DB;
			
			$this->Breadcrumbs[] = array('url'=>'user-account','title' => 'Личный кабинет');
			$this->UserInfo = GetRow('	SELECT u.* 
										FROM users u
										WHERE u.id = \''.$this->UserAccount['id'].'\'');
			//if(count($this->UrlParts)<3){
				switch ( $this->UrlParts[1] ){
				
				
				case 'data':
					$this->Breadcrumbs[] = array('title' => 'Личные данные');
					$this->AddTitle('Личные данные');
					$this->Tpl = 'main-info.php';	
				break;
				
				case 'change-password':
					$this->Breadcrumbs[] = array('title' => 'Изменить пароль');
					$this->AddTitle('Изменить пароль');
					$this->Tpl = 'change-password.php';	
				break;
				
				case 'choice':
					$this->Breadcrumbs[] = array('title' => 'Выбор продукта');
					
					// $this->AddJS('/public/js/photoUpload.js?1');
					//$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1'); 
					
					$this->AddTitle('Загрузить фотографии');
					$this->Tpl = 'choice.php';
					
				break;
				case 'upload':
				
					$this->Breadcrumbs[] = array('title' => 'Загрузить фотографии');
					
					if(!$_SESSION['Cur_order']) $this->_load_last_order();
					//_debug($this->UserInfo,1);
					
					$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
					//_debug($this->Chars,1);
					if($_SESSION['Cur_order']) $this->Photos = $_SESSION['Trash']['cart'];
					
					$this->AddTitle('Загрузить фотографии');
					$this->AddJS('/public/js/photoUpload.js?1');
					$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1');
					$this->Tpl = 'photo-loader.php';	
				break;
				
				default:
					
					
					$this->Breadcrumbs[] = array('url'=>'orders','title' => 'Мои заказы');
					$this->AddTitle('Мои заказы');
					if($order = i($this->UrlParts[2])){
						
						//if($_GET['alex']){_debug($this->UrlParts,1);}
						$this->AddTitle( (count($this->UrlParts)==4)?'Шаг 3 из 3 Заказ №'.$order : 'Шаг 2 из 3 Заказ №'.$order);
						$this->Breadcrumbs[] = array('title' => 'Заказ №'.$order, 'url' => $order);
						$this->_get_order($order);
						$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
						if($this->UrlParts[3] == 'add'){
							$this->Breadcrumbs[] = array('title' => 'Добавить', 'url' => 'add');
							$this->Tpl = 'order-add.php';
						}elseif($this->UrlParts[3] == 'info'){
							$this->Tpl = 'order-info.php';
						}else{
							if(!$_SESSION['Cur_order']) $this->_load_last_order();
							$this->Tpl = 'order.php';
						}
					}else{
						$this->_get_user_orders();
						$this->Tpl = 'orders.php';	
					}
					$this->AddCSS('/public/css/fileuploader.css');
				break;
				}
			//}
			/*
			if(count($this->UrlParts)>2){
				//_debug($_SESSION,1);
				switch ( $this->UrlParts[1] ){
				case 'photo':
					if( isset( $_SESSION['order_type'])){					   
					   if ($_SESSION['order_type']!='photos'){
							$_SESSION['order_type']='photos';
							unset($_SESSION['Trash']);
							unset($_SESSION['Cur_order']);
					   }
					}else{
						$_SESSION['order_type']='photos';
						unset($_SESSION['Trash']);
						unset($_SESSION['Cur_order']);
					}
					//_debug($_SESSION,1);
					
								
					$this->Breadcrumbs[] = array('title' => 'Загрузить фотографии');
					if(!$_SESSION['Cur_order']) $this->_load_last_order($type='photos');
					$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
					if($_SESSION['Cur_order']) $this->Photos = $_SESSION['Trash']['cart'];
					$this->AddTitle('Загрузить фотографии');
					$this->AddJS('/public/js/photoUpload.js?1');
					$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1');
					$this->Tpl = 'photo-loader.php';	
				break;
				case 'canvas':
						// _debug($this->AuthData['session'],1);
					if( isset( $_SESSION['order_type'])){
					   if ($_SESSION['order_type']!='canvas'){
							$_SESSION['order_type']='canvas';
							unset($_SESSION['Trash']);
							unset($_SESSION['Cur_order']);
					   }
					}else{
						$_SESSION['order_type']='canvas';
						unset($_SESSION['Trash']);
						unset($_SESSION['Cur_order']);
					}
					//_debug($_SESSION,1);
					
					$this->Breadcrumbs[] = array('title' => 'Загрузить фотографии');
					if(!$_SESSION['Cur_order']) $this->_load_last_order($type='canvas');
					$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE canvas = 1"));
					if($_SESSION['Cur_order']) $this->Photos = $_SESSION['Trash']['cart'];
					$this->AddTitle('Загрузить фотографии');
					$this->AddJS('/public/js/photoUpload.js?1');
					$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1');
					$this->Tpl = 'photo-loader.php';	
				break;
				case 'photobook':
					if( isset( $_SESSION['order_type'])){
					   if ($_SESSION['order_type']!='photobook'){
					   $_SESSION['order_type']='photobook';
							unset($_SESSION['Trash']);
							unset($_SESSION['Cur_order']);
					   }
					}else{
						unset($_SESSION['Trash']);
						unset($_SESSION['Cur_order']);
						$_SESSION['order_type']='photobook';
					}
					
					
					
					$this->Breadcrumbs[] = array('title' => 'Загрузить фотографии');
					if(!$_SESSION['Cur_order']) $this->_load_last_order($type='photobook');
					$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photobook = 1"));
					if($_SESSION['Cur_order']) $this->Photos = $_SESSION['Trash']['cart'];
					$this->AddTitle('Загрузить фотографии');
					$this->AddJS('/public/js/photoUpload.js?1');
					$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1');
					$this->Tpl = 'photo-loader.php';	
				break;
				case 'souvenirs':
					 
					if( isset( $_SESSION['order_type'])){
					   if ($_SESSION['order_type']!='souvenirs'){
							$_SESSION['order_type']='souvenirs';
							unset($_SESSION['Trash']);
							unset($_SESSION['Cur_order']);
					   }
					}else{
						unset($_SESSION['Trash']);
						unset($_SESSION['Cur_order']);
						$_SESSION['order_type']='souvenirs';
					}
					
					$this->Breadcrumbs[] = array('title' => 'Загрузить фотографии');
					if(!$_SESSION['Cur_order']) $this->_load_last_order($type='souvenirs');
					$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE souvenirs = 1"));
					if($_SESSION['Cur_order']) $this->Photos = $_SESSION['Trash']['cart'];
					$this->AddTitle('Загрузить фотографии');
					$this->AddJS('/public/js/photoUpload.js?1');
					$this->AddJS('/public/js/fileuploader.js?1');
					$this->AddCSS('/public/css/fileuploader.css?1');
					$this->Tpl = 'photo-loader.php';	
				break;
				default:
				
					$this->Breadcrumbs[] = array('url'=>'orders','title' => 'Мои заказы');
					$this->AddTitle('Мои заказы');
					if($order = i($this->UrlParts[2])){
						//if($_GET['alex']){_debug($this->UrlParts,1);}
						$this->AddTitle( (count($this->UrlParts)==4)?'Шаг 3 из 3 Заказ №'.$order : 'Шаг 2 из 3 Заказ №'.$order);
						$this->Breadcrumbs[] = array('title' => 'Заказ №'.$order, 'url' => $order);
						$this->_get_order($order);
						$this->Chars = $this->_get_cat_options(GetOne("SELECT id FROM categories WHERE photos = 1"));
						if($this->UrlParts[3] == 'add'){
							$this->Breadcrumbs[] = array('title' => 'Добавить', 'url' => 'add');
							$this->Tpl = 'order-add.php';
						}elseif($this->UrlParts[3] == 'info'){
							$this->Tpl = 'order-info.php';
						}else{
							if(!$_SESSION['Cur_order']) $this->_load_last_order();
							$this->Tpl = 'order.php';
						}
					}else{
						$this->_get_user_orders();
						$this->Tpl = 'orders.php';	
					}
					$this->AddCSS('/public/css/fileuploader.css');
				break;
				}
			} */
			$sql = 'SELECT p.* 
					FROM pages p 
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.end($this->UrlParts).'\'';
			
			$this->PageInfo = $DB->GetRow($sql);
			$this->SetTemplate('main.php');
		}

		
					
		public function OnSaveParamsLoader() {
		//_debug($_REQUEST,1);
			if($_REQUEST['option']){
				foreach($_REQUEST['option'] as $key => $val){
				
				$_SESSION['Trash']['options'][$key]=$val;
				
				}	
			}
			if($_REQUEST['kolvodvs']){
				$_SESSION['Trash']['options']['kolvodvs']=$_REQUEST['kolvodvs'];		
			}
			//die(json_encode($response));
			//_debug($_SESSION,1);
			die();
		}
		public function OnSaveParams($callCnt = 1) {
			//_debug($_SESSION,1);
			$order_id = $_SESSION['Cur_order'];
			$Products = $_SESSION['Trash']['cart'];
			// _debug($Products, 1);
			$params = $_REQUEST['option'];
			$pid = i($_REQUEST['pid']);
			//_debug($_REQUEST,1);
			if($pid > 0) {
				//exit('ffd');
				$asd = 0;
				foreach($params as $option_id=>$var_id) {
					$sql = "SELECT id FROM products_variants WHERE option_id = '".i($option_id)."' AND product_id = '".$pid."'";
					$pv_id = GetOne($sql);
					/*if($pv_id){ 
						$sql = "UPDATE products_variants SET variant_id = '".i($var_id)."' WHERE id = '".$pv_id."'"; 
					}else{
						$sql = "INSERT INTO products_variants (option_id, variant_id, product_id) VALUES ('".i($option_id)."','".i($var_id)."','".$pid."')";
					}
					Execute($sql);
					*/
					$_SESSION['Trash']['cart'][$pid]['options'][$option_id] = i($var_id);
					
					$size_etalon = GetRow('SELECT `side1`, `side2` FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
					if($size_etalon['side1'] && $size_etalon['side2']){
						$quality = $this->DetermineQuality($Products[$pid]['image_size'], $size_etalon);
						$_SESSION['Trash']['cart'][$pid]['quality'] = $quality;
						$_SESSION['Trash']['cart'][$pid]['side1']=$size_etalon['side1'];
						$_SESSION['Trash']['cart'][$pid]['side2']=$size_etalon['side2'];
						
					}else{
						
					}
					
					// _debug($_SESSION['Trash']['cart']);$asd++;echo $asd;
					
					// Цены
				
					$new_price = GetOne('SELECT IFNULL(price,0) FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
					//_debug($new_price);
					if(floatval($new_price) > 0) {
						$price_123=GetRow('SELECT price,price2,price3 FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
						$_SESSION['Trash']['cart'][$pid]['price1'] = $price_123['price'];
						$_SESSION['Trash']['cart'][$pid]['price2'] = $price_123['price2'];
						$_SESSION['Trash']['cart'][$pid]['price3'] = $price_123['price3'];
						
						$new_price = self::getDiscountPrice($pid);
						$cyr_price = $new_price;
						$old_price = $_SESSION['Trash']['cart'][$pid]['price'];
						$_SESSION['Trash']['cart'][$pid]['price'] = $new_price;
						$_SESSION['Trash']['Sum'] -= $old_price * $_SESSION['Trash']['cart'][$pid]['count'];
						$_SESSION['Trash']['Sum'] += $new_price * $_SESSION['Trash']['cart'][$pid]['count'];
						// Перебор товаров с такимже размером
						
					
					} else {$cyr_price = $_SESSION['Trash']['cart'][$pid]['price'];}
				}
			} else {
				//_debug($_SESSION,1);
				 // было
				foreach($Products as $prod) { 
					$pid = $prod['id'];
					if($_REQUEST['kolvodvs']>0) {
					$kolvo=$_REQUEST['kolvodvs'];
					$chc_price = $_SESSION['Trash']['cart'][$pid]['price'];
					$_SESSION['Trash']['Sum'] -= $chc_price *  $prod['count'];
					$_SESSION['Trash']['Sum'] += $chc_price *  $kolvo;
					$_SESSION['Trash']['cart'][$pid]['count']=$kolvo;
					$_SESSION['Trash']['Count'] -= $prod['count'];
					$_SESSION['Trash']['Count'] += $kolvo;
					}
					foreach($params as $option_id=>$var_id) {
						
						$_SESSION['Trash']['cart'][$pid]['options'][$option_id] = i($var_id);
						
						$size_etalon = GetRow('SELECT `side1`, `side2` FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
						if($size_etalon['side1'] && $size_etalon['side2']){
							$quality = $this->DetermineQuality($Products[$pid]['image_size'], $size_etalon);
							$_SESSION['Trash']['cart'][$pid]['quality'] = $quality;
							$response['photo_quality_arr'][$pid] = $quality;
							$_SESSION['Trash']['cart'][$pid]['side1']=$size_etalon['side1'];
							$_SESSION['Trash']['cart'][$pid]['side2']=$size_etalon['side2'];
						}else{
							// unset($_SESSION['Trash']['cart'][$pid]['quality']);
						}
						
						// Цены
						
						$new_price = GetOne('SELECT IFNULL(price,0) FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
						
						if(floatval($new_price) > 0) {
							$price_123=GetRow('SELECT price,price2,price3 FROM product_option_variants WHERE option_id = "'.$option_id.'" AND id = "'.$var_id.'"');
							$_SESSION['Trash']['cart'][$pid]['price1'] = $price_123['price'];
							$_SESSION['Trash']['cart'][$pid]['price2'] = $price_123['price2'];
							$_SESSION['Trash']['cart'][$pid]['price3'] = $price_123['price3'];
							$new_price = self::getDiscountPrice($pid);
							
							$cyr_price = $new_price;	
							$old_price = $_SESSION['Trash']['cart'][$pid]['price'];
							$_SESSION['Trash']['cart'][$pid]['price'] = $new_price;
							$_SESSION['Trash']['Sum'] -= $old_price * $_SESSION['Trash']['cart'][$pid]['count'];
							$_SESSION['Trash']['Sum'] += $new_price * $_SESSION['Trash']['cart'][$pid]['count'];
							
						 } 
					}
				}
			 // конец было	*/
			} 
			$Products_ather=$_SESSION['Trash']['cart'];
			foreach($Products_ather as $prod) {
								$p_id=$prod['id'];
								
								$old_price = $prod['price'];
								$new_price_pr = self::getDiscountPrice($p_id);
								$_SESSION['Trash']['cart'][$p_id]['price'] = $new_price_pr;
								$_SESSION['Trash']['Sum'] -= $old_price *  $prod['count'];
								$_SESSION['Trash']['Sum'] += $new_price_pr *  $prod['count'];
								$response['photo_priceall_arr'][$p_id] = number_format($new_price_pr * floatval($_SESSION['Trash']['cart'][$p_id]['count']),2);
								$response['photo_priceone_arr'][$p_id] = number_format($new_price_pr ,2);
								$response['photo_price_arr'][$p_id] = number_format($new_price_pr * floatval($_SESSION['Trash']['cart'][$p_id]['count']),2);
								
							} 
			Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");
			//var_dump($cyr_price);
			$response['photo_price'] = number_format($cyr_price, 2);
			$response['photo_price_cnt'] = number_format($_SESSION['Trash']['cart'][$pid]['count'] * floatval($cyr_price),2);
			$response['sum_photo_price'] = number_format($_SESSION['Trash']['Sum'],2);
			$response['photo_quality'] = $_SESSION['Trash']['cart'][$pid]['quality'];
			//_debug($_SESSION);
			/*echo $callCnt;
			if($callCnt == 1){
				return self::OnSaveParams($callCnt++);
			}else{*/
				die(json_encode($response));
			/*}*/
			
		}
		
		public function OnResetOptions(){
			if($_REQUEST['res']){
				//_debug($_SESSION,1);
				unset($_SESSION['Trash']['options']);
				$response=1;
				die(json_encode($response));
			}
		}
		
		public function OnChangeCount()
		{
			$pid = i($_REQUEST['pid']);
			$new_count = $_REQUEST['count'];
			if($pid && $new_count){
				//_debug($_SESSION,1);
				$old_count = $_SESSION['Trash']['cart'][$pid]['count'];
				
				$_SESSION['Trash']['cart'][$pid]['count'] = $new_count;
				$_SESSION['Trash']['Count'] -= $old_count;
				$_SESSION['Trash']['Count'] += $new_count;
				
				$old_price = $_SESSION['Trash']['cart'][$pid]['price'];
				$new_price = self::getDiscountPrice($pid);
				$_SESSION['Trash']['cart'][$pid]['price'] = $new_price;
				$_SESSION['Trash']['Sum'] -= $old_count * $old_price;
				$_SESSION['Trash']['Sum'] += $new_count * $new_price;
				
				$Products_ather=$_SESSION['Trash']['cart'];
				foreach($Products_ather as $prod) {
					$p_id=$prod['id'];
					if($p_id!=$pid){
						$old_price = $prod['price'];
						$new_price_pr = self::getDiscountPrice($p_id);
						$_SESSION['Trash']['cart'][$p_id]['price'] = $new_price_pr;
						$_SESSION['Trash']['Sum'] -= $old_price *  $prod['count'];
						$_SESSION['Trash']['Sum'] += $new_price_pr *  $prod['count'];
						$response['photo_priceall_arr'][$p_id] = number_format($new_price_pr * floatval($_SESSION['Trash']['cart'][$p_id]['count']),2);
						$response['photo_priceone_arr'][$p_id] = number_format($new_price_pr,2);
					}
				//$response['photo_price_arr'][$pid] = number_format($cyr_price * floatval($_SESSION['Trash']['cart'][$pid]['count']),2);
				}
				
				
				$order_id = $_SESSION['Cur_order'];
				Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");
				
				 
				$response['price_one']=$new_price;
				$response['photo_price'] = number_format(floatval($new_count) * $new_price, 2);
				$response['sum_photo_price'] = number_format($_SESSION['Trash']['Sum'], 2);
				die(json_encode($response));
			}
			die();
		}
		
		protected function getDiscountPrice($product_id, $trash = null)
		{
			if(!isset($trash)) $trash = $_SESSION['Trash'];
			// _debug($trash);
			$option_id = $this->_get_system_variable('option_size_type_id');
			$discount_cnt = explode(';', $this->_get_system_variable('discount_cnt'));
			
			$cnt = 0;
			foreach($trash['cart'] as $product){ 
				
				//if($product['options'][$option_id] == $trash['cart'][$product_id]['options'][$option_id]) $cnt += $product['count'];
				if($product['side1'] == $trash['cart'][$product_id]['side1'] && $product['side2'] == $trash['cart'][$product_id]['side2']) $cnt += $product['count'];
				
				
			}
			// _debug($cnt);
			$price = $trash['cart'][$product_id]['price1'];
			foreach($discount_cnt as $key => $val){
				if($cnt >= $val) $price = $trash['cart'][$product_id]['price'.($key + 2)];
			}
			// _debug($price);
			return $price;
		}
		
		public function OnSaveDelivPay(){//_debug($_GET, 1);
		
			$ik_payment_id = i($_GET['ik_payment_id']);
			$delivery_way = i($_GET['DeliveryMethod']);
			if($_GET['DeliveryOption']){
				$delivery_option = i($_GET['DeliveryOption']);
				if($_GET['DeliveryOptionDep']){
					$delivery_option= s($_GET['DeliveryOption']).' '.s($_GET['DeliveryOptionDep']);
				}
			}else{
				$delivery_option = NULL;
			}
			$payment_way = i($_GET['PaymentMethod']);
			if($_GET['message'] == $this->_get_lang_constant('FORM_COMMENT_TEXT')){
				$comment = '';
			}else{
				$comment = s($_GET['message']);
			}
			$address2 = s($_GET['address2']);
			
			
			if($ik_payment_id){
				$sql = "SELECT * FROM `orders` WHERE `id` = '$ik_payment_id' AND `buyer_id` = '".$this->UserAccount['id']."'";
				$order = GetRow($sql);
			}
			
			if($order){
				$phone = $_SESSION['user_account']['phone'];
				if($_SESSION['user_account']['surname']) $name_arr['surname'] = $_SESSION['user_account']['surname'];
				if($_SESSION['user_account']['name']) $name_arr['name'] = $_SESSION['user_account']['name'];
				if($_SESSION['user_account']['patronymic']) $name_arr['patronymic'] = $_SESSION['user_account']['patronymic'];
				$name = implode(' ', $name_arr);
				$email = $_SESSION['user_account']['email'];
				$address = $_SESSION['user_account']['address'];
				
				$sql = "UPDATE `orders` 
						SET 
							`delivery_way` = '$delivery_way', 
							`delivery_option` = '$delivery_option', 
							`address2` = '$address2', 
							`payment_way` = '$payment_way', 
							`comment` = '$comment', 
							
							`phone` = '$phone', 
							`name` = '$name', 
							`email` = '$email', 
							`address` = '$address' 
						WHERE `id` = '".$order['id']."'";
				// _debug($sql, 1);
				Execute($sql);
			}
			
			exit();
		}
		public function OnChangePassword() {
			
			if($this->_check_password($_POST)) {
				
				if($this->_update($this->UserAccount['id'], $_POST, 'users')){
					$_SESSION['message_success'] = 'Данные успешно сохранены!';	
				}
				$this->GoToUrl('/user-account/change-password.html');
			}else{
				$_SESSION['message_error'] = 'При сохранении данных возникли ошибки!';
			}
		}
		
		public function OnUpdateMainInfo() {
			global $config;
			//_debug($_POST,1);
			$this->UserInfo = GetRow('SELECT * FROM users WHERE id = \''.$this->UserAccount['id'].'\'');
			// _debug($this->UserAccount, 1);
			if($this->_check_data($_POST)) {
				$_POST['subscribe:int'] = $_POST['subscribe:int'];
				if($this->_update($this->UserAccount['id'], $_POST, 'users')){
					
					$_SESSION['message_success'] = 'Данные успешно сохранены!';	
					// $_SESSION['user_account']['email'] = $_POST['email:str'];
					$_SESSION['user_account'] = GetRow("SELECT * FROM `users` WHERE `id` = '".$this->UserAccount['id']."'");
					
				}
				$this->GoToUrl('/user-account.html');
			}else{
				$_SESSION['message_error'] = 'При сохранении данных возникли ошибки!';	
			}
		}
		
		public function OnUpdateDeliveryInfo() {
			global $config;
			
			if($this->_update($this->UserAccount['id'], $_POST, 'users')){

				$_SESSION['message_success'] = 'Данные успешно сохранены!';	
				$this->GoToUrl('/private-data/delivery-info.html');
			}else{
				$_SESSION['message_error'] = 'При сохранении данных возникли ошибки!';	
			}
		}
		
		/* public function OnSearch(){
			
			if(s($_REQUEST['keyword']) && ($_REQUEST['art'] || $_REQUEST['name'])){
				$where = array();
				if($_REQUEST['name']) $where[] = ' title LIKE \'%'.s($_REQUEST['keyword']).'%\'';
				if($_REQUEST['art']) $where[] = ' code LIKE \'%'.s($_REQUEST['keyword']).'%\'';
				$where = implode(' OR ',$where);
				$sql = 'SELECT t.*, IF(t.url != \'\',t.url,t.id) as url, ph.filename,ROUND(t.price*'.floatval($this->Currencies[$this->Currency][course]).',2) as price  
						FROM products t
						LEFT JOIN photos ph ON ph.product_id = t.id AND ph.default = 1 
						WHERE t.publish = 1 AND ('.$where.')';
				$this->Products = GetAll($sql);
				$this->Products_sz = sizeof($this->Products);
			}
			include PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'search-list.php';
			exit;
		} */
		
		public function OnRefresh(){
			$this->Products[items] = (array)$_SESSION['Trash']['cart'];
			$this->Products[cnt] = sizeof($this->Products[items]);
			include PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'my-basket-list.php';
			exit;
		}
		
		public function OnRecalculateAjax(){
			AttachClass('Order');
			Order::recalculate($this->UserAccount['discount']);
			$this->Products[items] = (array)$_SESSION['Trash']['cart'];
			$this->Products[cnt] = sizeof($this->Products[items]);
			include PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'my-basket-list.php';
			exit;
		}
		/*
		 * Private methods
		 */
		
		private function _check_password($data) {
			if(mb_strlen($data['password:pass']) < 6) {
				$this->ErrorsUser['password'] = 'Пароль должен быть не короче 6 символов!';
			}
			
			if(mb_strlen($data['password:pass']) > 20) {
				$this->ErrorsUser['password'] = 'Пароль не должен быть длиннее 20 символов!';
			}
			
			if($data['password:pass'] != $data['confirm']){
				$this->ErrorsUser['confirm'] = 'Подтверждение не совпадает с паролем!';
			}
			
			if($this->ErrorsUser)
				return false;
			else
				return true;
		}
		
		private function _check_data($data) {
			
			if(!$data['name:str']){
				$this->ErrorsUser['name'] = 'Введите своё имя!';
			}
			if(!$data['surname:str']){
				$this->ErrorsUser['surname'] = 'Введите свою фамилию!';
			}
			
			if(!$data['phone:str']){
				
				$this->ErrorsUser['phone'] = 'Введите свой номер телефона!';
			}
			if(!preg_match("/^\+380([0-9]){2}([0-9]){7}$/",$data['phone:str'])){
				$this->ErrorsUser['phone'] = 'Телефон введен неверно';
			}
			$sql = 'SELECT id 
					FROM users
					WHERE 	email 	= \''.$data['email:str'].'\' AND
							id 		!= \''.$this->UserAccount['id'].'\'';
				
			$res = GetOne($sql);
			
			if($res) {
				$this->ErrorsUser['email'] = 'Данный email используется другим пользователем!';
			}else{
				if (!$this->ValidEmail($data['email:str'])){
					$this->ErrorsUser['email'] = 'Email введён неверно!';
				}
			}
			
			if($this->ErrorsUser)
				return false;
			else
				return true;
		}
		
		
		public function _save_image($file, $path, $type_arr, $i = 0) {
			
			global $config;

			if(is_array($type_arr)){
								
				$type_arr = $this->_get_img_size_arr($type_arr);

				$bool = false;
				$path = $config['absolute-path'].$path;
				
				$ext = end(explode('.', $file['name'][$i]));
				$this->MD5Filename = md5($file['tmp_name'][$i].time()).'.'.$ext;

				foreach($type_arr as $t){
					
					if(mb_strlen($t[2]) > 8)
						$name = $t[2].'.'.$ext;
					else
						$name = $t[2].'-'.$this->MD5Filename; 
					$size = explode('x',$t[0]);

					copy($file['tmp_name'][$i], $path.$name);
					@chmod($path.$name,0777);

					if ($size[0] && $size[1])
						$bool = $this->resize($path.$name, $file['type'][$i], $size[0], $size[1]);
				}		
				return $bool;
			}	
		}
		
		protected function _load_incoming_messages(){
			
			$sql = 'SELECT m.*, u.login
					FROM messages m, users u
					WHERE 	m.recipient_id	= \''.$this->UserAccount['id'].'\' and
							m.sender_id		= u.id
					ORDER BY m.create_ts DESC';
			
			$this->Messages = GetAll($sql);
			$this->Messages_sz = count($this->Messages);
		}
		
		protected function _load_outbox_messages(){
			global $DB;
			
			$sql = 'SELECT m.*, u.login
					FROM messages m, users u
					WHERE	m.sender_id		= \''.$this->UserAccount['id'].'\' and
							m.recipient_id 	= u.id
					ORDER BY m.create_ts';
			
			$this->Messages = $DB->GetAll($sql);
			$this->Messages_sz = count($this->Messages);
		}
		
		protected function _load_message($id){

			$sql = 'SELECT m.*, u.login
					FROM messages m, users u
					WHERE 	m.id		= \''.$id.'\' and
							m.sender_id	= u.id';
			
			$this->Message = GetRow($sql);
			
			if($this->Message['recipient_id'] == $this->UserAccount['id'] && $this->Message['status'] == 1){
				
				Execute('	UPDATE messages
							SET status = 2
							WHERE 	id	= \''.$id.'\'');			
			}
		}
		
		private function _get_user_orders(){
			$sql = 'SELECT o.* 
					FROM orders o 
					WHERE o.buyer_id = '.$this->UserAccount['id'].'
					ORDER BY o.ts DESC';
			$this->Orders = GetAll($sql);
			$this->Orders_sz = sizeof($this->Orders);
		}
		
		private function _get_order($order){
			$sql = 'SELECT o.* 
					FROM orders o 
					WHERE o.buyer_id = '.$this->UserAccount['id'].' AND o.id = '.$order;
			$this->Order = GetRow($sql);
			$this->Products = unserialize($this->Order['cart']);
			$this->Products_sz = sizeof($this->Products);
		}
		
		private function _load_last_order($type=0){
			
			if ($type) {
				$where_type=' AND type_order= \''.$type.'\'';			
			}else{
				$where_type=' ';		
			}
			
			$last_order = GetRow("SELECT id, total, qty, cart FROM orders WHERE state = 'not_order' ".$where_type." AND buyer_id = '".$this->UserAccount['id']."'");					
					
			if($last_order) {
				$_SESSION['Cur_order'] = $last_order['id'];
				$_SESSION['Trash']['cart'] = unserialize($last_order['cart']);
				$_SESSION['Trash']['Sum'] = $last_order['total'];
				$_SESSION['Trash']['Count'] = $last_order['qty'];
			}
		}
		
	}

?>