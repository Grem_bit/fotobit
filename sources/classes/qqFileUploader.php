<?
class qqFileUploader {
    private $allowedExtensions = array();
   // private $sizeLimit = 104857600;
	private $sizeLimit = 314572800;
	private $Optionsizeqq = array(1=>1,2=>6,3=>24,4=>25,5=>26,6=>27,7=>28,8=>29);
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 314572800){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
      
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
           
			$size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'Размер файла не может превышать $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
		protected function GetNotInOptqq($pr=1){
				$wh='';
				foreach ($this->Optionsizeqq as $key=>$val){
					if($key==$pr){
					
					}else{
						$wh.=' AND pot.option_id !='.$val.' ';
					}
				}
				return $wh;
		}
		protected function _get_cat_options($id, $group = true, $for_ver=false){
			global $DB;
			$rr='';
			//ini_set('display_errors',1); error_reporting(E_ALL);
			//$rr=$this->GetNotInOpt($_SESSION['user_account']['pricenamber']);
			$rr=$this->GetNotInOptqq($_SESSION['user_account']['pricenamber']);
			$where=$rr;
			
			/* if($_SESSION['user_account']['pricenamber']){
				
				if($_SESSION['user_account']['pricenamber']==1){
					$where=' AND pot.option_id !=6 AND pot.option_id !=7';
				}elseif($_SESSION['user_account']['pricenamber']==2){
					$where=' AND pot.option_id !=1 AND pot.option_id !=7 ';
				}elseif($_SESSION['user_account']['pricenamber']==3){
					$where=' AND pot.option_id !=1 AND pot.option_id !=6 ';
				}else{
					$where='1=1';
				}
				
			} */
			$sql = 'SELECT 
						po.*, 
						pog.title as group_title, 
						pog.id as group_id 
					FROM 
						product_option_type pot 
						LEFT JOIN 
						product_options as po 
						ON po.id = pot.option_id
						LEFT JOIN product_options_groups pog
						ON pog.id = po.type_id
					WHERE 
						pot.category_id = '.$id.$add.'  '.$where.'    
					ORDER BY pog.sort_order, po.order_by
			';
			//_debug($sql,1);
			$res = $DB->GetAll($sql);
			
			if($res){
				foreach($res as $option){
					$sql = 'SELECT 
								* 
							FROM 
								product_option_variants
							WHERE 
								option_id = '.$option['id'].'
							ORDER BY
								sort
					';
					
					$option['variants'] = $DB->GetAll($sql);
					if($group){
						if($option['user_choice']){
							$options['user'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['user'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}else{
							$options['all'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['all'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}
					}else{
						$options[$option['id']] = $option;
					}
					
					
				}
			}

			return $options;
		}
		
	final public function resize($file, $type, $height, $width, $mode=0){
		
	    $img = false;
		// echo($type);
		// exit($type);
	    switch ($type){
			case 'image/jpeg':
			case 'image/jpg':
			case 'image/pjpeg':
				// exit($type);
				$img = @imagecreatefromjpeg($file);
				// exit($img);
				break;
			case 'image/x-png':
			case 'image/png':
				$img = imagecreatefrompng($file);
				break;
			case 'image/gif':
				$img = imagecreatefromgif($file);
				break;
	    }//die($type);
		// exit($type);
	    if(!$img){
			return false;
	    }
	    	   
	    $curr = @getimagesize($file);
// print_r($curr);
	    if(($width > $curr[0]) && ($height > $curr[1])){
			return true;
	    }
	    
	    $perc_w = $width / $curr[0];
	    $perc_h = $height / $curr[1];
	    
	    if($perc_h > $perc_w){
			$width = $width;
			$height = round($curr[1] * $perc_w);
	    } else {
			$height = $height;
			$width = round($curr[0] * $perc_h);
	    }
	    $nwimg = imagecreatetruecolor($width, $height);
	    imagecopyresampled($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[1]);

	    if($mode == 1){
	    	$nwimg = imagecreatetruecolor($width, $height);
	    	
	    	if ($curr[0]>$curr[1]) 
	        imagecopyresized($nwimg, $img, 0, 0, round((max($curr[0],$curr[1])-min($curr[0],$curr[1]))/2),0, $width, $height, min($curr[0],$curr[1]), min($curr[0],$curr[1])); 
	
	        if ($curr[0]<$curr[1]) 
	        imagecopyresized($nwimg, $img, 0, 0, 0, 0, $width, $height, min($curr[0],$curr[1]), min($curr[0],$curr[1])); 
	       
	        if ($curr[0]==$curr[1]) 
	        imagecopyresized($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[0]);
	    }
	   
	    switch ($type){
			case 'image/jpeg':
			case 'image/jpg':
			case 'image/pjpeg':
				imagejpeg($nwimg, $file);
				break;
			case 'image/x-png':
			case 'image/png':
				imagepng($nwimg, $file);
				break;
			case 'image/gif':
				imagegif($nwimg, $file);
				break;
	    }
	    
	    imagedestroy($nwimg);
	    imagedestroy($img);
		return true;
	}
		
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
	  function removeDirectory($dir) {
		if ($objs = glob($dir."/*")) {
		   foreach($objs as $obj) {
			 is_dir($obj) ? removeDirectory($obj) : unlink($obj);
		   }
		}
		rmdir($dir);
	  }

    function handleUploadarch($uploadDirectory, $order_id){
		global $config;
		
		if (!is_writable($uploadDirectory)){
            return array('error' => "Ошибка сервера. Загружаемая дирректория не записываемая.");
        }
       
        if (!$this->file){
            return array('error' => 'Файлы не загружены.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'Файл пустой');
        }
       
        if ($size > $this->sizeLimit) {
            return array('error' => 'Файл слишком большой');
        }
        		
		$pathinfo = pathinfo($this->file->getName());
        	
			if (preg_match('/[^A-Za-z0-9_\-]/', $this->file->getName())) {
				$filename = $this->translitIt($this->file->getName());
			}
		$count_point=substr_count($filename,'.');
		if($count_point>1){
			$cnt_z=$count_point-1;
			$filename=preg_replace("/\./", "_", $filename,$cnt_z);
		} 
		$file_name=explode('.',$filename);
		
		$filename=$file_name[0].'_'.time().'.'.$file_name[1];
		
        $ext = $pathinfo['extension'];
		
		//_debug($uploadDirectory.'arc'.DIRECTORY_SEPARATOR . $filename,1);
		
		if($this->file->save($uploadDirectory.'arc'.DIRECTORY_SEPARATOR . $filename)){
			
			@chmod($uploadDirectory . $filename,0777);		
		
			if($file_name[1]=='rar'){
				$rar_file = rar_open($uploadDirectory.'arc'.DIRECTORY_SEPARATOR . $filename) ;
				if(!$rar_file) ("Невозможно открыть RAR архив");
				$entries = rar_list($rar_file);
				//_debug($entries,1);
				foreach ($entries as $entry) {
					$pathinfo = pathinfo($entry->getName());
				
					if (preg_match('/[^A-Za-z0-9_\-]/', $entry->getName())) {
						$filename = $this->translitIt($entry->getName());
					}
					$count_point=substr_count($filename,'.');
					if($count_point>1){
						$cnt_z=$count_point-1;
						$filename=preg_replace("/\./", "_", $filename,$cnt_z);
					} 
					$file_name=explode('.',$filename);
					
					$filename=$file_name[0].'_'.time().'.'.$file_name[1];
					
					$ext = $pathinfo['extension'];
					
					
					if( $file_name[1]=='jpeg' || $file_name[1]=='jpg' || $file_name[1]=='JPEG' || $file_name[1]=='JPG'){
						$entry->extract($uploadDirectory.'arc'.DIRECTORY_SEPARATOR);
						@chmod($uploadDirectory.'arc'.DIRECTORY_SEPARATOR .$entry->getName() ,0777);
						
						
						copy($uploadDirectory .'arc'. DIRECTORY_SEPARATOR . $entry->getName(), $uploadDirectory.$filename);
						@chmod($uploadDirectory.$filename,0777);
						// --------
						if (file_exists($uploadDirectory . $filename)){
							
							Execute("INSERT INTO products(filename, img) VALUES ('" .$filename_old . "','" . $filename . '.' . $ext . "')");
							$product_id = GetOne('select LAST_INSERT_ID() as id from products');
							$product['id'] = $product_id;
							
							$_SESSION['Trash']['options']['kolvodvs']?$product['count']=$_SESSION['Trash']['options']['kolvodvs']:$product['count'] = 1;
							
							$product['img'] = $filename;
							$filesz =  filesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
							$product['size'] = round($filesz/1024/1024,1)."MB";
							$product['sizeb'] =$filesz;
							$product['image_size'] = getimagesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
							
							
							$_SESSION['order_type']='photos';
							$where_t= $_SESSION['order_type'].'=1';
							//_debug(" SELECT id FROM categories WHERE ".$where_t." " ,1);
							$this->Chars = $this->_get_cat_options(GetOne(" SELECT id FROM categories WHERE ".$where_t." "));
							
							$this->Chars = $this->Chars['all'][0]['items'];
							//_debug($this->Chars,1);
							foreach($this->Chars as $option) {
									if(isset($_SESSION['Trash']['options']) and is_array($_SESSION['Trash']['options'])){
										if(isset($_SESSION['Trash']['options'][$option['id']])){
											$val_opt=$_SESSION['Trash']['options'][$option['id']];
											$product['options'][$option['id']] = $val_opt;
											$variant_key=0;
											foreach($option['variants'] as $key_variant => $val_variant){
												if($val_variant['id'] == $val_opt ){
													$variant_key=$key_variant;
												}
											}
										}else{
											$variant_key=0;
											$product['options'][$option['id']] = $option['variants'][0]['id'];
										}
									}else{
										$variant_key=0;
										$product['options'][$option['id']] = $option['variants'][0]['id'];
									}
									
									//$product['options'][$option['id']] = $option['variants'][0]['id'];
									
									if($option['variants'][$variant_key]['price'] > 0){
										$product['price'] = $option['variants'][$variant_key]['price'];
										$product['price1'] = $option['variants'][$variant_key]['price'];
										$product['price2'] = $option['variants'][$variant_key]['price2'];
										$product['price3'] = $option['variants'][$variant_key]['price3'];
									}
									if($option['variants'][$variant_key]['side1'] && $option['variants'][$variant_key]['side2']){
										$size_etalon['side1'] = $option['variants'][$variant_key]['side1'];
										$size_etalon['side2'] = $option['variants'][$variant_key]['side2'];
										$product['side1'] = $option['variants'][$variant_key]['side1'];
										$product['side2'] = $option['variants'][$variant_key]['side2'];
										$quality = ContentPage::DetermineQuality($product['image_size'], $size_etalon);
										$product['quality'] = $quality;
										// _debug($_SESSION['Trash']['cart'][$product_id]);
									}else{
										// unset($_SESSION['Trash']['cart'][$pid]['quality']);
									} 
							}
								
							$_SESSION['Trash']['cart'][$product_id] = $product;
							
							
							// old   $_SESSION['Trash']['Sum'] += $_SESSION['Trash']['cart'][$product_id]['price'];
							// old $_SESSION['Trash']['Count']++;
							  $price_load=$_SESSION['Trash']['cart'][$product_id]['price']*$product['count'];
							  $_SESSION['Trash']['Sum'] += ($_SESSION['Trash']['cart'][$product_id]['price']*$product['count']);
							  $_SESSION['Trash']['Count']+=$product['count'];
							$option_load=$_SESSION['Trash']['cart'][$product_id]['options'];
							Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");				
							
							copy($uploadDirectory . $filename, $uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
							@chmod($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename,0777);
							$size_info = @getimagesize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
							$i_type =  $size_info['mime'];
							//_debug($_SESSION['Trash'],1);
							 $this->resize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename, $i_type, 120, 150); // отключено, т.к. не работает imagecreatefromjpeg()
							$arr_img[]=array(
								'success' => true,
								'file' => DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'orders'.DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR.$filename,
								'pid' => $product_id,
								'size' => $filesz,
								'count_cnt' => $product['count'],
								'option_l'=>$option_load,
								'price' => number_format($_SESSION['Trash']['cart'][$product_id]['price'],2),
								'price_all' => number_format($price_load,2),
								'quality' => $product['quality'],
								'sum_price' => number_format($_SESSION['Trash']['Sum'],2)
							);
							/* return array(
								'success' => true,
								'file' => DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'orders'.DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR.$filename,
								'pid' => $product_id,
								'size' => $filesz,
								'count_cnt' => $product['count'],
								'option_l'=>$option_load,
								'price' => number_format($_SESSION['Trash']['cart'][$product_id]['price'],2),
								'price_all' => number_format($price_load,2),
								'quality' => $product['quality'],
								'sum_price' => number_format($_SESSION['Trash']['Sum'],2)
							); */
						} else {
							
							return array('error'=> 'Нет возможности сохранить файл на сервере.' .
								'Загрузка отменена, так как сервер вернул ошибку');
						}		
						// --------
					}
				}
				$this->removeDirectory($uploadDirectory.'arc');
				$arr_img['arr']=true;
				return  $arr_img;
				rar_close($rar_file); 
			}elseif( $file_name[1]=='zip'){
					
				$zip     = new ZipArchive;
				if ($zip->open($uploadDirectory.'arc'.DIRECTORY_SEPARATOR . $filename) === true) {
					
					for($i = 0; $i < $zip->numFiles; $i++) {
						$filename_old = $zip->getNameIndex($i);
						
						$pathinfo = pathinfo($filename_old);
						if (preg_match('/[^A-Za-z0-9_\-]/',$filename_old)) {
							$filename = $this->translitIt($filename_old);
						}
						$count_point=substr_count($filename,'.');
						if($count_point>1){
							$cnt_z=$count_point-1;
							$filename=preg_replace("/\./", "_", $filename,$cnt_z);
						} 
						$file_name=explode('.',$filename);
						
						$filename=$file_name[0].'_'.time().'.'.$file_name[1];
						
						$ext = $pathinfo['extension'];
		
						if( $file_name[1]=='jpeg' || $file_name[1]=='jpg' || $file_name[1]=='JPEG' || $file_name[1]=='JPG'){
							$zip->extractTo($uploadDirectory.'arc'.DIRECTORY_SEPARATOR, array($filename_old));
							@chmod($uploadDirectory.'arc'.DIRECTORY_SEPARATOR .$filename_old ,0777);

							copy($uploadDirectory .'arc'. DIRECTORY_SEPARATOR . $filename_old, $uploadDirectory.$filename);
							@chmod($uploadDirectory.$filename,0777);
							
						//----------
										
						if (file_exists($uploadDirectory . $filename)){
							
							Execute("INSERT INTO products(filename, img) VALUES ('" .$filename_old . "','" . $filename . '.' . $ext . "')");
							$product_id = GetOne('select LAST_INSERT_ID() as id from products');
							$product['id'] = $product_id;
							
							$_SESSION['Trash']['options']['kolvodvs']?$product['count']=$_SESSION['Trash']['options']['kolvodvs']:$product['count'] = 1;
							
							$product['img'] = $filename;
							$filesz =  filesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
							$product['size'] = round($filesz/1024/1024,1)."MB";
							$product['sizeb'] =$filesz;
							$product['image_size'] = getimagesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
							
							
							$_SESSION['order_type']='photos';
							$where_t= $_SESSION['order_type'].'=1';
							//_debug(" SELECT id FROM categories WHERE ".$where_t." " ,1);
							$this->Chars = $this->_get_cat_options(GetOne(" SELECT id FROM categories WHERE ".$where_t." "));
							
							$this->Chars = $this->Chars['all'][0]['items'];
							//_debug($this->Chars,1);
							foreach($this->Chars as $option) {
									if(isset($_SESSION['Trash']['options']) and is_array($_SESSION['Trash']['options'])){
										if(isset($_SESSION['Trash']['options'][$option['id']])){
											$val_opt=$_SESSION['Trash']['options'][$option['id']];
											$product['options'][$option['id']] = $val_opt;
											$variant_key=0;
											foreach($option['variants'] as $key_variant => $val_variant){
												if($val_variant['id'] == $val_opt ){
													$variant_key=$key_variant;
												}
											}
										}else{
											$variant_key=0;
											$product['options'][$option['id']] = $option['variants'][0]['id'];
										}
									}else{
										$variant_key=0;
										$product['options'][$option['id']] = $option['variants'][0]['id'];
									}
									
									//$product['options'][$option['id']] = $option['variants'][0]['id'];
									
									if($option['variants'][$variant_key]['price'] > 0){
										$product['price'] = $option['variants'][$variant_key]['price'];
										$product['price1'] = $option['variants'][$variant_key]['price'];
										$product['price2'] = $option['variants'][$variant_key]['price2'];
										$product['price3'] = $option['variants'][$variant_key]['price3'];
									}
									if($option['variants'][$variant_key]['side1'] && $option['variants'][$variant_key]['side2']){
										$size_etalon['side1'] = $option['variants'][$variant_key]['side1'];
										$size_etalon['side2'] = $option['variants'][$variant_key]['side2'];
										$product['side1'] = $option['variants'][$variant_key]['side1'];
										$product['side2'] = $option['variants'][$variant_key]['side2'];
										$quality = ContentPage::DetermineQuality($product['image_size'], $size_etalon);
										$product['quality'] = $quality;
										// _debug($_SESSION['Trash']['cart'][$product_id]);
									}else{
										// unset($_SESSION['Trash']['cart'][$pid]['quality']);
									} 
							}
								
							$_SESSION['Trash']['cart'][$product_id] = $product;
							
							
							// old   $_SESSION['Trash']['Sum'] += $_SESSION['Trash']['cart'][$product_id]['price'];
							// old $_SESSION['Trash']['Count']++;
							  $price_load=$_SESSION['Trash']['cart'][$product_id]['price']*$product['count'];
							  $_SESSION['Trash']['Sum'] += ($_SESSION['Trash']['cart'][$product_id]['price']*$product['count']);
							  $_SESSION['Trash']['Count']+=$product['count'];
							$option_load=$_SESSION['Trash']['cart'][$product_id]['options'];
							Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");				
							
							copy($uploadDirectory . $filename, $uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
							@chmod($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename,0777);
							$size_info = @getimagesize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
							$i_type =  $size_info['mime'];
							//_debug($_SESSION['Trash'],1);
							 $this->resize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename, $i_type, 120, 150); // отключено, т.к. не работает imagecreatefromjpeg()
							$arr_img[]=array(
								'success' => true,
								'file' => DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'orders'.DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR.$filename,
								'pid' => $product_id,
								'size' => $filesz,
								'count_cnt' => $product['count'],
								'option_l'=>$option_load,
								'price' => number_format($_SESSION['Trash']['cart'][$product_id]['price'],2),
								'price_all' => number_format($price_load,2),
								'quality' => $product['quality'],
								'sum_price' => number_format($_SESSION['Trash']['Sum'],2)
							);
							/* return array(
								'success' => true,
								'file' => DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'orders'.DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR.$filename,
								'pid' => $product_id,
								'size' => $filesz,
								'count_cnt' => $product['count'],
								'option_l'=>$option_load,
								'price' => number_format($_SESSION['Trash']['cart'][$product_id]['price'],2),
								'price_all' => number_format($price_load,2),
								'quality' => $product['quality'],
								'sum_price' => number_format($_SESSION['Trash']['Sum'],2)
							); */
						} else {
							return array('error'=> 'Нет возможности сохранить файл на сервере.' .
								'Загрузка отменена, так как сервер вернул ошибку');
						}
												
						//-----------
						}
					}
					$arr_img['arr']=true;					
					 return  $arr_img;
				}
				$zip->close();   
			}
		}else{	
			
			$arr_img[]=array(
			'arr' => true,
			'error' =>'Нет возможности сохранить архив на сервере.' .
                'Загрузка отменена, так как сервер вернул ошибку'
			);
		/* 	return array('error'=> 'Нет возможности сохранить файл на сервере.' .
                'Загрузка отменена, так как сервер вернул ошибку'); */
				return $arr_img;
		}
	}
    
	function handleUpload($uploadDirectory, $order_id){
    	global $config;
        if (!is_writable($uploadDirectory)){
            return array('error' => "Ошибка сервера. Загружаемая дирректория не записываемая.");
        }
        
        if (!$this->file){
            return array('error' => 'Файлы не загружены.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'Файл пустой');
        }
       
        if ($size > $this->sizeLimit) {
            return array('error' => 'Файл слишком большой');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        	
			if (preg_match('/[^A-Za-z0-9_\-]/', $this->file->getName())) {
			    $filename = $this->translitIt($this->file->getName());
			} 
		$count_point=substr_count($filename,'.');
		if($count_point>1){
			$cnt_z=$count_point-1;
			$filename=preg_replace("/\./", "_", $filename,$cnt_z);
		} 
		$file_name=explode('.',$filename);
		
		$filename=$file_name[0].'_'.time().'.'.$file_name[1];
		
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'Файл имеет не верный тип. Принимаются файлы только '. $these . '.');
        }
        
        
        if ($this->file->save($uploadDirectory . $filename)){
			@chmod($uploadDirectory . $filename,0777);
			Execute("INSERT INTO products(filename, img) VALUES ('" .$this->file->getName() . "','" . $filename . '.' . $ext . "')");
			$product_id = GetOne('select LAST_INSERT_ID() as id from products');
			$product['id'] = $product_id;
			
			$_SESSION['Trash']['options']['kolvodvs']?$product['count']=$_SESSION['Trash']['options']['kolvodvs']:$product['count'] = 1;
			
			$product['img'] = $filename;
			$filesz =  filesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
			$product['size'] = round($filesz/1024/1024,1)."MB";
			$product['sizeb'] =$filesz;
			$product['image_size'] = getimagesize($config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR.$filename);
			
			
			$_SESSION['order_type']='photos';
			$where_t= $_SESSION['order_type'].'=1';
			//_debug(" SELECT id FROM categories WHERE ".$where_t." " ,1);
			$this->Chars = $this->_get_cat_options(GetOne(" SELECT id FROM categories WHERE ".$where_t." "));
			
			$this->Chars = $this->Chars['all'][0]['items'];
			//_debug($this->Chars,1);
			foreach($this->Chars as $option) {
					if(isset($_SESSION['Trash']['options']) and is_array($_SESSION['Trash']['options'])){
						if(isset($_SESSION['Trash']['options'][$option['id']])){
							$val_opt=$_SESSION['Trash']['options'][$option['id']];
							$product['options'][$option['id']] = $val_opt;
							$variant_key=0;
							foreach($option['variants'] as $key_variant => $val_variant){
								if($val_variant['id'] == $val_opt ){
									$variant_key=$key_variant;
								}
							}
						}else{
							$variant_key=0;
							$product['options'][$option['id']] = $option['variants'][0]['id'];
						}
					}else{
						$variant_key=0;
						$product['options'][$option['id']] = $option['variants'][0]['id'];
					}
					
					//$product['options'][$option['id']] = $option['variants'][0]['id'];
					
					if($option['variants'][$variant_key]['price'] > 0){
						$product['price'] = $option['variants'][$variant_key]['price'];
						$product['price1'] = $option['variants'][$variant_key]['price'];
						$product['price2'] = $option['variants'][$variant_key]['price2'];
						$product['price3'] = $option['variants'][$variant_key]['price3'];
					}
					if($option['variants'][$variant_key]['side1'] && $option['variants'][$variant_key]['side2']){
						$size_etalon['side1'] = $option['variants'][$variant_key]['side1'];
						$size_etalon['side2'] = $option['variants'][$variant_key]['side2'];
						$product['side1'] = $option['variants'][$variant_key]['side1'];
						$product['side2'] = $option['variants'][$variant_key]['side2'];
						$quality = ContentPage::DetermineQuality($product['image_size'], $size_etalon);
						$product['quality'] = $quality;
						// _debug($_SESSION['Trash']['cart'][$product_id]);
					}else{
						// unset($_SESSION['Trash']['cart'][$pid]['quality']);
					} 
				
				/* $product['options'][$option['id']] = $option['variants'][0]['id'];
				if($option['variants'][0]['price'] > 0){
					$product['price'] = $option['variants'][0]['price'];
					$product['price1'] = $option['variants'][0]['price'];
					$product['price2'] = $option['variants'][0]['price2'];
					$product['price3'] = $option['variants'][0]['price3'];
				}
				if($option['variants'][0]['side1'] && $option['variants'][0]['side2']){
					$size_etalon['side1'] = $option['variants'][0]['side1'];
					$size_etalon['side2'] = $option['variants'][0]['side2'];
					$product['side1'] = $option['variants'][0]['side1'];
					$product['side2'] = $option['variants'][0]['side2'];
					$quality = ContentPage::DetermineQuality($product['image_size'], $size_etalon);
					$product['quality'] = $quality;
					// _debug($_SESSION['Trash']['cart'][$product_id]);
				}else{
					// unset($_SESSION['Trash']['cart'][$pid]['quality']);
				} 
				*/
			}
				
			$_SESSION['Trash']['cart'][$product_id] = $product;
			
			
			// old   $_SESSION['Trash']['Sum'] += $_SESSION['Trash']['cart'][$product_id]['price'];
			// old $_SESSION['Trash']['Count']++;
			  $price_load=$_SESSION['Trash']['cart'][$product_id]['price']*$product['count'];
			  $_SESSION['Trash']['Sum'] += ($_SESSION['Trash']['cart'][$product_id]['price']*$product['count']);
			  $_SESSION['Trash']['Count']+=$product['count'];
			$option_load=$_SESSION['Trash']['cart'][$product_id]['options'];
			Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");				
			
			copy($uploadDirectory . $filename, $uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
			@chmod($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename,0777);
			$size_info = @getimagesize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename);
			$i_type =  $size_info['mime'];
			//_debug($_SESSION['Trash'],1);
			// $this->resize($uploadDirectory."thumb".DIRECTORY_SEPARATOR.$filename, $i_type, 100, 100); // отключено, т.к. не работает imagecreatefromjpeg()
            return array(
				'success' => true,
				'file' => DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'orders'.DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR.$filename,
				'pid' => $product_id,
				'size' => $filesz,
				'count_cnt' => $product['count'],
				'option_l'=>$option_load,
				'price' => number_format($_SESSION['Trash']['cart'][$product_id]['price'],2),
				'price_all' => number_format($price_load,2),
				'quality' => $product['quality'],
				'sum_price' => number_format($_SESSION['Trash']['Sum'],2)
			);
        } else {
            return array('error'=> 'Нет возможности сохранить файл на сервере.' .
                'Загрузка отменена, так как сервер вернул ошибку');
        }
        
    }   

	function translitIt($str) 
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", 
	        " "=> "_", "/"=> "_"
	    );
	    return strtr($str,$tr);
	}

}
?>