<?

	class SocialAuth extends ContentPage {
		
		/*
		 * Protected Properties
		 */
		
		protected $TemplatesBaseDir		= 'soc-auth/';		
		
		/*
		 * Public methods
		 */
		
		public function OnCreate(){
			global $config;
			
			$auth = false;
			$this->AddTitle('Авторизация');
			switch($this->UrlParts[1]){
				case 'vkontakte':
					// $this->SetTemplate('pay.php');
					// $this->Status();
					// _debug($_GET, 1);
					if($this->OnVKAuth()){
						$auth = true;
					}
				case 'facebook':
					// $this->SetTemplate('pay.php');
					// $this->Status();
					// _debug($_GET, 1);
					if($this->OnFBAuth()){
						$auth = true;
					}
					exit;
				break;
				case 'twitter':
					$user_data = $this->OnTwitterAccessToken();
					$user_data = objectToArray($user_data);
					if($user_data){
						// _debug($user_data, 1);
						if($this->TWAuth($user_data)){
							$auth = true;
						}
					}
				break;
			}
			// $this->SetTemplate('editable-page.php');
			if($auth){
				$this->GoToUrl('/user-account/data.html');
			}
		}
		
		/*
		 * Авторизация через Twitter
		 */
		
		
		// Переход в Твиттер с запросом авторизовать приложение
		public function OnTwitterRequestToken(){
		
			define ('TWITTER_CONSUMER_KEY', 'XgwnMvCuoYMeXtZj4uEAqQ');
			define ('TWITTER_CONSUMER_SECRET', 'D832xmFTMBBjz4kSKJKPkC6AGxUxEWhJuQWbmIXvY');
			define ('TWITTER_URL_CALLBACK', 'http://photo.stepshop.com.ua/soc-auth/twitter.html');

			// define ('URL_REQUEST_TOKEN', 'https://api.twitter.com/oauth/request_token');
			define ('URL_REQUEST_TOKEN', 'http://api.twitter.com/oauth/request_token');
			define ('URL_AUTHORIZE', 'https://api.twitter.com/oauth/authorize');
			define ('URL_ACCESS_TOKEN', 'https://api.twitter.com/oauth/access_token');
			define ('URL_ACCOUNT_DATA', 'http://twitter.com/users/show');
			
			// рандомная строка (для безопасности)
			$oauth_nonce = md5(uniqid(rand(), true)); // ae058c443ef60f0fea73f10a89104eb9

			// время когда будет выполняться запрос (в секундых)
			$oauth_timestamp = time(); // 1310727371

			/**
			 * Константы смотрите чуть повыше
			 *
			 * Обратите внимание на использование функции urlencode и расположение амперсандов.
			 * Если поменяете положение параметров oauth_... или уберете где-нибудь urlencode - получите ошибку
			 *
			 */
			$oauth_base_text = "GET&";
			$oauth_base_text .= urlencode(URL_REQUEST_TOKEN)."&";
			$oauth_base_text .= urlencode("oauth_callback=".urlencode(TWITTER_URL_CALLBACK)."&");
			$oauth_base_text .= urlencode("oauth_consumer_key=".TWITTER_CONSUMER_KEY."&");
			$oauth_base_text .= urlencode("oauth_nonce=".$oauth_nonce."&");
			$oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
			$oauth_base_text .= urlencode("oauth_timestamp=".$oauth_timestamp."&");
			$oauth_base_text .= urlencode("oauth_version=1.0");

			/**
			 * В результате получим строку, такого вида:

			GET&https%3A%2F%2Fapi.twitter.com%2Foauth%2Frequest_token&
			oauth_callback%3Dhttp%253A%252F%252Fexpange.ru%252Ftwitter_auth.php%253Fauth%253D1%26
			oauth_consumer_key%3DO0IgnYHonW4KGL6pJr0YCQ%26
			oauth_nonce%3Dae058c443ef60f0fea73f10a89104eb9%26
			oauth_signature_method%3DHMAC-SHA1%26
			oauth_timestamp%3D1310727371%26
			oauth_version%3D1.0

			 * Естественно строка должны быть однострочной (переносы сделаны для удобства сравнения)
			 */
			 
			 
			 $key = TWITTER_CONSUMER_SECRET."&"; // На конце должен быть амперсанд & !!!
			// key: OYUjBgJPl4yra3N32sSpSSVGboLSCo5pLGsky20VJE&

			$oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));
			// oauth_signature: BB6w/jAdrHQD1/iUqqEZiI8o2M0=
			
			
			/**
			 * Опять же внимательно смотрим на функцию urlencode
			 *
			 */
			$url = URL_REQUEST_TOKEN;
			$url .= '?oauth_callback='.urlencode(TWITTER_URL_CALLBACK);
			$url .= '&oauth_consumer_key='.TWITTER_CONSUMER_KEY;
			$url .= '&oauth_nonce='.$oauth_nonce;
			$url .= '&oauth_signature='.urlencode($oauth_signature);
			$url .= '&oauth_signature_method=HMAC-SHA1';
			$url .= '&oauth_timestamp='.$oauth_timestamp;
			$url .= '&oauth_version=1.0';
			/**
			 * Строка запроса (Переносы строк естественно не нужны):

			https://api.twitter.com/oauth/request_token
			?oauth_callback=http%3A%2F%2Fexpange.ru%2Ftwitter_auth.php%3Fauth%3D1
			&oauth_consumer_key=O0IgnYHonW4KGL6pJr0YCQ
			&oauth_nonce=ae058c443ef60f0fea73f10a89104eb9
			&oauth_signature=BB6w%2FjAdrHQD1%2FiUqqEZiI8o2M0%3D
			&oauth_signature_method=HMAC-SHA1
			&oauth_timestamp=1310727371
			&oauth_version=1.0

			**/


			/**
			 * Получить результат GET-запроса будем функцией file_get_contents
			 * можно и даже лучше использовать curl, но здесь и эта функция справляется отлично
			 *
			 */
			$response = file_get_contents($url);
			// _debug($url, 0);
			// _debug($response, 1);
			// если все сделано правильно, $response будет содержать нечто подобное:
			// oauth_token=DZmWEaKh7EqOJKScI48IgYMxYyFF2riTyD5N9wqTA&oauth_token_secret=NuAL0AvzocI9zxO7VnVPrNEcb9EW8kwpwJmcqg5pMWg&oauth_callback_confirmed=true
			//
			// Если что-то не так, будет выведено следующее:
			// Failed to validate oauth signature and token
			// Самая распространенная ошибка, означающая, что в большинстве случаев
			// подпись oauth_signature сформирована неправильно.
			// Еще раз внимательно просмотрите как формируется строка и кодируется oauth_signature,
			// сверьте с примером использования функций urlencode

			// И так все выведено правильно. Разбираем полученную строку.

			parse_str($response, $result);
			// _debug($result, 1);
			/**
			 * $result:
			Array
			(
				[oauth_token] => DZmWEaKh7EqOJKScI48IgYMxYyFF2riTyD5N9wqTA
				[oauth_token_secret] => NuAL0AvzocI9zxO7VnVPrNEcb9EW8kwpwJmcqg5pMWg
				[oauth_callback_confirmed] => true
			)
			*/


			// session_start вначале я думаю у вас вызывается
			$_SESSION['oauth_token'] = $oauth_token = $result['oauth_token'];
			$_SESSION['oauth_token_secret'] = $oauth_token_secret = $result['oauth_token_secret'];
			
			
			$url = URL_AUTHORIZE;
			$url .= '?oauth_token='.$oauth_token;
			$this->GoToUrl($url);

			// url: https://api.twitter.com/oauth/authorize?oauth_token=qexSk7ySxVV5DPr9j9zE0RuxT5Zxbp1rOPqemizeU
		}
		
		public function OnTwitterAccessToken(){
		
			define ('TWITTER_CONSUMER_KEY', 'XgwnMvCuoYMeXtZj4uEAqQ');
			define ('TWITTER_CONSUMER_SECRET', 'D832xmFTMBBjz4kSKJKPkC6AGxUxEWhJuQWbmIXvY');
			define ('TWITTER_URL_CALLBACK', 'http://photo.stepshop.com.ua/soc-auth/twitter.html');

			define ('URL_REQUEST_TOKEN', 'https://api.twitter.com/oauth/request_token');
			define ('URL_AUTHORIZE', 'https://api.twitter.com/oauth/authorize');
			// define ('URL_ACCESS_TOKEN', 'https://api.twitter.com/oauth/access_token');
			define ('URL_ACCESS_TOKEN', 'http://api.twitter.com/oauth/access_token');
			define ('URL_ACCOUNT_DATA', 'http://twitter.com/users/show');
		
			// Заново создаем oauth_nonce и oauth_timestamp
			// рандомная строка (для безопасности)
			$oauth_nonce = md5(uniqid(rand(), true)); // c775a2221c0d3a187438628e8427f262

			// время когда будет выполняться запрос (в секундах)
			$oauth_timestamp = time(); // 1310727378

			// oauth_token
			$oauth_token = $_GET['oauth_token'];

			// oauth_verifier
			$oauth_verifier = $_GET['oauth_verifier'];

			// oauth_token_secret получаем из сессии, которую зарегистрировали
			// во время запроса request_token
			$oauth_token_secret = $_SESSION['oauth_token_secret'];

			/**
			 * Обратите внимание на использование функции urlencode и расположение амперсандов.
			 * Если поменяете положение параметров oauth_... или уберете где-нибудь urlencode - получите ошибку
			 *
			 */
			$oauth_base_text = "GET&";
			$oauth_base_text .= urlencode(URL_ACCESS_TOKEN)."&";
			$oauth_base_text .= urlencode("oauth_consumer_key=".TWITTER_CONSUMER_KEY."&");
			$oauth_base_text .= urlencode("oauth_nonce=".$oauth_nonce."&");
			$oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
			$oauth_base_text .= urlencode("oauth_token=".$oauth_token."&");
			$oauth_base_text .= urlencode("oauth_timestamp=".$oauth_timestamp."&");
			$oauth_base_text .= urlencode("oauth_verifier=".$oauth_verifier."&");
			$oauth_base_text .= urlencode("oauth_version=1.0");

			/**
			 * В результате получим строку, такого вида:

			GET&https%3A%2F%2Fapi.twitter.com%2Foauth%2Faccess_token&
			oauth_consumer_key%3DO0IgnYHonW4KGL6pJr0YCQ%26
			oauth_nonce%3Dc775a2221c0d3a187438628e8427f262%26
			oauth_signature_method%3DHMAC-SHA1%26
			oauth_token%3DDZmWEaKh7EqOJKScI48IgYMxYyFF2riTyD5N9wqTA%26
			oauth_timestamp%3D1310727378%26
			oauth_verifier%3DEJxb8eSgNdiUZPwi5Qwwt7JPE13nfXpCXOZSwCqBU%26
			oauth_version%3D1.0

			 * Естественно строка должны быть однострочной (переносы сделаны для удобства сравнения)
			 */
			
			
			$key = TWITTER_CONSUMER_SECRET."&".$oauth_token_secret;
			// key: OYUjBgJPl4yra3N32sSpSSVGboLSCo5pLGsky20VJE&NuAL0AvzocI9zxO7VnVPrNEcb9EW8kwpwJmcqg5pMWg

			$oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));
			// oauth_signature: WqIpf1g6fEdNk67Rc+cP9zxji5k=
			
			
			/**
			 * Опять же внимательно смотрим на функцию urlencode
			 *
			 */

			// Формируем GET-запрос
			$url = URL_ACCESS_TOKEN;
			$url .= '?oauth_nonce='.$oauth_nonce;
			$url .= '&oauth_signature_method=HMAC-SHA1';
			$url .= '&oauth_timestamp='.$oauth_timestamp;
			$url .= '&oauth_consumer_key='.TWITTER_CONSUMER_KEY;
			$url .= '&oauth_token='.urlencode($oauth_token);
			$url .= '&oauth_verifier='.urlencode($oauth_verifier);
			$url .= '&oauth_signature='.urlencode($oauth_signature);
			$url .= '&oauth_version=1.0';
			
			/**
			 * Строка запроса $url (Переносы строк естественно не нужны):

			https://api.twitter.com/oauth/access_token
			?oauth_nonce=c775a2221c0d3a187438628e8427f262
			&oauth_signature_method=HMAC-SHA1
			&oauth_timestamp=1310727378
			&oauth_consumer_key=O0IgnYHonW4KGL6pJr0YCQ
			&oauth_token=DZmWEaKh7EqOJKScI48IgYMxYyFF2riTyD5N9wqTA
			&oauth_verifier=EJxb8eSgNdiUZPwi5Qwwt7JPE13nfXpCXOZSwCqBU
			&oauth_signature=WqIpf1g6fEdNk67Rc%2BcP9zxji5k%3D
			&oauth_version=1.0

			**/

			/**
			 * Получить результат GET-запроса будем функцией file_get_contents
			 * можно и даже лучше использовать curl, но здесь и эта функция справляется отлично
			 */

			$response = file_get_contents($url);
			// _debug($url, 0);
			// _debug($response, 1);
			// если все сделано правильно, $response будет содержать нечто подобное:
			// oauth_token=228497030-pmcYm211OCBvlRnLBA9pjbtpKtMQ7ofghRwWJYlA&oauth_token_secret=IyZ5SFgTfRWtluEyavSAkUi8MJMgdYCZUlt5aNNMg&user_id=228497030&screen_name=expange

			// Разбираем полученную строку.
			parse_str($response, $result);
			// _debug($result, 1);
			/**
			 * $result:
			Array
			(
				[oauth_token] => 228497030-pmcYm211OCBvlRnLBA9pjbtpKtMQ7ofghRwWJYlA
				[oauth_token_secret] => IyZ5SFgTfRWtluEyavSAkUi8MJMgdYCZUlt5aNNMg
				[user_id] => 228497030
				[screen_name] => expange
			)
			*/
			
			
			$user_id = $result['user_id'];

			$url = 'http://twitter.com/users/show.json?user_id='.$user_id;

			// делаем запрос
			$response = file_get_contents($url);

			// разбираем запрос
			$user_data = json_decode($response);

			/*echo '<pre>';
			print_r($user_data);
			echo '</pre>';
			exit();*/
			
			/** Результат в виде объекта будет примерно следующего содержания:

			stdClass Object
			(
				[default_profile_image] => 
				[profile_background_tile] => 
				[protected] => 
				[default_profile] => 
				[contributors_enabled] => 
				[url] => http://expange.ru
				[name] => expange.ru
				[id_str] => 228497030
				[is_translator] => 
				[show_all_inline_media] => 1
				[geo_enabled] => 
				[profile_link_color] => 1F98C7
				[follow_request_sent] => 
				[utc_offset] => 10800
				[created_at] => Sun Dec 19 22:12:45 +0000 2010
				[friends_count] => 2
				[profile_sidebar_border_color] => C6E2EE
				[following] => 
				[time_zone] => Moscow
				[profile_image_url] => http://a3.twimg.com/profile_images/1228124572/expange_normal.png
				[description] => 
				[statuses_count] => 106
				[profile_use_background_image] => 1
				[favourites_count] => 0
				[status] => stdClass Object
					(
						[place] => 
						[truncated] => 
						[id_str] => 91646116151570432
						[in_reply_to_status_id] => 
						[text] => Авторизация на сайте через twitter (PHP) http://t.co/SRtztLi via @expange
						[created_at] => Thu Jul 14 23:11:51 +0000 2011
						[geo] => 
						[favorited] => 
						[in_reply_to_status_id_str] => 
						[coordinates] => 
						[id] => 91646116151570432
						[in_reply_to_screen_name] => 
						[source] => Tweet Button
						[in_reply_to_user_id_str] => 
						[in_reply_to_user_id] => 
						[contributors] => 
						[retweeted] => 
						[retweet_count] => 0
					)

				[verified] => 
				[profile_background_color] => C6E2EE
				[screen_name] => expange
				[listed_count] => 0
				[profile_background_image_url] => http://a1.twimg.com/images/themes/theme2/bg.gif
				[id] => 228497030
				[notifications] => 
				[profile_background_image_url_https] => https://si0.twimg.com/images/themes/theme2/bg.gif
				[profile_text_color] => 663B12
				[lang] => en
				[profile_sidebar_fill_color] => DAECF4
				[followers_count] => 1
				[profile_image_url_https] => https://si0.twimg.com/profile_images/1228124572/expange_normal.png
				[location] => Moscow
			)

			**/
			
			if($user_data){
				return $user_data;
			}else{
				return false;
			}
		}
		
		 /*
		 * Авторизация через Twitter END
		 */
		
		public function OnVKAuth(){
			
			$auth = false;
			$id = $_REQUEST['uid'];
			if($id && $_REQUEST['hash'] == md5('3056064'.$id.'B9AizcGVUEm87120XMKp')){
				$user = GetRow("SELECT * FROM `users` WHERE `VK_id` = '$id' AND `active` = '1'");
				if(is_for($user)){
					$this->SetUser($user);
					$auth = true;
				}else{
					if($this->RegisterUser('VK_id')){
						$auth = true;
					}
				}
			}
			if($auth){
				// $this->GoToUrl('/user-account.html');
				return true;
			}else{
				// exit('error');
				//$this->GoToUrl('/?noauth=1');
				return false;
			}
			
		}
		
		public function TWAuth($data){
			
			$auth = false;
			$id = $data['id'];
			if($id){
				$user = GetRow("SELECT * FROM `users` WHERE `TW_id` = '$id' AND `active` = '1'");
				if(is_for($user)){
					$this->SetUser($user);
					$auth = true;
				}else{
					$_REQUEST['uid'] = $data['id'];
					$_REQUEST['first_name'] = $data['name'];
					if($this->RegisterUser('TW_id')){
						$auth = true;
					}
				}
			}
			if($auth){
				return true;
			}else{
				return false;
			}
			
		}
		
		public function OnFBAuth(){
		
			_log($_POST);
			$auth = false;
			$idFB = base64_decode($_GET['codeId']);
			if($idFB){
				$user = GetRow("SELECT * FROM `users` WHERE `FB_id` = '".$idFB."' AND `active` = '1'");
				if(is_for($user)){
					$this->SetUser($user);
					$auth = true;
				}
			}
			if($auth){
				return true;
			}else{
				return false;
			}
			
		}
		
		private function SetUser($user){
			$this->UserAccount = $user;
			$_SESSION['user_account'] = $user;
			setcookie("asd1",base64_encode(serialize(array($this->UserAccount['username'],$this->UserAccount['password']))),time()+60*60*24*31,"/");
		}
		
		private function RegisterUser($social_field_name){//_debug($_REQUEST, 1);
		
			switch($social_field_name){
				case 'VK_id':
					$id = $user['VK_id'] = $_REQUEST['uid'];
				break;
				case 'TW_id':
					$id = $user['TW_id'] = $_REQUEST['uid'];
				break;
			}
			$first_name = $user['name'] = $_REQUEST['first_name'];
			$last_name = $user['surname'] = $_REQUEST['last_name'];
			$sql = "INSERT INTO `users` 
					SET
						`$social_field_name` = '$id', 
						`name` = '$first_name', 
						`surname` = '$last_name', 
						`active` = '1'
				";
			// _debug($sql, 1);
			if(Execute($sql)){
				$user['id'] = GetOne('SELECT LAST_INSERT_ID() AS `id` FROM `users`');
				$this->UserAccount = $user;
				// _debug($this->UserAccount, 1);
				$_SESSION['user_account'] = $user;
				setcookie("asd1",base64_encode(serialize(array($this->UserAccount['username'],$this->UserAccount['password']))),time()+60*60*24*31,"/");
				return true;
			}
		}
	}
?>