<?	class Articles extends ContentPage {
		
		/*
		 * Protected properties
		 */
		
		public $News				= array();
		public $News_sz				= 0;
		public $PageNavigator		= null;
		public $Url 				= '';
		public $Page 				= '';
		public $Comments			= array();
		public $Comments_sz			= 0;
		public $OneNews 			= array();
		
		public $ViewEvent 			= '';
		public $Lang				= '';
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			global $config, $url_parts;
			
			$this->SetTitle('Статьи');	
			$count = count($this->UrlParts);
			
			
				switch($this->UrlParts[$count - 2]){
					case '':
					case NULL:
						// load all news
						$this->_load_all_news();
						$this->ViewEvent = 'AllNews';
						break;
					case 'view':
						//load news by ID
						$this->_load_news($this->UrlParts[$count-1],true);
						$this->ViewEvent = 'View';
						break;
					case 'page':
						//load news by ID
						$this->Page = $this->UrlParts[$count-1];
						$this->_load_all_news();
						$this->ViewEvent = 'AllNews';
						break;
					default:
						//load news by SEO url
						$this->_load_news($this->UrlParts[$count],true);
						$this->ViewEvent = 'View';
						$this->SetTitle($this->OneNews['title']);	
						break;
						
				
				}

				$this->SetTemplate('articles.html');
			
			return;
			
		}
		
		
		
	
		
		
		
		/*
		 * Private methods
		 */

		private function _load_news($url, $seo = true){
			$this->ViewEvent == 'ViewNews';
			global $DB;
			switch($seo){
				case true;				
					$sql = "SELECT * FROM art WHERE published = '1' AND url = '$url'";
					$this->OneNews = $DB->GetRow($sql);
					break; 
				case false;
					$sql = "SELECT * FROM art WHERE published = '1' AND  id = '$url'";
					$this->OneNews = $DB->GetRow($sql);
					break;
			}
			
			
			
			$sql = "SELECT 
							* 
						FROM 
							art_translation 
						WHERE 
							obj_id = '{$this->OneNews['id']}' AND lang = '".$this->SiteLang."' 
						";
			
			$_news = $DB->GetRow($sql);
			$_news['date'] = date($this->_get_system_variables('date_format'),$this->OneNews['publish_ts']);
			$this->OneNews = array('date'=> $_news['date'], 
								'title' => $_news['title'], 
								'introtext' => $_news['introtext'],
								'fulltext' => $_news['fulltext'],
								'url'=>$this->OneNews['url'], 
								'id'=>$this->OneNews['id']
			
			);
		
			$sql = 'SELECT pt.*, p.*  
					FROM pages p
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						 	p.publish = 1 AND
						  p.url=\'articles\'';
			$this->PageInfo = $DB->GetRow($sql);
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);
			$this->SetTitle($_news['page_title']);
			//print_r($this->PageInfo);
			$this->SetMetaDescription($_news['description']);
			$this->SetMetaKeywords($_news['keywords']);
		}
		
		private function _load_news_id($url, $seo = true){
			global $DB;
			switch($seo){
				case true;				
					$sql = "SELECT id FROM art WHERE published = '1' AND url = '$url'";
					$this->NewsId = $DB->GetOne($sql);
					break; 
				case false;
					$sql = "SELECT id FROM art WHERE published = '1' AND  id = '$url'";
					$this->NewsId = $DB->GetOne($sql);
					break;
			}
		}
		
		private function _load_all_news(){
			
			global $DB;
			
			$items_per_page = $this->_get_system_variable('news_per_page');
			$max_pages_cnt = $this->_get_system_variable('max_news_pages_cnt');
			$page = $this->Page;
			
			if($page < 1){				
				$page = 1;
			}

			$url = 'news/';
			
				$sql = ' 
						SELECT SQL_CALC_FOUND_ROWS nt.* , n.url, n.publish_ts
					FROM art n
					LEFT JOIN art_translation  nt ON (nt.obj_id = n.id AND nt.lang = \''.$this->SiteLang.'\')
					WHERE 
						n.published = \'1\' AND nt.title <> "" 
					ORDER BY	n.publish_ts  DESC
					
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page.'
					';
		
			//echo $sqlecho $sql;
			$this->News = GetAll($sql);
			// _debug($this->News, 1);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			AttachLib('PageNavigator');
			// echo $this->RootUrl, $url, $page; exit();
			$this->PageNavigator = new PageNavigator($res['total'], $items_per_page, $max_pages_cnt, $this->RootUrl .($this->SiteLang == 'en'?$this->SiteLang.'/':''). $url, $page, 'page/%s',true, true);
					
			
				
			$sql = 'SELECT pt.*, p.* 
					FROM pages p 
						LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = \''.$this->SiteLang.'\')
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.$this->UrlParts[0].'\'';
				
				
						
			$this->PageInfo = $DB->GetRow($sql);
			
			$this->PageInfo['content_parts'] = explode('{[News]}', $this->PageInfo['content']);

			$this->SetMetaDescription($this->PageInfo['meta_description']);
			$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
			
			
			
		}
		
		private function _load_comments($art_id, $parent_id = 0) {
			
			global $DB;
			static $level = 0;
			
			$sql = 'SELECT ac.*, u.login 
					FROM news_comments ac,
						 users u
					WHERE 	ac.news_id	= \''.$this->NewsId.'\' and 
							ac.published 	= 1 and
						  	ac.user_id 		= u.id and
						  	ac.parent_id	= \''.$parent_id.'\'
					ORDER BY ac.create_ts DESC';
		
			$cat = $DB->GetAll($sql);
			$cat_sz = count($cat);
			
			for($i = 0; $i < $cat_sz; $i++) {
				
				$cat[$i]['level'] = $level;
				array_push($this->Comments, $cat[$i]);
				$level++;
				$this->_load_comments($art_id, $cat[$i]['id']);
				
				$level--;
			}
			
			if($parent_id == 0) {
				
				$this->Comments_sz = count($this->Comments);
			}
			
		}
		
		private function _load_top_level_categories() {
			
			global $DB;
			
			$sql = 'SELECT title, 
						   url 
					FROM categories 
					WHERE parent_id=\'0\'';
			$this->Categories = $DB->GetAll($sql);
			$this->Categories_sz = count($this->Categories);
			
			for($i = 0; $i < $this->Categories_sz; $i++) {
				
				$this->Categories[$i]['href'] = $this->RootUrl.$this->Categories[$i]['url'].'/';
			}
		}
	}

?>