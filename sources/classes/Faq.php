<?

	class Faq extends ContentPage {

		
		
		protected $Category				= array();
		protected $Category_sz			= 0;
			
		protected $Faq				= array();
		protected $Faq_sz			= 0;
		
		protected $Faq_u				= array();
		protected $Faq_u_sz			= 0;
		
		protected $FaqInfo				= array();
		
		protected $PN					= null;
		protected $ItemsPerPage			= 16;
		protected $PagesPerPage			= 9;
		protected $UrlParts				= array();
		protected $UrlParts_sz			= 0;
		protected $Page					= 1;
		protected $Filter				= '';
		protected $Urlp					= '';
		protected $ArtComents				= array();
		protected $ArtComents_sz			= 0;
		/*
		 * Public methods
		 */
		
		public function OnCreate() {			
			
			global $url_parts, $config;

			$this->_get_page_content();
			$this->_set_meta_data();
			$this->Faq = $this->_get_all_list('','');
			$this->Faq_sz = count($this->Faq);
		
			$this->SetTemplate('faq.php');
			
				
		}
				
		/*
		 * Private methods
		 */

		private function _question_info($parent, $url) {
			global $DB;
			
			$sql = 'SELECT q.question, q.answer, q.url, c.question as p_question, q.id  
					FROM faq q
						LEFT JOIN faq c ON (q.parent_id = c.id)  
					WHERE q.url=\''.$url.'\' and
						  q.publish = 1 and
						  c.url=\''.$parent.'\' and
						  c.publish = 1';
		
			$this->FaqInfo = $DB->GetRow($sql);
		}
		
		private function _get_all_list($parent, $url,$users = 0) {
			
			global $DB;
			static $level = 0;
			static $cat_list = array();

			$sql = 'SELECT * 
					FROM faq 
					WHERE parent_id=\'0\' and  publish = 1';
			
			if($parent){
				$sql.= ' and url=\''.$parent.'\'';
			}
			
			$categories = $DB->GetAll($sql);
			$categories_sz = count($categories);
			
			for($i = 0; $i < $categories_sz; $i++) {
				
				$sql = 'SELECT q.question, q.url, c.url as parent_url, q.answer, q.id, c.id as p_id 
						FROM faq q
							LEFT JOIN faq c ON (q.parent_id = c.id)  
						WHERE q.parent_id=\''.$categories[$i]['id'].'\' and
							  q.publish = 1';
				
				if($url){
					$sql .= ' and  q.url !=\''.$url.'\'';	
				}
				
				$questions = $DB->GetAll($sql);
				$questions_sz = count($questions);
				
				for($k=0; $k < $questions_sz; $k++){
				
					$questions[$k]['href'] = $this->_create_faq_url($questions[$k]);
				}
				
				$categories[$i]['questions'] = $questions;
			}
			
			return $categories;
		}
		
		
		private function _get_page_content(){
			global $DB;
			
			$sql = 'SELECT p.*  
					FROM pages p
					WHERE 
						  p.publish=\'1\' AND 
						  p.url=\''.end($this->UrlParts).'\'';
			$this->PageInfo = $DB->GetRow($sql);
		}
		
		private function _set_meta_data() {
			// Title
			$this->SetTitle($this->GetConfigParam('title'));
			if($this->PageInfo['title']) {
				$this->SetTitle($this->PageInfo['title']);
			}
			// Keywords 
			if($this->PageInfo['meta_keywords']) {
				$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
			}
			else {
				$this->SetMetaKeywords($this->GetConfigParam('meta-keywords'));
			}
			
			// Description
			if($this->PageInfo['meta_description']) {
				$this->SetMetaDescription($this->PageInfo['meta_description']);
			}
			else {
				$this->SetMetaDescription($this->GetConfigParam('meta-description'));
			}
		}
		
		public final function OnSendFaqMessage(){
			
			$this->_load_static_data();
			
			global $config, $DB;
			
			$this->Fio	= $_POST['fio'];
			$this->Email	= $_POST['email'];		
			$this->SiteLang = 'rus';	
			$this->Message	= $_POST['message'];
			if(strcmp($_SESSION['captcha_keystring'],$_POST['captcha']) == 0) $this->Error = $this->QuestionData[$this->SiteLang]['wrong_captcha'];
			if(!$this->Message) $this->Error = $this->QuestionData[$this->SiteLang]['empty_message'];
			if(!$this->ValidEmail($this->Email)) $this->Error = $this->QuestionData[$this->SiteLang]['wrong_email'];
			if(!$this->Fio) $this->Error = $this->QuestionData[$this->SiteLang]['empty_name'];
			if(!$this->Error){
				// send email
				AttachLib('Mailer');
				$mail = new Mailer();
				$sql = "SELECT * FROM emails WHERE faq_question = '1'";
				$row = $DB->GetAll($sql);
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				$mail->AddVars(array('fio'=>$this->Fio,'email'=>$this->Email,'message'=>$this->Message,'date'=>date('d:m:Y H:i',time())));
				$mail->Send('question.html',
							$this->_get_system_variables('faq_form_subject'),
							$this->_get_system_variables('noreply_email'), $this->_get_system_variables('sender_name'));
				header('Location: ?Event=SendMessageSuccess');
			}
		
		}
		
}

?>