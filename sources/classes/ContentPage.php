<?

	class ContentPage extends AbstractPage {
		
		/*
		 * Public properties
		 */
		public $Monthnow				= array('ЯНВАРЬ', 'ФЕВРАЛЬ', 'МАРТ', 'АПРЕЛЬ', 'МАЙ', 'ИЮНЬ', 'ИЮЛЬ', 'АВГУСТ', 'СЕНТЯБРЬ', 'ОКТЯБРЬ', 'НОЯБРЬ', 'ДЕКАБРЬ');
		public $Monthnowua				= array('СIЧЕНЬ', 'ЛЮТИЙ', 'БЕРЕЗЕНЬ', 'КВIТЕНЬ', 'ТРАВЕНЬ', 'ЧЕРВЕНЬ', 'ЛИПЕНЬ', 'СЕРПЕНЬ', 'ВЕРЕСЕНЬ', 'ЖОВТЕНЬ', 'ЛИСТОПАД', 'ГРУДЕНЬ');
		public $Month					= array('ЯНВАРЯ', 'ФЕВРАЛЯ', 'МАРТА', 'АПРЕЛЯ', 'МАЯ', 'ИЮНЯ', 'ИЮЛЯ', 'АВГУСТА', 'СЕНТЯБРЯ', 'ОКТЯБРЯ', 'НОЯБРЯ', 'ДЕКАБРЯ');
		public $Monthua					= array('СIЧНЯ', 'ЛЮТОГО', 'БЕРЕЗНЯ', 'КВIТНЯ', 'ТРАВНЯ', 'ЧЕРВНЯ', 'ЛИПНЯ', 'СЕРПНЯ', 'ВЕРЕСНЯ', 'ЖОВТНЯ', 'ЛИСТОПАДА', 'ГРУДНЯ');
		public $Week					= array('ПОНЕДЕЛЬНИК', 'ВТОРНИК', 'СРЕДА', 'ЧЕТВЕРГ', 'ПЯТНИЦА', 'СУББОТА', 'ВОСКРЕСЕНЬЕ');
		public $Weekua					= array('ПОНЕДIЛОК', 'ВIВТОРОК', 'СЕРЕДА', 'ЧЕТВЕР', 'П`ЯТНИЦЯ', 'СУБОТА', 'НЕДIЛЯ');
		public $MonthS					= array('янв', 'фев', 'март', 'апр', 'мая', 'июнm', 'июлm', 'авг', 'сен', 'окт', 'ноя', 'дек');
		public $Num						= array('first','second','third','fourth');
		protected $StaticData			= array();
		protected $SiteLang				= 'ru';
		protected $UrlParts				= array();
		public $Optionsize				= array(1=>1,2=>6,3=>24,4=>25,5=>26,6=>27,7=>28,8=>29);
		public $ProductUrl				= array();
		public $Breadcrumbs				= array();
		
		/*
		 * Private properties
		 */
		
		private $SnippetData			= array();
		private $SnippetData_sz			= 0;
		
		public $FrontPage				= false;
		public $IsCat					= false;
		
		protected $Error				= '';
		protected $LangId				= 0;
		protected $PageInfo				= array();
		
		
		protected $SuccessSend 			= false;
		protected $MainCurrency			= 0;
		public $LN = '';
		public $LN_url = '';
		/*
		 * Public methods
		 */
		

		public final function OnBeforeCreate() {
			global $server;
			global $Serv;
			global $url_parts, $config;
			
			if($Serv=='net'){
				$this->Serv='net';
			}elseif($Serv=='com'){
				$this->Serv='com';
			}
			//_debug($_SESSION['user_account'],1);
			//$_SESSION['user_account']['address']=$_SESSION['user_account']['city'].' '.$_SESSION['user_account']['street'].' '.$_SESSION['user_account']['house'].' '.$_SESSION['user_account']['apartment'];
			$this->UserAccount = $_SESSION['user_account'];


			if($url_parts[0] == 'ua'){
				unset($url_parts[0]);
				$temp = $url_parts; 
				unset($url_parts);
				if(is_array($temp)){
					foreach ($temp as $val ){
						$url_parts[] = $val;		
					}
				}
				$this->LN = 'ua_';
				$this->LN_url = '/ua/';
				$this->SiteLang = 'ua';
			}elseif($url_parts[0] == 'en'){
				unset($url_parts[0]);
				$temp = $url_parts; 
				unset($url_parts);
				if(is_array($temp)){
					foreach ($temp as $val ){
						$url_parts[] = $val;		
					}
				}
				$this->LN = 'en_';
				$this->LN_url = '/en/';
				$this->SiteLang = 'en';
			}else{
				$this->SiteLang = 'ru';
				$this->LN = '';
				$this->LN_url = '/';
			}
			
			$this->DocumentExtention = $this->_get_system_variable('document_extention'); // Расширение документов сайта
			
			$this->UrlParts = $url_parts;
			
			if($this->UrlParts[0] == '') {
				$this->UrlParts[0] = 'index';
			}
			
			$this->news_list = $this->_load_content_translation('news',
														't.published = 1 AND t.main = 0 AND 
														t.news = 0 AND tr.title <> ""',
														't.publish_ts DESC',
														'','',3);
			$sql = ' 
					SELECT SQL_CALC_FOUND_ROWS nt.* , n.url, n.publish_ts,n.filename1
					FROM news n
					LEFT JOIN news_translation  nt ON (nt.obj_id = n.id AND nt.lang = \''.$this->SiteLang.'\')
					WHERE 
						n.news = 0 AND t.main = 0 AND
						n.published = \'1\' AND nt.title <> "" 
					ORDER BY	n.publish_ts  DESC
	
					';
			$info = GetAll($sql);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			AttachLib('PageNavigator');
			$url = '';
			$page = 1;
			$this->PageNavigatorCalendar = new PageNavigator($res['total'], $this->_get_system_variable('news_left_limit'), 5, $this->RootUrl .($this->SiteLang == 'ua'?$this->SiteLang.'/':''). $url, $page, '?page=%s',true, true);
			$this->PageNavigatorCalendar->Template = 'page-navigator-news.html';
		
			/*$cur_arr = $this->_load_content('course','','`default` DESC');
			//_debug($cur_arr);
			foreach($cur_arr as $cur){
				$this->Currencies[$cur['id']] = $cur;
				if(intval($cur['default'] == 1)){
					$this->MainCurrency = $cur;
				}
			}
			$this->Currency = $cur_arr[0][id];
			*/
			/*
			if($_COOKIE['currency']){
				$this->Currency = intval($_COOKIE['currency']);
			}
			*/
			
			//$
			
			$this->_load_articles_and_news();
		
			// $this->MainMenu = $this->GetTopMenu(0);
			//_debug($this->MainMenu,1);
			
			
			$this->SetTitle($this->_get_system_variable('default_title'));
			$this->SetMetaKeywords($this->_get_system_variable('default_meta_keywords'));
			$this->SetMetaDescription($this->_get_system_variable('default_meta_description'));
		}
		
		public function _show_price_old(&$price, $currency = 0){
			if($currency && $this->Currency != $currency){
				if($currency == $this->MainCurrency){
					$price = floatval($price) * floatval($this->Currencies[$this->Currency][course]);
				}else{
					$price = money(floatval($price) * floatval($this->Currencies[$currency][course]) * floatval($this->Currencies[$this->Currency][course]));
				}
			}else{
				$price = floatval($price);
			}
			if($this->Currencies[$this->Currency]['sign_pos']){
				return money($price).' <span>'.$this->Currencies[$this->Currency]['sign'].'</span>';
			}else{
				return $this->Currencies[$this->Currency]['sign'].money($price);
			}
		}
		
		public function _show_price($price){
			if($price){
				return money($price).' <span>'.$this->Currencies[$this->Currency]['sign'].'</span>';
			}else{
				return '0.00 <span>'.$this->Currencies[$this->Currency]['sign'].'</span>';
			}
		}
		
		public function OnRemovePhoto() {
			global $config;
			$pid = i($_REQUEST['pid']);
			$order_id = $_SESSION['Cur_order'];
			$path = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR;
			if($_SESSION['Trash']['cart'][$pid]['img']){unlink($path.$_SESSION['Trash']['cart'][$pid]['img']);}
			if($_SESSION['Trash']['cart'][$pid]['img']){unlink($path."thumb".DIRECTORY_SEPARATOR.$_SESSION['Trash']['cart'][$pid]['img']);}
			$_SESSION['Trash']['Count'] -= $_SESSION['Trash']['cart'][$pid]['count'];
			$_SESSION['Trash']['Sum'] -= $_SESSION['Trash']['cart'][$pid]['price']*$_SESSION['Trash']['cart'][$pid]['count'];
			unset($_SESSION['Trash']['cart'][$pid]);
			if(!$_SESSION['Trash']['Count']) {
				RemoveDir($path);
				Execute("DELETE FROM orders WHERE id = '".$_SESSION['Cur_order']."'");
				unset($_SESSION['Trash']);
				unset($_SESSION['Cur_order']);
				$result['hide'] = 1;
			}
			Execute("DELETE FROM products WHERE id = '".$pid."'");
			Execute("UPDATE orders SET cart = '". serialize($_SESSION['Trash']['cart']) ."', qty = '".$_SESSION['Trash']['Count']."', total = '".$_SESSION['Trash']['Sum']."' WHERE id = '".$order_id."'");				
			$result['sum'] = number_format($_SESSION['Trash']['Sum'],2);
			die(json_encode($result));
		}
		
		public function OnOrderPhoto() {//_debug($_SESSION, 0);
			$buyer_id = i($_SESSION['user_account']['id']);
			$order_id = i($_SESSION['Cur_order']);
			$cart = $_SESSION['Trash']['cart'];
			if($buyer_id && $order_id && is_array($cart)) {
				$ts = time();
				$qty = $_SESSION['Trash']['Count'];
				$total = $_SESSION['Trash']['Sum'];
				// $state = 'new';
				$state = 'not_order';
				$cart = serialize($_SESSION['Trash']['cart']);
				$phone = $_SESSION['user_account']['phone'];
				
				if($_SESSION['user_account']['surname']) $name_arr['surname'] = $_SESSION['user_account']['surname'];
				if($_SESSION['user_account']['name']) $name_arr['name'] = $_SESSION['user_account']['name'];
				if($_SESSION['user_account']['patronymic']) $name_arr['patronymic'] = $_SESSION['user_account']['patronymic'];
				$name = implode(' ', $name_arr);
				
				$email = $_SESSION['user_account']['email'];
				$address = $_SESSION['user_account']['address'];
				
				Execute("UPDATE orders SET buyer_id = '".$buyer_id."', ts = '".$ts."', qty = '".$qty."', total = '".$total."', state = '".$state."', cart = '".$cart."', phone = '".$phone."', name = '".$name."', email = '".$email."', address = '".$address."' WHERE id = '".$order_id."'");
				// unset($_SESSION['Trash']);
				// unset($_SESSION['Cur_order']);
				$this->GoToUrl('/user-account/orders/'.$order_id.'.html');
			}
		}
		
		public function OnLoadPhotos() {
			global $config;
			
			require_once(PATH_CLASSES."qqFileUploader.php"); 
			require_once(PATH_CLASSES."qqUploadedFileForm.php");
			require_once(PATH_CLASSES."qqUploadedFileXhr.php");

			//$allowedExtensions = array("jpeg","jpg","png");
			$allowedExtensions = array("jpeg","jpg");
			// max file size in bytes
			//$sizeLimit = 10485760;
			$sizeLimit = 31457280;
			
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			
			$user = $_SESSION["user_account"];
			if(i($user['id'])) {
				if($_SESSION['Cur_order']) $order_id = $_SESSION['Cur_order'];
				else {
					if($user['surname']) $name_arr['surname'] = $user['surname'];
					if($user['name']) $name_arr['name'] = $user['name'];
					if($user['patronymic']) $name_arr['patronymic'] = $user['patronymic'];
					$name = implode(' ', $name_arr);
					//_debug($_SESSION,1);
					$_SESSION['order_type']='photos';
					$sql = "INSERT INTO orders(buyer_id, ts, qty, phone, name, email, address,type_order) VALUES ('".$user['id']."', '".time()."', '".count($photos)."', '".$user['phone']."', '".$name."', '".$user['email']."', '".$user['address']."', '".$_SESSION['order_type']."')";
					// _debug($sql, 1);
					Execute($sql);
					$order_id = GetOne('select LAST_INSERT_ID() as id from orders');	
					$_SESSION['Cur_order'] = $order_id;
				}

				$i = 0;
				$sql_values = NULL;

				$path = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR;
				
				
				if(!file_exists($path)) { 
					
					mkdir($path,0777); 
					chmod($path,0777); 
					mkdir($path."thumb".DIRECTORY_SEPARATOR); 
					chmod($path."thumb".DIRECTORY_SEPARATOR,0777);
				}
				
				$result = $uploader->handleUpload($path, $order_id);
			
				if(isset($result['error'])) {
					echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
				} else {
					
					$image = new Imagick($config["absolute-path-withoutslash"].$result['file']);
					$image->thumbnailImage(150,0); 
					$image->writeImage($config["absolute-path-withoutslash"].$result['file']);
				
					echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
				}	
			}
			die();
		}
		// Загрузка архивом rar zip
		
		public function OnLoadArchive() {
			//_debug($_SERVER,1);
			//ini_set('memory_limit', '1000M');
			global $config;
			
			require_once(PATH_CLASSES."qqFileUploader.php"); 
			require_once(PATH_CLASSES."qqUploadedFileForm.php");
			require_once(PATH_CLASSES."qqUploadedFileXhr.php");
				
			//$allowedExtensions = array("jpeg","jpg","png");
			$allowedExtensions = array("rar","zip");
			// max file size in bytes
			//$sizeLimit = 10485760;
			$sizeLimit = 314572800;
			
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			
			$user = $_SESSION["user_account"];
			if(i($user['id'])) {
				if($_SESSION['Cur_order']) $order_id = $_SESSION['Cur_order'];
				else {
					if($user['surname']) $name_arr['surname'] = $user['surname'];
					if($user['name']) $name_arr['name'] = $user['name'];
					if($user['patronymic']) $name_arr['patronymic'] = $user['patronymic'];
					$name = implode(' ', $name_arr);
					//_debug($_SESSION,1);
					$_SESSION['order_type']='photos';
					$sql = "INSERT INTO orders(buyer_id, ts, qty, phone, name, email, address,type_order) VALUES ('".$user['id']."', '".time()."', '".count($photos)."', '".$user['phone']."', '".$name."', '".$user['email']."', '".$user['address']."', '".$_SESSION['order_type']."')";
					// _debug($sql, 1);
					Execute($sql);
					$order_id = GetOne('select LAST_INSERT_ID() as id from orders');	
					$_SESSION['Cur_order'] = $order_id;
				}

				$i = 0;
				$sql_values = NULL;

				$path = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id.DIRECTORY_SEPARATOR;
				
				
				if(!file_exists($path)) { 
					
					mkdir($path,0777); 
					chmod($path,0777); 
					mkdir($path."thumb".DIRECTORY_SEPARATOR); 
					chmod($path."thumb".DIRECTORY_SEPARATOR,0777);
					
				
				}
				if(!file_exists($path."arc".DIRECTORY_SEPARATOR)) { 
					mkdir($path."arc".DIRECTORY_SEPARATOR); 
					chmod($path."arc".DIRECTORY_SEPARATOR,0777);
				}
				
				$result = $uploader->handleUploadarch($path, $order_id);
				//$resultt = $uploader->handleUploadarch($path, $order_id);
				//_debug( $resultt,1);
				//foreach($resultt as $result){
					
					if(isset($result['error'])) {
						
						echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
					} else {
						
						//$image = new Imagick($config["absolute-path-withoutslash"].$result['file']);
						//$image->thumbnailImage(150,0); 
						//$image->writeImage($config["absolute-path-withoutslash"].$result['file']);
					
						echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
					}
				//}				
			}
			die();
		}
		// конец загрузка арзивом
		
		public function DetermineQuality($size, $size_etalon){
			
			// http://photo.stepshop.com.ua/user-account/?Event=DetermineQuality&size0=640&size1=480&size_etalon0=1205&size_etalon1=1795
			// $size[0] = $_GET['size0'];
			// $size[1] = $_GET['size1'];
			// $size_etalon[0] = $_GET['size_etalon0'];
			// $size_etalon[1] = $_GET['size_etalon1'];
			// _debug($size_etalon, 1);
			$fullsize = i($size[0]) * i($size[1]);
			$fullsize_etalon = i($size_etalon['side1']) * i($size_etalon['side2']);
			$ratio = $fullsize / $fullsize_etalon;
			if($ratio < 0.75){
				$quality = 'Плохое';
			}elseif($ratio < 1){
				$quality = 'Хорошее';
			}else{
				$quality = 'Отличное';
			}
			// echo $fullsize, ' ', $fullsize_etalon, ' ', $ratio, ' ', $quality;
			// exit();
			
			return $quality;
		}
		
		public function OnAfterLogin() {
			$this->recalculate();
		}
		
		public function OnDisplay() {
			
			ob_start();
			if($this->Header) {
				require_once PATH_HTML_TEMPLATES.$this->Header;
			}
			
			require_once PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.$this->Template;
		
			if($this->Footer){
				require_once PATH_HTML_TEMPLATES.$this->Footer;
			}
			
			$this->PageContent = ob_get_contents();
			
			$this->_load_lang_constants();
			$this->PageContent = $this->_process_constants($this->PageContent);
			ob_end_clean();
			echo $this->PageContent;
			
		}
		public final function OnOrderPhone(){
			
			global $config;
			
			$this->Error = array();
			
			if(!$_POST['phone']) $this->Error['phone'] = true;
			
			if(!$this->Error){
				
				AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE contact = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				
				$mail->AddVars(array(
									
									'phone'		=> $_POST['phone'],
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'order-phone.html',
								$this->_get_system_variables('contact_email_subject'),
								$this->_get_system_variables('noreply_email'), 
								$this->_get_system_variables('sender_name')
							);
				
				echo json_encode(array('success' => true));
				
			}else{
				$errors = array();
				foreach($this->Error as $el => $item){
					$errors[] = $el;
				}		
				echo json_encode( array(
						'success'	=> false,
						'errors'	=> $errors
					));		
			}
			exit();
		t;
		}		
		
		public final function OnSendMessageFoot(){
			// _debug($_POST,1);_debug($_FILES, 0);
			global $config;
			
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Имя') $this->Error['name'] = true;
			// if(!$_POST['theme']) $this->Error['theme'] = true;
			if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			//if(!$_POST['department']) $this->Error['department'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Сообщение') $this->Error['message'] = true;
			// _debug($this->Error, 1);
			
			if(!$this->Error){
				
				AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE contact = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				
				$mail->AddVars(array(
									'name'		=> $_POST['name'],
									'email'		=> $_POST['email'],
									'message'	=> $_POST['message'],
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'contact.html',
								$_POST['theme'] ? $_POST['theme'] : $this->_get_system_variable('contact_email_subject'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
				
				 echo json_encode(array('success' => true,'text'=>$this->_get_system_variable('contact_success')));
				$this->Success = true;
				$this->SaveContactMessage($_POST);
			
			}else{
				
				 echo json_encode( array('success'	=> false));		
			}
			 exit();
		}
		
		
		public final function OnSendMessage(){
			// _debug($_POST);_debug($_FILES, 0);
			global $config;
			
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Ваше имя') $this->Error['name'] = true;
			// if(!$_POST['theme']) $this->Error['theme'] = true;
			if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			if(!$_POST['department']) $this->Error['department'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Ваше сообщение') $this->Error['message'] = true;
			// _debug($this->Error, 1);
			
			if(!$this->Error){
				
				AttachLib('Mailer');
				$mail = new Mailer();

				$sql = "SELECT * FROM manager_emails WHERE contact = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);                    
				}
				
				$mail->AddVars(array(
									'name'		=> $_POST['name'],
									'email'		=> $_POST['email'],
									'message'	=> $_POST['message'],
									'date'		=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'contact.html',
								$_POST['theme'] ? $_POST['theme'] : $this->_get_system_variable('contact_email_subject'),
								$this->_get_system_variable('noreply_email'), 
								$this->_get_system_variable('sender_name')
							);
				
				// echo json_encode(array('success' => true));
				$this->Success = true;
				
			}else{
				
				// echo json_encode( array('success'	=> false));		
			}
			// exit();
		}
		
		public final function OnSendMessageAtt(){
			
			global $config;
			
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Ваше имя') $this->Error['name'] = true;
			// if(!$_POST['theme']) $this->Error['theme'] = true;
			if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			// if(!$_POST['department']) $this->Error['department'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Ваше сообщение') $this->Error['message'] = true;
			// _debug($this->Error, 1);
			
			if(!$this->Error){
					
				$fio = $_POST['name'];
				$phone = $_POST['phone'];
				$email = $_POST['email'];
				$department = $_POST['department'];
				$message = $_POST['message'];
				ob_start();
					// include PATH_MAIL_TEMPLATES.'contact-att.html';
					include PATH_MAIL_TEMPLATES.'contact.html';
					$text = ob_get_contents();
				ob_end_clean();
				//_debug($text,1);
				$un		= strtoupper(uniqid(time()));
				
				$subject = "=?utf-8?B?" . base64_encode( ($_POST['subject'] ? $_POST['subject'] : 'Контактное сообщение') ) . "?=";
				
				$head = 'From: '.$this->_get_system_variable('sender_name').' <'.$this->_get_system_variable('noreply_email').'>'."\r\n";
				$head .= 'Reply-To: '.$this->_get_system_variable('noreply_email')."\r\n";
				// _debug($head, 1);
				$head .= "Mime-Version: 1.0\n";
				$head .= "Content-Type:multipart/mixed;";
				$head .= "boundary=\"----------".$un."\"\n\n";
			
				$body   = "------------".$un."\nContent-Type:text/html; charset=UTF-8\n";
				$body   .= "Content-Transfer-Encoding: 8bit\n\n".$text.'<br /><br />'."\n\n";
				
				
				foreach($_FILES as $value)
				{
					if($value['size'] > 0){
					$body   .= "------------".$un."\n";
					$body   .= "Content-Type: ".$value['type'].";";
					$body   .= "name=\"".basename($value['name'])."\"\n";
					$body   .= "Content-Transfer-Encoding:base64\n";
					$body   .= "Content-Disposition:attachment;";
					$body   .= "filename=\"".basename($value['name'])."\"\n\n";
					$body   .= chunk_split(base64_encode(file_get_contents($value['tmp_name'])))."\n";
					}
				}
				
				
				//_log($body);
				$email = GetAll('SELECT email FROM manager_emails WHERE `contact` = 1');
				
				if( $email && is_array($email) ){
					foreach ( $email as $e ){
						mail( $e['email'], $subject, $body, $head );
					}	
				}
				$this->Success = true;
				$this->SaveContactMessage($_POST);
				// $this->GoToUrl('/about.html?reply=yes');
			}else{
				// $this->GoToUrl('/about.html?reply=no');
			}
		}

		private function SaveContactMessage($data){
			 $sql = 'INSERT INTO `contact_messages` 
					SET `name` = \''.$data['name'].'\',
						`email` = \''.$data['email'].'\',
						`message` = \''.$data['message'].'\',
						'.($_FILES['file']['name'] ? 'attach_mess = \'1\', ' : '').'
						`new` = \'1\',
						`create_ts` = \''.time().'\'
					';
			Execute($sql);
		}
		
		public final function OnSendReviews(){
			global $config;
			
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Ваше имя,город') $this->Error['name'] = true;	
			//if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Введите свой отзыв') $this->Error['message'] = true;
			if(!$this->Error){	
				$fio = $_POST['name'];
				$message = $_POST['message'];
				ob_start();
					include PATH_MAIL_TEMPLATES.'reviews.html';
					$text = ob_get_contents();
				ob_end_clean();
				//_debug($text,1);
				$un		= strtoupper(uniqid(time()));
				
				$subject = "=?utf-8?B?" . base64_encode( ($_POST['subject'] ? $_POST['subject'] : 'Контактное сообщение') ) . "?=";
				
				$head = 'From: '.$this->_get_system_variable('sender_name').' <'.$this->_get_system_variable('noreply_email').'>'."\r\n";
				$head .= 'Reply-To: '.$this->_get_system_variable('noreply_email')."\r\n";
				// _debug($head, 1);
				$head .= "Mime-Version: 1.0\n";
				$head .= "Content-Type:multipart/mixed;";
				$head .= "boundary=\"----------".$un."\"\n\n";
			
				$body   = "------------".$un."\nContent-Type:text/html; charset=UTF-8\n";
				$body   .= "Content-Transfer-Encoding: 8bit\n\n".$text.'<br /><br />'."\n\n";

				foreach($_FILES as $value)
				{
					if($value['size'] > 0){
					$body   .= "------------".$un."\n";
					$body   .= "Content-Type: ".$value['type'].";";
					$body   .= "name=\"".basename($value['name'])."\"\n";
					$body   .= "Content-Transfer-Encoding:base64\n";
					$body   .= "Content-Disposition:attachment;";
					$body   .= "filename=\"".basename($value['name'])."\"\n\n";
					$body   .= chunk_split(base64_encode(file_get_contents($value['tmp_name'])))."\n";
					}
				}
				
				//_log($body);
				$email = GetAll('SELECT email FROM manager_emails WHERE `contact` = 1');
				if( $email && is_array($email) ){
					foreach ( $email as $e ){
						//mail( $e['email'], $subject, $body, $head );
					}	
				}
				 echo json_encode(array('success' => true,'text'=>'Спасибо за Ваш комментарий, он будет опубликован после проверки модератором!'));
				$this->Success = true;
				$this->SaveReviewsMessage($_POST);
				
			}else{
				 echo json_encode(array('success' => false));
			}
			exit();
		}
		private function SaveReviewsMessage($data){
			
			$sql = 'INSERT INTO `reviews` 
					SET `title` = \''.$data['name'].'\',
						`fulltext` = \''.$data['message'].'\',
						`publish` = \'0\',
						`publish_ts` = \''.time().'\',
						`create_ts` = \''.time().'\'
					';
				
			Execute($sql);
		}
		public final function OnSendMessageFriend(){

			global $config;
			
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Ваше имя') $this->Error['name'] = true;
			// if(!$_POST['theme']) $this->Error['theme'] = true;
			if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			if(!$this->ValidEmail($_POST['email_friend'])) $this->Error['email_friend'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Ваше сообщение') $this->Error['message'] = true;
			// _debug($this->Error, 1);
			
			if(!$this->Error){
				
				AttachLib('Mailer');
				$mail = new Mailer();

				/*$sql = "SELECT * FROM manager_emails WHERE contact = '1'";
				$row = GetAll($sql);
				
				for($i = 0, $count = sizeof($row); $i < $count; $i++){
					$mail->AddRecepient($row[$i]['email']);
				}*/
				$mail->AddRecepient($_POST['email_friend']);
				
				$mail->AddVars(array(
									'name'			=> $_POST['name'],
									'email'			=> $_POST['email'],
									'email_friend'	=> $_POST['email_friend'],
									'message'		=> $_POST['message'],
									'date'			=> date('d:m:Y H:i',time())								
				));
				$mail->Send(	'to-friend.html',
								// $_POST['theme'] ? $_POST['theme'] : $this->_get_system_variable('contact_email_subject'),
								$_POST['theme'] ? $_POST['theme'] : $this->_get_lang_constant('TO_FRIEND_EMAIL_SUBJECT'),
								$_POST['email'],
								$_POST['name']
							);
				
				echo json_encode(array('success' => true));
				// $this->Success = true;
				
			}else{
				$errors = array();
				foreach($this->Error as $el => $item){
					$errors[] = $el;
				}
				echo json_encode( array(
						'success'	=> false,
						'errors'	=> $errors
					));
			}
			if ($_REQUEST['ajax']) exit();
		}
		
		public function OnGetForm(){
			ob_start();
			include PATH_SNIPPETS.'order-form.php';
			$this->PageContent = ob_get_contents();
			ob_end_clean();			
			$this->_load_lang_constants();
			$this->PageContent = $this->_process_constants($this->PageContent);			
			echo $this->PageContent;
			exit;
		}
		//public function OngetDepartment($_POST){
		public function OngetDepartment(){
			//if(empty($_POST['city_name']))$_POST['city_name']='';
			$name = s($_POST['city_name']);
			$sql='SELECT id FROM `cities` where name=\''.$name.'\'';
			$id=GetOne($sql);
			$sql='SELECT * FROM `departments` WHERE city_id=\''.$id.'\'';
			$dep=GetAll($sql);
			
			echo json_encode($dep);
			exit();
		}
		/*
		 * Protected methods
		 */
		
		
		protected function _get_snippet_contact_form() {
			
			include PATH_SNIPPETS.'contact-form.html';
		}
		protected function _get_snippet_buybuttton() {
			
			include PATH_SNIPPETS.'buybuttton.html';
		}
		protected function _get_snippet_contact_form_reviews() {
			
			include PATH_SNIPPETS.'contact-form-reviews.html';
		}

		protected function _get_snippet_ask_question_form() {
			
			include PATH_SNIPPETS.'ask-question-form.html';
		}

		protected function _get_snippet_map() {
			
			include PATH_SNIPPETS.'map.php';
		}
		
		protected function _get_snippet_events() {
			
			include PATH_SNIPPETS.'events.html';
		}
		
		
		
		protected function generate_date($field, $diapazon = true, $format = ''){
			switch($this->SiteLang){
				case 'rus':
					$month = array(
						'01'=>'Января','02'=>'Февраля','03'=>'Марта','04'=>'Апреля','05'=>'Мая',
						'06'=>'Июня','07'=>'Июля','08'=>'Августа','09'=>'Сентября',
						'10'=>'Октября','11'=>'Ноября','12'=>'Декабря'
					
					);
					break;
				case 'en':
					$month = array(
						'01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May',
						'06'=>'June','07'=>'July','08'=>'August','09'=>'September',
						'10'=>'October','11'=>'November','12'=>'December'
					);
					break;
				case 'lat':
					$month = array(
						'01'=>'janv&#257;ris','02'=>'febru&#257;ris','03'=>'marts','04'=>'apr&#299;lis','05'=>'maijs',
						'06'=>'j&#363;nijs','07'=>'j&#363;lijs','08'=>'augusts','09'=>'septembris',
						'10'=>'oktobris','11'=>'novembris','12'=>'decembris'
					);
					break;
			}
			
			$d1 = date("d",$field['create_ts']);
			$m1 = date("m",$field['create_ts']);
			$y1 = date("Y",$field['create_ts']);					
			
			if(!$diapazon) {
				
				if($this->SiteLang == 'en') {
					
					return $month[$m1].' '.$d1.', '.$y1; 
				}
				elseif($this->SiteLang == 'lat') {
					
					return $y1.'. gada '.$d1.'. '.$month[$m1]; 
				}
				else{
				
					return $d1.' '.$month[$m1].' '.$y1;
				}				
			}
			else {
				
				$d2 = date("d",$field['create_ts']);
				$m2 = date("m",$field['create_ts']);
				$y2 = date("Y",$field['create_ts']);	
				
				$res = '';
				
				if($this->SiteLang == 'en') {
					
					if($m1 != $m2){
						
						$res = $month[$m1].' '.$d1.' - '.$month[$m2].' '.$d2;
					}
					else{
						
						$res = $month[$m1].' ';
						
						if($d1 != $d2){
							
							$res .= $d1.'-'.$d2;
						}
						else{
							
							$res .= $d1;
						}
					}
					
					if($y1 != $y2){
						
						$res = $month[$m1].' '.$d1.', '.$y1.' - '.$month[$m2].' '.$d2.', '.$y2;
					}
					else{
						
						$res .= ', '.$y1;
					}
				}
				elseif($this->SiteLang == 'lat') {
					
					if($y1 != $y2 && $y2){
						
						$res = $y1.'. gada '.$d1.'. '.$month[$m1].' - '.$y2.'. gada '.$d2.'. '.$month[$m2];
					}
					else{
						
						if($m1 != $m2) {
							
							$res = $y1.'. gada '.$d1.'. '.$month[$m1].' - '.$d2.'. '.$month[$m2];
						}
						else {
							
							$res = $y1.'. gada '.$d1.'. - '.$d2.'. '.$month[$m1];
						}
					}
				}
				else {
					
					if($d1 != $d2){
						
						$res = $d1.'-'.$d2;
						
					}else{
						
						$res = $d1;
						
					}
					
					if($m1 != $m2){
						$res = $d1.' '.$month[$m1].' - '.$d2.' '.$month[$m2];
						
					}else{
						
						$res .= ' '.$month[$m1];
						
					}
					
					if($y1 != $y2){
						
						$res = $d1.' '.$month[$m1].' '.$y1.' - '.$d2.' '.$month[$m2].' '.$y2;
						
					}else{
						$res .= ' '.$y1;
						
					}
				}
				
				return $res;
			}
		}
		
		protected function _get_snippet_content($name) {
			
			ob_start();
			
			switch($name) {
				
				case 'contact-form':
					$this->_get_snippet_contact_form();
					break;
				case 'buybuttton':
					$this->_get_snippet_buybuttton();
					break;
				case 'contact-form-reviews':
					
					$this->_get_snippet_contact_form_reviews();
					break;
				
				case 'ask-question-form':
					$this->_get_snippet_ask_question_form();
					break;
				
				case 'map':
					$this->_get_snippet_map();
					break;
				
				case 'ask-question-form':
					$this->_get_snippet_ask_question_form();
					break;
				
				case 'events':
					$this->_get_snippet_events();
					break;
				
				case 'news':
					$this->_get_snippet_news();
					break;
			}
			
			$content = ob_get_contents();
			ob_end_clean();
			
			return $content;
		}
		
		protected $SV		=	array();
		protected function _get_system_variables($name){
			global $DB;
			$value = '';
			if(!$this->SV[$name]){
				$value = $this->SV[$name];
			}else{
				echo $sql = "SELECT var_value FROM sys_vars WHERE var_name = '$name' ";			
				$value = $DB->GetOne($sql);
			}
			return $value;			
		}
		
		protected function _get_system_variable_array($name){
			
			return explode('|',GetOne("SELECT var_value FROM sys_vars WHERE var_name = '$name' "));			
		}
		
	
		protected function _get_system_variable($name){
			global $DB;

			$sql = "SELECT var_value FROM sys_vars WHERE var_name = '$name' ";			
			return $DB->GetOne($sql);			
		}
		
		protected function _load_lang_constants() {
			global $DB;
			
			$sql = "SELECT *
					FROM language_constants
					WHERE lang = '$this->SiteLang'";
			
			$constants = $DB->GetAll($sql);
			$this->Search = array();
			$this->Replace = array();
			foreach((array)$constants as $constant){
				$this->Search[] = '{[' . $constant['name'] . ']}';
				$this->Replace[$constant['name']] = intval($constant['html']) ? $constant['value'] : strip_tags($constant['value']);
			}
		}
		
		protected function _get_lang_constant($name){
			$sql = 'SELECT * FROM language_constants 
					WHERE lang = \''.$this->SiteLang.'\' AND name = \''.$name.'\'';
			$constant = GetRow($sql);
			return (intval($constant['html']) ? $constant['value'] : strip_tags($constant['value']));
		}
		
		protected function _process_constants($content){
			
			return str_ireplace($this->Search, $this->Replace, $content);
		}
		
		final public function is_picture($type){
			
			switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = true;
					break;
				/* case 'image/x-png':
				case 'image/png':
					$img = true;
					break; */
				case 'image/gif':
					$img = true;
					break;
		    }
		    
		    if(!$img) $img = false;
		    
		    return $img;
		}
		
		final public function check_size($file_size, $max_size){
			
			if($file_size <= $max_size){
				return true;
			}else{
				return false;
			}
		}
		
		protected function _get_file_ext($filename) {
			
			return end(explode('.', $filename));
		}
		
		protected function _check_email($email){
			
			$sql = 'SELECT e.id, e.email, e.user_id
					FROM emails e
					WHERE e.email = \''.s($email).'\'';

			return GetRow($sql);
		}
		
		protected function _insert_email($email, $type){
			
			$sql = 'INSERT INTO emails (email, type, user_id)
					VALUES (\''.$email.'\', \''.$type.'\', \''.$this->UserAccount['id'].'\')';
			Execute($sql);
			
			$sql = 'SELECT max(id) as id FROM emails';
			return GetOne($sql);
		}
		

		protected function _load_content_translation($table, $where = '', $order_by = '', $select='', $left_join = '',$limit = '', $debug = 0){
			$select = ', tr.*, t.id '.$select;
			
			$table_translation = str_replace('`', '', $table).'_translation';
			// $left_join = 'LEFT JOIN '.$table.'_translation tr ON t.id = tr.obj_id AND tr.lang = \''.$this->SiteLang.'\' '.$left_join;
			$left_join = 'LEFT JOIN `'.$table_translation.'` tr ON t.`id` = tr.`obj_id` AND tr.`lang` = \''.$this->SiteLang.'\' '.$left_join;
			//echo $left_join;
			//exit();
			return $this->_load_content($table, $where, $order_by, $select, $left_join, $limit, $debug);
		}
		
		protected function _load_content($table, $where = '', $order_by = '', $select='', $left_join = '', $limit, $debug = 0){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			
			$sql = 'SELECT t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$order_by.'
					'.($limit ? 'LIMIT '.$limit.' ' : '').'';
					
			if($debug) echo $sql;
			return GetAll($sql);
		}
		
		protected function _load_content_assoc($table, $where, $order_by, $select='', $left_join = ''){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			
			$sql = 'SELECT t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$order_by;
			
			return GetAssoc($sql);
		}
		
	
		
		protected function _load_content_info($table, $left_join = '', $where = '', $select = '', $debug = 0){
			
			if($where) $where = ' WHERE '.$where;
			if(!$select) $select = '* ';
		
			$sql = 'SELECT '.$select.'
					FROM '.$table.' '.$left_join.' 
					'.$where;
			//_debug($sql);
			if($debug) echo $sql;
			return GetRow($sql);
		}
		
		protected function _load_content_info_assoc($table, $left_join, $where, $select){
			
			if($where) $where = ' WHERE '.$where;
		
			$sql = 'SELECT '.$select.'
					FROM '.$table.' '.$left_join.' 
					'.$where;
			
			return GetAssoc($sql);
		}
		
		protected function _delete_content($table, $where){
			
			if($where) $where = ' WHERE '.$where;
		
			$sql = 'DELETE
					FROM '.$table.'
					'.$where;
			
			return Execute($sql);
		}
		
		public function _insert($data, $table) {
			
			$query = $this->_create_arr_for_sql($data);
			
			$sql = 'INSERT INTO '.$table.'  
					SET '.implode(', ', $query);
			
			if(Execute($sql)){
				return $this->_get_last_id($table);		
			}else{
				return false;
			}
		}
		
		protected function _create_arr_for_sql($data){
			global $DB;
			
			$sql_arr = array();
			if(is_array($data)){
				foreach($data as $key => $val){
					
					$field_type = end(explode(':', $key));
					$field = current(explode(':', $key));
					
					switch ($field_type){
						case 'str':
							if(!trim($val))
								$sql_arr[] = '`'.$field.'` = \'\'';	
							else
								$sql_arr[] = '`'.$field.'` = \''.s($val).'\'';
						break;
						case 'int':
							if(!i(trim($val)))
								$sql_arr[] = '`'.$field.'` = 0';	
							else
								$sql_arr[] = '`'.$field.'` = \''.i($val).'\'';
						break;
						case 'pass':
							$sql_arr[] = '`'.$field.'` = \''.$this->CryptPassword($val).'\'';
						break;
						case 'enum':
							if(trim($val))
								$sql_arr[] = '`'.$field.'` = \''.s($val).'\'';
						break;
					}
				}
			}	
			
			return $sql_arr;
		}
		
		protected function _get_last_id($table){
			
			return GetOne('select LAST_INSERT_ID() as id from '.$table);
		}
		
		public function _update($id, $data, $table, $debug = false) {
			
			$query = $this->_create_arr_for_sql($data);
			
			$sql = 'UPDATE '.$table.' 
					SET	'.implode(', ', $query).' 
					WHERE id=\''.$id.'\'';
			if($debug) _debug($sql, 1);
			
			return Execute($sql);
		}
		
	/*	
		public function _save_photo($file, $order_id) {
				global $config;
				$path = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order_id."/";
				if(!file_exists($path)) { mkdir($path,0777); @chmod($path,0777);}
				$ext = end(explode('.', $file['name']));
				$NewFilename = md5($file['tmp_name'].time()).'.'.$ext;
				move_uploaded_file($file['tmp_name'], $path.$NewFilename);
				@chmod($path.$NewFilename,0777);
				copy($path.$NewFilename, $path."thumb-".$NewFilename);
				@chmod($path."thumb-".$NewFilename,0777);
				
				$this->resize($path."thumb-".$NewFilename, $file['type'], 100, 100);
				return $NewFilename;
		}
		*/
		/**
		 * @param file field (ex. $_FILES['filename'])
		 * @param path
		 * @param width default 0
		 * @param height default 0 (if width and height 0 not resize)
		 * @param prefix default ''
		 */
		public function _save_image($file, $path, $type_arr, $i = 0) {
			
			global $config;

			if(is_array($type_arr)){
								
				$type_arr = $this->_get_img_size_arr($type_arr);

				$bool = false;
				$path = $config['absolute-path'].$path;
				
				$ext = end(explode('.', $file['name'][$i]));
				$this->MD5Filename = md5($file['tmp_name'][$i].time()).'.'.$ext;

				foreach($type_arr as $t){
					
					if(mb_strlen($t[2]) > 8)
						$name = $t[2].'.'.$ext;
					else
						$name = $t[2].'-'.$this->MD5Filename; 
					$size = explode('x',$t[0]);

					copy($file['tmp_name'][$i], $path.$name);
					@chmod($path.$name,0777);

					if ($size[0] && $size[1])
						$bool = $this->resize($path.$name, $file['type'][$i], $size[0], $size[1]);
				}		
				return $bool;
			}	
		}
		
		final public function resize($file, $type, $height, $width, $mode=0){
			
		    $img = false;
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					$img = imagecreatefromjpeg($file);
					break;
				/* case 'image/x-png':
				case 'image/png':
					$img = imagecreatefrompng($file);
					break; */
				case 'image/gif':
					$img = imagecreatefromgif($file);
					break;
		    }
		    if(!$img){
				return false;
		    }
		    
		    $curr = @getimagesize($file);

		    if(($width > $curr[0]) && ($height > $curr[1])){
				return true;
		    }
		    
		    $perc_w = $width / $curr[0];
		    $perc_h = $height / $curr[1];
		    
		    if($perc_h > $perc_w){
				$width = $width;
				$height = round($curr[1] * $perc_w);
		    } else {
				$height = $height;
				$width = round($curr[0] * $perc_h);
		    }
		    $nwimg = imagecreatetruecolor($width, $height);
		    imagecopyresampled($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[1]);
		    
		    if($mode == 1){
		    	$nwimg = imagecreatetruecolor($width, $height);
		    	
		    	if ($curr[0]>$curr[1]) 
		        imagecopyresized($nwimg, $img, 0, 0, round((max($curr[0],$curr[1])-min($curr[0],$curr[1]))/2),0, $width, $height, min($curr[0],$curr[1]), min($curr[0],$curr[1])); 
		
		        if ($curr[0]<$curr[1]) 
		        imagecopyresized($nwimg, $img, 0, 0, 0, 0, $width, $height, min($curr[0],$curr[1]), min($curr[0],$curr[1])); 
		       
		        if ($curr[0]==$curr[1]) 
		        imagecopyresized($nwimg, $img, 0, 0, 0, 0, $width, $height, $curr[0], $curr[0]);
		    }
		    
		    switch ($type){
				case 'image/jpeg':
				case 'image/jpg':
				case 'image/pjpeg':
					imagejpeg($nwimg, $file);
					break;
				/* case 'image/x-png':
				case 'image/png':
					imagepng($nwimg, $file);
					break; */
				case 'image/gif':
					imagegif($nwimg, $file);
					break;
		    }
		    
		    imagedestroy($nwimg);
		    imagedestroy($img);
			return true;
		}
		
		public function _get_img_size_arr($arr){
			if(is_array($arr)){
				foreach($arr as &$a){
					$a = explode(':', $a);
				}
				return $arr;
			}else
				return false;	
		}
		
		protected function _get_binded_items($id, $type, $what, $sort = NULL, $limit = NULL){
			global $DB;
			
			$tbl = '';
			$id_field = '';

			switch($what){
					
				case 'logo':
					$tbl = 'pictures';
					$id_field = 'id';
					break;
				
				default:
					return array();
			};
			
			$select = '';
			
			$sql = "SELECT 
						tb.*
					FROM 
						bindings b 
						INNER JOIN $tbl tb ON (b.to_obj = '$what' AND b.to_id = tb.$id_field)
					WHERE 
						b.from_obj = '$type' AND b.from_id = $id".($sort != NULL ? " ORDER BY create_ts ".$sort : "").($limit != NULL ? " LIMIT ".$limit : "");
			return GetAll($sql);
		}
		
		protected function mb_wordwrap($str, $width = 75, $break = "\n", $cut = false, $charset = null) {
    		if ($charset === null) $charset = mb_internal_encoding();

		    $pieces = split($break, $str);
		    $result = array();
		    foreach ($pieces as $piece) {
		      $current = $piece;
		      while ($cut && mb_strlen($current) > $width) {
		        $result[] = mb_substr($current, 0, $width, $charset);
		        $current = mb_substr($current, $width, 2048, $charset);
		      }
		      $result[] = $current;
		    }
		    return implode($break, $result);
		}

		protected function generate_url($el, $parent = false){
			if($el['popup']){
				return $el['url'];
			}
			if($parent){
				return '/'.$parent['url'].'/'.$el['url'];			
			}else{
				return '/'.$el['url'];
			}
		}
		
		public function getProdCategoryUrl($id,$ext = 0){
			global $DB;
			
			$sql = "SELECT c.url, c.parent_id, c.".$this->LN."title as title FROM categories c WHERE c.id = '".$id."'";
			$res = $DB->GetRow($sql);
			if($ext){
				array_unshift($this->ProductUrl,$res);
			}else{
				array_unshift($this->ProductUrl,$res['url']);
			}
			if($res['parent_id'] != 0){
				$this->getProdCategoryUrl($res['parent_id'],$ext);				
			}else{
				return;
			}
			return;
		}
		
		public function getPostAuthorUrl($id,$ext = 0){
			global $DB;
			
			$sql = "SELECT c.url, c.parent_id, c.".$this->LN."title as title FROM experts c WHERE c.id = '".$id."'";
			$res = $DB->GetRow($sql);
			if($ext){
				array_unshift($this->PostUrl,$res);
			}else{
				array_unshift($this->PostUrl,$res['url']);
			}
			if($res['parent_id'] != 0){
				$this->getPostAuthorUrl($res['parent_id'],$ext);				
			}else{
				return;
			}
			return;
		}
		
		protected function _create_faq_url($arr){
			global $DB;
			
			$url = '/faq/'.$arr['parent_url'].'/'.$arr['url'].'.html';
			
			return $url;
		}
		
		public function recalculate(){
			foreach($_SESSION['Trash']['cart'] as $id=> $trash){
				if(is_numeric($_POST['count'][$id])){
					$_SESSION['Trash']['count'] -= $_SESSION['Trash']['cart'][$id]['count'];
					$_SESSION['Trash']['count'] += $_POST['count'][$id];
					$_SESSION['Trash']['sum'] -= $_SESSION['Trash']['cart'][$id]['price']*$_SESSION['Trash']['cart'][$id]['count'];
					$_SESSION['Trash']['sum'] += $_SESSION['Trash']['cart'][$id]['price']*$_POST['count'][$id];
					$_SESSION['Trash']['cart'][$id]['count'] = $_POST['count'][$id];
				}
			}
			/*if($_SESSION['Trash']){
				unset($_SESSION['Trash']['count'],$_SESSION['Trash']['sum']); 
				foreach($_SESSION['Trash']['cart'] as $id => $product){
					$discount = 0;
					//$product = GetRow('SELECT * FROM products WHERE id = '.$id);
					
					if($product && !$_REQUEST['delete'][$id]){
						if(($count = intval($_POST['count'][$id]))){
							$_SESSION['Trash']['cart'][$id]['count'] = $count;
						}
						$_SESSION['Trash']['cart'][$id]['discount'] = $this->calculate_discount($product);
						$_SESSION['Trash']['count'] += $_SESSION['Trash']['cart'][$id]['count'];
					}else{
						unset($_SESSION['Trash']['cart'][$id]);
					}
				}
			}*/
		}
		
		public function calculate_discount($product){
			$discount = 0;
			if(i($this->_get_system_variable('discount_action')) || !$product['new']){
				$discount_arr = $this->_get_category_discount($product['category_id']);
				$discount_arr[] = intval($product['discount']);
				$discount_arr[] = intval($this->UserAccount['discount']);
				$discount = max($discount_arr);
				$discount = round($product['price'] * $discount/100, 2);
			}
			return $discount;
		}
		
		protected function _get_category_discount($parent_id, $discount = array()){
			$category = $this->_load_content_info('categories','','id = '.$parent_id,'');
			
			$discount[] = intval($category['discount']);
			if(intval($category['parent_id']) != 0){
				$discount = $this->_get_category_discount($category['parent_id'], $discount);
			}
			return $discount;
		}
		
		protected function _calc_order_price($cart){
			if(!is_array($cart)){
				$cart = unserialize($cart);
			}
			$sum = 0;
			foreach ((array)$cart as $product) {
				$this->_show_price($product['price'],$product['currency_id']);
				$this->_show_price($product['discount'],$product['currency_id']);
				$sum += round(($product['price'] - $product['discount']) * $product['count'], 2) ;
			}
			return $sum;
		}
		
		public function GetMenu($position){
			global $DB;
			$sql = "SELECT p.id,p.url,p.title,p.parent_id, p.filename
					FROM pages p 						
					WHERE $position AND p.publish = '1' ORDER BY p.order_by ASC";			
			return $DB->GetAll($sql);
		}
		
		public function _get_url($add = '',$q = ''){
			if(mb_strpos ($add, '.html' ,0, 'utf-8')){
				return $this->RootUrl.$this->LN_url.$add.$q;
			}else{
				return $this->RootUrl.$this->LN_url.$add.'.html'.$q;
			}
		}
		
		public function GetCategories($position){
			global $DB;
			$sql = "SELECT p.id,p.url,p.".$this->LN."title as title,p.parent_id, p.group
					FROM categories p 						
					WHERE $position AND `onFront` = '1' AND  `publish` = '1'  ORDER BY  p.group ASC, p.order_by ASC";
					//_debug($sql);			
			return $DB->GetAll($sql);
		}
		
		private function _get_where(&$where){
			if(i($_GET['maker'])){
				$where .= ' AND t.maker_id  = \''.i($_GET['maker']).'\' ';
			}
			
			if($_GET['сollection']){
				$where .= ' AND t.сollection_id  = \''.i($_GET['сollection']).'\' ';
			}
			
			if($_GET['style']){
				$where .= ' AND t.style  = \''.m($_GET['style']).'\' ';
			}
			
			if($_GET['type']){
				$where .= ' AND t.`type`  = \''.m($_GET['type']).'\' ';
			}
			
			if(trim($_REQUEST['keywords'])){
				$keyword = trim($_REQUEST['keywords']);
				
				if($keyword){
					$where = ' AND (MATCH t.`title` AGAINST (\''.$keyword.'\') >0';
					$arr = explode(' ', $keyword);
					if (is_for($arr)) {
						$where .= '  OR ( ';
						foreach ($arr as $val) {
							if ( mb_strlen($val,'UTF-8') > 3) {
								$val = mb_substr($val,0,-1,'utf-8');
							}
							if(trim($val) != ''){
								$where .=  $and . ' pppg.title LIKE \'%'.m($val).'%\' OR  c.title LIKE \'%'.m($val).'%\' ';
								$and = ' OR '; 		
							}		
						}
						$where .=  ' )) '; } 
						return ', MATCH t.`title` AGAINST (\''.$keyword.'\') as relev';
					} 
			}
		}
		
		public function getProduct($id, $forbidden_id, $new){
		
			if(is_array($forbidden_id)) $forbidden_id = implode(',', $forbidden_id);

			$where = '';
			if($id){
				$where = "AND t.`id` = '$id'";
			}elseif($forbidden_id){
				$where = "AND t.`id` NOT IN ($forbidden_id)";
			}
			if($new) $where .= " AND t.`new` = '1'";
			
			$this->Product = $this->_load_content_info('products t',
														'LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1 
														LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
														',
														"t.publish = 1 $where ORDER BY RAND()",
														't.*, IF(t.url != \'\',t.url,t.id) as url,  p.filename AS `img`, pt.title, pt.`fron_text`, pt.`content` 
														 '
														);
		}
		
		public function _get_products($where='',$order_by = 't.order_by, t.create_ts',$select='', $name){
		
			$items_per_page = $this->_get_system_variable('products_per_page');
			
			$pages_cnt = $this->_get_system_variable('products_pages_cnt');
			$this->Query = $_GET;
			unset($this->Query['page']);
			$this->Query = http_build_query($this->Query);
			
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}
			
			//$select = $this->_get_where($where);
			
			if(i($_GET['items'])){
					$items_per_page = $_GET['items'];
			}elseif($_GET['items'] == 'all'){
					$items_per_page = 0;
			}
			
			if($name){
				$this->Products1 = $this->_load_content_with_pn('products', 
																't.publish = 1 '.$where,
																mysql_real_escape_string($order_by),
																', t.url, c.title as category, p.filename,
																   IF(t.price_old !=\'\', t.price_old, t.price)* cr.course as real_price
																   '.$select,
																'INNER JOIN categories c ON c.id = t.category_id 
																 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																 LEFT JOIN course cr ON t.currency_id = cr.id
																 ',
																 $items_per_page,$pages_cnt,$this->Query
				);
			}else{
				$this->Products = $this->_load_content_with_pn('products', 
																't.publish = 1 '.$where,
																mysql_real_escape_string($order_by),
																', t.url, c.title as category, p.filename, pt.*				  
																   ',
																'LEFT JOIN categories c ON c.id = t.category_id AND c.publish = 1 
																 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																 ',
																 $items_per_page,$pages_cnt,$this->Query
				);
				
					
			}
			
			
		}
		
		public function _get_posts($where='',$order_by = 't.create_ts',$select='', $name){
		
			$items_per_page = $this->_get_system_variable('products_per_page');
			
			$pages_cnt = $this->_get_system_variable('products_pages_cnt');
			$this->Query = $_GET;
			unset($this->Query['page']);
			$this->Query = http_build_query($this->Query);
			
			if($_GET['sort'] && $_GET['direct']){
				$order_by = $_GET['sort'].' '.$_GET['direct'];
			}
			
			//$select = $this->_get_where($where);
			
			if(i($_GET['items'])){
					$items_per_page = $_GET['items'];
			}elseif($_GET['items'] == 'all'){
					$items_per_page = 0;
			}
			
			if($name){
				$this->Posts1 = $this->_load_content_with_pn('blogs', 
																't.publish = 1 '.$where,
																mysql_real_escape_string($order_by),
																', t.url, c.title as category, p.filename,
																   IF(t.price_old !=\'\', t.price_old, t.price)* cr.course as real_price
																   '.$select,
																'INNER JOIN experts c ON c.id = t.category_id 
																 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																 LEFT JOIN course cr ON t.currency_id = cr.id
																 ',
																 $items_per_page,$pages_cnt,$this->Query
				);
			}else{
				$this->Posts = $this->_load_content_with_pn('blogs', 
																't.publish = 1 '.$where,
																mysql_real_escape_string($order_by),
																', t.url, c.title as category, p.filename, pt.*				  
																   ',
																'LEFT JOIN experts c ON c.id = t.category_id AND c.publish = 1 
																 LEFT JOIN blogs_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
																 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1
																 ',
																 $items_per_page,$pages_cnt,$this->Query
				);
				
					
			}
			
			
		}
		
		
		protected function _load_content_with_pn($table, $where, $order_by, $select='', $left_join = '', $it = 0, $pc = 9, $req_arr = '', $page_url = 'page', $group_by = ''){
			
			if($where) $where = ' WHERE '.$where;
			if($order_by) $order_by = ' ORDER BY '.$order_by;
			if($group_by) $group_by = ' GROUP BY '.$group_by;
			
			$page = intval($_GET[$page_url]);
			
			if($page < 1){		
				$page = 1;
				$url = explode(".",$_SERVER['REDIRECT_URL']);
				$u = $url[0]; 
				$p = '?'.$page_url.'=%s';
			}else{
	
				$u = $_SERVER['REDIRECT_URL'];
				$p = '?'.$page_url.'=%s';
			}
			AttachLib('PageNavigator');
			
			 $sql = 'SELECT SQL_CALC_FOUND_ROWS t.* '.$select.'
					FROM '.$table.' t 
						'.$left_join.'
					'.$where.'
					'.$group_by.'
					'.$order_by.
					($it ? ' LIMIT '.(($page - 1) * $it).', '.$it:'');
					// _debug($sql,1);
			$result = GetAll($sql);
			$res = GetRow("SELECT FOUND_ROWS() as total");
			
			if(!$it) $it = $res['total'];
			$pn = new PageNavigator($res['total'], $it, $pc, $u, $page, $p, '&'.$req_arr, true, true);
			$pn->Template = 'page-navigator-news.html';
			return array('items' => $result, 'pn' => $pn, 'cnt' => $res['total']);
		}
		
		public function _edit_qs($replase=array(), $del=array()){
			$output = array();		
			parse_str($_SERVER['QUERY_STRING'],$output);
			_log($output);
			if(is_for($output)){
				foreach ($output as $key => $val){
					if(in_array($key,array_keys($replase))){
						$output[$key] = $replase[$key];
					}
					if(in_array($key,$del)){
						unset($output[$key]);
					}
				}
			}
			return http_build_query($output);  
		}
		
		public function _sort($field,$asc='ASC'){
		//	$asc = ($_GET[$field]) ?  'ASC' : 'DESC';
			$res = _cur_url();
			$get = $_GET;
			unset($get['sort']);
			unset($get['direct']);
			$get['sort'] = $field;
			$get['direct'] = $asc;
			$get = http_build_query($get);	
			return  $res.'?'.$get;		
		}
		
	      
		public function getBredcrumbs($start,$page_type){
			global $DB;
			
			$lang = explode('/',$this->SiteLang);
			unset($lang[0]);
			$lang = implode('', $lang);

			$lang = $this->SiteLang == 'ru' ? '' : 'ua/';

			$breadcrumbs = '<a href="'.$this->GetConfigParam('site_url').$lang.'">{[MAIN]}</a>';
			
			if($page_type == 'page' && $this->ClassName != 'Catalog'){
				if ($this->ClassName == 'Search'){
						$breadcrumbs .= ' <span>→</span> ';		
						$breadcrumbs .='Поиск';
				}else{
					$this->getPageParents($start);
					
					if(sizeof($this->Breadcrumbs)){
						$last = '';
						$this->Breadcrumbs = array_reverse($this->Breadcrumbs);
						 					
						//echo sizeof($this->Breadcrumbs);
						for($i = 0, $cnt = sizeof($this->Breadcrumbs); $i < $cnt; $i++){
							 
							if($i == $cnt - 1 ){
								if($this->Breadcrumbs[$i]['title']){
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs .=''.$this->Breadcrumbs[$i]['title'].'';
								}else{
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs .= '{[BREADCRUMBS_FAIL]}';	
								}							
							}elseif($i < $cnt  - 1 ){
								$breadcrumbs .= ' <span>→</span> ';
								$breadcrumbs .= '<a href="'.$this->GetConfigParam('site_url').$lang.$last.$this->Breadcrumbs[$i]['url'].'.html">'.$this->Breadcrumbs[$i]['title'].'</a>';
								
							}
							$last.= $this->Breadcrumbs[$i]['url'].'/';
																				
						}										
					}
				}
			}
			
			if($page_type == 'page_doc'){
				$this->getPageParents($start);
				
				if(sizeof($this->Breadcrumbs)){
					$last = '';
					$this->Breadcrumbs = array_reverse($this->Breadcrumbs);
					 					
					//echo sizeof($this->Breadcrumbs);
					for($i = 0, $cnt = sizeof($this->Breadcrumbs); $i < $cnt; $i++){
						$breadcrumbs .= '<a href="'.$this->GetConfigParam('site_url').$last.$this->Breadcrumbs[$i]['url'].'.html">'.$this->Breadcrumbs[$i]['title'].'</a>';
						$breadcrumbs .= ' <span>→</span> '; 						
						$last.= $this->Breadcrumbs[$i]['url'].'/';
																			
					}

					$document = $this->getDocument($this->UrlParts[1]);
					$breadcrumbs .= ''.$document['content'].'';	
				}
			}
			
			if($page_type == 'news'){
				if($start){
					$info = $this->getNewsInfo($start);
					$breadcrumbs .= ' <span>→</span> <a href="'.($this->SiteLang != 'ru' ? '/'.$this->SiteLang : '').'/news.html">{[NEWS]}</a>';
					$breadcrumbs .= ' <span>→</span> ';
					$breadcrumbs .= ''.$info['title'].'';					
				}else{
					$breadcrumbs .= ' <span>→</span> {[NEWS]}';										
				}			
			}
			
			if($page_type == 'auth'){
				$breadcrumbs .= '<li>Личный кабинет</li>';					
			}
			
			if($page_type == 'order'){
				$breadcrumbs .= '<li><img src="/public/images/next.jpg" alt=""/></li>';
				if($this->SiteLang == 'ru'){
					$breadcrumbs .= '<li>Корзина</li>';
				}elseif($this->SiteLang == 'en'){
					$breadcrumbs .= '<li>Basket</li>';
				}
			}
			
			if($this->ClassName == 'Catalog'){
				if(sizeof($this->Breadcrumbs)){
					$last = '';
					//echo sizeof($this->Breadcrumbs);
					
					for($i = 0, $cnt = sizeof($this->Breadcrumbs); $i < $cnt; $i++){
						 
						if($i == $cnt - 1 ){
							if($this->Product){
								if($this->Breadcrumbs[$i]['title']){
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs.= '<a href="'.$this->GetConfigParam('site_url').$lang.$last.$this->Breadcrumbs[$i]['url'].'.html">'.$this->Breadcrumbs[$i]['title'].'</a>';
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs.= $this->Product['title'];
								}else{
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs .= '{[BREADCRUMBS_FAIL]}';	
								}
							}else{
								if($this->Breadcrumbs[$i]['title']){
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs.= $this->Breadcrumbs[$i]['title'];
								}else{
									$breadcrumbs .= ' <span>→</span> ';		
									$breadcrumbs .= '{[BREADCRUMBS_FAIL]}';	
								}
							}
						}elseif($i < $cnt  - 1 ){
							$breadcrumbs .= '<span>→</span>';
							//$breadcrumbs .= '<li><a href="'.$this->_get_url($this->Breadcrumbs[$i]['url']).'">'.$this->Breadcrumbs[$i]['title'].'</a></li>';
							$breadcrumbs .= '<a href="'.$this->GetConfigParam('site_url').$lang.$last.$this->Breadcrumbs[$i]['url'].'.html">'.$this->Breadcrumbs[$i]['title'].'</a>';
							
						}
						$last.= $this->Breadcrumbs[$i]['url'].'/';
																			
					}										
				}					
			}
			
			$breadcrumbs .= '';
			
			return $breadcrumbs; 
		}
	    public function getNewsInfo($id){
			global $DB;
			$sql = "SELECT nt.* FROM news n INNER JOIN news_translation nt ON (n.id = nt.obj_id AND n.id = '".$id."' AND nt.lang = '".$this->SiteLang."')";			
			return $DB->GetRow($sql);
		}
		private function _load_articles_and_news(){				
			$items_per_page = $this->_get_system_variable('count_news_in_front');
			$order_by = ' t.publish_ts DESC';
						
			$this->FrontNews = $this->_load_content_translation('news', 'published = 1', $order_by, '', '',  $items_per_page);
			$this->FrontArticles = $this->_load_content_translation('art', 'published = 1', $order_by, '', '',  $items_per_page);	
			
		}

		public function getPageParents($id){
			global $DB;
			$sql = "SELECT p.*, pt.title 
					FROM pages p
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = '".$this->SiteLang."')
					WHERE p.id='".$id."'";
			$res = $DB->GetRow($sql);
			$this->Breadcrumbs[] = $res;
			
			if($res['parent_id'] != 0 ){
				$this->getPageParents($res['parent_id']);				
			}
		}
		public function _switch_url(){
						  $urlLn = preg_replace('$(en\/)$', '', $_SERVER['REQUEST_URI']);
						  $urlLn = preg_replace('$(&lng=en)$', '', $urlLn);
						  $urlLn = preg_replace('$(&lng=ru)$', '', $urlLn);	
						  $urlLn = preg_replace('$(\?lng=en)$', '', $urlLn);
						  return  preg_replace('$(\?lng=ru)$', '', $urlLn);
						  return $urlLn;
		}			
		
	
		function _breadcrumbs($id, $table, $fronttext='', $classname = '', $idview = false, $html = true, $crumbs = array(), $i = 0){
			global $DB;
			$sql = "SELECT `id`, `".$this->LN."title` as title, `parent_id` FROM `".$table."` WHERE id='".$id."'";
			$res = array();
			//$res = $DB->GetRow($sql);
			$res = $DB->CacheGetRow($sql);
			$crumbs[$i]['title'] = $res['title'];
			$crumbs[$i]['url']	 =  _fullurl($res['id'], $table, $fronttext, $classname, $idview, $html);	
			$i++;			
			if($res['parent_id'] != 0 ){
				return _breadcrumbs($res['parent_id'], $table, $fronttext, $classname, $idview, $html, $crumbs, $i);		
			} else {
				return array_reverse($crumbs);
			}
		}
		
		public  function _load_photo($id, $size ='') {
			global $DB;
			$sql = "SELECT `filename`
					FROM photos
					WHERE product_id=".intval($id)." AND
					`default` = '1'					
					ORDER BY id DESC
					LIMIT 1";
			$res = GetRow($sql);
			if (!isset($res['filename'])) {
				$sql = "SELECT `filename`
					FROM photos
					WHERE product_id=".intval($id)." 
					ORDER BY id DESC
					LIMIT 1";
				$res = GetOne($sql);
			}
			if (!isset($res['filename'])) {
					$res['filename'] = 'not_photo.jpeg';			
			}
			return $size. $this->RootUrl . 'public/files/products/' . $res['filename']; 
		}
		
		public function GetTopMenu($id = 0){
			global $DB;
			$sql = "SELECT p.*, pt.title 
					FROM pages p
							LEFT JOIN pages_translation pt ON (pt.obj_id = p.id AND pt.lang = '".$this->SiteLang."')
					WHERE p.`publish` = '1' AND p.parent_id ='".$id."' AND p.header_menu = 1
					ORDER BY p.order_by ASC";
			$res = $DB->GetAll($sql);
			$res_sz = count($res);
			
			for($i = 0; $i < $res_sz; $i++) {
				if ($res[$i]['url'] == 'catalog'){
					$sq = 'SELECT id, url, '.$this->LN.'title as title FROM `categories` WHERE parent_id = 0 AND publish = 1';
					$res[$i]['child'] = $this->TopCatalog = $DB->GetAll($sq);
				}else{
					$res[$i]['child'] = $this->GetTopMenu($res[$i]['id']);
				}
			}
			return $res;
			
		}
		
	public function OnLoadMap(){
			if (i($_REQUEST['id'])){
				$sql = 'SELECT name FROM country WHERE id = \''.$_REQUEST['id'].'\'';
				$region = mb_convert_case(GetOne($sql), MB_CASE_UPPER, "UTF-8");
 				$services = $this->_load_content('`shops`', '`parent_id` = \''.$_REQUEST['id'].'\' AND `type` = 1', 'title');
 				if (is_for($services)){
 					$i=0; foreach($services as $items){ $i++;
						$items['url'] = str_replace('http://', '', $items['url']);
						if ($i == 1){
							$str1 .= '<tr>';
						}
						/*$str1 .= '<td><a class="link" href="#" title="">'.$items['title'].'</a>'.$items['content'].'</td>';*/
						$str1 .= "<td><a ".($items['url'] ? 'class="link" href="http://'.$items['url'].'"' : '')."  title=''>".$items['title']."</a>".$items['content']."</td>";
						if ($i == 3){
							$i = 0;
							$str1 .= '</tr>';
						}
					}
 				}else{
 					// $str1 .= '<tr><td><p class="info-text">Магазинов не найдено</p></td></tr>';
					$str1 = '';
 				}
				
				$services = $this->_load_content('`shops`', '`type` = 2', 'title');
 				if (is_for($services)){
 					$i=0; foreach($services as $items){ $i++;
						$items['url'] = str_replace('http://', '', $items['url']);
						if ($i == 1){
							$str2 .= '<tr>';
						}
						/*$str2 .= '<td><a class="link" href="#" title="">'.$items['title'].'</a>'.$items['content'].'</td>';*/
						$str2 .= "<td><a ".($items['url'] ? 'class="link" href="http://'.$items['url'].'"' : '')."  title=''>".$items['title']."</a>".$items['content']."</td>";
						if ($i == 5){
							$i = 0;
							$str2 .= '</tr>';
						}
					}
 				}else{
 					// $str2 .= '<tr><td><p class="info-text">Магазинов не найдено</p></td></tr>';
					$str2 = '';
 				}
				
				$services = $this->_load_content('`shops`', '`parent_id` = \''.$_REQUEST['id'].'\' AND `type` = 3', 'title');
 				if (is_for($services)){
 					$i=0; foreach($services as $items){ $i++;
						$items['url'] = str_replace('http://', '', $items['url']);
						if ($i == 1){
							$str3 .= '<tr>';
						}
						/*$str3 .= '<td><a class="link" href="#" title="">'.$items['title'].'</a>'.$items['content'].'</td>';*/
						$str3 .= "<td><a ".($items['url'] ? 'class="link" href="http://'.$items['url'].'"' : '')."  title=''>".$items['title']."</a>".$items['content']."</td>";
						if ($i == 5){
							$i = 0;
							$str3 .= '</tr>';
						}
					}
 				}else{
 					// $str3 .= '<tr><td><p class="info-text">Магазинов не найдено</p></td></tr>';
					$str3 = '';
 				}
			
				echo json_encode( array('success'	=> true, 'data1' => $str1, 'data2' => $str2, 'data3' => $str3,  'region' => $region));
			}else{
				echo json_encode( array('success'	=> false));
			}
			exit(); 
		}
		
		public function OnGetCalendarNews(){
			
			global $DB;
			$items_per_page = 3;
			$max_pages_cnt = 5;
			$page = $_REQUEST['page'];
			if($page < 1 || !$page){				
				$page = 1;
			}
			$url = '';
			
				   $sql = ' 
						SELECT nt.* , n.url, n.publish_ts,n.filename1
					FROM news n
					LEFT JOIN news_translation  nt ON (nt.obj_id = n.id AND nt.lang = \''.$this->SiteLang.'\')
					WHERE 
						n.news = 0 AND   n.main = 0 AND   
						n.published = \'1\' AND nt.title <> "" 
					ORDER BY	n.publish_ts  DESC
					
					LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page.'
					';
			$Info = GetAll($sql);
			
			if(is_for($Info)){
				foreach ($Info as $news_item){
						$str .= '<div class="events">
			                         <a class="eventlink" href="'.($this->SiteLang == 'ua'?'/ua/':'/').'news/view/'.($news_item['url']).'.html">
											<span class="eventdate cufon">
												<span class="eventday">'.date('d',$news_item['publish_ts']).'</span>
												<span class="eventmonth">'.($this->SiteLang == 'ua' ? $this->Monthua[date('n',$news_item['publish_ts'])] : $this->Month[date('n',$news_item['publish_ts'])]).'</span>
											</span>
											<span class="eventdatehover cufon">
												<span class="eventday">'.date('d',$news_item['publish_ts']).'</span>
												<span class="eventmonth">'.($this->SiteLang == 'ua' ? $this->Monthua[date('n',$news_item['publish_ts'])] : $this->Month[date('n',$news_item['publish_ts'])]).'</span>
											</span>
											<span class="event">
												<span class="eventtextheader cufon">'.$news_item['title'].'</span>
												<span class="eventtext">'.strip_tags( $news_item['introtext']).'</span>
											</span>
										</a>
									</div>';
				}
				//echo $str;
				/*$res = GetRow("SELECT FOUND_ROWS() as total");
				AttachLib('PageNavigator');
				$this->PageNavigatorInfo = new PageNavigator($res['total'], $items_per_page, $max_pages_cnt, $this->RootUrl .($this->SiteLang == 'en'?$this->SiteLang.'/':''). $url, $page, 'page-info/%s',true, true);
				$this->PageNavigatorInfo->Template = 'page-navigator-news.html';
				*/
				echo json_encode( array('success'	=> true, 'data' => $str));
			}else{
				echo json_encode( array('success'	=> false));
			}
			
			exit();
				
		}
		
		public function OnGetVideo(){
			if (i($_REQUEST['id'])){
				echo GetOne('SELECT title FROM video WHERE id = \''.$_REQUEST['id'].'\' ');
			}
			exit();
		}
		
		public function getProductUrl($id){
		
			$sql = "SELECT pr.`url` AS 'product_url', cat.`url` AS 'category_url' 
					FROM `products` pr 
					LEFT JOIN `categories` cat ON (cat.`id` = pr.`category_id`) 
					WHERE pr.`id` = '$id' 
					";
			$urls = GetRow($sql);
			$url = 'catalog/'.$urls['category_url'].'/'.$urls['product_url'].'.html';
			
			return $url;
		}
		
		/* отключено, т.к. удаляются все заказы; вызов в cron остался */
		public function OnDeleteOldOrders(){
			global $config;
		
			$lifetime = $this->_get_system_variable('order_lifetime');
			$date = time() - $lifetime;
			$path = $config['absolute-path'].'public/files/images/orders/';
			
			$sql = "SELECT `id`, `cart` FROM `orders` WHERE `ts` < '$date' AND `state` = 'not_order'";
			$orders = GetAll($sql);
			
			foreach($orders as $order){
				$order_id = $order['id'];
				if($order_id){
					$sql = 'SELECT * FROM orders WHERE id=\''.$order_id.'\'';
					$OrderInfo = GetRow($sql);
					$items = unserialize($OrderInfo['cart']);
					foreach($items as $id=>$item){
						$sql = 'DELETE FROM products_variants WHERE product_id=\''.$id.'\'';
						Execute($sql);
						$sql = 'DELETE FROM products WHERE id=\''.$id.'\'';
						Execute($sql);
					}
					RemoveDir($path.$order_id);
					Execute("DELETE FROM `orders` WHERE `id` = '$order_id'");
				}
			}
			
			exit;
		}
		public function _get_type_order($type=0){
			$translete='';
			
			if($type){
				switch($type){
					case 'photos':
						$translete='Фото';
						break;
					case 'canvas':
						$translete='Холст';
						break;
					case 'photobook':
						$translete='Фотокниги';
						break;
					case 'souvenirs':
						$translete='Сувенирная продукция';
						break;
					
				}
			}
			
			return $translete;
		}
		
		public function OnGetSession(){
		
			echo json_encode($_SESSION);
			exit();
		}
		public function GetNotInOpt($pr=1){
			$wh='';
			foreach ($this->Optionsize as $key=>$val){
				if($key==$pr){
				
				}else{
					$wh.=' AND pot.option_id !='.$val.' ';
				}
			}
			return $wh;
		}
		protected function _get_cat_options($id, $group = true, $for_ver=false){
			global $DB;
			if($this->UserInfo['pricenamber']){ 
				$rr='';
				
				$rr=$this->GetNotInOpt($this->UserInfo['pricenamber']);
				$where=$rr;
				/*if($this->UserInfo['id']==7){
					$rr=$this->GetNotInOpt($this->UserInfo['pricenamber']);
				}
				 if($this->UserInfo['pricenamber']==1){
					$where=' AND pot.option_id !=6 AND pot.option_id !=7 AND pot.option_id<8  ';
				}elseif($this->UserInfo['pricenamber']==2){
					$where=' AND pot.option_id !=1 AND pot.option_id !=7 AND pot.option_id<8 ';
				}elseif($this->UserInfo['pricenamber']==3){
					$where=' AND pot.option_id !=1 AND pot.option_id !=6 AND pot.option_id<8 ';
				}else{
					
					$where='1=1 AND pot.option_id<8 ';
				} */
				//	_debug($this->UserInfo,1);
			}
			//if($_GET['alex']) {_debug($id,1);}
			$sql = 'SELECT 
						po.*, 
						pog.title as group_title, 
						pog.id as group_id 
					FROM 
						product_option_type pot 
						LEFT JOIN 
						product_options as po 
						ON po.id = pot.option_id
						LEFT JOIN product_options_groups pog
						ON pog.id = po.type_id
					WHERE 
						pot.category_id = '.$id.$add.' '.$where.'  
					ORDER BY pog.sort_order, po.order_by
			';
			//_debug($sql,1);
			$res = $DB->GetAll($sql);
			
			if($res){
				foreach($res as $option){
					
					/* _debug( $res);
					_debug( $option,1); */
					$sql = 'SELECT 
								* 
							FROM 
								product_option_variants
							WHERE 
							option_id = '.$option['id'].'  
							/*	option_id = '.$option['id'].'  AND option_id<8  */
							ORDER BY
								sort
					';
					
					$option['variants'] = $DB->GetAll($sql);
					if($group){
						if($option['user_choice']){
							$options['user'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['user'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}else{
							$options['all'][intval($option['group_id'])]['name'] = $option['group_title'];
							$options['all'][intval($option['group_id'])]['items'][$option['id']] = $option;
						}
					}else{
						$options[$option['id']] = $option;
					}
					
					
				}
			}
			//_debug($options,1);
			return $options;
		}
		
		/* protected function get_prod_option_field($prod, $chars, $flag, $field){
		
			foreach($chars['all'][0]['items'] as  $option){
				if($option[$flag]){
					foreach($option['variants'] as $variant){
						if($variant['id'] == $prod['options'][$option['id']]){
							$photo_print_size = $variant[$field];
						}
					}
				}
			}
			return $photo_print_size;
		} */
		protected function get_prod_option_field($prod, $chars, $flag, $flag_val, $field){
		
			foreach($chars['all'][0]['items'] as $kay=> $option){
				if($option['id']==$flag){
				
					foreach($option['variants'] as $variant){
						if($variant['id'] == $prod['options'][$flag]){
							$photo_print_size = $variant['title'];
						}
					}
				}
			}
			return $photo_print_size;
		}
		
	}
?>
