<?	
	class Subscribe extends ContentPage{
		
		/*
		 * Protected properties
		 */
		
		
		
		/*
		 * Public methods
		 */
		
		public function OnCreate(){
		
			$this->SetTitle('{[SUBSCRIBE]}');
			$this->SetTemplate('subscribe.php');
			
		}
		
		public function OnSubscribe(){

			$name = s($_POST['name:str']);
			$email = s($_POST['email:str']);
			
			if($this->ValidEmail($email)){
					
				$code = md5($email.time());
				
				$subscriber = GetRow('SELECT * FROM users WHERE id = \''.$this->UserAccount['id'].'\' ');
				
				$subscriber['name:str']		= $name;
				$subscriber['email:str']	= $email;
				$subscriber['code:str']		= $code;
				$subscriber['lang:str']		= $this->SiteLang;
				
				if($subscriber['id']){
					if($this->_update($subscriber['id'], $subscriber, 'users')){
						$this->Success = $this->_get_lang_constant('SUBSCRIBE_REQUEST_SUCCESS');
					}else{
						$this->Error = $this->_get_lang_constant('SUBSCRIBE_REQUEST_FAIL');
					}
				}else{
					if($this->_insert($subscriber, 'users')){
						$this->Success = $this->_get_lang_constant('SUBSCRIBE_REQUEST_SUCCESS');
					}else{
						$this->Error = $this->_get_lang_constant('SUBSCRIBE_REQUEST_FAIL');
					}
				}
				
				$url	= "?Event=ConfirmSubscribe&email=$email&code=$code";
				$url2	= "?Event=UnSubscribe&email=$email&code=$code";
				
				AttachLib('Mailer');
				$mail = new Mailer();
				$mail->AddRecepient($email);                    
				$mail->AddVars(array(
					'name'	=> $subscriber['name'].' '.$subscriber['patronymic'], 
					'site'	=> $this->GetConfigParam('site_url'),
					'date'	=> date('d:m:Y H:i',time()),
					'code1'	=> $url,
					'code2'	=> $url2          
				));
				
				$mail->Send($this->LN.'subscribe.html', // $this->LN_url переделать на сайте на $this->LNG_url
							$this->_get_system_variable('subscribe_subject'),
							$this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name')
				);
			}else{
				$this->Error = $this->_get_lang_constant('EMAIL_INCORRECT');
			}
			
			if($_REQUEST['ajax']){
				if($this->Success){
					echo json_encode(array(
						'success'	=> true,
						'callback'	=> '',
						'text'	=> $this->Success
					));
				}elseif($this->Error){
					echo json_encode(array(
						'success'	=> false,
						'callback'	=> '',
						'text'	=> $this->Error
					));
				}
				
				exit;
			}
		}
		
		public function OnConfirmSubscribe(){
			   
			$email = s($_GET['email']);
			$code = s($_GET['code']);
			
			if($email && $code){
				$id = GetOne("SELECT `id` FROM `users` WHERE `email` = '$email' AND `code` = '$code'");
				if($id){
					if(Execute("UPDATE `users` SET `subscribe` = '1' WHERE `id` = '$id'")){
						$this->Success = '{[SUBSCRIBE_CONFIRM_SUCCESS]}';
					}
				}
			}else{
				$id = $this->UserAccount['id'];
				if(Execute("UPDATE `users` SET `subscribe` = '1' WHERE `id` = '$id'")){
					$this->Success = '{[SUBSCRIBE_CONFIRM_SUCCESS]}';
				}else{
					$this->Fail = '{[SUBSCRIBE_CONFIRM_FAIL]}';
				}
			}
		}

		public function OnUnSubscribe(){
					   
			$email	= s($_GET['email']);
			$code	= s($_GET['code']);
			
			if($email && $code){
				$id = GetOne("SELECT `id` FROM `users` WHERE `email` = '$email' AND `code` = '$code'");
				if($id){
					if(Execute("UPDATE `users` SET `subscribe` = '0' WHERE `id` = '$id'")){
						$this->Success = '{[SUBSCRIBE_UN_SUCCESS]}';
					}
				}
			}else{
				$id = $this->UserAccount['id'];
				if(Execute("UPDATE `users` SET `subscribe` = '1' WHERE `id` = '$id'")){
					$this->Success = '{[SUBSCRIBE_UN_SUCCESS]}';
				}else{
					$this->Fail = '{[SUBSCRIBE_UN_FAIL]}';
				}
			}
		}
	
	}
?>