<?

	class Search extends ContentPage {
		
		public $Products = array();
		
		public function OnCreate() {
					
			$this->AddTitle('Поиск');
			//$this->_get_simple_result();
			
			if($keyword = s(trim($_REQUEST['find']))){
				$this->_product_search($keyword);
			}
			$this->SetTemplate('search.php');
			
		}
		
		public function OnSearchProducts(){
			include PATH_HTML_TEMPLATES.'search.php';
			exit;
		}
		
		public function OnSearchProductsOrder(){
			include PATH_SNIPPETS.'order-add-list.php';
			exit;
		}
		
		private function _product_search($keyword){
			$items_per_page = $this->_get_system_variable('products_per_page');
			$query = $_GET;
			unset($query['page'],$query['Event']);
			$query = http_build_query($query);
			$order_by = ' t.create_ts';
			if(i($_GET['items'])){
					$items_per_page = $_GET['items'];
			}elseif($_GET['items'] == 'all'){
					$items_per_page = 0;
			}
			$this->Products = $this->_load_content_with_pn('products', 
								't.publish = 1 AND (pt.title LIKE \'%'.$keyword.'%\')',
								mysql_real_escape_string($order_by),
								', IF(t.url != \'\',t.url,t.id) as url, c.title as category, p.filename, pt.*',
								'INNER JOIN categories c ON c.id = t.category_id 
								 LEFT JOIN products_translation pt ON t.id = pt.obj_id  AND pt.lang = \''.$this->SiteLang.'\'
								 LEFT JOIN photos p ON p.product_id = t.id AND p.default = 1',
								 $items_per_page,9,$query
			);
		}

	}		
?>