<?
/*	 if($_REQUEST['Event'] == 'UserRegister' || $_REQUEST['Event'] == 'CompanyRegister'){
		define('IN_PHPBB', true);

		// set scope for variables required later 
	    global $phpbb_root_path;
	    global $phpEx;
	    global $db;
	    global $user;
	    global $auth;
	    global $cache;
	    global $template;
	
	    $phpEx = substr(strrchr(__FILE__, '.'), 1);
	    $phpbb_root_path = './forum/';
	
	   //includes all the libraries etc. required 
	    require($phpbb_root_path ."common.php");
	
	    $user->session_begin();
	    $auth->acl($user->data);
	
	     //the file with the actual goodies 
	    require($phpbb_root_path .'includes/functions_user.php');
	    require($phpbb_root_path .'includes/functions_module.php');
	}
*/    
	class Registration extends ContentPage {

		/*
		 * Protected properties
		 */
		
		protected $TemplatesBaseDir			= 'registration/';
		
		protected $UserInfo 				= array();	
		protected $ErrorsUser 				= array();
		protected $ErrorsUser_sz 			= 0;		
		protected $Category					= array();
		protected $Category_sz				= 0;
		
		
		/*
		 * Public methods
		 */
		
		public function OnCreate() {
			global $config, $url_parts;
			
			if($_SESSION['user_account'])
				$this->GoToUrl($this->RootUrl);

			$this->PageInfo = GetRow('	SELECT p.*
										FROM pages p 
										WHERE 
						  						p.publish=\'1\' AND 
						  						p.url=\''.implode('/', $this->UrlParts).'\'');	
			// $this->Shops = $this->getShops();
			$this->AddTitle('Регистрация');
			switch($this->UrlParts[1]){
				default:
					$this->SetTemplate('user-registration.php');
				break;
				case 'success':
					$this->SetTemplate('success.html');
				break;
				case 'fail':
					$this->SetTemplate('fail.html');
				break;
			}
			
				
		}
		
		public function OnSuccess() {
			// $this->GoToUrl('/registration/success.html');
			$this->GoToUrl('/user-account.html');
		}

		public function OnFail() {
			$this->GoToUrl('/registration/fail.html');
		}
		
		public function OnConfirmAccount() {
			
			if($_GET['email'] && $_GET['code']){
				$email = s($_GET['email']);
				$code = s($_GET['code']);
				$sql = "SELECT `id` FROM `users` WHERE `email` = '$email' AND `code` = '$code'";
				$id = GetOne($sql);
				if($id){
					// $this->GoToUrl('/registration-success.html');
					Execute("UPDATE `users` SET `active` = '1' WHERE `id` = '$id'");
					$this->ConfirmAccount = 'success';
					
					$sql = "SELECT *  
							FROM users u 
							WHERE u.`id` = '$id'
							";
					// _debug($sql, 1);
					$this->UserAccount = GetRow($sql);
					// _debug($this->UserAccount, 1);
					// _debug($this->AuthData, 1);
					// $_SESSION[$this->AuthData['session']] = $this->UserAccount;
					$_SESSION['user_account'] = $this->UserAccount;
					
					$this->OnSuccess();
				}else{
					// $this->GoToUrl('/registration-fail.html');
					$this->OnFail();
				}
			}else{
					// $this->GoToUrl('/registration-fail.html');
					$this->OnFail();
				}
		}
		
		public function OnUserRegister() {//_debug($_POST, 1);
			
			// if($_POST['login:str'] == 'Логин*') unset($_POST['login:str']);
			// if($_POST['surname:str'] == 'Фамилия') unset($_POST['surname:str']);
			// if($_POST['name:str'] == 'Имя*') unset($_POST['name:str']);
			// if($_POST['patronymic:str'] == 'Отчество') unset($_POST['patronymic:str']);
			// if($_POST['email:str'] == 'Email*') unset($_POST['email:str']);
			
			$_POST['create_ts:str'] = time();
			// $_POST['birthday:str'] = strtotime($_POST['birthday:str']);
			// $_POST['date_purchase:str'] = strtotime($_POST['date_purchase:str']);
			
			if($this->_check_data($_POST)){
				if($id = $this->_add_user($_POST)){//_debug($_POST, 1);
					if($id){
						$this->_send_mail($_POST, $id);
						// $this->_send_user_mail($_POST, $id);
						// $this->_send_user_mail_att($_POST, $id);
						// $coupon = $this->getCoupon($_POST, $id);
						// _debug($coupon, 0);
					}
					if($_REQUEST['ajax']){
						echo json_encode(array(
							'success' => true,
							'text' => '<strong style="color:#999">На Ваш e-mail была отправлена ссылка для активации<br /> 
							Вашего аккаунта и сгенерированный пароль. Вы его <br /> можете поменять в любой момент в личном кабинете.</strong>'							
						));
						exit();
					}else{
						$this->OnSuccess();
					}
				}else{
					if($_REQUEST['ajax']){
						echo json_encode( array(
							'success'	=> false,
							'text'	=> 'Ошибка! За помощью обратитесь к администрации сайта'
						));
						exit();
					}else{
						$this->OnFail();
					}
				}
			}
			
		}
		
		
		/*
		 * Private methods
		 */
	
		private function _add_user($data){//_debug($data, 1);
			global $DB;
			
/*			$user_row = array(
						'username' => s($data['nickname']),
						'user_password' => md5($data['password']), 'user_email' => $data['email'],
						'group_id' => '2',
						'user_timezone' => '1.00',
						'user_dst' => 0,
						'user_lang' => 'ru',
						'user_type' => '0',
						'user_actkey' => '',
						'user_dateformat' => 'd M Y H:i',
						'user_style' => '0',
						'user_regdate' => time(),
						);
*/			
			/* END PHPBB REGISTRATION*/ 	
			
			$id = $this->_insert($data, 'users');
						
			if($id){
//				$phpbb_user_id = user_add($user_row);
				return $id;
			}else{
				return false;
			}
		}
		
		/*private function _send_mail($data, $id) {
			global $config;
			
			AttachLib('Mailer');
			
			$sql = 'SELECT * 
					FROM users
					WHERE id = \''.$id.'\'';
			
			$res = GetRow($sql);
			
			$v = base64_encode(serialize($res)); 
			
			$d['url'] = $v;
	
			$m = new Mailer();
						
			$m->AddRecepient($res['email']);
			$m->AddVars($d);
			$m->Send('registration-mail.html', 'Подтверждение регистрации', $this->_get_system_variable('reg_email'), $this->_get_system_variable('sender_name'));
		}*/
		
		private function _send_mail($data, $id) {//_debug($data, 1);
			global $config;
			
			AttachLib('Mailer');
			
			$sql = 'SELECT * 
					FROM users
					WHERE id = \''.$id.'\'';
			
			$res = GetRow($sql);
			
			$v = base64_encode(serialize($res));
			$email = $res['email'];
			$code = md5($res['email'].$this->GenRandomString(64)).time();
			$pass_length = $this->_get_system_variable('pass_length_generate');
			$pass = generate_password($pass_length);
			$pass_md5 = $this->CryptPassword($pass);
			$url = "registration.html?Event=ConfirmAccount&email=$email&code=$code";
			$site = $this->RootUrl;
			
			// $d['url'] = $v;
			$d['url'] = $url;
			$d['email'] = $email;
			$d['code'] = $code;
			$d['pass'] = $pass;
			$d['site'] = $site;
			
			$sql = "UPDATE `users` 
					SET 
						`password` = '$pass_md5', 
						`code` = '$code' 
					WHERE `id` = '$id'
				";
			// _debug($sql, 1);
			Execute($sql);
			
			$m = new Mailer();
						
			$m->AddRecepient($res['email']);
			$m->AddVars($d);
			// $m->Send('registration-mail.html', 'Подтверждение регистрации', $this->_get_system_variable('reg_email'), $this->_get_system_variable('sender_name'));
			// _debug($m, 1);
			$m->Send('user-registration.html', 'Подтверждение регистрации', $this->_get_system_variable('noreply_email'), $this->_get_system_variable('sender_name'));
			// _debug($m, 1);
		}
		
		private function _send_user_mail($data, $id) {//_debug($data, 1);
			global $config;
			
			// PDF
			$html = $this->getCoupon($data, $id);
			
			include("MPDF54/mpdf.php");

			$mpdf = new mPDF(); 

			$mpdf->WriteHTML($html);
			// $mpdf->Output();
			// $content = $mpdf->Output('', 'S');
			// _debug($content, 1);
			// $mpdf->Output('MPDF54/asd.pdf');
			$mpdf->Output('public/files/coupons/coupon_'.$id.'.pdf');
			// exit;
			// /PDF
			
			AttachLib('Mailer');
			
			$d['fio'] = trim(implode(' ',array($data['surname:str'],$data['name:str'],$data['patronymic:str'])));
			$d['login'] = $data['login:str'];
			$d['pass'] = $data['password:pass'];
			$d['site'] = $this->GetConfigParam('site_url');
			$m = new Mailer();
						
			$m->AddRecepient($data['email:str']);
			$m->AddVars($d);
			$m->Send('user-registration.html', 
					$this->_get_system_variable('registration_subject'), 
					$this->_get_system_variable('noreply_email'), 
					$this->_get_system_variable('sender_name'));
			// _debug($m, 1);
		}
		
		public final function _send_user_mail_att($data, $id){
			
			global $config;
			/*
			$this->Error = array();
			if(!$_POST['name'] || $_POST['name'] == 'Ваше имя') $this->Error['name'] = true;
			// if(!$_POST['theme']) $this->Error['theme'] = true;
			if(!$this->ValidEmail($_POST['email'])) $this->Error['email'] = true;
			if(!$_POST['department']) $this->Error['department'] = true;
			if(!$_POST['message'] || $_POST['message'] == 'Ваше сообщение') $this->Error['message'] = true;
			// _debug($this->Error, 1);
			*/
			if(!$this->Error){
					
				$fio = $_POST['name'];
				$phone = $_POST['phone'];
				$email = $_POST['email:str'];
				$department = $_POST['department'];
				$message = $_POST['message'];
				ob_start();
					// include PATH_MAIL_TEMPLATES.'contact-att.html';
					include PATH_MAIL_TEMPLATES.'user-registration.html';
					$text = ob_get_contents();
				ob_end_clean();
				//_debug($text,1);
				$un		= strtoupper(uniqid(time()));
				
				$subject = "=?utf-8?B?" . base64_encode( ($_POST['subject'] ? $_POST['subject'] : $this->_get_system_variable('registration_subject')) ) . "?=";
				
				$head = 'From: '.$this->_get_system_variable('sender_name').' <'.$this->_get_system_variable('noreply_email').'>'."\r\n";
				$head .= 'Reply-To: '.$this->_get_system_variable('noreply_email')."\r\n";
				// _debug($head, 1);
				$head .= "Mime-Version: 1.0\n";
				$head .= "Content-Type:multipart/mixed;";
				$head .= "boundary=\"----------".$un."\"\n\n";
			
				$body   = "------------".$un."\nContent-Type:text/html; charset=UTF-8\n";
				$body   .= "Content-Transfer-Encoding: 8bit\n\n".$text.'<br /><br />'."\n\n";
				
				if(!$_FILES){
					// PDF
					$html = $this->getCoupon($data, $id, $no_link = true);
					
					include("MPDF54/mpdf.php");

					$mpdf = new mPDF();

					$mpdf->WriteHTML($html);
					// $mpdf->Output();
					// $content = $mpdf->Output('', 'S');
					// _debug($content, 1);
					// $mpdf->Output('MPDF54/asd.pdf');
					$mpdf->Output('public/files/coupons/coupon_'.$id.'.pdf');
					// exit;
					// /PDF
					
					$_FILES = array(array(
						'size' => '1',
						'type' => 'text/html',
						'name' => 'coupon_'.$id.'.pdf',
						'tmp_name' => 'public/files/coupons/coupon_'.$id.'.pdf'
					));
				}
				// _debug($_FILES, 1);
				foreach($_FILES as $value)
				{
					if($value['size'] > 0){
					$body   .= "------------".$un."\n";
					$body   .= "Content-Type: ".$value['type'].";";
					$body   .= "name=\"".basename($value['name'])."\"\n";
					$body   .= "Content-Transfer-Encoding:base64\n";
					$body   .= "Content-Disposition:attachment;";
					$body   .= "filename=\"".basename($value['name'])."\"\n\n";
					$body   .= chunk_split(base64_encode(file_get_contents($value['tmp_name'])))."\n";
					}
				}
				// _debug($body, 1);
				
				//_log($body);
				/*$email = GetRow('SELECT email FROM manager_emails WHERE `contact` = 1');

				if( $email && is_array($email) ){
					foreach ( $email as $e ){
						mail( $e, $subject, $body, $head );
					}	
				}*/
				// _debug($email, 1);
				mail($email, $subject, $body, $head);
				
				$this->Success = true;
				// $this->GoToUrl('/about.html?reply=yes');
			}else{
				// $this->GoToUrl('/about.html?reply=no');
			}
		}
		
		
		private function _check_data($data) {
			
			/*
			if(mb_strlen($data['password:pass']) < 6) {
				$this->ErrorsUser['password:pass'] = 'Пароль должен быть не короче 6 символов!';
			}
			
			if(mb_strlen($data['password:pass']) > 20) {
				$this->ErrorsUser['password:pass'] = 'Пароль не должен быть длиннее 20 символов!';
			}
			
			if($data['password:pass'] != $data['confirm']){
				$this->ErrorsUser['confirm'] = 'Подтверждение не совпадает с паролем!';
			}
			
			if(!$data['name:str']){
				$this->ErrorsUser['name:str'] = 'Введите своё имя!';
			}
			*/
			/*
			if(!$data['fio:str']){
				$this->ErrorsUser['fio:str'] = 'ФИО участника акции';
			}
			if(!$data['date_purchase:str']){
				$this->ErrorsUser['date_purchase:str'] = 'Дата приобретения товара';
			}
			if(!$data['product_name:str']){
				$this->ErrorsUser['product_name:str'] = 'Наименования приобретенных товаров';
			}
			if(!$data['product_serial_number:str']){
				$this->ErrorsUser['product_serial_number:str'] = 'Серийный номер/-а приобретенного товара';
			}
			if(!$data['shop_id:int'] && !$data['shop_title:str']){
				$this->ErrorsUser['shop_id:int'] = 'Место покупки';
			}
			if(!$data['city:str']){
				$this->ErrorsUser['city:str'] = 'Город';
			}
			*/
			/*if(!$data['phone:str']){
				$this->ErrorsUser['phone:str'] = 'Введите свой телефон!';
			}*/
			
			/*if(strtolower($_SESSION['VeryficationNumber']) != strtolower($data['captcha'])) {
				$this->ErrorsUser['captcha'] = 'Неправильно введён код с картинки!';
			}*/
			
			$sql = 'SELECT id 
					FROM users
					WHERE email = \''.$data['email:str'].'\'';
			$res = GetOne($sql);
			
			if($res) {
				$this->ErrorsUser['email:str'] = 'Данный e-mail используется другим пользователем!';
			}else{
				if (!$this->ValidEmail($data['email:str'])){
					$this->ErrorsUser['email:str'] = 'E-mail введён неверно!';
				}
			}
			if(!$data['email:str']){
				$this->ErrorsUser['email:str'] = 'Адрес электронной почты';
			}
			
			/*if($data['login:str']){
				if(GetOne('select id from users where login = \''.$data['login:str'].'\'')){
					$this->ErrorsUser['login:str'] = 'Введённый вами логин уже используется!';	
				}
				$res =  eregi("^[a-z0-9_.-]*$", $data['login:str']);			
				if(!$res){
					$this->ErrorsUser['login:str'] = 'Логин может содержать только символы английского алфавита!';				
				}
			}else{
				$this->ErrorsUser['login:str'] = 'Не введён логин!';
			}*/

			if($this->ErrorsUser){
				$errors = array(); $text = '';
				foreach($this->ErrorsUser as $el => $item){
					$errors[] = $el;
					$text .= '<li>'.$item.'</li>';
				}
				if($_REQUEST['ajax']){
					echo json_encode( array(
							'success'	=> false,
							// 'errortext'	=> '<ul>'.$text.'</ul>',
							'text'	=> $this->ErrorsUser['email:str'],
							'errors'	=> $errors
						));
					exit;
				}
			}else{
				return true;
			}	
		}
		
		/*private function getShops(){
		
			$sql = "SELECT `id`, `title` AS 'title' FROM `shops`";
			$res = GetAll($sql);
			
			return $res;
		}*/
		
		/*private function getCoupon($data, $id, $no_link = false){
		
			ob_start();
				// include PATH_SNIPPETS.'coupon.php';
				include PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'coupon-print.html';
				$coupon = ob_get_contents();
			ob_end_clean();
			// ob_end_flush();
			// _debug($coupon, 0);
			
			// $coupon = file_get_contents(PATH_HTML_TEMPLATES.$this->TemplatesBaseDir.'coupon-print.html');
			return $coupon;
		}*/
		
		/*protected function OnPrintCoupon(){
			
			$id = i($_GET['id']);
			$sql = "SELECT `id`, `city`, `fio` FROM `users` WHERE `id` = '$id'";
			$this->data = GetRow($sql);
			
			$this->SetHeader('header-print.html');
			$this->SetTemplate('coupon-print.html');
			$this->SetFooter('footer-print.html');			
		}*/
	}

?>