<?

		class Reviews extends ContentPage {
			
			/*
			 * Protected properties
			 */
			
			public $Articles		= array();
			public $Articles_sz	= 0;
			public    $PageNavigator	= null;
			public $Url = '';
			public $Page = '';
			
			public $OneArticles = array();
			
			public $ViewEvent = '';
			protected $Catalog		= array();
			/*
			 * Public methods
			 */
			
			public function OnCreate() {
				global $config;
				
				$this->PageInfo = $this->_load_content_info('pages','','url = \''.$this->UrlParts[0].'\'');
				$this->SetMetaDescription($this->PageInfo['meta_description']);
				$this->SetMetaKeywords($this->PageInfo['meta_keywords']);
				$this->AddTitle($this->PageInfo['title']);
				$this->Breadcrumbs[] = array('url'=>$this->PageInfo['url'], 'title' => $this->PageInfo['title']);
				
				$count = count($this->UrlParts);
				//echo $this->UrlParts[$count - 1];
				
				$page = ereg("page-[0-9]{1,4}",$this->UrlParts[$count - 1]);
				
				$url = explode("-",$this->UrlParts[$count - 1]);
				$page = $url[1];
				$url = end($url);
				
				$url = explode(".",$url);
				$this->Url = $url[0];
				$this->Page = $this->Url;
				$this->Catalog = $this->_get_categories_tree(0,0);
				$this->ViewEvent = 'AllReviews';
				if(count($this->UrlParts) == 1) {
					//exit('1111');
					$this->_get_articles();
					$this->ViewEvent = 'AllReviews';
				}else if(count($this->UrlParts) > 1) {
					
					if($this->UrlParts[1] != 'page') {
						//category
						/* $cat = array();
						foreach ($this->Catalog  as $el=>$item) {
							if($item['url'] == $this->UrlParts[1]){
								$cat = array('id' => $item['id'],'url' => $item['url']);
							}
						} */
						if(count($this->UrlParts) == 2) {
							
							// one article						
							$this->_get_one_articles($this->UrlParts[1],true);
							$this->ViewEvent = 'View';
						} else {
							$this->_get_articles($cat);
						}
						
					}else{
						//page 
						
						$this->_get_articles($cat);
					}
					
					
				}
				
				
				/*switch($this->UrlParts[$count - 1]){
					case 'articles':
					case NULL:
						
						// load all articles
						$this->_get_articles();
						$this->ViewEvent = 'AllArticles';
						break;
					case 'view':
						//load articles by ID
						$this->_get_one_articles($this->UrlParts[$count-1],true);
						$this->ViewEvent = 'View';
						break;
					case 'page':
						//load articles by ID
						$this->Page = $this->UrlParts[$count-1];
						$this->_get_articles();
						$this->ViewEvent = 'AllArticles';
						break;
					default:
						if($page){
							
							$this->ViewEvent = 'AllArticles';
						}else{
							//load articles by SEO url
							$this->_get_one_articles($this->UrlParts[$count-1],true);
							$this->ViewEvent = 'View';
							// $this->SetTitle($this->OneArticles['title']);
							$this->Breadcrumbs[] = array('url'=>$this->OneArticles['url'], 'title' => $this->OneArticles['title']);
						}
						break;
						
				
				}*/	
				$this->SetTemplate('reviews.php');
			
				return;
				
				
			}
			
			public function OnDefault() {

			}
			
			
			/*
			 * Private methods
			 */
			 
			private function _get_one_articles($url,$seo = false){
				
				$this->ViewEvent == 'ViewInformation'; 
				global $DB;
				$sql = "SELECT * FROM reviews WHERE publish = '1' AND url = '".$url."'";
				
				$this->OneArticles = $DB->GetRow($sql);
				if(!$this->OneArticles){
					
					$sql = "SELECT * FROM reviews WHERE publish = '1' AND  id = '".$url."'";
					
					$this->OneArticles = $DB->GetRow($sql);
				}
				
		
				$this->AddTitle($this->OneArticles['page_title']?$this->OneArticles['page_title']:$this->OneArticles['title']);
				//print_r($this->PageInfo);
				$this->SetMetaDescription($this->OneArticles['description']);
				$this->SetMetaKeywords($this->OneArticles['keywords']);
				
			}
			
			private function _get_articles($cat = array()){
				global $DB;
				$nl =  $this->_get_system_variables('news_per_page');
				$pages_cnt = $this->_get_system_variables('max_news_pages_cnt');
				$items_per_page = $nl;
				$max_pages_cnt = $pages_cnt;
				
				
				$page = $this->Page;
				if($page < 1){				
					$page = 1;
				}
				$where = '';
				if($cat){
					$where = ' AND a.category_id = '.$cat['id'];
				}
				$sql = 'SELECT COUNT(*) AS cnt 
						FROM reviews a
						WHERE a.publish = 1'.$where;
				$res = $DB->GetRow($sql);
					
				AttachLib('PageNavigator');
				$this->PageNavigator = new PageNavigator($res['cnt'], $items_per_page, $max_pages_cnt, $this->RootUrl.'reviews'.($cat? '/'.$cat['url'] : ''), $page, '/page/%s.html',true);		
				$sql = 'SELECT a.* , c.url as c_url
						FROM reviews  a
						LEFT JOIN categories c ON c.id = a.category_id
						WHERE 1 = 1 AND a.publish = 1 '. $where .'
						ORDER BY a.publish_ts DESC
						LIMIT '.(($page - 1) * $items_per_page).', '.$items_per_page;
				
				$this->Articles = $DB->GetAll($sql);
				
				$this->Articles_sz = count($this->Articles);
				
				
				
			}
			
			private function _load_top_level_categories() {
				
				global $DB;
				
				$sql = 'SELECT title, 
							   url 
						FROM categories 
						WHERE parent_id=\'0\'';
				$this->Categories = $DB->GetAll($sql);
				$this->Categories_sz = count($this->Categories);
				
				for($i = 0; $i < $this->Categories_sz; $i++) {
					
					$this->Categories[$i]['href'] = $this->RootUrl.$this->Categories[$i]['url'].'/';
				}
			}
		}

	?>