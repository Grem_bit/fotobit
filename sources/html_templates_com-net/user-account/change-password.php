<div class="contact-form">
<div class="SingleText">
		<?= $this->PageInfo['content']?>
<? if($_SESSION['message_success']) { ?>
	<div class="middle-text" >
		<img src="<?=$this->RootUrl?>public/images/action_success.png" /><span style="margin-left: 10px;" ><?= $_SESSION['message_success'] ?></span>
	</div>
	<? unset($_SESSION['message_success']); ?>	
<? }elseif($this->ErrorsUser) { ?>
	<div class="middle-text" >
		<img src="<?=$this->RootUrl?>public/images/action_error.png" /><span style="margin-left: 10px;" ><?= $_SESSION['message_error'] ?></span>
	</div>
	<? unset($_SESSION['message_error']); ?>
	<p>
		<ul style="list-style: none; margin: 0px; padding: 0px; color: red;" >
			<? foreach ( $this->ErrorsUser as $e ) { ?>
				<li><?= $e ?></li>
			<? } ?>
		</ul>
	</p>
<? } ?>
		<form id="regform" action="" method="post" enctype="multipart/form-data" class="registration_form">
			<table class="input-form">
			<tr>
				<td><label for="password">Пароль*:</label></td>
				<td>
				<input type="password" name="password:pass" value="" id="password" class="text-field input valid <?= $this->ErrorsUser['password'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all"/><br/>
				</td>
			</tr>
			<tr>
				<td><label for="confirm">Подтверждение пароля*:</label></td>
				<td>
				<input type="password" name="confirm" value="" id="confirm" class="text-field input valid <?= $this->ErrorsUser['confirm'] ? 'ui-state-error' : '' ?> ui-widget-content ui-corner-all"/><br/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit"  value="Сохранить" class="yellow pie reg button but"/></td>
			</tr>
			</table>
			
			<input type="hidden" name="Event" value="ChangePassword" />
		</form>
</div>
</div>