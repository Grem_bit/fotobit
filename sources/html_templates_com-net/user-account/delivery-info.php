<div class="breadcrumbs">
	<a href="/">Главная</a> » <a href="/private-data.html">Личный кабинет</a> » <span>Адрес доставки</span>
</div>
<br />

<?= $this->PageInfo['content'] ? $this->PageInfo['content'] : '' ?>

<? if($_SESSION['message_success']) { ?>
	<div class="middle-text" >
		<img src="<?=$this->RootUrl?>public/images/action_success.png" /><span style="margin-left: 10px;" ><?= $_SESSION['message_success'] ?></span>
	</div>
	<? unset($_SESSION['message_success']); ?>	
<? }elseif($this->ErrorsUser || $_SESSION['message_error']) { ?>
	<div class="middle-text" >
		<img src="<?=$this->RootUrl?>public/images/action_error.png" /><span style="margin-left: 10px;" ><?= $_SESSION['message_error'] ?></span>
	</div>
	<? unset($_SESSION['message_error']); ?>
	<p>
		<ul style="list-style: none; margin: 0px; padding: 0px; color: red;" >
			<? foreach ( $this->ErrorsUser as $e ) { ?>
				<li><?= $e ?></li>
			<? } ?>
		</ul>
	</p>
<? } ?>
		<form id="regform" action="" method="post" enctype="multipart/form-data" class="registration_form">
            <fieldset>
              <dl>
              	<dt><label for="name">ФИО*:	</label></dt>
                <dd>
                  <?= $this->UserInfo['name'] ?><br/>
                </dd>
              </dl>
              <dl>
                <dt><label for="country">Страна*: </label></dt>
                <dd>
                  <input type="text" name="country:str" value="<?= $_POST['country:str'] ? $_POST['country:str'] : $this->UserInfo['country'] ?>" id="country" class="input valid <?= $this->ErrorsUser['country'] ? 'error-ico' : '' ?>"/><br/>
                </dd>
              </dl>
              <dl>
                <dt><label for="region">Область: </label></dt>
                <dd>
                  <input type="text" name="region:str" value="<?= $_POST['region:str'] ? $_POST['region:str'] : $this->UserInfo['region'] ?>" id="region" class="input"/><br/>
                </dd>
              </dl>
              <dl>
                <dt><label for="city">Город*:</label></dt>
                <dd>
                  <input type="text" name="city:str" id="city" value="<?= $_POST['city:str'] ? $_POST['city:str'] : $this->UserInfo['city'] ?>" class="input valid <?= $this->ErrorsUser['city'] ? 'error-ico' : '' ?>"/><br/>
                </dd>
              </dl>
               <dl>
                <dt><label for="adres">Улица/дом/квартира*:</label></dt>
                <dd>
                  <input type="text" name="adres:str" id="adres" value="<?= $_POST['adres:str'] ? $_POST['adres:str'] : $this->UserInfo['adres'] ?>" class="input valid <?= $this->ErrorsUser['adres'] ? 'error-ico' : '' ?>"/><br/>
                </dd>
              </dl>
              <dl>
                <dt><label for="post_index">Почтовый индекс*:</label></dt>
                <dd>
                  <input type="text" name="post_index:str" id="post_index" value="<?= $_POST['post_index:str'] ? $_POST['post_index:str'] : $this->UserInfo['post_index'] ?>" class="input valid <?= $this->ErrorsUser['post_index'] ? 'error-ico' : '' ?>"/><br/>
                </dd>
              </dl>
              <?
              		if($this->UserInfo['contact_phone']){
              			$phone = $this->UserInfo['contact_phone']; 	
              		}else{
              			$phone = $this->UserInfo['mobile'];
              		}
					
              ?>
              <dl>
                <dt><label for="contact_phone">Контактный телефон*:</label></dt>
                <dd>
                  <input type="text" name="contact_phone:str" id="contact_phone" value="<?= $phone ? $phone : '' ?>" class="input valid <?= $this->ErrorsUser['contact_phone'] ? 'error-ico' : '' ?>"/><br/>
               </dd>
              </dl>
              <div class="clearing"></div>
              <input type="image" name="eaddress" id="eaddress" src="<?=$this->RootUrl?>public/images/save.png" title="сохранить" class="reg"/>
				<input type="hidden" name="Event" value="UpdateDeliveryInfo" />

            </fieldset>
          </form>