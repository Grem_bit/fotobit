<?
	// _debug($this->Category, 0, '$this->Category');
	// _debug($this->Products, 1, '$this->Products');
?>

<div class="product main">
	<div class="sect-name"><img src="<?= $this->Category['filename']?>" alt=""/>
	
	</div>
	
	<?include PATH_SNIPPETS.'nav-block-right.php'?>
	<div class="tents product-table"  style="padding-right: 10px;">
		<div class="product-row" style="font-size: 15px; line-height: 20px; font-family:'PFDinDisplayPro',Arial,Helvetica,sans-serif;"><?=$this->PageInfo['content']?></div>
	</div>
	<?if($this->Products['items']){?>
		<div class="tents product-table" <?=$this->PageInfo['content']?'style="margin-top:20px;padding-right:10px;"':'style="padding-right: 10px;"'?>>
			
			<?
				if($this->Category['count_in_row']){
					$cntItemInRow = $this->Category['count_in_row'];
				}else{
					$cntItemInRow = 1;
				}
				$cntProducts = count($this->Products['items']);
				$cntRows = ceil($cntProducts / $cntItemInRow);
				
				// $prod_img_w = 300;
				// $prod_img_h = 300;
				/*if($this->Category['prod_img_w']) $prod_img_w = $this->Category['prod_img_w'];
				if($this->Category['prod_img_h']) $prod_img_h = $this->Category['prod_img_h'];*/
			?>
			<?for($i = 0; $i < $cntRows; $i++){?>
				<div class="product-row">
					<?for($j = 0; $j < $cntItemInRow; $j++){?>
						<?$productNumber = $i * $cntItemInRow + $j?>
						<?if($this->Products['items'][$productNumber]){?>
							<div class="product-cell all pie">
								<div class="">
									<div class="img"><a href="<?= $this->LN_url?><?= $this->getProductUrl($this->Products['items'][$productNumber]['obj_id'])?>" title="">
									<div class="first"><img<?/*  width="<?= $prod_img_w?>" height="<?= $prod_img_h?>" */?> src="<?/* /public/files/images/products/ */?><?= $this->Products['items'][$productNumber]['img_preview']?>" alt=""/></div><?/*<div class="second"><img width="<?= $prod_img_w?>" height="<?= $prod_img_h?>" src="<?= $this->Products['items'][$productNumber]['img_glow']?>" alt=""/></div>*/?></a></div>
								</div>
								<div class="product-cell description-text">
									<h3 class="product-header"><a href="<?= $this->LN_url?><?= $this->getProductUrl($this->Products['items'][$productNumber]['obj_id'])?>" title=""><?= $this->Products['items'][$productNumber]['title']?></a></h3>
									<?= $this->Products['items'][$productNumber]['catalog_description']?>
								
								</div>
							</div>
						<?}?>
					<?}?>
				</div>
				<div class="product-row">
					<?for($j = 0; $j < $cntItemInRow; $j++){?>
						<?$productNumber = $i * $cntItemInRow + $j?>
						<?if($this->Products['items'][$productNumber]){?>
								
						<?}?>
					<?}?>
				</div>
			<?}?>
		</div>
	<?}else{?>
		<div class="tents product-table">
		<p>{[NO_PORODUCTS]}</p>
		</div>
	<?}?>
	<div class="clear"></div>
</div>