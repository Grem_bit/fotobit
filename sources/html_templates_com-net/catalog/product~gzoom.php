<?
	// _debug($this->Product, 1, '$this->Product');
	// _debug($this->prev_product, 1, '$this->prev_product');
	// _debug($this->next_product, 1, '$this->next_product');
	// $this->Product['slider'] = array_merge($this->Product['slider'], $this->Product['slider']);
?>

<?//if(count($this->Product['slider']) > 3){?>
	<script type="text/javascript">
		$(document).ready(function(){
			$.fn.FlyUpSlider({
				id: '#gallery',
				mainPhoto: '#main-photo',
				left: '.l-up',
				right: '.l-down',
				vertical: true,
				round: false
			});
			
			$(".zoom_no_lbox").gzoom({	sW			: 350,
										sH			: 350,
										lW			: 1000,
										lH			: 1000,
										frameColor	: "#000000",
										lightbox	: true
									});
		});
	</script>
<?//}?>

<div class="cat-bl calendar main">
	<?if($this->prev_product){?>
		<a class="prev-arr" href="<?= $this->LN_url?>catalog/<?= $this->prev_product['category_url'].'/'.$this->prev_product['product_url']?>.html" title=""></a>
	<?}?>
	<?if($this->next_product){?>
		<a class="next-arr" href="<?= $this->LN_url?>catalog/<?= $this->next_product['category_url'].'/'.$this->next_product['product_url']?>.html" title=""></a>
	<?}?>
	<div class="main-left-box">
		<?if($this->Product['slider']){?>
			<div class="l-gallery">
				<a class="l-up" href="#" title=""></a>
				<div class="gallery-box">
					<div id="gallery" class="gallery-thumbs">
						<ul>
							<?foreach($this->Product['slider'] as $slide){?>
								<li><a href="#" title=""><img height="94" src="<?= $this->RootUrl?>public/files/images/products/<?= $slide['filename']?>" alt=""/></a></li>
							<?}?>
						</ul>
					</div>
				</div>
				<a class="l-down" href="#" title=""></a>
			</div>
		<?}?>
		<?if($this->Product['filename']){?>
			<div class="block-3d">
				<div class="img-3d zoom_no_lbox">
					<img id="main-photo" <?/*width="300 212 */?>" <?/*height="300 362 */?>" src="<?= $this->RootUrl?><?= $this->Product['img_preview']?>" alt="" />
				</div>
				<?/* <div class="nav-3d">
					<div class="slider-3d"></div>
				</div> */?>
			</div>
		<?}?>
		<div class="clear"></div>
		<div class="prod-desc">
			<h1><?= $this->Product['title']?></h1>
			<?= $this->Product['fron_text']?>
			<?if($this->Product['features']){?>
				<ul>
					<li class="features"><b>Особенности модели:</b></li>
				</ul>
				<div class="feat">
					<ul>
						<?foreach($this->Product['features'] as $feature){?>
							<li>
								<?if($feature['title']){?>
									<div class="hz-feat"><?= $feature['title']?></div>
								<?}?>
								<img width="82" height="120" src="<?= $this->RootUrl?>public/files/images/products/<?= $feature['filename']?>" alt=""/>
							</li>
						<?}?>
					</ul>
					<div class="clear"></div>
				</div>
			<?}?>
		</div>
	</div>
	<div class="main-row">
		<div class="name-bag" style="background:url('<?= $this->Product['filename3']?>') no-repeat scroll center center transparent"></div>
		<div class="crazy-nav">
			<a class="crazy-top nav-lnk" href="#" title="" rel="1|right">особенности</a>
			<?if($this->Product['warranty']){?>
				<a class="crazy-right nav-lnk" href="#" title="" rel="2|left|right"><span></span></a>
			<?}?>
			<a class="crazy-left nav-lnk active" href="#" title="" rel="3|left|right"><span></span></a>
			<?if($this->Product['video']){?>
				<a class="crazy-bottom nav-lnk" href="#" title="" rel="4|left">ВИДЕО</a>
			<?}?>
			<?
				$features = $this->Product['content'];
				preg_match_all("!<li>(.*?)</li>!si", $features, $features);
				// _debug($features[1], 1);
			?>
			<div class="crazy-box tab1">
				<table>
					<?/* <tr>
						<td rowspan="<?= count($features[1])?>">ОСОБЕННОСТИ</td>
						<td><?= $features[1][0]?></td>
					</tr> */?>
					<?for($i=0; $i<count($features[1]); $i++){?>
						<tr>
							<td></td>
							<td><?= $features[1][$i]?></td>
						</tr>
					<?}?>
				</table>
			</div>
			<?if($this->Product['warranty']){?>
				<div class="crazy-box tab2">
					<table>
						<tr>
							<td colspan="2"><?= $this->Product['warranty']?></td>
						</tr>
					</table>
				</div>
			<?}?>
			<?
				$charact_tmp = $this->Product['fron_text'];
				preg_match_all("!<li>(.*?)</li>!si", $charact_tmp, $charact_tmp);
				foreach($charact_tmp[1] as $item){
					preg_match("!<b>(.*?)</b>!si", $item, $res[]);
				}
				$i = 0;
				foreach($res as $item){
					$charact[$i][0] = str_replace('<b>', '', $item[0]);
					$charact[$i][0] = str_replace('</b>', '', $charact[$i][0]);
					$charact[$i][0] = mb_convert_case($charact[$i][0], MB_CASE_UPPER, "UTF-8");
					$charact[$i][1] = str_replace($item[0], '', $charact_tmp[1][$i]);
					$i++;
				}
			?>
			<div class="crazy-box tab3 active">
				<table>
					<?foreach($charact as $item){?>
						<tr>
							<td><?= $item[0]?></td>
							<td><?= $item[1]?></td>
						</tr>
					<?}?>
				</table>
			</div>
			<?if($this->Product['video']){?>
				<div class="crazy-box tab4">
					<table>
						<tr>
							<td colspan="2" class="video-td">
								<iframe width="284" height="160" src="<?= $this->Product['video']?>" frameborder="0" allowfullscreen></iframe>
							</td>
						</tr>
					</table>
				</div>
			<?}?>
		</div>
		<div class="cal-right2 cal-right"><a href="#" title=""><span class="action001"></span><img src="<?= $this->RootUrl?>public/images/ban7.jpg" alt=""/></a></div>
		<div class="main-line2 main-line">
			<?include PATH_SNIPPETS.'products-in-main.php'?>
		</div>
	</div>
	<div class="clear"></div>
</div>