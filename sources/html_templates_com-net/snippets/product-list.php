<?/*
<div class="catalogsection" style="margin-top:100px;">
	<?if (is_for($this->Products['items'])){?>
		<div class="pagequantity">Показывать на странице по 
			<div><a href="?find=<?=$_GET['find']?>&items=8" <?= (!$_GET['items'] || $_GET['items'] == 8) ? 'class="active"' : ''?> title="" >8</a>
			<a class="pagequantity16 <?= ($_GET['items'] == 16) ? 'active' : ''?>" href="?find=<?=$_GET['find']?>&items=16">16</a>
			<a class="pagequantityall <?= ($_GET['items'] == 'all') ? 'active' : ''?>" href="?find=<?=$_GET['find']?>&items=all">ВСЕ</a>
			</div>
		</div>
		<?foreach($this->Products['items'] as $items){?>
		<?$url = _fullurl($items['category_id'],'categories','catalog');?>
		<a class="itemthumb" href="<?=$this->LN_url.$url.'/'.$items['url']?>.html">
			<?if ($items['filename']){?>
				<img src="/public/files/products/middle-<?=$items['filename']?>" alt="<?=$items['title']?>"/>
			<?}?>
			<span><span><?=$items['title']?></span><?=_trim_word(strip_tags($items['fron_text']), 30, 0, ' ')?></span>
		</a>
		<?}?>
		<?=$this->Products['pn']->Run()?>
			
	<?}else{?>
		<p>{[NO_PORODUCTS]}</p>
	<?}?>
</div>
*/?>


<?
	// _debug($this->Category, 0, '$this->Category');
	// _debug($this->Products, 1, '$this->Products');
?>

<div class="product main">
	<div class="sect-name"><img src="<?= $this->Category['filename']?>" alt=""/></div>
	
	<?include PATH_SNIPPETS.'nav-block-right.php'?>
	
	<?if($this->Products['items']){?>
		<div class="tents product-table">
			<?
				if($this->Category['count_in_row']){
					$cntItemInRow = $this->Category['count_in_row'];
				}elseif($this->_get_system_variable('search_items_in_row')){
					$cntItemInRow = $this->_get_system_variable('search_items_in_row');
				}else{
					$cntItemInRow = 1;
				}
				$cntProducts = count($this->Products['items']);
				$cntRows = ceil($cntProducts / $cntItemInRow);
				// $width = 
			?>
			<?for($i = 0; $i < $cntRows; $i++){?>
				<div class="product-row">
					<?for($j = 0; $j < $cntItemInRow; $j++){?>
						<?$productNumber = $i * $cntItemInRow + $j?>
						<?if($this->Products['items'][$productNumber]){?>
							<div class="product-cell">
								<?/* <div class="img"><a href="<?= $this->LN_url?><?= $this->getProductUrl($this->Products['items'][$productNumber]['obj_id'])?>" title=""><img width="278" height="165" src="/public/files/images/products/<?= $this->Products['items'][$productNumber]['filename']?>" alt=""/></a></div> */?>
								<div class="img"><a href="<?= $this->LN_url?><?= $this->getProductUrl($this->Products['items'][$productNumber]['obj_id'])?>" title=""><img src="<?= $this->Products['items'][$productNumber]['img_preview']?>" alt=""/></a></div>
							</div>
						<?}?>
					<?}?>
				</div>
				<div class="product-row">
					<?for($j = 0; $j < $cntItemInRow; $j++){?>
						<?$productNumber = $i * $cntItemInRow + $j?>
						<?if($this->Products['items'][$productNumber]){?>
							<div class="product-cell description-text">
								<h3 class="product-header"><a href="<?= $this->LN_url?><?= $this->getProductUrl($this->Products['items'][$productNumber]['obj_id'])?>" title=""><?= $this->Products['items'][$productNumber]['title']?></a></h3>
								<?= $this->Products['items'][$productNumber]['fron_text']?>
								<?if($this->Products['items'][$productNumber]['content']){?>
									<ul><li class="features"><b>Особенности модели:</b>
										<?= $this->Products['items'][$productNumber]['content']?>
									</li></ul>
								<?}?>
							</div>
						<?}?>
					<?}?>
				</div>
			<?}?>
		</div>
	<?}else{?>
		<div class="tents product-table">
		<p>{[NO_PORODUCTS]}</p>
		</div>
	<?}?>
	<div class="clear"></div>
</div>