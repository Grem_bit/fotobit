<div class="calendar-box">
	<?$calendar = $this->getCalendar($_GET["date"], 1)?>
	<div class="month-name">
		<?= $calendar['head']?>
	</div>
	<div class="calendar-n">
		<?= $calendar['body']?>
	</div>
</div>
<div>
	<div class="small-cal calendar-box">
		<?
			if(isset($_GET["date"])){
				$month = (int)substr($_GET["date"], 4, 2);
				$year  = (int)substr($_GET["date"], 0, 4);
			}
			else{ // иначе выводить текущие месяц и год
				$month = date("m", mktime(0,0,0,date('m'),1,date('Y')));
				$year  = date("Y", mktime(0,0,0,date('m'),1,date('Y')));
			}
			$date_sec = mktime(0, 0, 0, $month, 1, $year);
			$date = date('Ym', strtotime("last Month", $date_sec));
			
			$calendar = $this->getCalendar($date);
		?>
		<div class="month-name">
			<?= $calendar['head']?>
		</div>
		<div class="calendar-n">
			<?= $calendar['body']?>
		</div>
	</div>
	<div class="small-cal calendar-box">
		<?
			if(isset($_GET["date"])){
				$month = (int)substr($_GET["date"], 4, 2);
				$year  = (int)substr($_GET["date"], 0, 4);
			}
			else{ // иначе выводить текущие месяц и год
				$month = date("m", mktime(0,0,0,date('m'),1,date('Y')));
				$year  = date("Y", mktime(0,0,0,date('m'),1,date('Y')));
			}
			$date_sec = mktime(0, 0, 0, $month, 1, $year);
			$date = date('Ym', strtotime("next Month", $date_sec));
			
			$calendar = $this->getCalendar($date);
		?>
		<div class="month-name">
			<?= $calendar['head']?>
		</div>
		<div class="calendar-n">
			<?= $calendar['body']?>
		</div>
	</div>
</div>