<script type="text/javascript" id="sourcecode">
	$(function(){
		$('.scroll-pane').jScrollPane({
			autoReinitialise: true,
			verticalDragMinHeight: 20,
			verticalDragMaxHeight: 20
		});
	});
</script>

<div class="cal-news">
	<a class="cw" href="#" title=""></a>
	<?if($this->OneNews['filename'] || $this->OneNews['extratext1'] || $this->OneNews['extratext2']){?>
		<div class="cal-n-h font-cond" style="background:url('/core/lib/phpThumb/phpThumb.php?w=409&h=126&f=png&src=<?= $this->OneNews['filename']?>') no-repeat scroll center transparent;">
			<?if($this->OneNews['extratext1']){?>
				<div class="caln-date"><?= $this->OneNews['extratext1']?></div> 
			<?}?>
			<?= $this->OneNews['extratext2']?>                       
		</div>
	<?}else{?>
		<div style="padding-top:20px;"></div>
	<?}?>
	<div id="container">
		<div class="scroll-cal scroll-pane font-cond">
			<div class="sc-box">
				<h1 class="sc-header"><?= mb_convert_case($this->OneNews['title'], MB_CASE_UPPER, "UTF-8")?></h1>
				<?= $this->OneNews['fulltext']?>
			</div>
		</div>
	</div>
</div>