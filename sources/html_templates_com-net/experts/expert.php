<?
	// _debug($this->Expert, 0, '$this->Expert');
	// _debug($this->Experts, 1, '$this->Experts');
	// _debug($this->Products, 1, '$this->Products');
	// _debug($this->Posts, 1, '$this->Posts');
	// $this->getPostUrl(1);
?>

<?include PATH_SNIPPETS.'subscribe-link.php'?>

<div style="display:none">
	<?include PATH_SNIPPETS.'subscribe-form.php'?>
</div>

<div class="exp main">
	<div class="main-left-box">
		<div class="sect-name"><img src="<?= $this->Expert['filename']?>" alt=""/></div>
		<div class="dsc-field">
			<h2 class="font-cond"><?= $this->Expert['profession']?>.</h2>
			<?= $this->Expert['content']?>
		</div>
		<?if($this->Posts){?>
			<?include PATH_SNIPPETS.'expert-all-posts.php'?>
		<?}?>
	</div>
	<?if($this->Experts){?>
		<div class="main-row">
			<div class="blog">
				<div class="oexp">{[EXPERTS_OTHER]}</div>
				<?foreach($this->Experts as $expert){?>
					<?include PATH_SNIPPETS.'experts.php'?>
				<?}?>
			</div>
		</div>
	<?}?>
	<div class="clear"></div>
</div>