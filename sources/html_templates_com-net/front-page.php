	<div class="mainbar clearfix">			
		<div class="page-wrap clearfix">
			
				<?if($this->PageInfo['content']){?>
					<div class="fr_content"><?= $this->PageInfo['content']?></div>
				<?}?>
			
			<div class="sidebar left">
				<div class="info-list">
					<div class="item clearfix">
						<div class="img">
							<?if($_SESSION['user_account']){?>
								<img src="public/images/pictures/user.png" alt="#" />
							<? }else{ ?>
								<a href="#login-form" rel="fb"><img src="public/images/pictures/user.png" alt="#" /></a>
							<?}?>
						</div>
						<div class="txt">
							 <?if($_SESSION['user_account']){?>
								{[SIGN_UP]}
							 <? }else{ ?>
							  <a href="#login-form" rel="fb" class="front_text_link" >{[SIGN_UP]}</a>
							 <? } ?>
							
						</div>
					</div>							
					<div class="item clearfix">
						<div class="img">
							<?if($_SESSION['user_account']){?>
								<a href="/user-account/upload.html"><img src="public/images/pictures/up.png" alt="#" /></a>
							<? }else{ ?>
								<a href="#login-form" rel="fb"><img src="public/images/pictures/up.png" alt="#" /></a>
							<?}?>
						</div>
						<div class="txt">
								<?if($_SESSION['user_account']){?>
								<a href="/user-account/upload.html" class="front_text_link"> {[UPLOAD_PHOTOS]}</a>
								 <? }else{ ?>
								<a href="#login-form" rel="fb" class="front_text_link" > {[UPLOAD_PHOTOS]}</a>
								 <? } ?>
							
						</div>
					</div>
				</div>
			</div>
			<div class="sidebar right">
				<div class="info-list">
					<div class="item clearfix">
						<div class="img"><a href="<?= $this->RootUrl?>sroci_pechati.html"><img src="public/images/pictures/clock.png" alt="#" /></a></div>
						<div class="txt">
							<a class="front_text_link" href="<?= $this->RootUrl?>sroci_pechati.html">
								{[WAIT]}
							</a>
						</div>
					</div>							
					<div class="item clearfix">
						<div class="img"><a href="/delivery.html"><img src="public/images/pictures/play.png" alt="#" /></a></div>
						<div class="txt">
							<a href="/delivery.html" class="front_text_link">
							{[ACCEPT]}
							</a>
						</div>
					</div>		
				</div>	
			</div>
			<div class="photo-bar">
				<img src="public/images/pictures/photo-set.png" alt="#" />
				<?if($_SESSION['user_account']){?>
					<a class="button orange pie" <? //  href="/user-account/choice.html"  ?>  href="/user-account/upload.html" title=""><span>{[UPLOAD_PHOTO]}</span></a>
				<?} else{ ?>				
					<a class="button orange pie"  href="#login-form" title="" rel="fb"><span>{[UPLOAD_PHOTO]}</span></a>
				<?}?>
				<? /* <a class="button orange pie" href="#"><span>загрузить фото</span></a> */ ?>
			</div>
		</div>
	</div>	
	<div class="coments">
		<div class="page-wrap clearfix">
			<div class="mess-slider">	
				<?
					$cnt=$this->_get_system_variable('reviews_front');
					$sql = "SELECT * FROM `reviews` where publish='1' ORDER BY create_ts LIMIT $cnt ";
						$rev=GetAll($sql); 
				?>
				<? if ( isset($rev)  && $rev){ ?>
				<ul>
					<? foreach ($rev as $val) { ?>
					<li>
						<div class="mess-text pie">
							<h2><?=$val['title'] ?> <span class="date"><?= date("d.m.y",$val['create_ts'])?></span></h2>
							
								<p><a href="<?= $this->RootUrl?>reviews.html"><?= $val['fulltext'] ?></a></p>
							
						</div>	
					</li>	
					<? } ?>
				</ul>
				<? } ?>
			</div>
		</div>
	</div>
	<div class="service">
		<div class="page-wrap">
			<h2>{[WHOT_WE_DO]}</h2>
			<div class="price-table clearfix">
				<div class="price-td pie">
					<div class="violet-title blue "><img src="public/images/pictures/print.png" alt="#" /><h3>{[FRONT_COLUMN1_TITLE]}</h3></div>
					<div class="txt">{[FRONT_COLUMN1_CONTENT]}</div>
					<a class="details" href="{[FRONT_COLUMN1_LINK]}">{[DETAILS_PRICE1]}</a>
				</div>						
				<div class="price-td pie">
					<div class="violet-title red"><img src="public/images/pictures/book.png" alt="#" /><h3>{[FRONT_COLUMN2_TITLE]}</h3></div>
					<div class="txt">{[FRONT_COLUMN2_CONTENT]}</div>
					<a class="details" href="{[FRONT_COLUMN2_LINK]}">{[DETAILS_PRICE2]}</a>
				</div>						
				<div class="price-td pie">
					<div class="violet-title green"><img src="public/images/pictures/scan.png" alt="#" /><h3>{[FRONT_COLUMN3_TITLE]}</h3></div>
					<div class="txt">{[FRONT_COLUMN3_CONTENT]}</div>						
					<a class="details" href="{[FRONT_COLUMN3_LINK]}">{[DETAILS_PRICE3]}</a>
				</div>
			</div>
		</div>	
	</div>
	
<? if (0){?>
<div class="mainbar">
			<img class="camera right" src="<?= $this->RootUrl?>public/img/camera.png" alt=""/>
			<h1>{[FRONT_PAGE_TITLE]}</h1>
			
			<?include PATH_SNIPPETS.'button-block.php'?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div style="color:#FFF"><?=$this->PageInfo['content']?></div>
		<div class="price-table">
			<div class="price-td left">
				<a class="table_link"  href="<?= $this->_get_system_variable('table_link1')?>" title="">
				<h2 class="violet-title">{[FRONT_COLUMN1_TITLE]}</h2>
				{[FRONT_COLUMN1_CONTENT]}
				<div class="bottom-button">
				<?if($_SESSION['user_account']){?>
				<!-- <a class="button" href="/user-account.html" title="">{[ORDER]}</a> -->
				<?} else{ ?>				
				<a class="button" rel="fb" href="#login-form" title="">{[REGISTRATION]}</a>
				<?}?>
				</div>
				
				</a>
			</div>
			<div class="price-td left">
				<h2 class="orange-title">{[FRONT_COLUMN2_TITLE]}</h2>
				{[FRONT_COLUMN2_CONTENT]}
				
			</div>
			<div class="active price-td left">
				<a class="table_link" href="<?= $this->_get_system_variable('table_link3')?>" title="">
				<img class="best" src="<?= $this->RootUrl?>public/img/best.png" alt=""/>
				<h2 class="blue-title">{[FRONT_COLUMN3_TITLE]}</h2>
				{[FRONT_COLUMN3_CONTENT]}
				<div class="bottom-button">
					<?if($_SESSION['user_account']){?>
					<!--<a class="button big" href="/user-account.html" title="">{[ORDER]}</a> -->
					<?} else{ ?>				
					<a class="button big" rel="fb" href="#login-form" title="">{[REGISTRATION]}</a>
					<?}?>
				</div>
				</a>
			</div>
			<div class="price-td left">
				<a class="table_link" href="<?= $this->_get_system_variable('table_link4')?>" title="">
				<h2 class="yellow-title">{[FRONT_COLUMN4_TITLE]}</h2>
				{[FRONT_COLUMN4_CONTENT]}
				<div class="bottom-button">
					<?if($_SESSION['user_account']){?>
					<!--<a class="button" href="/user-account.html" title=""><{[ORDER]}</a>-->
					<?} else{ ?>				
					<a class="button" rel="fb" href="#login-form" title="">{[REGISTRATION]}</a>
					<?}?>
				</div>
				
				</a>
			</div>
			<div class="clear"></div>
		</div>
	
		
			
		<div class="clear"></div>

<? } ?> 
