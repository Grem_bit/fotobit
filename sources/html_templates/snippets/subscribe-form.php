<div id="SubscribeForm" class="main-left-box" style="width:auto;">
	<form class="validation-form" method="post" action="<?= $this->LN_url?>subscribe.html">
		<div class="lineForm">
			<input name="name:str" type="text" class="name-field valid" value="{[FORM_NAME]}" title="{[FORM_NAME]}" onclick="if(this.value == '{[FORM_NAME]}') this.value=''" onblur="if(this.value =='') this.value='{[FORM_NAME]}'" />
			
			<input name="email:str" type="text" class="mail-field valid email" value="{[FORM_EMAIL]}" title="{[FORM_EMAIL]}" onclick="if(this.value == '{[FORM_EMAIL]}') this.value=''" onblur="if(this.value =='') this.value='{[FORM_EMAIL]}'" />
			
			<div class="clear"></div>
			
			<input class="button" type="submit" value="{[SEND]}" />
			<div class="clear"></div>
			<div class="thanks"></div>
		</div>
		<input  name="Event" type="hidden" value="Subscribe" />
	</form>
</div>