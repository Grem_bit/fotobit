<a class="calendarbutton cufon"  href="#"><span>КАЛЕНДАРЬ ТУРИСТА</span></a>
					<div class="calendarshadow calendar" style="display:none;">
						<div class="date">
							<div class="datetoday">
								<div class="today"><span class="cufonshadowwhite">{[TODAY]}</span></div>
								<div class="day"><span class="cufonshadoworange"><?= date("d");?></span></div>
								<?if ($this->SiteLang == 'ru'){
									$month = $this->Month[date("n")-1];
									$monthnow = $this->Monthnow[date("n")-1];
								}else{
									$month = $this->Monthua[date("n")-1];
									$monthnow = $this->Monthnowua[date("n")-1];
								}?>
								
								<div class="month"><span class="cufonshadoworange"><?=$month?></span></div>
								<?if ($this->SiteLang == 'ru'){
									$week = $this->Week[date("w")];
								}else{
									$week = $this->Weekua[date("w")];
								}?>
								<div class="weekday"><span class="cufonshadowwhite"><?=$week?></span></div>
							</div>
<!--							<div class="datehover">
								<div class="day"><span class="cufonshadoworange">29</span></div>
								<div class="month"><span class="cufonshadoworange">ФЕВРАЛЯ</span></div>
								<div class="weekday"><span class="cufonshadowwhite">ЧЕТВЕРГ</span></div>
							</div>
-->						</div>
						<div class="calendartable">
							<div class="monthheader">
								<div><a class="monthleftarrow" href="#"></a><a title="" href="/news.html?now=true" class="cufon month"><?=$monthnow?> <?=date("Y")?></a><a class="monthrightarrow" href="#"></a></div>
							</div>
							<div class="days">
								<div class="daysrow">
									<?if ($this->SiteLang == 'ru'){?>
									<div class="workday">пн</div><div class="workday">вт</div><div class="workday">ср</div><div class="workday">чт</div><div class="workday">пт</div><div class="holyday">сб</div><div class="holyday">вс</div>
									<?}else{?>
									<div class="workday">пн</div><div class="workday">вт</div><div class="workday">ср</div><div class="workday">чт</div><div class="workday">пт</div><div class="holyday">сб</div><div class="holyday">нд</div>
									<?}?>
								</div>
								<div class="daysrow">
									<div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="workday"><span>1</span></div><div class="workday"><span>2</span></div><div class="workday"><span>3</span></div><div class="holyday"><span>4</span></div><div class="holyday"><span>5</span></div>
								</div>
								<div class="daysrow">
									<div class="workday"><span>6</span></div><div class="workday"><span>7</span></div><div class="workday"><span>8</span></div><div class="workday"><span>9</span></div><div class="workday"><span>10</span></div><div class="holyday"><span>11</span></div><div class="holyday"><span>12</span></div>
								</div>
								<div class="daysrow">
									<div class="workday"><span>13</span></div><div class="workday"><span>14</span></div><div class="workday"><span>15</span></div><div class="workday"><span>16</span></div><div class="workday"><span>17</span></div><div class="holyday"><span>18</span></div><div class="holyday"><span>19</span></div>
								</div>
								<div class="daysrow">
									<div class="workday"><span>20</span></div><div class="workday"><span>21</span></div><div class="workday"><span>22</span></div><div class="workday"><span>23</span></div><div class="workday"><span>24</span></div><div class="holyday"><span>25</span></div><div class="holyday"><span>26</span></div>
								</div>
								<div class="daysrow">
									<div class="workday"><span>27</span></div><div class="workday"><span>28</span></div><div class="workday"><span>29</span></div><div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="holyday"><span> </span></div><div class="holyday"><span> </span></div>
								</div>
								<div class="daysrow">
									<div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="workday"><span> </span></div><div class="holyday"><span> </span></div><div class="holyday"><span> </span></div>
								</div>
							</div>
							
						</div>
						<?/*
						<script type="text/javascript">
						$('.calendartable').DatePicker({
							flat: true,
							date: ['2008-07-28','2008-07-31'],
							current: '2008-07-31',
							calendars: 1,
							mode: 'range',
							starts: 1
						});
						</script>
						*/?>
						<?$mainnews = $this->_load_content_translation('news', 'published = 1 AND main = 1 AND news = 0', $order_by, '', '',  1);?>
						<?if($mainnews){?>
						<div class="recommend">
							<div>
								<span class="cufon"><?=$mainnews[0]['title']?></span>
							</div>
							<?if ($mainnews[0]['filename']){?>
							<div>
								<a style="background: url('<?=$mainnews[0]['filename']?>') no-repeat right bottom;" class="reclink" href="<?=$this->LN_url?>news/view/<?=$mainnews[0]['url']?>.html"></a>
							</div>
							<?}?>
						</div>
						<?}?>
						<div class="monthevents">
							<span class="month cufon"><?=$monthnow?> <?=date("Y")?></span>
							<div id="calendar_news">
							<?if(is_for($this->news_list)){?>
								<?foreach ($this->news_list as $news_item){?>
									<div class="events">
			                              <a class="eventlink" href="<?=($this->SiteLang == 'ua'?'/ua/':'/').'news/view/'.($news_item['url']).'.html'?>">
											<span class="eventdate cufon">
												<span class="eventday"><?=date('d',$news_item['publish_ts'])?></span>
												<span class="eventmonth"><?=$this->SiteLang == 'ua'? $this->Monthua[date('n',$news_item['publish_ts'])] : $this->Month[date('n',$news_item['publish_ts'])]?></span>
											</span>
											<span class="eventdatehover cufon">
												<span class="eventday"><?=date('d',$news_item['publish_ts'])?></span>
												<span class="eventmonth"><?=$this->SiteLang == 'ua'? $this->Monthua[date('n',$news_item['publish_ts'])] : $this->Month[date('n',$news_item['publish_ts'])]?></span>
											</span>
											<span class="event">
												<span class="eventtextheader cufon"><?=$news_item['title']?></span>
												<span class="eventtext"><?=strip_tags( $news_item['introtext'])?></span>
											</span>
										</a>
									</div>
								<?}?>
							<?}?>
							</div>
							
							<?= $this->PageNavigatorCalendar->Run(); ?>
						</div>
						<?if($mainnews){?>
						<div class="calendarnote">
							<span class="cufon"><?=$mainnews[0]['introtext']?>
							</span>
						</div>
						<?}?>
					</div>