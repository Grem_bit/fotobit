<h1>{[BASKET]}</h1>	

	<div class="cart-table">						
	
	<?if(is_array($_SESSION['Trash']) && $_SESSION['Trash']['count']){?>
	<form id="ProductsForm" action=""  method="post">
		<table cellspacing="1" cellpadding="0">
				<tbody><tr>
					<th class="first">{[TITLE]}</th>       
					<th>{[PRICE_UNIT]}</th>       
					<th>{[QUANTITY]}</th>       
					<th>{[SUM]}, <?=$this->Currencies[$this->Currency][sign]?></th>       
					<th class="last">{[DELETE]}</th>
				</tr>
					
			<?	foreach((array)$_SESSION['Trash']['cart'] as $prod){ ?>
			<tr>
				
				<td><?=$this->SiteLang == 'ru'?$prod['title']:$prod['en_title']?>  </td>
				<td><?= $prod['price'] * $this->Currencies[$this->Currency][course]?> <?=$this->Currencies[$this->Currency][sign]?></td>
				<td><input type="text" class="recal" name="count[<?=$prod['id']?>]" value="<?= $prod['count']?>"/></td>
				<td><?= $prod['price'] * $this->Currencies[$this->Currency][course] * $prod['count']?> <?=$this->Currencies[$this->Currency][sign]?></td>
				<td><a href="/order?Event=DelFromCart&id=<?=$prod['id']?>">X</a></td>
				
				
			</tr>
			<?}?>
			</tbody>
		</table>
		<div class="navigation">
				<a href="/order?Event=DelCart" class="pie">{[CLEAR_BASKET]}</a>
				<input type="hidden" name="Event" value="Recalculate"></input>
				<a href="#" onClick="$('#ProductsForm').submit();" class="pie">{[RECALCULATE]}</a>
		</div>

		
	</form>
	
	<form method="post" id="addOrder" action="/order.html">
		<div class="form_content">
		<div class="delivery">
			<p><label>{[FIO]}*:<input type="text" value="<?= $_POST['name'] ? $_POST['name'] : ''?>" name="name" class="text-field pie <?= $this->Error['name'] ? 'ui-state-error':''?>" /></label></p>
			<p><label>{[CONTACT_PHONE]}*:	<input type="text" value="<?= $_POST['phone'] ? $_POST['phone'] : ''?>" name="phone" class="text-field pie <?= $this->Error['phone'] ? 'ui-state-error':''?>" /></label></p>
			<p><label>{[EMAIL]}:	<input type="text" value="<?= $_POST['email'] ? $_POST['email'] : ''?>" name="email" class="text-field pie email <?= $this->Error['email'] ? 'ui-state-error':''?>" /></label></p>
			<div class="radio">
			<p><label><input type="radio" checked="checked" name="dostavka" value="0" class=" pie" /> {[PICKUP]} </label>          
			<label class="radio"><input name="dostavka" type="radio" value="1" class=" pie" /> {[DELIVERY]}</label></p>
			</div>
			<p><label>{[ADDRESS_DELIVERY]}:	<input type="text" name="address" value="<?= $_POST['address'] ? $_POST['address'] : ''?>" class="text-field pie" /></label><br />
			<p><label class="more">({[ADDRESS_DELIVERY2]})<input type="text" name="address2" value="<?= $_POST['address2'] ? $_POST['address2'] : ''?>" class="text-field pie" /></label></p>
			<p><label>{[COMMENT]}:	<input type="text" name="comment" value="<?= $_POST['comment'] ? $_POST['comment'] : ''?>" class="text-field pie " /></label></p>
			<p><label>{[METHOD_PAYMENT]}:<input type="text" name="pay" value="<?= $_POST['pay'] ? $_POST['pay'] : ''?>" class="text-field pie" /></label></p>
			<p class="send-order"><input type="image" onClick="$('#addOrder').submit();" src="/public/images/make-order.gif"></p>
		</div>
		<input type="hidden" name="Event" value="MakeOrder"/>
		</div>
	</form>
	<?}else{?>
		<p>{[YOUR_BASKET_EMPTY]}</p>
	<?}?>
	<?if($_GET['Success']){?>
		<p class="thanks" ><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>{[ORDER_SUCCESS_MESSAGE]}</p>
	<?}?>
</div>