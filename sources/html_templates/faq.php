<div class="fullConte">
	
<?if($this->PageInfo){?>
	<div class="linkWay">
		<a href="/">Главная</a>»<span><?= $this->PageInfo['title'] ?></span>
	</div>
												
	<a name="top"></a>

	<?= $this->PageInfo['content'] ?>


	<?for($i=0; $i<$this->Faq_sz; $i++){?>
		<h2 class="faq-title" id="Up_<?= $this->Faq[$i]['id']?>" ><?= $this->Faq[$i]['question']?></h2>
		<ul class="faq-questions">
			<?for($k=0; $k < count($this->Faq[$i]['questions']); $k++){?>
				<li class="faq-li">
					<b><?= $k+1 ?>.</b> <a href="#Down_<?= $this->Faq[$i]['questions'][$k]['id']?>">
						<?= $this->Faq[$i]['questions'][$k]['question']?>
					</a>
				</li>
			<?}?>
		</ul>
	<?}?>
	<?for($i=0; $i<$this->Faq_sz; $i++){?>
		<ul class="faq-answers" >
			<?for($k=0; $k < count($this->Faq[$i]['questions']); $k++){?>
				<li  class="faq-li" id="Down_<?= $this->Faq[$i]['questions'][$k]['id']?>">
					<b><?= $k+1 ?>.</b> <a href="#Up_<?= $this->Faq[$i]['questions'][$k]['p_id']?>">
						<?= $this->Faq[$i]['questions'][$k]['question']?>
					</a><a style="text-decoration: underline;" href="#top">Вверх</a>
					<p><?= $this->Faq[$i]['questions'][$k]['answer']?></p>
				</li>
			<?}?>
		</ul>	
	<?}?>
			
<?}else{?>
	<p>
		Страница находится на стадии наполнения. 
		Приносим свои извинения. 
		Попробуйте обновить страницу (F5) или перейти на <a href="<?= $this->RootUrl?>">главную</a> и там поискать то что требуется.
	</p>
<?}?>
</div>	  
	</div>

