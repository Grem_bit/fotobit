<div class="ProducList">
<?if($_REQUEST['ik_payment_state'] == 'success'){?>
	<div style="text-align:center; color:#529B2B; padding-bottom:10px;">Заказ оплачен. В ближайшее время с Вами свяжется менеджер.</div>
<?}?>
 
<?if($this->Orders){?>
	<table cellpadding="0" cellspacing="0" width="100%" class="c_table">
		<tr>
			<th>Номер</th>
			<th>Тип продукта</th>
			<th>Дата</th>
			<th>Кол-во</th>
			<th>Стоимость</th>
			<th>Статус</th>
			<th>Действия</th>
			<? /* <th>Удаление</th> */ ?>
		</tr>
		<?  $ord_info=array() ;$i=0;$cat=''; foreach((array)$this->Orders as $order){?>
			<tr class="<?= $i%2 ? 'even':'odd'?>">
				
				<td class="<?= !$i ? 'topleft' : ($this->Orders_sz-1 == $i && $this->Orders_sz > 1? 'bottomleft' : 'left')?>">
					<?/* <p class="order-num">№<?= $order['id']?></p> */?>
					№ <?= $order['id']?>
				</td>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<?= $this->_get_type_order($order['type_order'])?>
				</td>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<?/* <a href="/user-account/orders/<?= $order['id']?>.html"> */?><?= date("d.m.Y H:i",$order['ts'])?><?/* </a> */?>
				</td>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<?= $order['qty']?> шт.
				</td>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<p><?= $this->_show_price($this->_calc_order_price($order['cart']))?></p>
				</td>
				<?/* <td class="<?= !$i ? 'topright' : ($this->Orders_sz-1 == $i && $this->Orders_sz > 1? 'bottomright' : 'right')?>"> */?>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<? if($ord_info[$order['state']]){ 
						$ord_info[$order['state']]['cnt']++;
						$ord_info[$order['state']]['sum']+=$this->_show_price($this->_calc_order_price($order['cart']));
					}else{
						$ord_info[$order['state']]['cnt']=1;
						$ord_info[$order['state']]['sum']=$this->_show_price($this->_calc_order_price($order['cart']));
					}?>
					<? //_debug($order['state'],1);?>
					
					<?= $this->OrderStatus[$order['state']]?>
				</td>
				<td <?= $this->Orders_sz-1 == $i && $this->Orders_sz > 1 ? 'class="bottom"' : ''?>>
					<a class="view-order" href="/user-account/orders/<?= $order['id']?>.html" title="Просмотреть"></a>
					<?if($order['state'] == 'not_order'){?>
						<a class="edit-order" href="/user-account/upload.html#upload" title="Редактировать"></a>
						<?if($this->_calc_order_price($order['cart']) >= $this->_get_system_variable('order_min_amount')){?>
							<a class="pay-order" href="/user-account/orders/<?= $order['id']?>.html#payment" title="Оплатить"></a>
						<?}?>
					<?}?>
					<? if ($order['state']=='inprint' || $order['state']=='printed' ){ ?>
					<? }else{ ?>
						<a class="del-order" rel="<?= $order['id']?>" href="/order.html?Event=DeleteOrder&order_id=<?= $order['id']?>" title="Удалить"></a>
					<? } ?>
				</td>
				
				<? /*<td class="<?= !$i ? 'topright' : ($this->Orders_sz-1 == $i && $this->Orders_sz > 1? 'bottomright' : 'right-td')?>">
					<?if($order['state'] == 'not_order'){?>
						<?
							$lifetime = $this->_get_system_variable('order_lifetime');
							$time_left = $order['ts'] + $lifetime - time();
							$days_left = floor($time_left/(60*60*24));
						?>
						Через <?= $days_left.' '.declOfNum($days_left, array('день','дня','дней'))?>
					<?}?>
				</td> */ ?>
			</tr>
		<?$i++;}?>
	</table>
	<div style="padding-top:10px" class='orders_info'>
	<h1>Статистика</h1>
	<? /*
		Заказы<br/>
		<? $obj=0; $ord_cnt=0 ; foreach ($ord_info as $kay=>$val){ ?>
			<?= $this->OrderStatus[$kay].' : '.$val['cnt'].' - '.$val['sum'].' грн.'?><br/>
		<? $obj+= $val['sum'];
		$ord_cnt+=$val['cnt'];
		} ?>
		Всего заказоы : <?=$ord_cnt?>.</br>
		Общая сумма : <?= $obj ?> грн.
	*/
	?>
	<table class="c_table" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			<tr><th>Статус </th><th>Количество заказов </th> <th>Сумма </th></tr> 
			<?  $obj=0; $ord_cnt=0 ; foreach ($ord_info as $kay=>$val){?>
			<tr><td><?= $this->OrderStatus[$kay]?> </td><td><?= $val['cnt']?> </td> <td> <?=$val['sum']?> грн.</td></tr> 
			<?  $obj+= $val['sum'];
				$ord_cnt+=$val['cnt'];
			}?>
			<tr><td>Всего </td><td><?=$ord_cnt?> </td> <td> <?= $obj ?> грн.</td></tr> 
		</tbody>
	</table>
	</div>
<?}else{?>

	<p class="no-orders">Вы не делали заказов</p>
<?}?>
</div>