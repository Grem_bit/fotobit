
<?if($this->Order['state'] == 'not_order'){?>
		<h2><strong>Шаг 2 из 3:</strong> Информация о заказе. Выбор способа доставки и оплаты</h2><a class="back" href="/user-account/orders.html" title="Вернуться к заказам">Вернуться к заказам</a>
		{[LOAD_STEP2_TEXT]}
<?}else{?>
<a class="back" href="/user-account/orders.html" title="Вернуться к заказам">Вернуться к заказам</a>
<?}?>
<!--<div class="clear"></div> -->

<div class="ProducList" id="my_order">       
	<? include PATH_SNIPPETS.'order-cart-list.php'?>
	<?
		$string = $this->_get_system_variable('delivery_methods');
		$delivery_methods = explodeMultiString($string);
		$string = $this->_get_system_variable('payment_methods');
		$payment_methods = explodeMultiString($string);
	?>
	<?if($this->Order['state'] == 'not_order'){?>
		<a class="qqq-ord qq-order-button" href="/user-account/upload.html">Добавить/Редактировать фото к заказу</a>
		
		<?
			$ik_shop_id = $this->_get_system_variable('ik_shop_id');
			// $ik_form_action = '/interkassa/'.$this->_get_system_variable('ik_status_url').'?debug_pay=1&ik_sign_hash=1';
			// $ik_form_action = $this->_get_system_variable('ik_form_action');
			// $ik_form_action = '/?Event=OrderPhoto';
			// $ik_form_action = '/order.html?Event=Order';
			// $ik_form_action = '/user-account/orders/'.$this->Order['id'].'/info.html';
			$ik_form_action = '/order.html?Event=PreOrder';
			$ik_payment_amount = $_SESSION['Trash']['Sum'];
			$ik_payment_id = $_SESSION['Cur_order'];
			//_debug($_SESSION,1);
			if($_SESSION['user_account']['deliv_cour']){ // Минимальная сумма для заказа через курьера
				$ik_deliv_cour = $_SESSION['user_account']['deliv_cour'];
			}else{ $ik_deliv_cour = $this->_get_system_variable('min_deliv_cour'); }
			
			if($_SESSION['user_account']['deliv_np']){ // Минимальная сумма для заказа через новую почту
				$ik_deliv_np = $_SESSION['user_account']['deliv_np'];		
			}else{ $ik_deliv_np = $this->_get_system_variable('min_deliv_np'); }
			
			$site_name = $this->_get_lang_constant('SITE_NAME');
			$ik_payment_desc = 'Оплата заказа № '.$_SESSION['Cur_order'].' на сайте '.$site_name;
		?>
		<a name="payment"></a>
		<form id="ik_payment" name="payment" action="<?= $ik_form_action?>" method="post" enctype="application/x-www-form-urlencoded" <? // accept-charset="UTF-8"?> accept-charset="cp1251" >
			<input type="hidden" name="ik_shop_id" value="<?= $ik_shop_id?>" />
			<input type="hidden" name="ik_payment_amount" value="<?= $ik_payment_amount?>" />
			<input type="hidden" name="ik_payment_id" value="<?= $ik_payment_id?>" />
			<input type="hidden" name="ik_deliv_cour" value="<?= $ik_deliv_cour?>" />
			<input type="hidden" name="ik_deliv_np" value="<?= $ik_deliv_np?>" />
			<input type="hidden" name="ik_payment_desc" value="<?= $ik_payment_desc?>" />
			<input type="hidden" name="ik_paysystem_alias" value="" />
			<input type="hidden" name="min_sum_courier" value="<?= $this->_get_system_variable('min_sum_courier') ?>" />
			<input type="hidden" name="sum_courier" value="<?= $this->_get_system_variable('sum_courier')?>" />
			<input type="hidden" name="dobavljt" value="0" />
			<p class="method">Способ доставки:</p>
			<?$sql='SELECT * FROM `cities`'?>
			<?$citys=GetAll($sql);?>
			<?$i = 0?>
			<?//_debug($_SESSION['user_account']);?>
			<?//_debug($citys);?>
			<ul class="deliv">
			<?foreach($delivery_methods as $key=>$method){ //_debug($delivery_methods);_debug($method,1);?>
				<?//if($key==0){ $method['item']= ucfirst($method['item']);}?>
				<li>
					<label><input type="radio" name="DeliveryMethod" value="<?= $i?>" /><?= $method['item']?></label>
					<?if($method['options']){?>
						<?if($method['options'][0] == 'address'){?>
							<span class="address"><span>, укажите адрес:</span> <input type="text" name="address2" value="<?= $_SESSION['user_account']['city'].', '.$_SESSION['user_account']['street'].', '.$_SESSION['user_account']['house'].', '.$_SESSION['user_account']['apartment']?>" disabled="disabled" /></span>
						<?}elseif( $method['options'][0]== 'newpost'){?>
							<select name="DeliveryOption" disabled="disabled" onchange="loadAdress(this.options[this.selectedIndex].value)">
										<option value=" "> Выберите город </option>
									<?foreach($citys as $city){?>
										<option value="<?= $city['name']?>"> <?= $city['name']?> </option>
									<?}?>
							</select>
							<select name='DeliveryOptionDep' style='display:none'>
							</select>
							<br />
						<?}else{?>
							<select name="DeliveryOption" disabled="disabled">
								<?$j = 0?>
								<?foreach($method['options'] as $option){?>
									<option value="<?= $j?>"><?= $option?></option>
									<?$j++?>
								<?}?>
							</select>
							<br />
						<?}?>
					<?}else{?>
						<br />
					<?}?>
					<?$i++?>
				</li>
			<?}?>
			</ul>
			<p class="method">Способ оплаты:</p>
			<?$i = 0?>
			
			<ul class="deliv">
			<?foreach($payment_methods as $method){?>
				<li>
					<label><input type="radio" name="PaymentMethod" value="<?= $i?>" /><?= $method['item']?></label>
					<?if($method['options']){?>
						<span>(<?= $method['options'][0]?>)</span>
						<br />
					<?}else{?>
						<br />
					<?}?>
					<?$i++?>
				</li>
			<?}?>
			</ul>
			<p class="comment">Комментарий к заказу:</p>
			<?$comment_text = $this->_get_lang_constant('FORM_COMMENT_TEXT')?>
			<div class="ord-com">
				<textarea class="valid " onblur="if(this.value =='') this.value='<?= $comment_text?>'" onclick="if(this.value == '<?= $comment_text?>') this.value=''" title="<?= $comment_text?>" cols="" rows="" name="message"><?= $comment_text?></textarea>
				<input type="submit" name="process" value="Сформировать заказ" class="qq-order-button" <?= (!$_SESSION['Trash'] || (isset($_SESSION['Trash']['Sum']) && $_SESSION['Trash']['Sum'] < $this->_get_system_variable('order_min_amount'))) ? "style='display: none'" : NULL?> />
			</div>
			<div><strong>Шаг 2 из 3</strong>. {[LOAD_STEP2_TEXT2]}</div>
		</form>
	<?}else{?>
		<table class="order-info">
			<tr>
				<td>Общая стоимость:</td>
				<td><?= $this->Order['total']?> грн.</td>
				
			</tr>
			<tr>
				<td>Кол-во фотографий:</td>
				<td><?= $this->Order['qty']?> шт.</td>
			</tr>
			<tr>
				<td>Способ доставки:</td>
				<?/* <td><?= $delivery_methods[$this->Order['delivery_way']]['item'].($this->Order['delivery_option'] ? ' ('.$delivery_methods[$this->Order['delivery_way']]['options'][$this->Order['delivery_option']].')':'')?><?= $this->Order['address2'] ? ' ('.$this->Order['address2'].')' : ''?></td>
				 */?>
				 <td><?= $delivery_methods[$this->Order['delivery_way']]['item'].($this->Order['delivery_option'] ? ' ('.($delivery_methods[$this->Order['delivery_way']]['options'][$this->Order['delivery_option']]? $delivery_methods[$this->Order['delivery_way']]['options'][$this->Order['delivery_option']] : $this->Order['delivery_option'] ).')':'')?><?= $this->Order['address2'] ? ' ('.$this->Order['address2'].')' : ''?></td>
			</tr>
			<tr>
				<td>Способ оплаты:</td>
				<td><?= $payment_methods[$this->Order['payment_way']]['item']?></td>
			</tr>
			<?if($this->Order['comment']){?>
				<tr>
					<td>Комментарий к заказу:</td>
					<td><?= $this->Order['comment']?></td>
				</tr>
			<?}?>
		</table>
	<?}?>
</div>

<div style="padding: 10px 0 20px;">
	<a class="back" href="/user-account/orders.html" title="Вернуться к заказам">Вернуться к заказам</a>
</div>
<div class="clear"></div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		
		$('.ph-count').keyup(function() {
			var pid = $(this).attr('rel');
			var count = $(this).val();

			$.ajax({
				url: "?Event=ChangeCount",
				data: { pid: pid, count: count },
				dataType:  'json'
			}).done(function(data) {
				if(data) {
					console.log(data);
					data.sum_photo_price < <?= $this->_get_system_variable('order_min_amount')?> ? $('#ik_payment .qq-order-button').hide() : $('#ik_payment .qq-order-button').show();
					$('#ph-price-'+pid).html(data.photo_price);
					$('#sum-prices').html(data.sum_photo_price);
					// paymentFormRefresh(data.sum_photo_price);
					$("input[name='ik_payment_amount']").val(data.sum_photo_price);
				}
			});
		});
		var sum_courier_triger= 0;
		$('#ik_payment input[name="DeliveryMethod"]').change(function(){
			$('#ik_payment select').attr('disabled', 'disabled');
			$('#ik_payment input[type=text]').attr('disabled', 'disabled');
			
			minsumcoirier = $("input[name='min_sum_courier']").val();
			min_deliv_cour = $("input[name='ik_deliv_cour']").val();
			min_deliv_np = $("input[name='ik_deliv_np']").val();
			sum_plus = $("input[name='sum_courier']").val();
			oldsum = $("input[name='ik_payment_amount']").val();
			
			$(this).parent('label').next('select').removeAttr('disabled');
			$(this).parent('label').next('span').children('input').removeAttr('disabled');
			
			//if(($(this).parent('label').next('div')).length>0){
			if(($(this).parent('label').next('span')).length>0){
				
				if((min_deliv_cour*1)>(oldsum*1)){
					$(this).parent('label').next('span').children('input').attr('disabled', 'disabled');
					//checked="checked"
					humanMsg.displayMsg(' Минимальная сумма для доставки курьером '+min_deliv_cour);
					//$(this).checked=false;
					$(this).removeAttr("checked");
					//console.log(this);
					$('#sum-add-courier').html(' ');
					$("input[name='dobavljt']").val('0');
				} else {
					if((minsumcoirier*1)>(oldsum*1)){ // для добавки на курьера				
						if(!sum_courier_triger){						
						$('#sum-add-courier').html(' + '+sum_plus+'грн');
						//sum=oldsum*1+sum_plus*1;
						//$("input[name='ik_payment_amount']").val(sum.toFixed(2));
						$("input[name='dobavljt']").val('1');
						sum_courier_triger = 1;
						}
					}else{
					
						$('#sum-add-courier').html(' ');
						$("input[name='dobavljt']").val('0');
						}
				}
			}else if (($(this).parent('label').next('select')).length>0) {
				if((min_deliv_np*1)>(oldsum*1)){
					
					humanMsg.displayMsg(' Минимальная сумма для доставки через "Новую Почту" '+min_deliv_np);
					$(this).removeAttr("checked");
					$(this).parent('label').next('select').attr('disabled', 'disabled');
				} else {
					if((minsumcoirier*1)>(oldsum*1)){
					$('#sum-add-courier').html('+ стоимоcть доставки курьерской cлужбой " Новая Почта" (цену смотрите на сайте "Новая Почта")');
					}
					if(sum_courier_triger){
						sum=oldsum*1-sum_plus*1;
						//$("input[name='ik_payment_amount']").val(sum.toFixed(2));
						$("input[name='dobavljt']").val('0');
						sum_courier_triger=0;
					}
				}
			}else{
				$('#sum-add-courier').html('');
				if(sum_courier_triger){
					sum=oldsum*1-sum_plus*1;
					//$("input[name='ik_payment_amount']").val((sum).toFixed(2));
					$("input[name='dobavljt']").val('0');
					sum_courier_triger=0;
				}
			}
			
		});
		
		/*
		$('#ik_payment input[name="PaymentMethod"]').change(function(){
			text = $(this).parent('label').text();
			textLow = text.toLowerCase();
			if((/interkassa/.test(textLow)) || (/интеркасса/.test(textLow))){
				$('form#ik_payment').attr('action', '<?= $this->_get_system_variable('ik_form_action')?>');
			}else{
				$('form#ik_payment').attr('action', '<?= $ik_form_action?>');
			}
		});
		*/
		
		$('#ik_payment input, #ik_payment select, #ik_payment textarea').change(function() {
			
			var pid = $('input[name="ik_payment_id"]').val();
			//alert(this);
			$.ajax({
				url: "?Event=SaveDelivPay&"+$('#ik_payment').serialize(),
				dataType:  'json'
			});
		});
		
		$('#ik_payment').submit(function(event){
			// event.preventDefault();
			var errors = new Array();
			if (
					$(this).find("input[name='DeliveryMethod']:radio:checked").length == 0
					||
					$(this).find("select:enabled").val() == 0 || $(this).find("select[name=DeliveryOptionDep]:enabled ").val() == 0
				) {
				// errors['DeliveryMethod'] = 'Выберите способ доставки';
				errors[0] = 'Выберите способ доставки';
			}
			if ($(this).find("input[name='PaymentMethod']:radio:checked").length == 0) {
				// errors['PaymentMethod'] = 'Выберите способ оплаты';
				errors[1] = 'Выберите способ оплаты';
			}
			cheklabel=$(this).find("input[name='DeliveryMethod']:radio:checked").parent('label');
			if( cheklabel.next(".address").length>0){
				if(! cheklabel.next(".address").find("input").val().length>0){
					errors[2]= 'Укажите адрес' ; 
				}
			}
			
			// console.log(errors);
			// console.log(errors.length);
			var message = '';
			if(errors.length > 0){
				// console.log(errors.length);
				errors.forEach(function(val) {
					// message += '<p>'+val+'</p>';
					message += val+'\n';
				});
				
				alert(message);
				return false;
			}
			// return false;
		});
		
	});
	//---- podgryzka adressov
	function loadAdress(select)
	{
		var depart =$('select[name="DeliveryOptionDep"]');
		var citySelectid = select;
		
		action='/?Event=getDepartment';
		$.post(action, 'city_name='+citySelectid ,
				function(data){
					//console.log(data)
					depart.html(''); // очищаем список городов
					depart.append('<option value=""> Выберите адресс </option>');
					$.each(data, function(i){
						depart.append('<option value="' + this.address + '">' + this.address + '</option>');
					});
					//depart.css("display","  ");
					depart.removeAttr("disabled")
					depart.show()
		//alert('aaaaaaaaaaa22222');
			  }, "json");
	}
</script>