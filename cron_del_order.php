<?php  /* Удаляет заказы которые больше месяца с любым статусом */
 
/* Define constants * */
	 
	$RootPath = '/var/www/fotoservice/data/www/foto-servis.net/';

	define('PATH_CORE', $RootPath.'core/');
	define('PATH_LIB', $RootPath.'core/lib/');
	define('PATH_MAIL_TEMPLATES',  $RootPath.'sources/mail_templates/');
	
/* Include site configuration */

	require_once PATH_CORE.'configuration.php';
	
	function AttachLib($name) {
		
		require_once PATH_LIB.$name.'.php';
	}
	
/* Init ADODB */

   	include PATH_LIB.'adodb/adodb.inc.php';
	
   	$DB = NewADOConnection($config['DB']['type']);
	$DB->Connect($config['DB']['host'].':'.$config['DB']['port'], $config['DB']['user'], $config['DB']['password'], $config['DB']['database']);
	
	AdvNotif();
	
	function AdvNotif(){
		global $DB, $config;
		
		$now = time();
		$mes=$now-3600*24*30;
		
		$sql="SELECT buyer_id,id,ts,qty,state,total FROM `orders` 
			WHERE  ts<".$mes."  ";
		
		$orders= $DB->GetAll($sql);
		
		if(is_array($orders) && $orders){
			foreach($orders as $order ){
				$sql="INSERT INTO  order_info 
							SET 
								user_id = '".$order['buyer_id']."'
								,order_id = '".$order['id']."'
								,state = '".$order['state']."'
								,qty = '".$order['qty']."'
								,total = '".$order['total']."'
								,ts = '".$order['ts']."'
				";
				$DB->Execute($sql);
				$sql = "DELETE FROM `orders` WHERE `id` ='".$order['id']."' ";
				$DB->Execute($sql);
				$DeletedFolder = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."orders".DIRECTORY_SEPARATOR.$order['id'];
				RemoveDir($DeletedFolder);
				$DeletedZip = $config["absolute-path"]."public".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR."zip".DIRECTORY_SEPARATOR.$order['id'].".zip";
				unlink($DeletedZip);
				
			}
		}
		
	/* 	AttachLib('Mailer');
		$m = new Mailer();
		require_once PATH_LIB."/SMSSender/smssender.php";
		foreach($orders as $order ){
			
				$sql=' SELECT phone,email FROM `users` WHERE id=\''.$order['buyer_id'].'\'';
				$phone_email = $DB->GetRow($sql);
				$phone_u=$phone_email['phone'];
				$email_u=$phone_email['email'];
		
				 $sql=' UPDATE `orders` SET
						not_issued=1 WHERE id='.$order['id'];
				
				$DB->Execute($sql);
				//$d['id_order'] = $order['id'];
				
				
				$m->DelRecepient();
				$m->AddRecepient($email_u);
				
				$m->DelVars();
				$m->AddVars(array(
								'order_id'		=> $order['id'],
								
								'date'		=> date('d:m:Y H:i',time())								
				));
				$norep=get_system_variable('noreply_email');
				$otkogo=get_system_variable('hostt');
				
				
				$m->Send( 'not_is.html', 'Заберите свой заказ', ''.$norep.'', ''.$otkogo.'');
				
				if(true){
					$sms = new SMSSender();
					$result = mysql_query("SELECT * FROM `language_constants` where id in (257, 259)");
					$row =mysql_fetch_array($result);
					//_debug($row);
					//TODO построение текста смс;
					$text = 'Заберите '.$row['value'].$order['id'].". http://www.foto-servis.net ";
					$row =mysql_fetch_array($result);
					
					//exit($text);
					$sms->registerSender('Foto-Servis','ua');
					$ph=$phone_u;
					//$ph='+380636885351,+388555';
			
					$phd= str_replace('+38','',$ph);
					$phd=substr($phd,0,10);
					//$phone =  "38".$this->OrderInfo['phone'];
					$phone =  "38".$phd;
					//$phone =  "380636885351";
					
					
					if ($order['buyer_id']!=7) $sms->sendSMS($phone,$text);
				}
		} */
		
		
	}
	
	function RemoveDir($path){
		if(file_exists($path) && is_dir($path)){
			$dirHandle = opendir($path);
			while (false !== ($file = readdir($dirHandle))){
				if ($file!='.' && $file!='..'){// исключаем папки с названием '.' и '..'
					$tmpPath=$path.'/'.$file;
					chmod($tmpPath, 0777);
					
					if (is_dir($tmpPath)){  // если папка
						RemoveDir($tmpPath);
					}else{
						if(file_exists($tmpPath)){
							// удаляем файл
							unlink($tmpPath);
						}
					}
				}
			}
			closedir($dirHandle);
			
			// удаляем текущую папку
			if(file_exists($path))
			{
				rmdir($path);
			}
		}else{
			echo "Удаляемой папки не существует или это файл!";
		}
	}
	function get_system_variable($name){
		global $DB;
		$sql = "SELECT var_value FROM sys_vars WHERE var_name = '$name' ";			
		return $DB->GetOne($sql);			
	}

/*
		$sql = 'SELECT  u.email as user_email,u.id as user_id, cn.id, cn.company_id, cn.title, u.email ,u.city_id as city_id
				FROM	actions cn, companies c
					INNER JOIN users u ON (u.id = c.user_id)
				WHERE 	cn.notified = 0 AND
						c.id = cn.company_id AND
						u.active = 1 AND
						(((cn.end_ts - '.$now.') / 86400) <= '.get_system_variable('action_notif_days').')';  
		
		$adv = $DB->GetAll($sql);	

		if($adv){
		
			$where = ' WHERE ';
			$notif_arr = array();
			
			foreach($adv as $a){
				
				$notif_arr[] = 'id = '.$a['id'];
			}
			
			$where .= implode(' OR ', $notif_arr);
			
			 $sql = 'UPDATE actions
					SET notified = 1
					'.$where; 

			$DB->Execute($sql);
			
			AttachLib('Mailer');
			
			$m = new Mailer();
			
			for($i=0; $i < count($adv); $i++){
				$d = array();
				//--key
				$url_key= md5($adv[$i]['user_email'].time());
				 $sql = 'UPDATE users
					SET cron_action_key = \''.$url_key.'\'
					WHERE id=\''.$adv[$i]['user_id'].'\'';  
				$DB->Execute($sql);
				$d['url_key'] = $url_key;
				
				$d['title'] = $adv[$i]['title'];
				$d['days'] = get_system_variable('action_notif_days');
				
				$m->DelRecepient();
				$m->AddRecepient($adv[$i]['email']);
				$m->AddCity($adv[$i]['city_id']);
				$m->DelVars();
				$m->AddVars($d);
				$norep=$m->_get_system_variable('smpt_user');
				$otkogo=$m->_get_system_variable('http_host');
				
				
				$m->Send( 'action-notif-mail.html', 'Уведомление о конце срока публикации вашей акции', ''.$norep.'', ''.$otkogo.'');
			}
		}	
		
		$sql = 'SELECT cn.id, cn.company_id, cn.title, u.email, u.id as u_id 
				FROM	actions cn, companies c
					INNER JOIN users u ON (u.id = c.user_id)
				WHERE 	c.id = cn.company_id AND
						u.active = 1 AND
						((('.$now.' - (cn.end_ts + '.get_system_variable('action_days_delete').' * 86400)) / 86400) > 0)';
		//exit($sql);
		$del_adv = $DB->GetAll($sql);
		
		if($del_adv){
			$where = ' WHERE ';
			$del_arr = array();
			$user_arr = array();
			
			foreach($del_adv as $a){
				
				$del_arr[] = 'id = '.$a['id'];
				$user_arr[] = 'user_id = '.$a['u_id'];
			}
			
			$where .= implode(' OR ', $del_arr);
			
			$sql = 'DELETE FROM actions
					'.$where;
		
			$DB->Execute($sql);
			
			$where = implode(' OR ', $user_arr);
			
			$sql = 'DELETE FROM user_action_time
					WHERE ('.$where.') and table = \'action\'';
		
			$DB->Execute($sql);
		}
		*/
?>
