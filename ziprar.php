<?php	
	$RootPath = '/var/www/fotoservice/data/www/foto-servis.net/';

	define('PATH_ZIP', $RootPath.'zip/');
	define('PATH_CORE', $RootPath.'core/');
	define('PATH_LIB', $RootPath.'core/lib/');
	define('PATH_MAIL_TEMPLATES',  $RootPath.'sources/mail_templates/');
	
/* Include site configuration */

	require_once PATH_CORE.'configuration.php';
	
	function AttachLib($name) {
		
		require_once PATH_LIB.$name.'.php';
	}
	
/* Init ADODB */

   	include PATH_LIB.'adodb/adodb.inc.php';
	
   	$DB = NewADOConnection($config['DB']['type']);
	$DB->Connect($config['DB']['host'].':'.$config['DB']['port'], $config['DB']['user'], $config['DB']['password'], $config['DB']['database']);
		
	AdvNotif();
	
	function AdvNotif(){
		global $DB,$config;
		
		
		// ZIP
		if(false){
			
			$testzip = 'mytest.zip';
			$zip     = new ZipArchive;
			if (true === $zip->open(PATH_ZIP.$testzip, ZIPARCHIVE::CREATE)) {
				$zip->addEmptyDir('dir1'); # создаём директорию
				$zip->addEmptyDir('dir2'); # ещё один
				# поместим тестовую строку в архив, в виде текстового файл
				$zip->addFromString('dir1/123.txt', 'test string');
				$zip->addEmptyDir('dir3'); # создаём ещё одну директорию
				# удаляем директорию, созданную второй (внимание, слеш!!!)
				$zip->deleteName('dir2/');
				# поместим в архив текущий файл
				$zip->addFile(__FILE__, 'dir3/' . pathinfo(__FILE__, PATHINFO_FILENAME));
				$zip->close();
			} else echo 'не удалось создать архив' . $testzip;
		
			if ( $zip -> open(PATH_ZIP."mytest.zip") === true ) {
			   $zip -> extractTo(PATH_ZIP);
			   $zip -> close();
			   echo "<p>Архив распакован</p>";
			} else {
			   echo "<p>Ошибка при извлечении файлов из архива</p>";
			}
	
		}
		
		
		
		
		
		 // RAR
		/*  $zip = new RarArchive;
		$res = $zip->open(PATH_ZIP.'rararh.rar');
		if ($res === TRUE) {
			echo 'ok';
				$zip->extractTo(PATH_ZIP);
				$zip->close();
		} else {
			echo 'failed, code:' . $res;
		} */
		 
		 
	
		$rar_file = rar_open(PATH_ZIP.'rararh.rar') ;
		if(!$rar_file) ("Невозможно открыть RAR архив");
		$entries = rar_list($rar_file);

		foreach ($entries as $entry) {
			echo 'Имя файла: ' . $entry->getName() . "\n";
			echo 'Упакованный размер: ' . $entry->getPackedSize() . "\n";
			echo 'Распакованный размер: ' . $entry->getUnpackedSize() . "\n";

			$entry->extract(PATH_ZIP);
		}

		rar_close($rar_file); 
		
		
		
		
	}
	
	function get_system_variable($name){
		global $DB;
		$sql = "SELECT var_value FROM sys_vars WHERE var_name = '$name' ";			
		return $DB->GetOne($sql);			
	}
		
			
									
?>